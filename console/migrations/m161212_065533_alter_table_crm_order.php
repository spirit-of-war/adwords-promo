<?php

use yii\db\Migration;

class m161212_065533_alter_table_crm_order extends Migration
{
    public function up()
    {
        $this->dropColumn('crm_order', 'company_id');
    }
}
