<?php

use yii\db\Migration;

class m160916_051942_create_table_page extends Migration
{
	public function up()
    {	
		$this->createTable('page', [
			'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
			'title' => $this->string()->notNull(),
			'announcement' => $this->string()->notNull(),
			'body' => 'LONGBLOB NOT NULL',
			'created' => $this->integer()->notNull(),
			'icon' => $this->string()->notNull(),
			'type' => $this->integer()->notNull(),
			'PRIMARY KEY(id)'
		]);
    }

    public function down()
    {	
        $this->dropTable('page');
    }
}
