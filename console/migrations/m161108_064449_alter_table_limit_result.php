<?php

use yii\db\Migration;

class m161108_064449_alter_table_limit_result extends Migration
{
    public function up()
    {
        $this->addColumn('limit_result', 'task_id', $this->integer());
    }
}
