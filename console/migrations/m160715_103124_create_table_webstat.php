<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_webstat`.
 */
class m160715_103124_create_table_webstat extends MigrationBase
{
    private $name = 'webstat';
    private $fkCampaignName = 'fk_webstat_campaign';
    private $fkAdgroupName = 'fk_webstat_adgroup';
    private $fkAdName = 'fk_webstat_ad';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'goal_reaches' =>  $this->float(),
            'revenue' => $this->float(),
            'denial' =>  $this->float(),
            'views' =>  $this->float(),
            'duration' =>  $this->float(),
            'campaign_id' => $this->integer(),
            'adgroup_id' => $this->integer(),
            'ad_id' => $this->integer(),
            'state' => $this->integer()->notNull(),
            'source_id' => $this->integer(),
        ]);
        $this->addForeignKey($this->fkCampaignName, $this->name, 'campaign_id', 'campaign', 'id');
        $this->addForeignKey($this->fkAdgroupName, $this->name, 'adgroup_id', 'adgroup', 'id');
        $this->addForeignKey($this->fkAdName, $this->name, 'ad_id', 'ad', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkAdName, $this->name);
        $this->dropForeignKey($this->fkAdgroupName, $this->name);
        $this->dropForeignKey($this->fkCampaignName, $this->name);
        $this->dropTable($this->name);
    }
}
