<?php

use yii\db\Migration;

class m161226_081852_add_statistics_table extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'metrika_conversions_lastsign', $this->integer()->defaultValue(0));
        $this->addColumn('statistics', 'metrika_conversions_last', $this->integer()->defaultValue(0));
        $this->addColumn('statistics', 'metrika_conversions_first', $this->integer()->defaultValue(0));
    }
}
