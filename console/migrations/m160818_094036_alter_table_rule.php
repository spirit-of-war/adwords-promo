<?php

use yii\db\Migration;

class m160818_094036_alter_table_rule extends Migration
{
    public function up()
    {
        $this->addColumn('rule', 'weekday', $this->integer());
        $this->addColumn('rule', 'monthday', $this->integer());
        $this->addColumn('rule', 'instant_start', $this->boolean()->notNull());
        $this->addColumn('rule', 'onceday', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('rule', 'onceday');
        $this->dropColumn('rule', 'weekday');
        $this->dropColumn('rule', 'monthday');
        $this->dropColumn('rule', 'instant_start');
    }
}
