<?php

use yii\db\Migration;

class m160825_080225_drop_table_crmorder extends Migration
{
    public function up()
    {
        $this->dropTable('crm_order');
    }
}
