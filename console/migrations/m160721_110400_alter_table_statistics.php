<?php

use yii\db\Migration;

class m160721_110400_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->dropColumn('statistics', 'stat_date');
        $this->addColumn('statistics', 'start_date', $this->integer()->notNull());
        $this->addColumn('statistics', 'end_date', $this->integer()->notNull());
        $this->addColumn('statistics', 'keyword_id', $this->integer());
        $this->addColumn('statistics', 'state', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('statistics', 'state');
        $this->dropColumn('statistics', 'keyword_id');
        $this->dropColumn('statistics', 'end_date');
        $this->dropColumn('statistics', 'start_date');
        $this->addColumn('statistics', 'stat_date', $this->integer());
    }
}
