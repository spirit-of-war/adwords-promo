<?php

use yii\db\Migration;

class m161025_095958_alter_table_crm_order extends Migration
{
    public function up()
    {
        $this->dropIndex('crm_order_ui', 'crm_order');
        $this->createIndex('crm_order_ui', 'crm_order', [
            'visit_id',
            'campaign_id',
            'adgroup_id',
            'keyword_id',
            'ad_id',
            'order_id',
            'date',
            'company_id'
        ], true);
    }
}
