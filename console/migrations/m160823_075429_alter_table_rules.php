<?php

use yii\db\Migration;

class m160823_075429_alter_table_rules extends Migration
{
    public function up()
    {
        $this->dropColumn('rule', 'all_campaigns');
    }
}
