<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `campaign_balance`.
 */
class m160708_065227_create_campaign_balance extends MigrationBase
{
    private $tableName = 'balance';
    private $fkName = 'fk_balance_campaign';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->tableName, [
            'sum' => $this->float(),
            'rest' => $this->float(),
            'campaign_id' => $this->integer()
        ]);
        $this->addForeignKey($this->fkName, $this->tableName, 'campaign_id', 'campaign', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->tableName);
        $this->dropTable($this->tableName);
    }
}
