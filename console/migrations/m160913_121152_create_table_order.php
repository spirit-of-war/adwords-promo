<?php

use yii\db\Migration;

class m160913_121152_create_table_order extends Migration
{
	public function up()
    {		
		$this->createTable('order', [
			'id' => 'INT(11) NOT NULL AUTO_INCREMENT',
			'name' => $this->string()->notNull(),
			'phone' => $this->string()->notNull(),
			'site' => $this->string()->notNull(),
			'email' => $this->string()->notNull(),
			'created_at' => $this->integer()->notNull(),
			'PRIMARY KEY(id)',
		]);
		
		//$this->addPrimaryKey('primary_id','order','id');
    }

    public function down()
    {	
        $this->dropTable('order');
    }
}