<?php

use yii\db\Migration;

class m160916_055652_alter_table_task extends Migration
{
    public function up()
    {
        $this->addColumn('task', 'error_details', $this->string(1024));
    }
}
