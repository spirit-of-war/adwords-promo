<?php

use yii\db\Migration;

class m160708_080420_add_columns_account extends Migration
{
    private $tableName = 'account';
    
    private $columnName1 = 'g_client_id';
    private $columnName2 = 'g_client_secret';
    private $columnName3 = 'g_refresh_token';
    private $columnName4 = 'g_user_agent';
    private $columnName5 = 'g_adwords_client_customer_id';
    private $columnName6 = 'g_adwords_developer_key';

    public function up()
    {
        $this->addColumn($this->tableName, $this->columnName1, $this->string());
        $this->addColumn($this->tableName, $this->columnName2, $this->string());
        $this->addColumn($this->tableName, $this->columnName3, $this->string());
        $this->addColumn($this->tableName, $this->columnName4, $this->string());
        $this->addColumn($this->tableName, $this->columnName5, $this->string());
        $this->addColumn($this->tableName, $this->columnName6, $this->string());
    }

    public function down()
    {
        $this->dropColumn($this->tableName, $this->columnName1);
        $this->dropColumn($this->tableName, $this->columnName2);
        $this->dropColumn($this->tableName, $this->columnName3);
        $this->dropColumn($this->tableName, $this->columnName4);
        $this->dropColumn($this->tableName, $this->columnName5);
        $this->dropColumn($this->tableName, $this->columnName6);
    }
}
