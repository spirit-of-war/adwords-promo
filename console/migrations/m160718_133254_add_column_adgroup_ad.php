<?php

use yii\db\Migration;

class m160718_133254_add_column_adgroup_ad extends Migration
{
    public function up()
    {
        $this->addColumn('adgroup', 'campaign_source_id', $this->integer());
        $this->addColumn('ad', 'campaign_source_id', $this->integer());
        $this->addColumn('ad', 'adgroup_source_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('adgroup', 'campaign_source_id');
        $this->dropColumn('ad', 'campaign_source_id');
        $this->dropColumn('ad', 'adgroup_source_id');
    }
}
