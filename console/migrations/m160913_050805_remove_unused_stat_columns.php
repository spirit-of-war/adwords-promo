<?php

use yii\db\Migration;

class m160913_050805_remove_unused_stat_columns extends Migration
{
    public function up()
    {
        $this->dropColumn('statistics', 'session_depth_search');
        $this->dropColumn('statistics', 'session_depth_context');
        $this->dropColumn('statistics', 'goal_conversion_search');
        $this->dropColumn('statistics', 'goal_conversion_context');
        $this->dropColumn('statistics', 'goal_cost_search');
        $this->dropColumn('statistics', 'goal_cost_context');
    }
}
