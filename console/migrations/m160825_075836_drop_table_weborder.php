<?php

use yii\db\Migration;

class m160825_075836_drop_table_weborder extends Migration
{
    public function up()
    {
        $this->dropTable('web_order');
    }
}
