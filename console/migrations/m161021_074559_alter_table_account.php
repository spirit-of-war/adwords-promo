<?php

use yii\db\Migration;
use common\models\Account;

class m161021_074559_alter_table_account extends Migration
{
    public function up()
    {
        $this->addColumn(Account::tableName(), 'last_structure_update', $this->string());
    }
}
