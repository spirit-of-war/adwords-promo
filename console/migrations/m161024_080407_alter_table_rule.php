<?php

use yii\db\Migration;

class m161024_080407_alter_table_rule extends Migration
{
    public function up()
    {
        $this->addColumn('rule', 'type', $this->smallInteger(1)->defaultValue(0));
    }
}
