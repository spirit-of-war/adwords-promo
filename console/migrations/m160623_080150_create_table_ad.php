<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_ad`.
 */
class m160623_080150_create_table_ad extends MigrationBase
{
    private $name = 'ad';
    private $fkAdgroupName = 'fk_ad_adgroup';
    private $fkCampaignName = 'fk_ad_campaign';
    
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'source_id' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'adgroup_id' => $this->integer()->notNull(),
            'campaign_id' => $this->integer()->notNull(),
            'account_type' => $this->integer()->notNull(),
            'state' => $this->integer()->notNull()
        ]);
        $this->addForeignKey($this->fkAdgroupName, $this->name, 'adgroup_id', 'adgroup', 'id');
        $this->addForeignKey($this->fkCampaignName, $this->name, 'campaign_id', 'campaign', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkCampaignName, $this->name);
        $this->dropForeignKey($this->fkAdgroupName, $this->name);
        $this->dropTable($this->name);
    }
}
