<?php

use yii\db\Migration;

class m170118_120803_alter_table_questionnaires extends Migration
{
    public function up()
    {
        $this->addColumn('questionnaires', 'fio', $this->string());
        $this->addColumn('questionnaires', 'phone', $this->string());
        $this->alterColumn('questionnaires', 'created_by', $this->integer()->null());
        $this->alterColumn('questionnaires', 'updated_by', $this->integer()->null());

    }

    public function down()
    {
        $this->dropColumn('questionnaires', 'fio');
        $this->dropColumn('questionnaires', 'phone');
        $this->alterColumn('questionnaires', 'created_by', $this->integer()->notNull());
        $this->alterColumn('questionnaires', 'updated_by', $this->integer()->notNull());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
