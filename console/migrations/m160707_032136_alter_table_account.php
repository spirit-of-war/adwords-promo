<?php

use yii\db\Migration;

class m160707_032136_alter_table_account extends Migration
{
    private $tableName = 'account';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn($this->tableName, 'type');
        $this->dropColumn($this->tableName, 'token');
        $this->addColumn($this->tableName, 'advert_type', $this->integer()->notNull());
        $this->addColumn($this->tableName, 'stat_type', $this->integer()->notNull());
        $this->addColumn($this->tableName, 'advert_login', $this->string());
        $this->addColumn($this->tableName, 'stat_login', $this->string());
        $this->addColumn($this->tableName, 'advert_token', $this->string());
        $this->addColumn($this->tableName, 'advert_token_expires', $this->string());
        $this->addColumn($this->tableName, 'stat_token', $this->string());
        $this->addColumn($this->tableName, 'stat_token_expires', $this->string());
        $this->addColumn($this->tableName, 'is_account_common', $this->boolean()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->tableName, 'is_account_common');
        $this->dropColumn($this->tableName, 'stat_token_expires');
        $this->dropColumn($this->tableName, 'advert_token_expires');
        $this->dropColumn($this->tableName, 'advert_token');
        $this->dropColumn($this->tableName, 'stat_token');
        $this->dropColumn($this->tableName, 'stat_login');
        $this->dropColumn($this->tableName, 'advert_login');
        $this->dropColumn($this->tableName, 'stat_type');
        $this->dropColumn($this->tableName, 'advert_type');
        $this->addColumn($this->tableName, 'token', $this->string());
        $this->addColumn($this->tableName, 'type', $this->integer()->notNull());
    }
}
