<?php

use yii\db\Migration;

class m160823_063610_alter_table_rule extends Migration
{
    public function up()
    {
        $this->addColumn('rule', 'state', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('rule', 'state');
    }
}
