<?php

use yii\db\Migration;

class m160727_103734_alter_column_IsNegativeGoogle extends Migration
{
    public function up()
    {
        $this->addColumn('keyword', 'IsNegative', $this->string());
    }
    public function down()
    {
        $this->dropColumn('keyword', 'IsNegative');
    }
}
