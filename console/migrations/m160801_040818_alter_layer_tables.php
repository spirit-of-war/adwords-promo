<?php

use yii\db\Migration;

class m160801_040818_alter_layer_tables extends Migration
{
    public function up()
    {
        $this->addColumn('adgroup', 'account_id', $this->integer()->unsigned());
        $this->addColumn('ad', 'account_id', $this->integer()->unsigned());
        $this->addColumn('keyword', 'account_id', $this->integer()->unsigned());
    }

    public function down()
    {
        $this->dropColumn('keyword', 'account_id');
        $this->dropColumn('ad', 'account_id');
        $this->dropColumn('adgroup', 'account_id');
    }
}
