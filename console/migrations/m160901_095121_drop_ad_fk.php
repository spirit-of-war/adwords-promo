<?php

use yii\db\Migration;

class m160901_095121_drop_ad_fk extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_statistics_advert', 'statistics');
    }
}
