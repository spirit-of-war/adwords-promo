<?php

use yii\db\Migration;

class m160804_041843_drop_unused_tables extends Migration
{
    public function up()
    {
        $this->dropTable('balance');
        $this->dropTable('index');
    }

    public function down()
    {
        $this->createTable('balance', ['id' => $this->integer()]);
        $this->createTable('index', ['id' => $this->integer()]);
    }
}
