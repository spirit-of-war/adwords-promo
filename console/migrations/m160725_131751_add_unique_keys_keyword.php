<?php

use yii\db\Migration;

class m160725_131751_add_unique_keys_keyword extends Migration
{
    public function up()
    {
        $this->createIndex('ui_keyword', 'keyword', ['source_id', 'account_type'], true);
    }

    public function down()
    {
        $this->dropIndex('ui_keyword', 'keyword');
    }
}
