<?php

use yii\db\Migration;

class m170118_073459_alter_table_page extends Migration
{
    public function up()
    {
        $this->addColumn('page', 'slug', $this->string());
    }

    public function down()
    {
        $this->dropColumn('page', 'slug');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
