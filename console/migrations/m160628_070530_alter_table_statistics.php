<?php

use yii\db\Migration;

class m160628_070530_alter_table_statistics extends Migration
{
    private $tableName = 'statistics';
    private $fkName = 'fk_statistics_ad';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn($this->tableName, 'campaign_id', $this->integer());
        $this->addColumn($this->tableName, 'ad_id', $this->integer());
        $this->addForeignKey($this->fkName, $this->tableName, 'ad_id', 'ad', 'id');
    }

    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->tableName);
        $this->dropColumn($this->tableName, 'ad_id');
        $this->alterColumn($this->tableName, 'campaign_id', $this->integer()->notNull());
    }
}
