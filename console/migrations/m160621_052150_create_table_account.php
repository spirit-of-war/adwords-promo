<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_account`.
 */
class m160621_052150_create_table_account extends MigrationBase
{
    private $name = 'account';
    private $fkName = 'fk_account_company';
    
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'type' => $this->integer()->notNull(),
            'token' => $this->string(),
            'company_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey($this->fkName, $this->name, 'company_id', 'company', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->name);
        $this->dropTable($this->name);
    }
}
