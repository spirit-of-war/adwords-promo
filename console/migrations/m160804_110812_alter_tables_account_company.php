<?php

use yii\db\Migration;

class m160804_110812_alter_tables_account_company extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'state', $this->smallInteger()->notNull()->defaultValue(1));
        $this->addColumn('company', 'state', $this->smallInteger()->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('company', 'state');
        $this->dropColumn('account', 'state');
    }
}
