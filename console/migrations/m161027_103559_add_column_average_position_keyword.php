<?php

use yii\db\Migration;

class m161027_103559_add_column_average_position_keyword extends Migration
{
    public function up()
    {
        $this->addColumn('keyword', 'average_position', $this->float());
    }

    public function down()
    {
        $this->dropColumn('keyword', 'average_position');
    }
}