<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_index`.
 */
class m160708_075733_create_table_index extends MigrationBase
{
    private $tableName = 'index';
    private $campaignFK = 'fk_index_campaign';
    private $adgroupFK = 'fk_index_adgroup';
    private $adFK = 'fk_index_ad';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->tableName, [
            'type' => $this->integer()->notNull(),
            'period' => $this->integer()->notNull(),
            'campaign_id' => $this->integer(),
            'adgroup_id' => $this->integer(),
            'ad_id' => $this->integer()
        ]);
        $this->addForeignKey($this->campaignFK, $this->tableName, 'campaign_id', 'campaign', 'id');
        $this->addForeignKey($this->adgroupFK, $this->tableName, 'adgroup_id', 'adgroup', 'id');
        $this->addForeignKey($this->adFK, $this->tableName, 'ad_id', 'ad', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->adFK, $this->tableName);
        $this->dropForeignKey($this->adgroupFK, $this->tableName);
        $this->dropForeignKey($this->campaignFK, $this->tableName);
        $this->dropTable($this->tableName);
    }
}
