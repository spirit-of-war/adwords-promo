<?php

use yii\db\Migration;

class m170124_104331_alter_table_order extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'subject', $this->string()->null());
        $this->addColumn('order', 'body', $this->text()->null());

    }

    public function down()
    {
        $this->dropColumn('order', 'subject');
        $this->dropColumn('order', 'body');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
