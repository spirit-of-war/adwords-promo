<?php

use yii\db\Migration;

class m160823_112920_alter_table_ad extends Migration
{
    public function up()
    {
        $this->addColumn('ad', 'href', $this->string());
    }

    public function down()
    {
        $this->dropColumn('ad', 'href');
    }
}
