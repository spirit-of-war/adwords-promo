<?php

use console\migrations\MigrationBase;

class m161101_144224_create_table_limit_result extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('limit_result', [
            'rule_id' => $this->integer()->notNull(),
            'layer' => $this->smallInteger()->notNull(),
            'source_id' => $this->bigInteger()->unsigned()->notNull(),
            'action' => $this->smallInteger()->notNull()
        ]);

        $this->addForeignKey('limit_result_fk', 'limit_result', 'rule_id', 'rule', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('limit_result_fk', 'limit_result');
        $this->dropTable('limit_result');
    }
}
