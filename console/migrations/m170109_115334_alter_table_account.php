<?php

use yii\db\Migration;

class m170109_115334_alter_table_account extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'client_login', $this->string());
    }
}
