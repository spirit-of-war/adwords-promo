<?php

use yii\db\Migration;

class m160919_061831_alter_table_page extends Migration
{
    public function up()
    {
		$this->addColumn('page','is_seo_only',$this->integer()->notNull());
		$this->execute('ALTER TABLE page CHANGE body body TEXT;');
		$this->execute('ALTER TABLE page CHANGE header h1 varchar(255);');
		$this->addColumn('page','active',$this->integer()->notNull());
    }

    public function down()
    {
		$this->dropColumn('page','is_seo_only');
		$this->dropColumn('page','active');
		$this->execute('ALTER TABLE page CHANGE body body TEXT;');
		$this->execute('ALTER TABLE page CHANGE h1 header varchar(255);');
    }
}
