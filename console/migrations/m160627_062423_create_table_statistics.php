<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_statistics`.
 */
class m160627_062423_create_table_statistics extends MigrationBase
{
    private $name = 'statistics';
    private $fkName = 'fk_statistics_campaign';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'stat_date' => $this->integer(),
            'sum_search' => $this->float(),
            'sum_context' => $this->float(),
            'shows_search' => $this->integer(),
            'shows_context' => $this->integer(),
            'clicks_search' => $this->integer(),
            'clicks_context' => $this->integer(),
            'session_depth_search' => $this->float(),
            'session_depth_context' => $this->float(),
            'goal_conversion_search' => $this->float(),
            'goal_conversion_context' => $this->float(),
            'goal_cost_search' => $this->float(),
            'goal_cost_context' => $this->float(),
            'campaign_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey($this->fkName, $this->name, 'campaign_id', 'campaign', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->name);
        $this->dropTable($this->name);
    }
}