<?php

use yii\db\Migration;

class m160722_065141_alter_table_keyword extends Migration
{
    public function up()
    {
        $this->alterColumn('keyword', 'source_id', $this->bigInteger()->unsigned()->notNull());
        $this->alterColumn('keyword', 'adgroup_source_id', $this->bigInteger()->unsigned()->notNull());

        $this->alterColumn('ad', 'source_id', $this->bigInteger()->unsigned()->notNull());
        $this->alterColumn('ad', 'adgroup_source_id', $this->bigInteger()->unsigned()->notNull());

        $this->alterColumn('adgroup', 'source_id', $this->bigInteger()->unsigned()->notNull());
        $this->alterColumn('adgroup', 'campaign_source_id', $this->bigInteger()->unsigned()->notNull());

        $this->alterColumn('campaign', 'source_id', $this->bigInteger()->unsigned()->notNull());
    }

    public function down()
    {
    }
}
