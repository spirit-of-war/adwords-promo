<?php

use console\migrations\MigrationBase;

class m160804_051851_create_table_rule extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('rule', [
            'name' => $this->string(),
            'layer' => $this->smallInteger()->notNull(),
            'period' => $this->integer()->notNull(),
            'frequency' => $this->integer()->notNull(),
            'change' => $this->float()->notNull(),
            'account_id' => $this->integer()->notNull(),
            'all_campaigns' => $this->boolean()->notNull()
        ]);

        $this->addForeignKey('fk_rule_account', 'rule', 'account_id', 'account', 'id');

        $this->createCustomTable('rule_campaign', [
            'rule_id' => $this->integer()->notNull(),
            'campaign_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_rule_campaign_campaign', 'rule_campaign', 'campaign_id', 'campaign', 'id');
        $this->addForeignKey('fk_rule_campaign_rule', 'rule_campaign', 'rule_id', 'rule', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_rule_campaign_rule', 'rule_campaign');
        $this->dropForeignKey('fk_rule_campaign_campaign', 'rule_campaign');
        $this->dropTable('rule_campaign');
        $this->dropForeignKey('fk_rule_account', 'rule');
        $this->dropTable('rule');
    }
}