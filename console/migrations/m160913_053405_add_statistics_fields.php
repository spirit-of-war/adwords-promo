<?php

use yii\db\Migration;

class m160913_053405_add_statistics_fields extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'campaign_source_id', $this->bigInteger()->unsigned());
        $this->addColumn('statistics', 'adgroup_source_id', $this->bigInteger()->unsigned());
        $this->addColumn('statistics', 'crm_profit', $this->float());
    }
}
