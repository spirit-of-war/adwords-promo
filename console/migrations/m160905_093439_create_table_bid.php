<?php

use console\migrations\MigrationBase;

class m160905_093439_create_table_bid extends MigrationBase
{

    public function up()
    {
        $this->createCustomTable('bid', [
            'keyword_source_id' => $this->bigInteger()->unsigned(),
            'keyword_id' => $this->integer()->notNull(),
            'bid_search' => $this->integer(),
            'bid_context' => $this->integer(),
            'source_type' => $this->smallInteger()->notNull(),
            'state' => $this->smallInteger()->notNull(),
            'account_id' => $this->integer()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('bid');
    }
}