<?php

use yii\db\Migration;

class m160817_094412_alter_table_rule extends Migration
{
    public function up()
    {
        $this->addColumn('rule', 'action', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('rule', 'action');
    }
}
