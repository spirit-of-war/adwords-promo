<?php

use yii\db\Migration;

class m160802_104940_alter_table_negative_keyword extends Migration
{
    public function up()
    {
        $this->addColumn('negative_keyword', 'account_id', $this->integer()->unsigned());
    }

    public function down()
    {
        $this->dropColumn('negative_keyword', 'account_id');
    }
}
