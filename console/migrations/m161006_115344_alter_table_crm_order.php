<?php

use yii\db\Migration;

class m161006_115344_alter_table_crm_order extends Migration
{
    public function up()
    {
        $this->addColumn('crm_order', 'company_id', $this->integer()->notNull());
        $this->createIndex('crm_order_ui', 'crm_order', [
            'company_id',
            'date',
            'visit_id',
            'campaign_id',
            'adgroup_id',
            'keyword_id',
            'ad_id'
        ], true);
    }
}
