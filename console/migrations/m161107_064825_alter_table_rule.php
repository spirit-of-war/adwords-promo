<?php

use yii\db\Migration;

class m161107_064825_alter_table_rule extends Migration
{
    public function up()
    {
        $this->addColumn('rule', 'min_bid', $this->float());
        $this->addColumn('rule', 'max_bid', $this->float());
    }

    public function down()
    {
        $this->dropColumn('rule', 'min_bid');
        $this->dropColumn('rule', 'max_bid');
    }
}
