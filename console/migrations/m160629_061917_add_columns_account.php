<?php

use yii\db\Migration;

class m160629_061917_add_columns_account extends Migration
{
    private $tableName = 'account';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn($this->tableName, 'counter', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->tableName, 'counter');
    }
}
