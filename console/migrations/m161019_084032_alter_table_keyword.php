<?php

use yii\db\Migration;
use \common\models\structure\Keyword;

class m161019_084032_alter_table_keyword extends Migration
{
    public function up()
    {
        $this->addColumn(Keyword::tableName(), 'negative', $this->string(4096));
    }
}
