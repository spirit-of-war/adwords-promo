<?php

use yii\db\Migration;

class m160718_082749_add_unique_keys_campaign_adgroup_ad extends Migration
{

    public function up()
    {
        $this->alterColumn('adgroup', 'source_id', $this->integer()->notNull());
        $this->alterColumn('ad', 'source_id', $this->integer()->notNull());

        $this->createIndex('ui_campaign', 'campaign', ['source_id', 'account_type'], true);
        $this->createIndex('ui_adgroup', 'adgroup', ['source_id', 'account_type'], true);
        $this->createIndex('ui_ad', 'ad', ['source_id', 'account_type'], true);

    }

    public function down()
    {
        $this->dropIndex('ui_campaign', 'campaign');
        $this->dropIndex('ui_adgroup', 'adgroup');
        $this->dropIndex('ui_ad', 'ad');

        $this->alterColumn('adgroup', 'source_id', $this->string()->notNull());
        $this->alterColumn('ad', 'source_id', $this->string()->notNull());
    }
}