<?php

use yii\db\Migration;

class m160630_081553_add_columns_company extends Migration
{
    private $tableName = 'company';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn($this->tableName, 'token', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->tableName, 'token');
    }
}
