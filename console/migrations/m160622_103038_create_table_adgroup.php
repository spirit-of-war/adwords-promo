<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_adgroup`.
 */
class m160622_103038_create_table_adgroup extends MigrationBase
{
    private $name = 'adgroup';
    private $fkName = 'fk_adgroup_campaign';
    
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'source_id' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'campaign_id' => $this->integer()->notNull(),
            'account_type' => $this->integer()->notNull(),
            'state' => $this->integer()->notNull()
        ]);
        $this->addForeignKey($this->fkName, $this->name, 'campaign_id', 'campaign', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->name);
        $this->dropTable($this->name);
    }
}
