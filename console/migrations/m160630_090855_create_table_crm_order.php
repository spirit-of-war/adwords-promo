<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_crm_order`.
 */
class m160630_090855_create_table_crm_order extends MigrationBase
{
    private $tableName = 'crm_order';
    private $fkName = 'fk_crm_order_company';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->tableName, [
            'number' => $this->string()->notNull(),
            'date' => $this->integer()->notNull(),
            'profit' => $this->float()->notNull(),
            'company_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey($this->fkName, $this->tableName, 'company_id', 'company', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->tableName);
        $this->dropTable($this->tableName);
    }
}
