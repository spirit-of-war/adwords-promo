<?php

use console\migrations\MigrationBase;

class m160721_083606_create_table_keyword extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('keyword', [
            'source_id' => $this->integer()->notNull(),
            'adgroup_source_id' => $this->integer()->notNull(),
            'keyword' =>  $this->string()->notNull(),
            'campaign_id' => $this->integer(),
            'adgroup_id' => $this->integer(),
            'state' => $this->integer()->notNull(),
            'account_type' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_keyword_campaign', 'keyword', 'campaign_id', 'campaign', 'id');
        $this->addForeignKey('fk_keyword_adgroup', 'keyword', 'adgroup_id', 'adgroup', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_keyword_adgroup', 'keyword');
        $this->dropForeignKey('fk_keyword_campaign', 'keyword');
        $this->dropTable('keyword');
    }
}
