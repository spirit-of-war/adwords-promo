<?php

use yii\db\Migration;

class m160914_074850_alter_column_user extends Migration
{
    public function up()
    {
		$this->addColumn('user', 'role', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('role','user');
    }
}
