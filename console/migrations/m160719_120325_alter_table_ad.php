<?php

use yii\db\Migration;

class m160719_120325_alter_table_ad extends Migration
{
    public function up()
    {
        $this->alterColumn('ad', 'adgroup_id', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('ad', 'adgroup_id', $this->integer()->notNull());
    }
}
