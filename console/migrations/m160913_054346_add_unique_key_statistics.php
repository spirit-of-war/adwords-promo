<?php

use yii\db\Migration;

class m160913_054346_add_unique_key_statistics extends Migration
{
    public function up()
    {
        $this->createIndex('ui_statistics', 'statistics', [
            'start_date',
            'account_id',
            'campaign_source_id',
            'adgroup_source_id',
            'keyword_source_id',
            'ad_source_id'
        ], true);
    }
}
