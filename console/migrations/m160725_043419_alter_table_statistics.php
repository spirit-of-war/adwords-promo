<?php

use yii\db\Migration;

class m160725_043419_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'revenue', $this->float());
        $this->addColumn('statistics', 'source_roi', $this->float());
    }

    public function down()
    {
        $this->dropColumn('statistics', 'source_roi');
        $this->dropColumn('statistics', 'revenue');
    }
}
