<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_campaign`.
 */
class m160622_033121_create_table_campaign extends MigrationBase
{
    private $name = 'campaign';
    private $fkName = 'fk_campaign_account';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'source_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'source_state' => $this->string()->notNull(),
            'status' => $this->string()->notNull(),
            'currency' => $this->string()->notNull(),
            'status_payment' => $this->string()->notNull(),
            'account_type' => $this->integer()->notNull(),
            'state' => $this->integer()->notNull(),
            'clicks' => $this->integer(),
            'impressions' => $this->integer(),
            'account_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey($this->fkName, $this->name, 'account_id', 'account', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->name);
        $this->dropTable($this->name);
    }
}
