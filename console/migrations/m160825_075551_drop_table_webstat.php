<?php

use yii\db\Migration;

class m160825_075551_drop_table_webstat extends Migration
{
    public function up()
    {
        $this->dropTable('webstat');
    }

    public function down()
    {
    }
}
