<?php

use yii\db\Migration;

class m161229_092344_alter_table_company extends Migration
{
    public function up()
    {
        $this->dropColumn('company', 'website');
        $this->addColumn('company', 'website', $this->string());

        $this->dropColumn('company', 'name');
        $this->addColumn('company', 'name', $this->string()->notNull());
    }
}
