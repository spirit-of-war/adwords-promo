<?php

use console\migrations\MigrationBase;

class m160905_145621_create_table_bid_auction extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('bid_auction', [
            'keyword_source_id' => $this->bigInteger()->unsigned(),
            'keyword_id' => $this->bigInteger()->unsigned(),
            'p11_price' => $this->integer()->notNull(),
            'p11_bid' => $this->integer()->notNull(),
            'p12_price' => $this->integer()->notNull(),
            'p12_bid' => $this->integer()->notNull(),
            'p13_price' => $this->integer()->notNull(),
            'p13_bid' => $this->integer()->notNull(),
            'p21_price' => $this->integer()->notNull(),
            'p21_bid' => $this->integer()->notNull(),
            'p24_price' => $this->integer()->notNull(),
            'p24_bid' => $this->integer()->notNull(),
            'state' => $this->smallInteger()->notNull(),
            'account_id' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('bid_auction');
    }
}