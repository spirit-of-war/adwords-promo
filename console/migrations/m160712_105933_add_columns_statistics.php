<?php

use yii\db\Migration;

class m160712_105933_add_columns_statistics extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'source_id', $this->string());
    }

    public function down()
    {
        $this->dropColumn('statistics', 'source_id');
    }
}
