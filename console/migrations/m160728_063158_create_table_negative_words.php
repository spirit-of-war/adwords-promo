<?php

use console\migrations\MigrationBase;

class m160728_063158_create_table_negative_words extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('negative_keyword', [
            'source_id' => $this->integer()->notNull(),
            'adgroup_source_id' => $this->integer()->notNull(),
            'keyword' =>  $this->string()->notNull(),
            'campaign_id' => $this->integer(),
            'adgroup_id' => $this->integer(),
            'state' => $this->integer()->notNull(),
            'account_type' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_negative_keyword_campaign', 'negative_keyword', 'campaign_id', 'campaign', 'id');
        $this->addForeignKey('fk_negative_keyword_adgroup', 'negative_keyword', 'adgroup_id', 'adgroup', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_negative_keyword_adgroup', 'negative_keyword');
        $this->dropForeignKey('fk_negative_keyword_campaign', 'negative_keyword');
        $this->dropTable('negative_keyword');
    }
}
