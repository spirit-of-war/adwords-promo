<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_company`.
 */
class m160621_051649_create_table_company extends MigrationBase
{
    private $name = 'company';
    private $fkName = 'fk_company_user';
    
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'name' => $this->string()->notNull()->unique(),
            'website' => $this->string()->notNull()->unique(),
            'sales_report_url' => $this->string(),
            'user_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey($this->fkName, $this->name, 'user_id', 'user', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->name);
        $this->dropTable($this->name);
    }
}
