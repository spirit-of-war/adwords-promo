<?php

use console\migrations\MigrationBase;

class m160914_084904_create_table_report_settings extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('report_setting', [
            'account_id' => $this->integer()->notNull(),
            'layer' => $this->integer()->notNull(),
            'field' => $this->string()->notNull()
        ]);

        $this->addForeignKey('fk_report_setting_account', 'report_setting', 'account_id', 'account', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_report_setting_account', 'report_setting');
        $this->dropTable('report_setting');
    }
}
