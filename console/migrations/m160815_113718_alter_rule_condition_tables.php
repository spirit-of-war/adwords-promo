<?php

use yii\db\Migration;

class m160815_113718_alter_rule_condition_tables extends Migration
{
    public function up()
    {
        $this->alterColumn('rule', 'change', $this->float());
        $this->dropColumn('rule', 'period');

        $this->addColumn('condition', 'period', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('condition', 'period');

        $this->addColumn('rule', 'period', $this->integer()->notNull());
        $this->alterColumn('rule', 'change', $this->float()->notNull());
    }
}
