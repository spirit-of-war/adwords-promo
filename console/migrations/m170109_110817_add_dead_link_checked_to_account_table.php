<?php

use yii\db\Migration;

class m170109_110817_add_dead_link_checked_to_account_table extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'dead_link_checked', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('account', 'dead_link_checked');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
