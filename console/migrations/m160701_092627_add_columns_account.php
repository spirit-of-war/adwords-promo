<?php

use yii\db\Migration;

class m160701_092627_add_columns_account extends Migration
{
    private $tableName = 'account';
    private $columnName = 'token_life_time';

    public function up()
    {
        $this->addColumn($this->tableName, $this->columnName, $this->integer());
    }

    public function down()
    {
        $this->dropColumn($this->tableName, $this->columnName);
    }
}
