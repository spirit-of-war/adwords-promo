<?php

namespace console\migrations;

use yii\db\Migration;

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 21.06.16
 * Time: 11:19
 */
class MigrationBase extends Migration
{
    protected function createCustomTable($name, $fields)
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $fields = array_merge([
            'id' => $this->primaryKey()
        ], $fields);
        $fields['created_at'] = $this->integer()->notNull();
        $fields['updated_at'] = $this->integer()->notNull();
        $fields['created_by'] = $this->integer()->notNull();
        $fields['updated_by'] = $this->integer()->notNull();
        $this->createTable($name, $fields, $tableOptions);
    }
}