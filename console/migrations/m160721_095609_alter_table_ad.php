<?php

use yii\db\Migration;

class m160721_095609_alter_table_ad extends Migration
{
    public function up()
    {
        $this->dropColumn('ad', 'campaign_source_id');
    }

    public function down()
    {
        $this->addColumn('ad', 'campaign_source_id', $this->integer());
    }
}
