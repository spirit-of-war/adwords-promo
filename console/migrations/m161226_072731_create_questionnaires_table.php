<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation of table `questionnaires`.
 */
class m161226_072731_create_questionnaires_table extends MigrationBase
{
    private $name = 'questionnaires';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'monthly_budget' => $this->integer(),
            'max_cost_conversion' => $this->integer(),
            'advise_cost_conversion' => $this->integer(),
            'context_advertising_type' => $this->string(),
            'plannde_conversion_amount' => $this->integer(),
            'commodity' => $this->string(),
            'commodity_advantages' => $this->string(),
            'company_advantages' => $this->string(),
            'sales' => $this->string(),
            'delivery_condition' => $this->string(),
            'message' => $this->string(),
            'site_address' => $this->string(),
            'advertising_district' => $this->string(),
            'advertising_systems' => $this->string(),
            'user_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->name);
    }
}
