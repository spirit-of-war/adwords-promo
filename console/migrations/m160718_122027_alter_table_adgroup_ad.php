<?php

use yii\db\Migration;

class m160718_122027_alter_table_adgroup_ad extends Migration
{
    public function up()
    {
        $this->alterColumn('adgroup', 'campaign_id', $this->integer());
        $this->alterColumn('ad', 'campaign_id', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('adgroup', 'campaign_id', $this->integer()->notNull());
        $this->alterColumn('ad', 'campaign_id', $this->integer()->notNull());
    }
}
