<?php

use yii\db\Migration;

class m160902_052704_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'account_id', $this->integer()->notNull());
    }
}
