<?php

use yii\db\Migration;
use common\models\structure\Bid;

class m160928_104352_alter_table_bid extends Migration
{
    public function up()
    {
        $this->addColumn(Bid::tableName(), 'task_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn(Bid::tableName(), 'rule_id');
    }
}
