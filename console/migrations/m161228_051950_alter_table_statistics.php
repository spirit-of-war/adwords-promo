<?php

use yii\db\Migration;

class m161228_051950_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->alterColumn('statistics', 'sum_search', $this->float()->defaultValue(0));
        $this->alterColumn('statistics', 'sum_context', $this->float()->defaultValue(0));
        $this->alterColumn('statistics', 'shows_search', $this->integer()->defaultValue(0));
        $this->alterColumn('statistics', 'shows_context', $this->integer()->defaultValue(0));
        $this->alterColumn('statistics', 'clicks_search', $this->integer()->defaultValue(0));
        $this->alterColumn('statistics', 'clicks_context', $this->integer()->defaultValue(0));
        $this->alterColumn('statistics', 'revenue', $this->float()->defaultValue(0));
        $this->alterColumn('statistics', 'source_roi', $this->float()->defaultValue(0));

        $this->alterColumn('statistics', 'crm_conversions', $this->integer()->defaultValue(0));
        $this->alterColumn('statistics', 'crm_revenue', $this->float()->defaultValue(0));
        $this->alterColumn('statistics', 'crm_assoc_conversions', $this->integer()->defaultValue(0));

        $this->alterColumn('statistics', 'average_pos', $this->float()->defaultValue(0));
    }
}
