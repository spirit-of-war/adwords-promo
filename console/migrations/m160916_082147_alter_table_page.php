<?php

use yii\db\Migration;

class m160916_082147_alter_table_page extends Migration
{
    public function up()
    {
		$this->addColumn('page', 'header', $this->string()->notNull());
		$this->addColumn('page', 'keywords', $this->string()->notNull());
		$this->addColumn('page', 'description', $this->string()->notNull());
		$this->addColumn('page','url',$this->string()->notNull());
    }

    public function down()
    {
		$this->dropColumn('page','header');
		$this->dropColumn('page','keywords');
		$this->dropColumn('page','description');
		$this->dropColumn('page','url');
    }
}