<?php

use console\migrations\MigrationBase;

class m160826_085435_create_table_sitelink extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('sitelink', [
            'set_source_id' => $this->bigInteger()->unsigned(),
            'title' => $this->string()->notNull(),
            'href' => $this->string(1024)->notNull(),//1024 - длина строки href в Яндекс.Директ
            'state' => $this->smallInteger()->notNull(),
            'account_type' => $this->smallInteger(),
            'account_id' => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('sitelink');
    }
}
