<?php

use yii\db\Migration;

class m160826_081257_alter_table_ad extends Migration
{
    public function up()
    {
        $this->addColumn('ad', 'sitelink_set_source_id', $this->bigInteger()->unsigned());
        $this->alterColumn('ad', 'href', $this->string(1024));//1024 - длина href в Яндекс.Директ
    }

    public function down()
    {
        $this->dropColumn('ad', 'sitelink_set_source_id');
    }
}
