<?php

use yii\db\Migration;

class m160729_103056_change_table_account extends Migration
{
    private $table_name = 'account';
    public function up()
    {
        $this->dropColumn($this->table_name, 'g_client_id');
        $this->dropColumn($this->table_name, 'g_client_secret');
        $this->dropColumn($this->table_name, 'g_user_agent');
        $this->dropColumn($this->table_name, 'g_adwords_client_customer_id');
        $this->dropColumn($this->table_name, 'g_adwords_developer_key');

        $this->renameColumn($this->table_name,'g_refresh_token','refresh_token');
    }

    public function down()
    {
        $this->addColumn($this->table_name,'g_client_id',$this->string());
        $this->addColumn($this->table_name,'g_client_secret',$this->string());
        $this->addColumn($this->table_name,'g_user_agent',$this->string());
        $this->addColumn($this->table_name,'g_adwords_client_customer_id',$this->string());
        $this->addColumn($this->table_name,'g_adwords_developer_key',$this->string());

        $this->renameColumn($this->table_name,'refresh_token','g_refresh_token');
    }
}
