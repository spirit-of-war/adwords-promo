<?php

use yii\db\Migration;

class m160720_064310_alter_table_account extends Migration
{
    public function up()
    {
        $this->dropColumn('account', 'token_life_time');
    }

    public function down()
    {
        $this->addColumn('account', 'token_life_time', $this->integer());
    }
}
