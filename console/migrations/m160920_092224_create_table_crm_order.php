<?php

use yii\db\Migration;

class m160920_092224_create_table_crm_order extends Migration
{
    public function up()
    {
        $this->createTable('crm_order', [
            'visit_id' => $this->string(),
            'campaign_id' => $this->bigInteger()->unsigned(),
            'adgroup_id' => $this->bigInteger()->unsigned(),
            'keyword_id' => $this->bigInteger()->unsigned(),
            'ad_id' => $this->bigInteger()->unsigned(),
            'order_id' => $this->integer(),
            'revenue' => $this->integer(),
            'date' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('crm_order');
    }
}
