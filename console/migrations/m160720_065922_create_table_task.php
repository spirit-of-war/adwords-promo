<?php

use console\migrations\MigrationBase;

class m160720_065922_create_table_task extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('task', [
            'type' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'object_id' => $this->integer(),
            'completeness' => $this->float()
        ]);
        $this->alterColumn('task', 'created_by', $this->integer()->null());
        $this->alterColumn('task', 'updated_by', $this->integer()->null());
    }

    public function down()
    {
        $this->dropTable('task');
    }
}
