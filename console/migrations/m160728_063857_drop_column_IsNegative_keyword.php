<?php

use yii\db\Migration;

class m160728_063857_drop_column_IsNegative_keyword extends Migration
{
    public function up()
    {
        $this->dropColumn('keyword', 'IsNegative');
    }
    public function down()
    {
        $this->addColumn('keyword', 'IsNegative', $this->string());
    }
}
