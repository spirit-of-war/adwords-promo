<?php

use yii\db\Migration;

class m160919_045231_add_column_crm_conversions_statistic extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'crm_conversions', $this->integer());
    }
}
