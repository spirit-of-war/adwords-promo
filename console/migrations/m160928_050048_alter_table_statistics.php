<?php

use yii\db\Migration;
use \common\models\Statistics;

class m160928_050048_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->addColumn(Statistics::tableName(), 'crm_assoc_conversions', $this->integer());
    }
}
