<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation for table `table_web_order`.
 */
class m160629_070646_create_table_web_order extends MigrationBase
{
    private $name = 'web_order';
    private $fkName = 'fk_web_order_campaign';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'number' => $this->integer()->notNull(),
            'date' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'amount' => $this->float()->notNull(),
            'key_words' => $this->string(),
            'campaign_id' => $this->integer(),
        ]);
        $this->addForeignKey($this->fkName, $this->name, 'campaign_id', 'campaign', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->fkName, $this->name);
        $this->dropTable($this->name);
    }
}
