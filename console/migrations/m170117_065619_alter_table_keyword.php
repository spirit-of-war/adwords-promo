<?php

use yii\db\Migration;

class m170117_065619_alter_table_keyword extends Migration
{
    public function up()
    {
        $this->addColumn('keyword', 'auction_p11_price', $this->integer());
        $this->addColumn('keyword', 'auction_p12_price', $this->integer());
        $this->addColumn('keyword', 'auction_p13_price', $this->integer());
        $this->addColumn('keyword', 'auction_p21_price', $this->integer());
        $this->addColumn('keyword', 'auction_p24_price', $this->integer());

        $this->addColumn('keyword', 'auction_p11_bid', $this->integer());
        $this->addColumn('keyword', 'auction_p12_bid', $this->integer());
        $this->addColumn('keyword', 'auction_p13_bid', $this->integer());
        $this->addColumn('keyword', 'auction_p21_bid', $this->integer());
        $this->addColumn('keyword', 'auction_p24_bid', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('keyword', 'auction_p11_price');
        $this->dropColumn('keyword', 'auction_p12_price');
        $this->dropColumn('keyword', 'auction_p13_price');
        $this->dropColumn('keyword', 'auction_p21_price');
        $this->dropColumn('keyword', 'auction_p24_price');

        $this->dropColumn('keyword', 'auction_p11_bid');
        $this->dropColumn('keyword', 'auction_p12_bid');
        $this->dropColumn('keyword', 'auction_p13_bid');
        $this->dropColumn('keyword', 'auction_p21_bid');
        $this->dropColumn('keyword', 'auction_p24_bid');
    }
}
