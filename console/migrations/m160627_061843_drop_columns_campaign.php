<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `columns_campaign`.
 */
class m160627_061843_drop_columns_campaign extends Migration
{
    private $name = 'campaign';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn($this->name, 'clicks');
        $this->dropColumn($this->name, 'impressions');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn($this->name, 'clicks', $this->integer());
        $this->addColumn($this->name, 'impressions', $this->integer());
    }
}
