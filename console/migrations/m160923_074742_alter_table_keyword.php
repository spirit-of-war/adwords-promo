<?php

use yii\db\Migration;
use common\models\structure\Keyword;

class m160923_074742_alter_table_keyword extends Migration
{
    public function up()
    {
        $this->alterColumn(Keyword::tableName(), 'keyword', $this->string(2048)->notNull());
    }

    public function down()
    {
    }
}
