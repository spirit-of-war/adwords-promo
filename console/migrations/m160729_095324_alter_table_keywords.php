<?php

use yii\db\Migration;

class m160729_095324_alter_table_keywords extends Migration
{
    public function up()
    {
        $this->addColumn('keyword', 'bid_context', $this->float());
        $this->addColumn('keyword', 'bid_search', $this->float());
    }

    public function down()
    {
        $this->dropColumn('keyword', 'bid_search');
        $this->dropColumn('keyword', 'bid_context');
    }
}
