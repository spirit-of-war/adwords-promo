<?php

use yii\db\Migration;

class m160824_120916_alter_table_rule extends Migration
{
    public function up()
    {
        $this->addColumn('rule', 'position', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('rule', 'position');
    }
}

