<?php

use yii\db\Migration;

class m161012_101549_alter_table_keyword extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('ui_keyword', 'keyword');
        $this->createIndex('ui_keyword', 'keyword', ['source_id', 'account_id'], true);
    }

    public function safeDown()
    {
    }
}
