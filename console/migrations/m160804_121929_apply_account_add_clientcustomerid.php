<?php

use yii\db\Migration;

class m160804_121929_apply_account_add_clientcustomerid extends Migration
{
    public function up()
    {
        $this->addColumn('account','client_customer_id',$this->string());
    }

    public function down()
    {
       $this->dropColumn('account','client_customer_id',$this->string());
    }
}
