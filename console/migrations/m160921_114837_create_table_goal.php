<?php

use console\migrations\MigrationBase;

class m160921_114837_create_table_goal extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('goal', [
            'number' => $this->integer()->notNull(),
            'account_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('fk_goal_account', 'goal', 'account_id', 'account', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_goal_account', 'goal');
        $this->dropTable('goal');
    }
}
