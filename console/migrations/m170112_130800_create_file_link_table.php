<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation of table `file_link`.
 */
class m170112_130800_create_file_link_table extends MigrationBase
{
    private $name = 'file_link';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'file_path' => $this->string(),
            'file_name' => $this->string(),
            'account_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->name);
    }
}
