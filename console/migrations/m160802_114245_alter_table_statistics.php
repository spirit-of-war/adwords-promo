<?php

use yii\db\Migration;

class m160802_114245_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->alterColumn('statistics', 'adgroup_id', $this->integer());
        $this->createIndex('statistics_adgroup_id', 'statistics', 'adgroup_id');
    }

    public function down()
    {
        $this->dropIndex('statistics_adgroup_id', 'statistics');
        $this->alterColumn('statistics', 'adgroup_id', $this->bigInteger()->unsigned());
    }
}
