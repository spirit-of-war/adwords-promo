<?php

use yii\db\Migration;

class m160728_111547_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'adgroup_id', $this->bigInteger()->unsigned());
    }

    public function down()
    {
        $this->dropColumn('statistics', 'adgroup_id');
    }
}
