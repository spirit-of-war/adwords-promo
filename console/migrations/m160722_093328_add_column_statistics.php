<?php

use yii\db\Migration;

class m160722_093328_add_column_statistics extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'ad_source_id', $this->bigInteger()->unsigned()->notNull());
        $this->addColumn('statistics', 'keyword_source_id', $this->bigInteger()->unsigned());
        $this->dropColumn('statistics', 'source_id');
        $this->addForeignKey('fk_statistics_advert', 'statistics', 'ad_id', 'ad', 'id');
        $this->addForeignKey('fk_statistics_keyword', 'statistics', 'keyword_id', 'keyword', 'id');
        $this->addColumn('statistics', 'account_type', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('statistics', 'account_type');
        $this->dropForeignKey('fk_statistics_keyword', 'statistics');
        $this->dropForeignKey('fk_statistics_advert', 'statistics');
        $this->addColumn('statistics', 'source_id', $this->bigInteger()->unsigned()->notNull());
        $this->dropColumn('statistics', 'keyword_source_id');
        $this->dropColumn('statistics', 'ad_source_id');
    }
}
