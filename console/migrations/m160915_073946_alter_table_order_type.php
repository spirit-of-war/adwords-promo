<?php

use yii\db\Migration;

class m160915_073946_alter_table_order_type extends Migration
{
    public function up()
    {
		$this->addColumn('order', 'type', $this->integer()->notNull());
    }

    public function down()
    {
		$this->dropColumn('type','order');
    }
}
