<?php

use yii\db\Migration;

class m161207_053006_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->addColumn('statistics', 'average_pos', $this->float());
    }

    public function down()
    {
        $this->dropColumn('statistics', 'average_pos');
    }
}
