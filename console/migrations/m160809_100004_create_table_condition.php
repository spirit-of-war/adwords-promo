<?php

use console\migrations\MigrationBase;

class m160809_100004_create_table_condition extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('condition', [
            'rule_id' => $this->integer()->notNull(),
            'disjunction' => $this->integer()->notNull(),
            'index' => $this->string()->notNull(),
            'action' => $this->string()->notNull(),
            'value' => $this->string()->notNull()
        ]);

        $this->addForeignKey('fk_condition_rule', 'condition', 'rule_id', 'rule', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_condition_rule', 'condition');
        $this->dropTable('condition');
    }
}
