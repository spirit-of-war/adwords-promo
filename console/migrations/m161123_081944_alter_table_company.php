<?php

use yii\db\Migration;

class m161123_081944_alter_table_company extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'last_crm_stat_downloading', $this->integer());
    }
}
