<?php

use console\migrations\MigrationBase;

class m160818_071126_create_table_rule_execution extends MigrationBase
{
    public function up()
    {
        $this->createCustomTable('rule_execution', [
            'rule_id' => $this->integer()->notNull(),
            'request' => $this->string()->notNull(),
            'sql' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('rule_execution');
    }
}
