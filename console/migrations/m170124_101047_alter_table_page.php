<?php

use yii\db\Migration;

class m170124_101047_alter_table_page extends Migration
{
    public function up()
    {
        $this->addColumn('page', 'partial_name', $this->string()->null());
    }

    public function down()
    {
        $this->dropColumn('page', 'partial_name');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
