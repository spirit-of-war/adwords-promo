<?php

use yii\db\Migration;

class m160919_101910_alter_table extends Migration
{
    public function up()
    {
        $this->dropColumn('statistics', 'crm_profit');
        $this->addColumn('statistics', 'crm_revenue', $this->float());
    }
}
