<?php

use yii\db\Migration;
use backend\controllers\AccountController;

class m161210_093840_alter_table_report_setting extends Migration
{
    public function up()
    {
        $this->addColumn(
            'report_setting', 'type', $this->smallInteger() . ' DEFAULT ' . AccountController::SETTING_TYPE);
    }

    public function down()
    {
        $this->dropColumn('report_setting', 'type');
    }
}
