<?php

use console\migrations\MigrationBase;

/**
 * Handles the creation of table `dead_link`.
 */
class m161229_120926_create_dead_link_table extends MigrationBase
{
    private $name = 'dead_link';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createCustomTable($this->name, [
            'title' => $this->string(),
            'text' => $this->string(),
            'href' => $this->string()->notNull(),
            'source_id' => $this->integer(),
            'ad_id' => $this->integer(),
            'ad_group_name' => $this->string(),
            'company_name' => $this->string(),
            'campaign_name' => $this->string(),
            'account_name' => $this->string(),
            'error_message' => $this->string()
        ]);
        $this->alterColumn($this->name, 'created_by', $this->integer()->null());
        $this->alterColumn($this->name, 'updated_by', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->name);
    }
}
