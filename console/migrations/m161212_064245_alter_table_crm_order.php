<?php

use yii\db\Migration;

class m161212_064245_alter_table_crm_order extends Migration
{
    public function up()
    {
        $this->addColumn('crm_order', 'account_id', $this->integer());
    }
}
