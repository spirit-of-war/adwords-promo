<?php

use yii\db\Migration;
use \common\models\Statistics;

class m160923_040853_alter_table_statistics extends Migration
{
    public function up()
    {
        $this->addColumn(Statistics::tableName(), 'stat_conversions', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(Statistics::tableName(), 'stat_conversions');
    }
}
