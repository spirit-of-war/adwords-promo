<?php

use yii\db\Migration;

class m160825_082646_drop_table_rule_execution extends Migration
{
    public function up()
    {
        $this->dropTable('rule_execution');
    }
}
