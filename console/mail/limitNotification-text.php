<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 01.11.16
 * Time: 16:21
 */

use \common\models\structure\Layer;

$layer = 'кампании';
if ($limit->layer == Layer::LAYER_ADGROUPS)
    $layer = 'группы объявлений';
elseif ($limit->layer == Layer::LAYER_KEYWORDS)
    $layer = 'ключевые слова';

$limitLink = 'http://admin.profitpanda.ru/limit/view?id=' . $limit->id;
?>

    Здравствуйте, <?= $userName?>!

    Уведомляем Вас о том, что был сработан лимит "<?= $limit->name ?>".

    Под лимит попали следующие <?= $layer ?>:

<?php
for ($i = 0; $i < count($condElements); $i++)
    if ($i <= 20)
        echo '"' . $condElements[$i]->name . '", ' . PHP_EOL;

if (count($condElements) > 20)
    echo 'С полным списком элементов Вы можете ознакомиться на сайте: ' . $limitLink;