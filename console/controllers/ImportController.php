<?php

namespace console\controllers;

use common\models\Account;
use common\models\strategy\Rule;
use common\models\strategy\Limit;

class ImportController extends \yii\console\Controller
{
    /**
     * Updates whole structure from Yandex Direct or Google AdWords depending of Account type, updates statistics,
     * executes limits and rules.
     *
     */
    public function actionIndex($accountId = false)
    {
        $condition = [
            'state' => Account::STATE_ACTIVE,
            'advert_type' => Account::TYPE_YANDEX
        ];
        if ($accountId !== false)
            $condition['id'] = $accountId;

        $accounts = Account::findAll($condition);
        $this->exe($accounts);

        $condition['advert_type'] = Account::TYPE_GOOGLE;
        $accounts = Account::findAll($condition);
        $this->exe($accounts);
    }

    private function exe($accounts)
    {
        foreach ($accounts as $account) {
            $account->import->updateStructure();
            $account->import->updateStatistics();
            $this->actionLimits($account->id);
            $this->actionRules($account->id);
        }
    }

    public function actionStatistics($accountID, $from = null, $to = null)
    {
        $from = is_null($from) ? 'yesterday' : $from;
        $to = is_null($to) ? $from : $to;

        $account = Account::findOne($accountID);
        $account->import->updateStatistics($from, $to);
    }

    public function actionStructure($accountID)
    {
        $account = Account::findOne($accountID);
        $account->import->updateStructure();
    }

    public function actionRules($accountID)
    {
        $rules = Rule::findAll([
            'state' => Rule::STATE_ACTIVE,
            'account_id' => $accountID
        ]);
        foreach ($rules as $rule)
            if ($rule->frequency == Rule::FREQ_ONCE && $rule->onceday == strtotime('today')
                || $rule->frequency == Rule::FREQ_DAY
                || $rule->frequency == Rule::FREQ_WEEK && $rule->weekday == (int)date('w')
                || $rule->frequency == Rule::FREQ_MONTH && $rule->monthday == (int)date('j')
            )
                $rule->execute();
    }

    public function actionLimits($accountID)
    {
        $limits = Limit::findAll([
            'state' => Rule::STATE_ACTIVE,
            'account_id' => $accountID
        ]);
        foreach ($limits as $limit)
            if ($limit->frequency == Limit::FREQ_DAY
                || $limit->frequency == Limit::FREQ_WEEK && $limit->weekday == (int)date('w')
                || $limit->frequency == Limit::FREQ_MONTH && $limit->monthday == (int)date('j')
            )
                $limit->execute();
    }

    public function actionInit($accountID, $statPeriod)
    {
        $this->actionStructure($accountID);

        for ($i = $statPeriod; $i >= 1; $i--) {
            $date = $i . ' days ago';
            $this->actionStatistics($accountID, $date);
        }
    }

    public function actionRule($id)
    {
        $rule = Rule::findOne($id);
        $rule->execute();
    }
}
