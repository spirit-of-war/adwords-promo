<?php

namespace console\controllers;

use common\models\Account;
use common\models\DeadLink;
use common\models\FileLink;
use common\models\structure\Ad;


class DeadLinkController extends \yii\console\Controller
{

    private $account;
    public function actionCheckLinks()
    {
        $fileLink = FileLink::find()
            ->orderBy('id ASC')
            ->limit(1)
            ->one();

        if($fileLink){
            $this->account = Account::findOne($fileLink->account_id);

            $partLinks = $this->extractPartLinkFromFile($fileLink);
            if($deadLinks = $this->getDeadLinks($partLinks)){
                $this->saveDeadLinks($deadLinks);
            }
        }
    }

    private function getDeadLinks($rows)
    {
        $deadLinks = [];
        $ch = curl_init();
        foreach($rows as $i => $row) {
            curl_setopt_array($ch, array(
                CURLOPT_URL => $row['url_after_prepare'],
                CURLOPT_TIMEOUT => 30,
                CURLOPT_VERBOSE => True,
                CURLOPT_RETURNTRANSFER => True,
                CURLOPT_HEADER => True,
                CURLOPT_NOBODY => True
            ));
            curl_exec($ch);

            if (!curl_errno($ch)) {
                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if($http_code >= 400){
                    $row['error_message'] = 'code error: '.$http_code;
                    $deadLinks[] = $row;
                }
            } else {
                $row['error_message'] = 'this site not exist';
                $deadLinks[] = $row;
            }
        }
        curl_close($ch);

        return $deadLinks;

    }

    private function extractPartLinkFromFile($fileLink)
    {
        $fileName = $fileLink->file_path . '/' . $fileLink->file_name;
        $file=file($fileName);

        $isEnd = false;
        $linksArr = [];
        for($i=0;$i<100;$i++){
            if(!array_key_exists($i,$file)){
                $isEnd = true;
                break;
            }
            $linksArr[] = (array) json_decode($file[$i]);
            unset($file[$i]);
        }

        if(!$isEnd){
            $fp = fopen($fileName,"w");
            fputs($fp,implode("",$file));
            fclose($fp);
        } else {
            $this->endFileBehavior($fileLink);
        }


        return $linksArr;
    }

    private function endFileBehavior($fileLink)
    {
        $fileName = $fileLink->file_path . '/' . $fileLink->file_name;
        if(unlink($fileName)){
            $fileLink->delete();
            $this->account->dead_link_checked = time();
            $this->account->save();
        }
    }

    private function saveDeadLinks($rows)
    {
        foreach($rows as $row){
            $account = Account::findOne($row['account_id']);
            $ad = Ad::findOne(['sitelink_set_source_id' => $row['set_source_id']]);

            $fields = [];
            $fields['title'] = $row['title'];
            $fields['href'] = $row['url_after_prepare'];
            $fields['source_id'] = $row['set_source_id'];
            $fields['error_message'] = $row['error_message'];
            $fields['ad_id'] = $ad->id;
            $fields['ad_group_name'] = $ad->adgroup->name;
            $fields['campaign_name'] = $ad->campaign->name;
            $fields['account_name'] = $account->advert_login;

            $deadLink = new DeadLink();
            $deadLink->attributes = $fields;
            $deadLink->save();
        }
    }
}