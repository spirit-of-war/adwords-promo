<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 28.06.16
 * Time: 11:13
 */

namespace common\models;


interface ConvertDateInterface
{
    public function convertDateFormat($date);
}