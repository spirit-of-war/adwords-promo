<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dead_link".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $href
 * @property integer $source_id
 * @property integer $ad_id
 * @property string $ad_group_name
 * @property string $company_name
 * @property string $campaign_name
 * @property string $account_name
 * @property string $error_message
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class DeadLink extends \common\models\BehaviorModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dead_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['href'], 'required'],
            [['source_id', 'ad_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'text', 'href', 'ad_group_name', 'company_name', 'campaign_name', 'account_name', 'error_message'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'href' => 'Ссылки',
            'source_id' => 'Source ID',
            'ad_id' => 'ID объявления',
            'ad_group_name' => 'Имя группы',
            'company_name' => 'Company Name',
            'campaign_name' => 'Название компании',
            'account_name' => 'Имя аккаунта',
            'error_message' => 'Error Message',
            'created_at' => 'Дата создания',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
