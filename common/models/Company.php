<?php

namespace common\models;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $website
 * @property string $sales_report_url
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $token
 * @property integer $last_crm_stat_downloading
 *
 * @property Account[] $accounts
 * @property User $user
 * @property CrmOrder[] $crmOrders
 */
class Company extends \common\models\BehaviorModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_id'], 'required'],
            [['user_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'last_crm_stat_downloading'], 'integer'],
            [['name', 'website', 'sales_report_url', 'token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'website' => 'Веб-сайт',
            'sales_report_url' => 'Sales Report Url',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'token' => 'Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmOrders()
    {
        return $this->hasMany(CrmOrder::className(), ['company_id' => 'id']);
    }
}
