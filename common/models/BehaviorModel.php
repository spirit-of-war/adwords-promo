<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 22.06.16
 * Time: 10:11
 */

namespace common\models;

use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use Yii;

/**
 * Class BehaviorModel
 * @package common\models
 * @property $redis
 */
class BehaviorModel extends ActiveRecord
{
    const STATE_REMOVED_IN_SOURCE = 0;
    const STATE_ACTIVE = 1;
    const STATE_UPDATING_FROM_SOURCE = 2;
    const STATE_RAW = 3;
    const STATE_REMOVED_BY_USER = 4;
    const STATE_SUSPENDED = 5;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $result = parent::save($runValidation, $attributeNames);
        if (count($this->errors) > 0)
            \Yii::error(var_export($this->errors, true), 'Model saving');
        return $result;
    }

    public static function findOne($condition)
    {
        if (is_object($condition) && get_class($condition) == self::className())
            return $condition;
        return parent::findOne($condition);
    }

    function __get($name)
    {
        if ($name == 'redis') {
            $redis = Yii::$app->redis;
            if (!$redis->getIsActive())
                $redis->open();
            return $redis;
        }

        return parent::__get($name);
    }
}