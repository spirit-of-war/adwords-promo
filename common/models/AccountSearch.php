<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Account;

/**
 * AccountSearch represents the model behind the search form about `common\models\Account`.
 */
class AccountSearch extends Account
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'counter', 'advert_type', 'stat_type', 'is_account_common', 'state'], 'integer'],
            [['advert_login', 'stat_login', 'advert_token', 'advert_token_expires', 'stat_token', 'stat_token_expires', 'refresh_token', 'client_customer_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Account::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'counter' => $this->counter,
            'advert_type' => $this->advert_type,
            'stat_type' => $this->stat_type,
            'is_account_common' => $this->is_account_common,
            'state' => $this->state,
        ]);

        $query->andFilterWhere(['like', 'advert_login', $this->advert_login])
            ->andFilterWhere(['like', 'stat_login', $this->stat_login])
            ->andFilterWhere(['like', 'advert_token', $this->advert_token])
            ->andFilterWhere(['like', 'advert_token_expires', $this->advert_token_expires])
            ->andFilterWhere(['like', 'stat_token', $this->stat_token])
            ->andFilterWhere(['like', 'stat_token_expires', $this->stat_token_expires])
            ->andFilterWhere(['like', 'refresh_token', $this->refresh_token])
            ->andFilterWhere(['like', 'client_customer_id', $this->client_customer_id]);

        return $dataProvider;
    }
}
