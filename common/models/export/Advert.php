<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 22.08.16
 * Time: 16:42
 */

namespace common\models\export;

use common\models\Account;
use Yii;
use common\models\structure\Bid;
use common\models\structure\Keyword;

abstract class Advert
{
    protected $service, $account, $minSystemBid, $utm;

    /***
     * Добавляет наши UTM метки в конец урлов для всех переданных кампаний $campaignsIds.
     * Если $campaignIds = false, то для всех кампаний.
     * @param mixed $campaignIds
     * @return mixed
     */
    public abstract function setUtmContent($campaignIds = false);

    function __construct(Account $account)
    {
        $this->account = $account;
    }

    /***
     * Устанавливает значение ставки $bid всем переданным ключевым словам $keywords
     * @param $keywords
     * @param $bid
     * @param $taskID
     */
    public function setBidsConstantValue($keywords, $bid, $taskID)
    {
        if ($keywords) {
            $bid *= 1000000;
            $bids = array_map(function ($keyword) use ($bid) {
                return [
                    'KeywordId' => $keyword->source_id,
                    'Bid' => $this->checkBidValue($bid),
                    'ContextBid' => $this->checkBidValue($bid),
                    'AdGroupId' => $keyword->adgroup_source_id
                ];
            }, $keywords);

            $result = $this->service->setBids($bids);
            $this->logBids($result, $bids, true, $taskID);
        }
    }

    /***
     * Увеличивает или уменьшает значение ставки на $percent процентов всем переданным ключевым словам $keywords,
     * с учетом минимальной и максимальной ставки
     * @param $keywords
     * @param int $percent
     * @param $taskID
     * @param $minBid
     * @param $maxBid
     */
    public function setBidsByPercent($keywords, $percent, $taskID, $minBid = false, $maxBid = false)
    {
        if ($keywords) {
            $this->account->import->updateBids($keywords);

            $bids = array_map(function ($keyword) use ($percent, $minBid, $maxBid) {
                $bidValue = $keyword->bid_search / 100 * $percent + $keyword->bid_search;
                $bidValue = $this->checkBidValue($bidValue, $minBid, $maxBid);
                return [
                    'KeywordId' => $keyword->source_id,
                    'Bid' => $bidValue,
                    'ContextBid' => $bidValue,
                    'AdGroupId' => $keyword->adgroup_source_id
                ];
            }, $keywords);

            $result = $this->service->setBids($bids);
            $this->logBids($result, $bids, true, $taskID);
        }
    }

    /***
     * Разбирает ответ от сервиса(в дочернем классе), записывает новые ставки в таблицу Bid, а так же обновляет ставки
     * у ключевых слов в таблице Keyword
     * @param $result
     * @param $bids
     * @param $setContextBid
     * @param $taskID
     */
    protected function logBids($result, $bids, $setContextBid, $taskID)
    {
        if ($bids) {
            $this->account->import->updateKeywordBids($bids, $setContextBid, Bid::SOURCE_TYPE_RULE, $taskID);

            $bids = array_chunk($bids, 5000);
            foreach ($bids as $chunk) {
                $sql = '';
                foreach ($chunk as $item)
                    $sql .= sprintf(
                        'update %s set %s bid_search = %s where source_id = %s and account_id = %s;',
                        Keyword::tableName(),
                        $setContextBid ? 'bid_context = ' . $item['ContextBid'] . ', ' : '',
                        $item['Bid'],
                        $item['KeywordId'],
                        $this->account->id
                    );
                Yii::$app->db->createCommand($sql)->execute();
            }
        }
    }

    /***
     * Проверяет значение ставки $bidValue на минимальное и максильное значение,
     * а так же на минимально возможное значение в системе (например, для яндекса 30 коп, для гугла 1 коп)
     * @param $bidValue
     * @param bool $minBid
     * @param bool $maxBid
     * @return int
     */
    protected function checkBidValue($bidValue, $minBid = false, $maxBid = false)
    {
        if ($minBid !== false && $minBid != null) {
            $minBid *= 1000000;
            $bidValue = $bidValue < $minBid ? $minBid : $bidValue;
        }
        if ($maxBid !== false && $maxBid != null) {
            $maxBid *= 1000000;
            $bidValue = $bidValue > $maxBid ? $maxBid : $bidValue;
        }
        $bidValue = $bidValue < $this->minSystemBid ? $this->minSystemBid : $bidValue;
        return $bidValue;
    }

    protected function parseUrl($url)
    {
        $initialParams = parse_url($url, PHP_URL_QUERY);
        $initialParams = str_replace('?', '&', $initialParams);

        $params = [];
        parse_str($initialParams, $params);
        $allowedParams = [];
        $utmKeys = array_keys($this->utm);
        foreach ($params as $key => $val)
            if (!in_array($key, $utmKeys))
                $allowedParams[$key] = $val;

        $allowedParamsTmp = [];
        foreach ($allowedParams as $key => $val)
            if ($val != '')
                $allowedParamsTmp[] = $key . '=' . $val;

        $utm = array_map(function ($key, $value) {
            return $key . '=' . $value;
        }, array_keys($this->utm), array_values($this->utm));

        $allowedParams = array_merge($allowedParamsTmp, $utm);
        $allowedParams = implode('&', $allowedParams);

        $initialParams = parse_url($url, PHP_URL_QUERY);
        $url = str_replace($initialParams, $allowedParams, $url);

        if (parse_url($url, PHP_URL_QUERY) == null && substr($url, -1) == '?')
            return substr($url, 0, -1);

        return $url;
    }
}