<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 22.08.16
 * Time: 16:40
 */

namespace common\models\export;

use common\models\service\yandex\Direct as Service;
use common\models\Account;
use common\models\strategy\Limit;
use common\models\strategy\LimitResult;
use common\models\structure\BidAuction;
use common\models\structure\Keyword;
use common\models\structure\Layer;
use common\models\structure\Sitelink;
use common\models\Task;
use common\models\structure\Ad;
use Yii;
use common\models\structure\Bid;

class Direct extends Advert
{
    protected $utm = [
        'pos'         => '{position}',
        'block'       => '{position_type}',
        'type'        => '{source_type}',
        'added'       => '{addphrases}',
        'utm_source'  => 'yandex',
        'utm_medium'  => 'cpc',
        'utm_term'    => '{keyword}',
        'campaign_id' => '{campaign_id}',
        'gb_id'       => '{gbid}',
        'phrase_id'   => '{phrase_id}',
        'ad_id'       => '{ad_id}',
        'banner_id'   => '{banner_id}',
    ];

    function __construct(Account $account)
    {
        $this->minSystemBid = 300000;
        $this->service      = new Service($account->advert_token);
        parent::__construct($account);
    }

    /***
     * Подтягивает ставки ключевых слов $keywords до значения ставки выбранной позиции $position,
     * прибавляя или отнимая от нее процент $percent. Уитывает при этом минимальное и максимальное значение ставки.
     *
     * @param      $position
     * @param      $keywords
     * @param      $percent
     * @param      $taskID
     * @param bool $minBid
     * @param bool $maxBid
     */
    public function setBidsByPosition($position, $keywords, $percent, $taskID, $minBid = false, $maxBid = false)
    {
        if ($keywords) {
            $this->account->import->updateBids($keywords);

            $keywordIds = array_map(function ($keyword) {
                return $keyword['id'];
            }, $keywords);
            $keywords = Keyword::findAll(['id' => $keywordIds]);
            $bids = array_map(function ($bid) use ($position, $percent, $minBid, $maxBid) {
                if ($position == BidAuction::POSITION_P11)
                    $bidValue = $bid->auction_p11_bid;
                elseif ($position == BidAuction::POSITION_P12)
                    $bidValue = $bid->auction_p12_bid;
                elseif ($position == BidAuction::POSITION_P13)
                    $bidValue = $bid->auction_p13_bid;
                elseif ($position == BidAuction::POSITION_P21)
                    $bidValue = $bid->auction_p21_bid;
                elseif ($position == BidAuction::POSITION_P24)
                    $bidValue = $bid->auction_p24_bid;
                $bidValue = $bidValue / 100 * $percent + $bidValue;
                $bidValue = $this->checkBidValue($bidValue, $minBid, $maxBid);

                return [
                    'KeywordId' => $bid->source_id,
                    'Bid'       => $bidValue,
                ];
            }, $keywords);

            $result = $this->service->setBids($bids);
            $this->logBids($result, $bids, false, $taskID);
        }
    }

    /***
     * @inheritdoc
     *
     * @param $result
     * @param $bids
     * @param $setContextBid
     * @param $taskID
     */
    protected function logBids($result, $bids, $setContextBid, $taskID)
    {
        if ($result !== false) {
            $bidStory = [];
            foreach ($result as $row)
                if (array_key_exists('KeywordId', $row))
                    foreach ($bids as $bid)
                        if ($bid['KeywordId'] == $row['KeywordId']) {
                            $bidStoryItem = [
                                'KeywordId' => $bid['KeywordId'],
                                'Bid'       => $bid['Bid'],
                            ];
                            if ($setContextBid)
                                $bidStoryItem['ContextBid'] = $bid['ContextBid'];
                            $bidStory[] = $bidStoryItem;
                        }

            parent::logBids($result, $bids, $setContextBid, $taskID);
        }
    }

    public function setBid($keyword, $bidSearch = false, $bidContext = false)
    {
        $result = $this->service->setBid($keyword->source_id, $bidSearch, $bidContext);
        if ($result === false || !array_key_exists('KeywordId', $result))
            return false;
        else {
            $bidSettings = [
                'keyword_id'  => $keyword->id,
                'state'       => Bid::STATE_ACTIVE,
                'source_type' => Bid::SOURCE_TYPE_USER,
                'account_id'  => $this->account->id
            ];
            if ($bidSearch !== false) {
                $bidSettings['bid_search'] = $bidSearch;
                $keyword->bid_search       = $bidSearch;
            }
            if ($bidContext !== false) {
                $bidSettings['bid_context'] = $bidContext;
                $keyword->bid_context       = $bidContext;
            }
            $bid = new Bid($bidSettings);
            $bid->save();
            $keyword->update();

            return true;
        }
    }

    public function setUtmContent($campaignIds = false)
    {
        // TODO: Implement setUtmContent() method.
    }

    public function setUtmByAds($ads)
    {
        $task = new Task([
            'status'       => Task::STATUS_RUNNING,
            'type'         => Task::TYPE_UPDATE_UTM,
            'object_id'    => $this->account->id,
            'completeness' => 1
        ]);
        $task->save();

        $siteLinkSetIds = array_map(function ($ad) {
            return $ad->sitelink_set_source_id;
        }, $ads);
        $siteLinkSetIds = array_unique($siteLinkSetIds);
        $siteLinkSetIds = array_chunk($siteLinkSetIds, 1000);

        $siteLinkSets = [];
        foreach ($siteLinkSetIds as $chunk)
            $siteLinkSets = array_merge($siteLinkSets, $this->service->getSitelinks($chunk));

        $increase = 99 / count($siteLinkSets);

        foreach ($siteLinkSets as $set) {
            for ($i = 0; $i < count($set['Sitelinks']); $i++)
                $set['Sitelinks'][$i]['Href'] = $this->parseUrl($set['Sitelinks'][$i]['Href']);

            $oldSetId = $set['Id'];
            unset($set['Id']);

            $newSet = $this->service->addSitelinks($set);
            if ($newSet !== false) {
                $newSetId      = $newSet[0]['Id'];
                $adsWithOldSet = Ad::findAll([
                    'account_id'             => $this->account->id,
                    'sitelink_set_source_id' => $oldSetId
                ]);
                $ads           = array_chunk($adsWithOldSet, 1000);
                foreach ($ads as $chunk) {
                    $rawAds = array_map(function ($ad) use ($newSetId) {
                        return [
                            'id'            => $ad->source_id,
                            'href'          => $this->parseUrl($ad->href),
                            'sitelinkSetId' => $newSetId[0]['Id']
                        ];
                    }, $chunk);
                    $this->service->updateAds($rawAds);
                }

                $task->increaseCompleteness($increase);
            }
        }
    }

    function dump($x, $die = true)
    {
        if (is_array($x))
            echo '<br>Quantity: ' . count($x);
        VarDumper::dump($x, 10, true);
        if ($die)
            die;
    }

    public function setUtmByAds_tmp($ads)
    {
        $task = new Task([
            'status'       => Task::STATUS_RUNNING,
            'type'         => Task::TYPE_UPDATE_UTM,
            'object_id'    => $this->account->id,
            'completeness' => 1
        ]);
        $task->save();

        $siteLinkSetIds = array_map(function ($ad) {
            return $ad->sitelink_set_source_id;
        }, $ads);
        $siteLinkSetIds = array_unique($siteLinkSetIds);
        $siteLinkSetIds = array_chunk($siteLinkSetIds, 1000);
        $this->dump($ads);
        $siteLinkSets = [];
        foreach ($siteLinkSetIds as $chunk) {

            VarDumper::dump($this->service->getSitelinks($chunk), 10, true);
//            $siteLinkSets = array_merge($siteLinkSets, $this->service->getSitelinks($chunk));

        }
        die;
        VarDumper::dump($siteLinkSets, 10, true);
        die;
        $increase = 99 / count($siteLinkSets);

        $updatedAdsSourceIds = [];

        foreach ($siteLinkSets as $set) {
            $updateInSource = false;
            for ($i = 0; $i < count($set['Sitelinks']); $i++) {
                $rawHref    = $set['Sitelinks'][$i]['Href'];
                $parsedHref = $this->parseUrl($rawHref);
                if ($rawHref != $parsedHref) {
                    $updateInSource               = true;
                    $set['Sitelinks'][$i]['Href'] = $this->parseUrl($set['Sitelinks'][$i]['Href']);
                }
            }

            if ($updateInSource) {
                $oldSetId = $set['Id'];
                unset($set['Id']);

                $newSet = $this->service->addSitelinks($set);
                if ($newSet !== false) {
                    $newSetId      = $newSet[0]['Id'];
                    $adsWithOldSet = Ad::findAll([
                        'account_id'             => $this->account->id,
                        'sitelink_set_source_id' => $oldSetId
                    ]);
                    $adsWithOldSet = array_chunk($adsWithOldSet, 1000);

                    foreach ($adsWithOldSet as $chunk) {
                        $rawAds             = array_map(function ($ad) use ($newSetId) {
                            return [
                                'id'            => $ad->source_id,
                                'href'          => $this->parseUrl($ad->href),
                                'sitelinkSetId' => $newSetId[0]['Id']
                            ];
                        }, $chunk);
                        $updatedAdsInSource = $this->service->updateAds($rawAds);
                        if (is_array($updatedAdsInSource)) {
                            $updatedAdsInSource  = array_map(function ($ad) {
                                return $ad['Id'];
                            }, $updatedAdsInSource);
                            $updatedAdsSourceIds = array_merge($updatedAdsSourceIds, $updatedAdsInSource);
                        }
                    }

                    $task->increaseCompleteness($increase);
                }
            }
        }

        //Now updating ads that have correct sitelinks
        $adsSourceIds = array_map(function ($ad) {
            return $ad->source_id;
        }, $ads);

        $notUpdatedAdsSourceIds = array_diff($adsSourceIds, $updatedAdsSourceIds);
        $notUpdatedAds          = Ad::findAll([
            'account_id' => $this->account->id,
            'source_id'  => $notUpdatedAdsSourceIds
        ]);

        $adsWithIncorrectHref = [];
        foreach ($notUpdatedAds as $ad) {
            $parsedHref = $this->parseUrl($ad->href);
            if ($ad->href != $parsedHref)
                $adsWithIncorrectHref[] = [
                    'id'   => $ad->source_id,
                    'href' => $parsedHref
                ];
        }

        $adsWithIncorrectHref = array_chunk($adsWithIncorrectHref, 1000);
        foreach ($adsWithIncorrectHref as $chunk) {
            $rawAds = array_map(function ($ad) {
                return [
                    'id'   => $ad->source_id,
                    'href' => $this->parseUrl($ad->href)
                ];
            }, $chunk);
            $this->service->updateAds($rawAds);
        }
    }

    /**
     * @param bool|integer|array $campaignID
     * @param bool|integer|array $adID
     */
    public function setUtmContent_tmp($campaignID = false, $adID = false)
    {
        $service  = $this->service;
        $parseUrl = function ($url) {
            $initialParams = parse_url($url, PHP_URL_QUERY);
            $initialParams = str_replace('?', '&', $initialParams);
            $params        = [];
            parse_str($initialParams, $params);
            $params = array_merge($params, [
                'pos'         => '{position}',
                'block'       => '{position_type}',
                'type'        => '{source_type}',
                'added'       => '{addphrases}',
                'utm_source'  => 'yandex',
                'utm_medium'  => 'cpc',
                'utm_term'    => '{keyword}',
                'campaign_id' => '{campaign_id}',
                'gb_id'       => '{gbid}',
                'phrase_id'   => '{phrase_id}',
                'ad_id'       => '{ad_id}',
                'banner_id'   => '{banner_id}',
            ]);
            $params = array_flip($params);
            $params = array_flip($params);
            $params = array_map(function ($key, $value) {
                return $key . '=' . $value;
            }, array_keys($params), $params);
            $params = implode('&', $params);

            return str_replace($initialParams, $params, $url);
        };

        $task = new Task([
            'status'    => Task::STATUS_RUNNING,
            'type'      => Task::TYPE_UPDATE_SITELINKS,
            'object_id' => $this->account->id
        ]);
        $task->save();
        $this->account->import->updateSitelinks([], $task, 10);

        $sql          = sprintf('select distinct set_source_id from %s where account_id = %s',
            Sitelink::tableName(),
            $this->account->id);
        $sitelinkSets = Yii::$app->db->createCommand($sql)->queryColumn();
        $increase     = 70 / count($sitelinkSets);
        $sitelinkSets = array_chunk($sitelinkSets, 500);

        array_map(function ($chunk) use ($parseUrl, $service, $task, $increase) {
            $chunk     = implode(',', $chunk);
            $sql       = sprintf('select set_source_id, title, href from %s where set_source_id in (%s) order by set_source_id',
                Sitelink::tableName(), $chunk);
            $sitelinks = Yii::$app->db->createCommand($sql)->query()->readAll();
            $sitelinks = array_map(function ($sitelink) use ($parseUrl) {
                return [
                    'set_source_id' => $sitelink['set_source_id'],
                    'title'         => $sitelink['title'],
                    'href'          => $parseUrl($sitelink['href'])
                ];
            }, $sitelinks);
            $sets      = [];
            foreach ($sitelinks as $sitelink)
                $sets[$sitelink['set_source_id']][] = [
                    'title' => $sitelink['title'],
                    'href'  => $sitelink['href']
                ];
            foreach ($sets as $setId => $sitelinks) {
                $newSetId = $this->service->addSitelinks($sitelinks);
                if ($newSetId !== false) {
                    $newSetId = $newSetId[0]['Id'];
                    Yii::$app->db->createCommand()->update(Ad::tableName(), [
                        'sitelink_set_source_id' => $newSetId
                    ], [
                        'sitelink_set_source_id' => $setId,
                        'account_id'             => $this->account->id
                    ])->execute();
                    $task->increaseCompleteness($increase);
                }
            }
        }, $sitelinkSets);

        $sql      = sprintf('select count(*) from %s where account_id = %s', Ad::tableName(), $this->account->id);
        $increase = 20 / (int)Yii::$app->db->createCommand($sql)->queryScalar();
        $offset   = 0;
        do {
            $ads = $this->account->getAds($campaignID, $adID)->offset($offset)->limit(1000)->all();
            Yii::$app->db->close();
            if (count($ads) > 0) {
                $ads = array_map(function ($ad) use ($parseUrl) {
                    return [
                        'id'            => $ad->source_id,
                        'href'          => $parseUrl($ad->href),
                        'sitelinkSetId' => $ad->sitelink_set_source_id
                    ];
                }, $ads);
                $this->service->updateAds($ads);
                $task->increaseCompleteness($increase);
                $offset += 1000;
            }
        } while (count($ads) > 0);
    }

    public function suspendKeywords($keywords, $ruleID)
    {
        $keywords = array_map(function ($keyword) {
            return $keyword->source_id;
        }, $keywords);
        $keywords = array_chunk($keywords, 1000);

        foreach ($keywords as $chunk) {
            $results = $this->service->suspendKeywords($chunk);
            $rows    = [];
            foreach ($results as $result)
                $rows[] = [
                    $ruleID,
                    Layer::LAYER_KEYWORDS,
                    $result['Id'],
                    Limit::ACTION_STOP
                ];
            Yii::$app->db->createCommand()->batchInsert(
                LimitResult::tableName(),
                ['rule_id', 'layer', 'source_id', 'action'],
                $rows);
        }
    }

    public function suspendAdgroups($adgroups, $ruleID)
    {
        $adgroups = array_map(function ($adgroup) {
            return $adgroup->id;
        }, $adgroups);
        $keywords = Keyword::findAll([
            'adgroup_id' => $adgroups
        ]);

        return $this->suspendKeywords($keywords, $ruleID);
    }

    public function suspendCampaigns($campaigns, $ruleID)
    {
        $campaigns = array_map(function ($campaign) {
            return $campaign->source_id;
        }, $campaigns);
        $campaigns = array_chunk($campaigns, 1000);

        foreach ($campaigns as $chunk) {
            $results = $this->service->suspendCampaigns($chunk);
            $rows    = [];
            $time    = time();
            foreach ($results as $result)
                $rows[] = [
                    $ruleID,
                    Layer::LAYER_CAMPAIGNS,
                    $result['Id'],
                    Limit::ACTION_STOP,
                    $time,
                    $time
                ];
            Yii::$app->db->createCommand()->batchInsert(
                LimitResult::tableName(),
                ['rule_id', 'layer', 'source_id', 'action', 'created_at', 'updated_at'],
                $rows)->execute();
        }
    }

    public function getPrepareUrls()
    {

        $accountID = $this->account->id;

        $links = (new \yii\db\Query())
            ->from('sitelink')
            ->where(['account_id' => $accountID])
            ->groupBy('href')
            ->all();

        $resultLinks = [];
        foreach ($links as $key => $row) {
            $prepareUrl        = preg_split("/[&|?]/", $row['href']);
            $prepareUrl        = array_map(function ($elem) {
                if (!preg_match('/{([a-zA-Z0-9_]+)}|utm_source|utm_medium/', $elem)) {
                    return $elem;
                }
            }, $prepareUrl);
            $prepareUrl        = array_diff($prepareUrl, ['']);
            $resultLinks[$key] = implode('&', $prepareUrl);
        }

        $resultLinks = array_unique($resultLinks);

        foreach ($resultLinks as $key => $resLink) {
            $elem                      = $links[$key];
            $elem['url_after_prepare'] = $resLink;
            $resultLinks[$key]         = $elem;
        }

        return $resultLinks;
    }
}