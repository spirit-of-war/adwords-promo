<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 25.11.16
 * Time: 16:30
 */

namespace common\models\export;

use common\models\structure\Campaign;
use common\models\Task;
use common\models\Account;
use common\models\service\google\V201609 as Service;
use common\models\structure\Bid;

class Adwords extends Advert
{
    function __construct(Account $account)
    {
        $this->minSystemBid = 10000;
        $this->service = new Service($account);
        parent::__construct($account);
    }

    public function setUtmContent($campaignIds = false)
    {
        $task = new Task([
            'status' => Task::STATUS_RUNNING,
            'type' => Task::TYPE_UPDATE_UTM,
            'object_id' => $this->account->id,
            'completeness' => 1
        ]);
        $task->save();

        if ($campaignIds !== false)
            $campaignIds = [
                'id' => $campaignIds,
                'account_id' => $this->account->id
            ];
        else
            $campaignIds = [
                'account_id' => $this->account->id
            ];

        $campaignIds = array_map(
            function ($campaign) {
                return $campaign->source_id;
            },
            Campaign::findAll($campaignIds)
        );

        $this->service->setUtmContent($campaignIds, $task, 99);
    }

    /***
     * @inheritdoc
     * @param $result
     * @param $bids
     * @param $setContextBid
     * @param $taskID
     */
    protected function logBids($result, $bids, $setContextBid, $taskID)
    {
        $bidStory = [];
        foreach ($result->value as $row)
            foreach ($bids as $bid)
                if ($bid['KeywordId'] == $row->criterion->id)
                    $bidStory[] = [
                        'KeywordId' => $bid['KeywordId'],
                        'Bid' => $bid['Bid'],
                    ];

        parent::logBids($bidStory, $bids, false, $taskID);
    }

    public function setBid($keyword, $bid)
    {
        $result = $this->service->setBids([
            [
                'KeywordId' => $keyword->source_id,
                'AdGroupId' => $keyword->adgroup_source_id,
                'Bid' => $bid
            ]
        ]);

        if ($result === false)
            return false;
        else {
            $bid = new Bid([
                'keyword_id' => $keyword->id,
                'state' => Bid::STATE_ACTIVE,
                'source_type' => Bid::SOURCE_TYPE_USER,
                'account_id' => $this->account->id,
                'bid_search' => $bid
            ]);
            $bid->save();

            $keyword->bid_search = $bid;
            $keyword->update();

            return true;
        }
    }
}