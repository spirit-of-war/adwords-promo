<?php

/**
 * Created by PhpStorm.
 * User: Manager
 * Date: 27.07.2016
 * Time: 13:32
 */

namespace common\models\import;

use common\models\Account;
use common\models\structure\Campaign;
use common\models\structure\NegativeKeyword;
use common\models\Statistics;
use common\models\structure\Layer;
use common\models\Task;
use Yii;
use yii\base\Exception;
use common\models\service\google\V201609;
use common\models\structure\BidAuction;
use common\models\structure\Bid;

class Adwords extends Advert
{
    function __construct(Account $account)
    {
        parent::__construct($account);
        $this->service = new V201609($account);
    }

    /**
     * Updates whole structure.
     */
    public function updateStructure()
    {
        $task = $this->downloadMethodInit(Task::TYPE_UPDATE_STRUCTURE);

        try {
            $this->updateCampaigns($task, 14);

            $this->updatingCamps = Campaign::findAll([
                'account_id' => $this->account->id,
                'state' => Campaign::STATE_ACTIVE
            ]);
            $this->updatingCampsQuantity = count($this->updatingCamps);
            $this->updatingCamps = array_chunk($this->updatingCamps, 10);

            $this->updateAdgroups($task, 25);
            $this->updateKeywords($task, 35);
            $this->updateAds($task, 25);
        } catch (Exception $e) {
            $task->increaseCompleteness(0, true, $e->getName() . ' ' . $e->getMessage());
        }
    }

    protected function updateCampaigns($task = null, $completenessRange = 0)
    {
        $this->fields = [
            'source_id' => 'CampaignId',
            'name' => 'CampaignName',
            'type' => 'AdvertisingChannelType',
            'source_state' => 'CampaignStatus',
            'status' => 'ServingStatus',
            'currency' => '',
            'status_payment' => '',
            'account_id' => $this->account->id
        ];

        parent::updateCampaigns($task, $completenessRange);
    }

    protected function updateAdgroups($task = null, $completenessRange = 0)
    {
        $this->fields = [
            'source_id' => 'AdGroupId',
            'name' => 'AdGroupName',
            'type' => 'BiddingStrategyType',
            'campaign_source_id' => 'CampaignId',
            'account_id' => $this->account->id
        ];

        parent::updateAdgroups($task, $completenessRange);
    }

    protected function updateKeywords($task = null, $completenessRange = 0)
    {
        $completenessRange -= 3;
        $dbUpdatingCompletenessRange = 3 / count($this->updatingCamps);

        foreach ($this->updatingCamps as $chunk) {
            $this->sourceData = [];
            foreach ($chunk as $camp) {
                $result = $this->service->getKeywords($camp->source_id);
                if ($result !== false) {
                    $this->updatedCampaignIds[] = $camp->source_id;
                    $this->sourceData = array_merge($this->sourceData, $result);
                }

                $task->increaseCompleteness($completenessRange / count($this->updatingCamps) / count($chunk));
            }
            $this->fields = [
                'source_id' => 'Id',
                'keyword' => 'Criteria',
                'adgroup_source_id' => 'AdGroupId',
                'account_id' => $this->account->id,
            ];
            parent::updateKeywords($task, $dbUpdatingCompletenessRange);
        }
    }

    protected function updateAds($task = null, $completenessRange = 0)
    {
        $this->fields = [
            'source_id' => 'Id',
            'title' => 'Headline',
            'type' => 'AdType',
            'adgroup_source_id' => 'AdGroupId',
            'account_id' => $this->account->id,
        ];

        parent::updateAds($task, $completenessRange);
    }

    protected function updateNegativeKeywords($task = null, $completenessRange = 0)
    {
        $this->fields = [
            'source_id' => 'Id',
            'keyword' => 'Criteria',
            'adgroup_source_id' => 'AdGroupId',
            'account_id'=>$this->account->id
        ];

        $tableName = NegativeKeyword::tableName();
        $query = sprintf('update %s k
                  inner join adgroup g on g.source_id = k.adgroup_source_id
                  set k.adgroup_id = g.id, k.campaign_id = g.campaign_id, k.state = :stateSet
                  where k.account_id = :accountId  and k.state = :stateFilter', $tableName);

        $completenessRange -= 3;

        foreach ($this->updatingCamps as $chunk) {
            $ids = array_map(function ($campaign) {
                return (int)$campaign->source_id;
            }, $chunk);
            $result = $this->getKeyWords($ids, true);

            if ($result !== false) {
                $this->updatedCampaignIds = array_merge($this->updatedCampaignIds, $ids);
                $this->sourceData = array_merge($this->sourceData, $result);
            }
            $task->increaseCompleteness($completenessRange * (count($chunk) / $this->updatingCampsQuantity));
        }

        $this->updateLayer($tableName, Layer::LAYER_NEGATIVE_KETWORDS, $query, $task, 3);
    }

    protected function updateStatisticsFromContext($task, $completenessRange, $startDate = 'yesterday',
                                                   $endDate = 'yesterday')
    {
        $this->service = new V201609($this->account);
        $percent = $completenessRange / 100;

        $campaigns = array_map(
            function ($campaign) {
                return $campaign->source_id;
            },
            Campaign::findAll([
                'account_id' => $this->account->id,
                'state' => Campaign::STATE_ACTIVE
            ])
        );
        $campaigns = array_chunk($campaigns, 20);

        Yii::$app->db->close();

        $reports = [];
        $increase = 90 * $percent / count($campaigns);
        foreach ($campaigns as $chunk) {
            $report = $this->service->getAdsStat($chunk, $startDate, $endDate);
            $report = explode(PHP_EOL, $report);
            if (count($report) > 0) {
                $start = strtotime($startDate);
                $end = strtotime($startDate);
                foreach ($report as $row) {
                    if ($row != '') {
                        $row = explode(',', $row);
                        if (array_key_exists($startDate . $row[2] . $row[3], $reports)) {
                            $reports[$startDate . $row[2] . $row[3]] [6] += $row[4] / 1000000;   //sum
                            $reports[$startDate . $row[2] . $row[3]] [7] += $row[5];             //shows
                            $reports[$startDate . $row[2] . $row[3]] [8] += $row[6];             //clicks
                            $reports[$startDate . $row[2] . $row[3]] [9] += $row[7];             //revenue
                            $reports[$startDate . $row[2] . $row[3]] [10] += $row[8];            //average position
                        } else {
                            $reports[$startDate . $row[2] . $row[3]] = [
                                $start,
                                $end,
                                $row[0],            //campaign source id
                                $row[1],            //adgroup source id
                                $row[2],            //ad source id
                                $row[3],            //keyword source id
                                $row[4] / 1000000,  //sum search
                                $row[5],            //shows search
                                $row[6],            //clicks search
                                $row[7] / 1000000,  //revenue
                                $row[8],            //average_position
                                Statistics::STATE_RAW,
                                $this->account->advert_type,
                                time(),
                                time(),
                                $this->account->id,
                            ];
                        }
                    }
                }
            }
            $task->increaseCompleteness($increase);
        }

        if (count($reports) > 0) {
//            set_time_limit(10);
            $reports = array_chunk($reports, $this->chunkSize);
            $increase = 6 * $percent / count($reports);
            foreach ($reports as $chunk) {
                Yii::$app->db->createCommand()->batchInsert(
                    Statistics::tableName(),
                    [
                        'start_date',
                        'end_date',
                        'campaign_source_id',
                        'adgroup_source_id',
                        'ad_source_id',
                        'keyword_source_id',
                        'sum_search',
                        'shows_search',
                        'clicks_search',
                        'revenue',
                        'average_pos',
                        'state',
                        'account_type',
                        'created_at',
                        'updated_at',
                        'account_id'
                    ], $chunk)->execute();
                $task->increaseCompleteness($increase);
            }
            Yii::$app->db->createCommand(
                'update statistics s
                    left join ad a on a.source_id = s.ad_source_id and a.account_id = :accountID
                    left join keyword k on k.source_id = s.keyword_source_id and k.account_id = :accountID
                    left join adgroup g on g.source_id = s.adgroup_source_id and g.account_id = :accountID
                    set s.campaign_id = a.campaign_id,
                        s.adgroup_id = g.id,
                        s.ad_id = a.id,
                        s.keyword_id = k.id,
                        s.state = :stateSet
                    where s.state = :stateFilter', [
                'accountID' => $this->account->id,
                'stateSet' => Statistics::STATE_ACTIVE,
                'stateFilter' => Statistics::STATE_RAW
            ])->execute();
            $task->increaseCompleteness(4 * $percent);
        }
    }

    protected function updateAuctionBids($bids)
    {
        $time = time();

        $bids = array_map(function ($bid) use ($time) {
            return [
                $bid['KeywordId'],
                $bid['firstPageCpc'],//p11_bid
                $bid['topOfPageCpc'],//p12_bid
                $bid['firstPositionCpc'],//p13_bid
                BidAuction::STATE_RAW,
                $this->account->id,
                $time,
                $time,
                $time,
                $time,
            ];
        }, $bids);

        $bids = array_chunk($bids, $this->chunkSize);
        array_map(function ($chunk) {
            Yii::$app->db->createCommand()->batchInsert(
                BidAuction::tableName(),
                [
                    'keyword_source_id',
                    'p11_bid',
                    'p12_bid',
                    'p13_bid',
                    'state',
                    'account_id',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by'
                ],
                $chunk
            )->execute();
        }, $bids);

        Yii::$app->db->createCommand(
            'update bid_auction b
              left join keyword k on b.keyword_source_id = k.source_id and b.account_id = k.account_id
              set b.keyword_id = k.id, b.state = :stateSet
              where b.state = :stateFilter', [
            'stateSet' => Bid::STATE_ACTIVE,
            'stateFilter' => Bid::STATE_RAW
        ])->execute();
    }

    public function getClientList()
    {
        // TODO: Implement getClientList() method.
    }
}