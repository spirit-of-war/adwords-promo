<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 25.07.16
 * Time: 16:05
 */

namespace common\models\import;

use common\models\structure\Ad;
use common\models\Account;
use common\models\structure\Bid;
use common\models\structure\BidAuction;
use common\models\structure\Campaign;
use common\models\Statistics;
use common\models\structure\Keyword;
use common\models\Task;
use Yii;
use yii\base\Exception;
use \common\models\service\yandex\Direct as DirectService;

class Direct extends Advert
{
    private $changedCamps = [];
    private $changedAdgroups = [];
    private $changedAds = [];

    function __construct(Account $account)
    {
        parent::__construct($account);
        $clientLogin = in_array($account->client_login, ['', null]) ? false : $account->client_login;
        $this->service = new DirectService($account->advert_token, $clientLogin);
    }

    public function checkChanges($timestamp = null)
    {
        if(!$timestamp) $timestamp = $this->account->last_structure_update;

        $campChanges = $this->service->checkCampaigns($timestamp);
        $lastStructureUpdate = $campChanges['Timestamp'];

        $campsWithChildChanges = [];
        if(isset($campChanges['Campaigns'])){
            foreach ($campChanges['Campaigns'] as $camp) {
                if (in_array('SELF', $camp['ChangesIn']))
                    $this->changedCamps[] = $camp['CampaignId'];
                if (in_array('CHILDREN', $camp['ChangesIn']))
                    $campsWithChildChanges[] = $camp['CampaignId'];
            }
        }


        $x = $this;
        $checkRegion = function ($region, $childChanges) use ($x) {
            if (array_key_exists($region, $childChanges)) {
                $region = $childChanges[$region];
                if (array_key_exists('AdGroupIds', $region))
                    $x->changedAdgroups = array_merge($this->changedAdgroups, $region['AdGroupIds']);
                if (array_key_exists('AdIds', $region))
                    $x->changedAds = array_merge($this->changedAdgroups, $region['AdIds']);
            }
        };

        $campsWithChildChanges = array_chunk($campsWithChildChanges, 3000);
        foreach ($campsWithChildChanges as $chunk) {
            $childChanges = $this->service->check($chunk, $timestamp);
            $checkRegion('Modified', $childChanges);
            $checkRegion('NotFound', $childChanges);
        }
    }

    public function getChangedAds()
    {
        return $this->changedAds;
    }

    public function getPrepareLinks()
    {
        $timestamp = ($this->account->dead_link_checked ? $this->account->dead_link_checked : false);
        $dateToCheck = gmdate("Y-m-d\TH:i:s\Z", $timestamp);
        $this->checkChanges($dateToCheck);
        $ads = $this->getChangedAds();
        $ads = Ad::findAll(['source_id' => $ads]);

        $rows = [];
        foreach($ads as $ad){
            $rows = array_merge($rows, $ad->sitelinks);
        }

        return $this->prepareLinks($rows);
    }

    private function prepareLinks($links)
    {
        $resultLinks = [];
        foreach ($links as $key => $row) {
            $prepareUrl        = preg_split("/[&|?]/", $row['href']);
            $prepareUrl        = array_map(function ($elem) {
                if (!preg_match('/{([a-zA-Z0-9_]+)}|utm_source|utm_medium/', $elem)) {
                    return $elem;
                }
            }, $prepareUrl);
            $prepareUrl        = array_diff($prepareUrl, ['']);
            $resultLinks[$key] = implode('&', $prepareUrl);
        }

        $resultLinks = array_unique($resultLinks);

        foreach ($resultLinks as $key => $resLink) {
            $elem                      = $links[$key]->attributes;
            $elem['url_after_prepare'] = $resLink;
            $resultLinks[$key]         = $elem;
        }

        return $resultLinks;
    }

    /**
     * Updates whole structure.
     */
    public function updateStructure()
    {
        $task = $this->downloadMethodInit(Task::TYPE_UPDATE_STRUCTURE);

        try {
            $this->account->last_structure_update = $this->service->checkDictionaries();
            $this->account->update(true, ['last_structure_update']);
            $task->increaseCompleteness(4);

            $this->updateCampaigns($task, 15);

            $this->updatingCamps = Campaign::findAll([
                'account_id' => $this->account->id,
                'state' => Campaign::STATE_ACTIVE
            ]);
            $this->updatingCampsQuantity = count($this->updatingCamps);
            $this->updatingCamps = array_chunk($this->updatingCamps, 10);

            $this->updateAdgroups($task, 25);
            $this->updateKeywords($task, 30);
            $this->updateAds($task, 25);
        } catch (Exception $e) {
            $task->increaseCompleteness(0, true, $e->getName() . ' ' . $e->getMessage());
        }
    }

    protected function updateCampaigns($task = null, $completenessRange = 0)
    {
        $this->fields = [
            'source_id' => 'Id',
            'name' => 'Name',
            'type' => 'Type',
            'source_state' => 'State',
            'status' => 'Status',
            'currency' => 'Currency',
            'status_payment' => 'StatusPayment',
            'account_id' => $this->account->id
        ];

        parent::updateCampaigns($task, $completenessRange);
    }

    protected function updateAdgroups($task = null, $completenessRange = 0)
    {
        $this->fields = [
            'source_id' => 'Id',
            'name' => 'Name',
            'type' => 'Type',
            'campaign_source_id' => 'CampaignId',
            'account_id' => $this->account->id
        ];

        parent::updateAdgroups($task, $completenessRange);
    }

    protected function updateKeywords($task = null, $completenessRange = 0)
    {
        $this->fields = [
            'source_id' => 'Id',
            'keyword' => 'Keyword',
            'negative' => 'Negative',
            'adgroup_source_id' => 'AdGroupId',
            'bid_search' => 'Bid',
            'bid_context' => 'ContextBid',
            'account_id' => $this->account->id
        ];

        $completenessRange -= 3;
        $this->sourceData = [];
        foreach ($this->updatingCamps as $chunk) {
            $ids = array_map(function ($campaign) {
                return (int)$campaign->source_id;
            }, $chunk);

            $result = array_map(function ($keyword) {
                $keywords = explode(' ', $keyword['Keyword']);
                $common = [];
                $negative = [];
                foreach ($keywords as $item)
                    if (substr($item, 0, 1) == '-')
                        $negative[] = $item;
                    else
                        $common[] = $item;
                $keyword['Keyword'] = implode(' ', $common);
                $keyword['Negative'] = implode(' ', $negative);
                return $keyword;
            }, $this->service->getKeywords($ids));

            if ($result !== false) {
                $this->updatedCampaignIds = array_merge($this->updatedCampaignIds, $ids);
                $this->sourceData = array_merge($this->sourceData, $result);
            }
            $task->increaseCompleteness($completenessRange * (count($chunk) / $this->updatingCampsQuantity));
        }

        parent::updateKeywords($task, 3);
    }

    protected function updateAds($task = null, $completenessRange = 0)
    {
        //если добавляешь поле в fields, его также надо вписать и в fillRow
        $this->fields = [
            'source_id' => 'Id',
            'title' => 'Title',
            'type' => 'Type',
            'adgroup_source_id' => 'AdGroupId',
            'account_id' => $this->account->id,
            'href' => 'Href',
            'sitelink_set_source_id' => 'SitelinkSetId'
        ];
        $this->fillRow = function ($source) {
            $href = '';
            $sitelinkSetId = '';
            if ($source['Type'] == 'TEXT_AD') {
                $title = $source['TextAd']['Title'];
                $href = $source['TextAd']['Href'];
                $sitelinkSetId = $source['TextAd']['SitelinkSetId'];
            }
            else if ($source['Type'] == 'MOBILE_APP_AD') {
                $title = $source['MobileAppAd']['Title'];
            }
            else if ($source['Type'] == 'DYNAMIC_TEXT_AD') {
                $title = $source['DynamicTextAd']['Title'];
            }
            else {
                $title = 'Графическое объявление';
            }
            $row = [
                $source['Id'],
                $title,
                $source['Type'],
                $source['AdGroupId'],
                $this->account->id,
                $href,
                $sitelinkSetId
            ];
            return $row;
        };

        parent::updateAds($task, $completenessRange);
    }

    public function updateSitelinks($fields = [], $task = null, $completenessRange = 0, $fillRow = null)
    {
        $this->fields = [
            'set_source_id' => 'SetId',
            'title' => 'Title',
            'href' => 'Href',
//            'description' => 'Description',
            'account_id' => $this->account->id
        ];

        $query = sprintf('select distinct sitelink_set_source_id from %s where account_id = %s',
            Ad::tableName(),
            $this->account->id);
        $adIds = Yii::$app->db->createCommand($query)->queryColumn();
        $adIds = array_map(function ($id) {
            return (int)$id;
        }, $adIds);
        $adIds = array_chunk($adIds, 10000);
        $service = $this->service;
        $rawSitelinks = array_map(function ($chunk) use ($service) {
            return $service->getSitelinks($chunk);
        }, $adIds);

        foreach ($rawSitelinks as $chunk)
            foreach ($chunk as $sitelinkSet)
                foreach ($sitelinkSet['Sitelinks'] as $sitelink)
                    $this->sourceData[] = array_merge($sitelink, [
                        'SetId' => $sitelinkSet['Id']
                    ]);

        $task->increaseCompleteness($completenessRange);

        parent::updateSitelinks($task, $completenessRange);
    }

    protected function updateStatisticsFromContext($task, $completenessRange, $startDate = 'yesterday',
                                                   $endDate = 'yesterday')
    {
        $percent = $completenessRange / 100;
        $task->increaseCompleteness(1 * $percent);
        $campaigns = Campaign::findAll([
            'account_id' => $this->account->id,
            'state' => Campaign::STATE_ACTIVE
        ]);
        Yii::$app->db->close();
        $reports = [];
        $increase = 90 * $percent / count($campaigns);
        foreach ($campaigns as $campaign) {
//            set_time_limit(10);
            $report = $this->service->getAdsStat($campaign->source_id, $startDate, $endDate, $campaign->currency);
            if ($report !== false) {
                $startDate = strtotime($report['StartDate']);
                $endDate = strtotime($report['EndDate']);
                $report = $report['Stat'];
                foreach ($report as $row) {
                    ///////CRUTCH
                    if (array_key_exists('PhraseID', $row))
                        ///////////END OF CRUTCH
                        if (array_key_exists($startDate . $row['BannerID'] . $row['PhraseID'], $reports)) {
                            $reports[$startDate . $row['BannerID'] . $row['PhraseID']][2] += $row['SumSearch'];
                            $reports[$startDate . $row['BannerID'] . $row['PhraseID']][3] += $row['SumContext'];
                            $reports[$startDate . $row['BannerID'] . $row['PhraseID']][4] += $row['ShowsSearch'];
                            $reports[$startDate . $row['BannerID'] . $row['PhraseID']][5] += $row['ShowsContext'];
                            $reports[$startDate . $row['BannerID'] . $row['PhraseID']][6] += $row['ClicksSearch'];
                            $reports[$startDate . $row['BannerID'] . $row['PhraseID']][7] += $row['ClicksContext'];
                            $reports[$startDate . $row['BannerID'] . $row['PhraseID']][10] += $row['Revenue'];
                        } else {
                            $reports[$startDate . $row['BannerID'] . $row['PhraseID']] = [
                                $startDate,
                                $endDate,
                                $row['SumSearch'],
                                $row['SumContext'],
                                $row['ShowsSearch'],
                                $row['ShowsContext'],
                                $row['ClicksSearch'],
                                $row['ClicksContext'],
                                $row['PhraseID'],
                                $row['BannerID'],
                                $row['Revenue'],
                                array_key_exists('ROI', $row) ? $row['ROI'] : null,
                                $campaign->id,
                                Statistics::STATE_RAW,
                                $this->account->advert_type,
                                time(),
                                time(),
                                $this->account->id
                            ];
                        }
                }

            }
            $task->increaseCompleteness($increase);
        }
        if (count($reports) > 0) {
//            set_time_limit(10);
            $reports = array_chunk($reports, $this->chunkSize);
            $increase = 6 * $percent / count($reports);
            foreach ($reports as $chunk) {
                Yii::$app->db->createCommand()->batchInsert(
                    Statistics::tableName(),
                    [
                        'start_date',
                        'end_date',
                        'sum_search',
                        'sum_context',
                        'shows_search',
                        'shows_context',
                        'clicks_search',
                        'clicks_context',
                        'keyword_source_id',
                        'ad_source_id',
                        'revenue',
                        'source_roi',
                        'campaign_id',
                        'state',
                        'account_type',
                        'created_at',
                        'updated_at',
                        'account_id'
                    ], $chunk)->execute();
                $task->increaseCompleteness($increase);
            }
            Yii::$app->db->createCommand(
                'update statistics s
                    left join ad a on a.source_id = s.ad_source_id and a.account_id = :accountID
                    left join keyword k on k.source_id = s.keyword_source_id and k.account_id = :accountID
                    left join adgroup g on a.adgroup_source_id = g.source_id and g.account_id = :accountID
                    set s.campaign_id = a.campaign_id,
                        s.campaign_source_id = g.campaign_source_id,
                        s.adgroup_id = g.id,
                        s.adgroup_source_id = g.source_id,
                        s.ad_id = a.id,
                        s.keyword_id = k.id,
                        s.state = :stateSet
                    where s.state = :stateFilter', [
                'accountID' => $this->account->id,
                'stateSet' => Statistics::STATE_ACTIVE,
                'stateFilter' => Statistics::STATE_RAW
            ])->execute();
            $task->increaseCompleteness(3 * $percent);
        }
    }

    protected function updateAuctionBids($bids)
    {
        $bidsRaw = [];
        foreach ($bids as $bid) {
            $bidRaw = [
                'KeywordId' => $bid['KeywordId']
            ];
            foreach ($bid['AuctionBids'] as $auctionBid) {
                $position = strtolower($auctionBid['Position']);
                $bidRaw[$position . '_price'] = $auctionBid['Price'];
                $bidRaw[$position . '_bid'] = $auctionBid['Bid'];
            }
            $bidsRaw[] = $bidRaw;
        }

        $bids = [];
        foreach ($bidsRaw as $bidRaw) {
            $bids[] = [
                $bidRaw['KeywordId'],
                $this->account->id,
                $bidRaw['p11_price'],
                $bidRaw['p11_bid'],
                $bidRaw['p12_price'],
                $bidRaw['p12_bid'],
                $bidRaw['p13_price'],
                $bidRaw['p13_bid'],
                $bidRaw['p21_price'],
                $bidRaw['p21_bid'],
                $bidRaw['p24_price'],
                $bidRaw['p24_bid'],
            ];
        }

        $bids = array_chunk($bids, $this->chunkSize);
        array_map(function ($chunk) {
            $this->batchUpdate(
                Keyword::tableName(),
                [
                    'source_id',
                    'account_id',
                    'auction_p11_price',
                    'auction_p11_bid',
                    'auction_p12_price',
                    'auction_p12_bid',
                    'auction_p13_price',
                    'auction_p13_bid',
                    'auction_p21_price',
                    'auction_p21_bid',
                    'auction_p24_price',
                    'auction_p24_bid',
                ], $chunk);
        }, $bids);
    }

    public function getClientList()
    {
        $clients = $this->service->getClientsList();

        if ($clients === false)
            return false;

        return array_map(
            function ($client) {
                return $client['Login'];
            },
            $clients
        );
    }
}