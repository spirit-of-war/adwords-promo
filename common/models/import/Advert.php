<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 25.07.16
 * Time: 14:51
 */

namespace common\models\import;

use common\models\strategy\CrmOrder;
use common\models\service\yandex\Metrika;
use common\models\structure\Sitelink;
use common\models\structure\Layer;
use Yii;
use common\models\structure\Campaign;
use common\models\Account;
use common\models\structure\Adgroup;
use common\models\structure\Ad;
use common\models\structure\Keyword;
use common\models\Task;
use common\models\Statistics;
use yii\base\Exception;
use common\models\structure\Bid;

/**
 * Class Advert
 * @package common\models\import
 * @property Account $account
 */
abstract class Advert
{
    public $service;
    protected $account;
    protected $chunkSize = 1000;
    protected $updatedCampaignIds = [];

    protected $fields, $fillRow, $sourceData, $updatingCamps, $updatingCampsQuantity;

    abstract public function getClientList();
    abstract public function updateStructure();
    abstract protected function updateAuctionBids($bids);
    abstract protected function updateStatisticsFromContext($task, $completenessRange, $startDate = 'yesterday',
                                                   $endDate = 'yesterday');

    /**
     * Direct constructor.
     * @param Account $account
     */
    public function __construct($account)
    {
        $this->account = $account;
    }

    /**
     * This method should be used in the start of updating methods of console application.
     * @param integer $type
     * @return Task
     */
    protected function downloadMethodInit($type)
    {
        $task = new Task([
            'status' => Task::STATUS_RUNNING,
            'object_id' => $this->account->id,
            'type' => $type,
            'completeness' => 1
        ]);
        $task->save();

        register_shutdown_function(function () use ($task)
        {
            $lastError = error_get_last();
            if ($lastError != null && $lastError['type'] != E_NOTICE) {
                $task->increaseCompleteness(0, true, var_export($lastError, true));
                exit(1);
            } else {
                exit(0);
            }
        });

        return $task;
    }

    /**
     * @param string $tableName
     * @param array $fields
     * @param array $values
     * @return integer number of rows affected by the execution.
     */
    public function batchUpdate($tableName, $fields, $values, $updateStatement = '')
    {
        if ($updateStatement == '') {
            foreach ($fields as $field)
                $updateStatement .= $field . ' = values(' . $field . '),';
            $updateStatement = rtrim($updateStatement, ',');
        }

        $values = array_map(function ($row) {
            $valueRow = '';
            foreach ($row as $field)
                if (strlen($field) > 4 && substr($field, 0, 4) == 'sql:')
                    $valueRow .= substr($field, 4) . ',';
                else
                    $valueRow .= '\'' . str_replace('\'', '', $field) . '\',';
            $valueRow = substr($valueRow, 0, strlen($valueRow) - 1);
            return '(' . $valueRow . ')';
        }, $values);
        $values = implode(',', $values);
        $query = sprintf(
            'INSERT INTO %s (%s) VALUES %s on duplicate key update %s',
            $tableName,
            implode(',', $fields),
            $values,
            $updateStatement
        );
        Yii::$app->db->close();
        Yii::$app->db->open();
        return Yii::$app->db->createCommand($query)->execute();
    }

    protected function updateStatisticsInnerIDs()
    {
        Yii::$app->db->createCommand(
            'update statistics s
                    left join ad a on a.source_id = s.ad_source_id and a.account_id = :accountID
                    left join keyword k on k.source_id = s.keyword_source_id and k.account_id = :accountID
                    left join adgroup g on a.adgroup_source_id = g.source_id and g.account_id = :accountID
                    set s.campaign_id = a.campaign_id,
                        s.adgroup_id = g.id,
                        s.ad_id = a.id,
                        s.keyword_id = k.id,
                        s.state = :stateSet
                    where s.state = :stateFilter', [
            'accountID' => $this->account->id,
            'stateSet' => Statistics::STATE_ACTIVE,
            'stateFilter' => Statistics::STATE_RAW
        ])->execute();
    }

    protected function updateCampaigns($task = null, $completenessRange = 0)
    {
        $tableName = Campaign::tableName();
        $query = sprintf('update %s set state = :stateSet where account_id = :accountId and state = :stateFilter',
            $tableName);

        $this->sourceData = $this->service->getCampaigns();
        $task->increaseCompleteness($completenessRange / 100 * 70);

        $this->updateLayer($tableName, Layer::LAYER_CAMPAIGNS, $query, $task, $completenessRange / 100 * 30);
    }

    protected function updateAdgroups($task = null, $completenessRange = 0)
    {
        $tableName = Adgroup::tableName();
        $query = sprintf('update %s g
                  inner join campaign c on c.source_id = g.campaign_source_id
                  set g.campaign_id = c.id, g.state = :stateSet
                  where g.account_id = :accountId and g.state = :stateFilter', $tableName);

        $completenessRange -= 3;
        foreach ($this->updatingCamps as $chunk) {
            $ids = array_map(function ($campaign) {
                return (int)$campaign->source_id;
            }, $chunk);
            $result = $this->service->getAdgroups($ids);

            if ($result !== false) {
                $this->updatedCampaignIds = array_merge($this->updatedCampaignIds, $ids);
                $this->sourceData = array_merge($this->sourceData, $result);
            }
            $task->increaseCompleteness($completenessRange * (count($chunk) / $this->updatingCampsQuantity));
        }

        $this->updateLayer($tableName, Layer::LAYER_ADGROUPS, $query, $task, 3);
    }

    protected function updateKeywords($task = null, $completenessRange = 0)
    {
        $tableName = Keyword::tableName();
        $query = sprintf('update %s k
                  inner join adgroup g on g.source_id = k.adgroup_source_id
                  set k.adgroup_id = g.id, k.campaign_id = g.campaign_id, k.state = :stateSet
                  where k.account_id = :accountId and k.state = :stateFilter', $tableName);

        $this->updateLayer($tableName, Layer::LAYER_KEYWORDS, $query, $task, $completenessRange);
    }

    protected function updateAds($task = null, $completenessRange = 0)
    {
        $tableName = Ad::tableName();
        $query = sprintf('update %s a 
                  inner join adgroup g on g.source_id = a.adgroup_source_id
                  set a.campaign_id = g.campaign_id, a.adgroup_id = g.id, a.state = :stateSet
                  where a.account_id = :accountId and a.state = :stateFilter', $tableName);

        $completenessRange -= 3;

        foreach ($this->updatingCamps as $chunk) {
            $ids = array_map(function ($campaign) {
                return (int)$campaign->source_id;
            }, $chunk);
            $result = $this->service->getAds($ids);

            if ($result !== false) {
                $this->updatedCampaignIds = array_merge($this->updatedCampaignIds, $ids);
                $this->sourceData = array_merge($this->sourceData, $result);
            }
            $task->increaseCompleteness($completenessRange * (count($chunk) / $this->updatingCampsQuantity));
        }

        $this->updateLayer($tableName, Layer::LAYER_ADS, $query, $task, 3);
    }

    public function updateSitelinks($task = null, $completenessRange = 0, $fillRow = null)
    {
        $tableName = Sitelink::tableName();
        $query = sprintf('update %s k set k.state = :stateSet
                          where k.account_id = :accountId and k.state = :stateFilter', $tableName);
        $updateSources = $this->downloadSitelinks(Layer::LAYER_SITELINK, $task, $completenessRange - 3);
        Sitelink::deleteAll([
            'account_id' => $this->account->id
        ]);
        $this->updateLayer($tableName, Layer::LAYER_SITELINK, $query, $updateSources,
                $task, 3, $fillRow);
    }

    protected function updateLayer($tableName, $layer, $updateSql, $task = null, $completenessRange = null)
    {
        if (is_null($this->fillRow))
            $fillRow = function ($source, $sourceKeys) {
                $row = [];
                foreach ($sourceKeys as $key){
                    $row[] = is_string($key) && isset($source[$key]) ? $source[$key] : $key;
                }

                return $row;
            };
        else
            $fillRow = $this->fillRow;

        $sourceKeys = array_values($this->fields);
        $this->fields = array_keys($this->fields);
        $this->fields = array_merge($this->fields, [
            'state',
            'account_type',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at'
        ]);

        if ($this->sourceData) {
            $updateSources = array_chunk($this->sourceData, $this->chunkSize);
            foreach ($updateSources as $chunk) {
                $values = [];
                foreach ($chunk as $source) {
                    $row = $fillRow($source, $sourceKeys);
                    $row = array_merge($row, [
                        Adgroup::STATE_RAW,
                        $this->account->advert_type,
                        Yii::$app->className() == 'yii\console\Application' ? 0 : Yii::$app->user->id,
                        Yii::$app->className() == 'yii\console\Application' ? 0 : Yii::$app->user->id,
                        time(),
                        time(),
                    ]);
                    $values[] = $row;
                }
                $this->batchUpdate($tableName, $this->fields, $values);
            }
        }

        $filter = [
            'state' => Adgroup::STATE_ACTIVE,
            'account_id' => $this->account->id
        ];
        if ($layer != Layer::LAYER_CAMPAIGNS)
            $filter['campaign_id'] = $this->updatedCampaignIds;
        $query = Yii::$app->db->createCommand()->update(
                $tableName,
                ['state' => Adgroup::STATE_REMOVED_IN_SOURCE],
                $filter
            )->rawSql . ';';
        $query .= Yii::$app->db->createCommand(
                $updateSql,
                [
                    'accountId' => $this->account->id,
                    'stateSet' => Layer::STATE_ACTIVE,
                    'stateFilter' => Layer::STATE_RAW
                ])->rawSql . ';';
        Yii::$app->db->createCommand($query)->execute();
        if ($task)
            $task->increaseCompleteness($completenessRange);

        $this->sourceData = [];
    }

    public function updateStatistics($startDate = 'yesterday', $endDate = 'yesterday')
    {
        $task = $this->downloadMethodInit(Task::TYPE_UPDATE_STATISTICS);

        try {
            $updateFromCrm = trim($this->account->company->sales_report_url) ? true : false;
            $this->updateStatisticsFromContext($task, $updateFromCrm ? 69 : 89, $startDate, $endDate);
            if ($this->account->stat_type == Account::TYPE_YANDEX)
                $this->updateStatisticsFromMetrika($task, $updateFromCrm ? 10 : 10, $startDate, $endDate);
            else
                $this->updateStatisticsFromAnaytics($task, $updateFromCrm ? 10 : 10, $startDate, $endDate);
            if ($updateFromCrm)
                $this->updateStatisticsFromCrm($task, 20, $startDate);
        } catch (Exception $e) {
            $task->increaseCompleteness(0, true, $e->getName() . ' ' . $e->getMessage());
        }

    }

    private function updateStatisticsFromMetrika($task, $completenessRange, $date = 'yesterday')
    {
        $this->updateMetrikaColumn($task, $completenessRange / 3, $date, Metrika::ATTRIBUTION_LASTSIGN);
        $this->updateMetrikaColumn($task, $completenessRange / 3, $date, Metrika::ATTRIBUTION_LAST);
        $this->updateMetrikaColumn($task, $completenessRange / 3, $date, Metrika::ATTRIBUTION_FIRST);
    }

    private function updateMetrikaColumn($task, $completenessRange, $date = 'yesterday',
                                         $attribution = Metrika::ATTRIBUTION_LASTSIGN)
    {
        $service = new Metrika($this->account->stat_token, $this->account->counter);
        $goals = array_map(
            function ($goal) {
                return (int)$goal;
            },
            Yii::$app->db
                ->createCommand('select number from goal where account_id = ' . $this->account->id)
                ->queryColumn()
        );

        if ($goals) {
            $increase = $completenessRange / count($goals);
            foreach ($goals as $goal) {
                $reportRaw = [];

                $offset = 1;
                $limit = 1000;
                do {
                    $report = $service->getReport($goal, $date, $date, $offset, $limit, $attribution);
                    $report = $report['data'];

                    if (count($report) == 0)
                        break;

                    foreach ($report as $row) {
                        $adgroupSourceID = $row['dimensions'][0]['id'];
                        $campaignID = explode('-', $row['dimensions'][0]['cid'])[1];
                        $adID = explode('.', $row['dimensions'][2]['id'])[1];
                        $reportRaw[] = [
                            strtotime(date('d.m.Y', strtotime($date))),
                            strtotime(date('d.m.Y', strtotime($date))),
                            $this->account->id,
                            $campaignID,
                            $adgroupSourceID,
                            sprintf(
                                'sql:(select source_id from %s where keyword = \'%s\' and adgroup_source_id = %s and account_id = %s)',
                                Keyword::tableName(),
                                $row['dimensions'][1]['name'],
                                $adgroupSourceID,
                                $this->account->id),
                            $adID,
                            Statistics::STATE_RAW,
                            $row['metrics'][0],
                            time(),
                            time()
                        ];
                    }

                    $offset += $limit;
                    $limit += $limit;
                } while (true);

                $statFieldName = 'metrika_conversions_lastsign';
                if ($attribution == Metrika::ATTRIBUTION_LAST)
                    $statFieldName = 'metrika_conversions_last';
                if ($attribution == Metrika::ATTRIBUTION_FIRST)
                    $statFieldName = 'metrika_conversions_first';

                $fields = [
                    'start_date',
                    'end_date',
                    'account_id',
                    'campaign_source_id',
                    'adgroup_source_id',
                    'keyword_source_id',
                    'ad_source_id',
                    'state',
                    $statFieldName,
                    'created_at',
                    'updated_at',
                ];

                $updateStatement = '';
                foreach ($fields as $field)
                    if ($field == $statFieldName)
                        $updateStatement .= $field . '=' . $field . '+values(' . $field . '),';
                    else
                        $updateStatement .= $field . ' = values(' . $field . '),';
                $updateStatement = rtrim($updateStatement, ',');

                if (count($reportRaw) > 0) {
                    $reports = array_chunk($reportRaw, $this->chunkSize);
                    foreach ($reports as $chunk) {

                        $this->batchUpdate(Statistics::tableName(), $fields, $chunk, $updateStatement);
                    }
                    $this->updateStatisticsInnerIDs();
                }

                $task->increaseCompleteness($increase);
            }
        } else {
            $task->increaseCompleteness($completenessRange);
        }
    }

    private function updateStatisticsFromAnaytics($task, $completenessRange, $date = 'yesterday')
    {
        $task->increaseCompleteness($completenessRange);
    }

    /**
     * Saves downloading statistics from client CRM system to DB.
     *
     * @param Task $task
     * @param string $date
     */
    protected function updateStatisticsFromCrm($task, $completenessRange, $date = 'yesterday')
    {
        $percent = $completenessRange / 100;

        $this->loadDataFromCrm($task, 70 * $percent, $date);
        $this->updateDirectConversionsFromCrm($date);
        $task->increaseCompleteness(15 * $percent);
        $this->updateAssociativeConversions($date);
        $task->increaseCompleteness(15 * $percent);
    }

    /**
     * @param Task $task
     * @param float $completenessRange
     * @param string $date
     */
    private function loadDataFromCrm($task, $completenessRange, $date = 'yesterday')
    {
        $completenessRange = $completenessRange / 7;
        Yii::$app->db->createCommand('DELETE FROM crm_order WHERE date < ' . strtotime('30 days ago'))->execute();
        $date = strtotime($date . ' 00:00:00') + 86400;
        $date = date('o-m-d', $date);
        for ($k = 0; $k < 7; $k++) {
            $date = strtotime($date) - 86400;
            $date = date('o-m-d', $date);
            $url = sprintf(
                '%s&start=%s&end=%s&source=%s',
                $this->account->company->sales_report_url,
                $date,
                $date,
                $this->account->advert_type == Account::TYPE_YANDEX ? 'yandex' : 'google');
            $report = file_get_contents($url);
            $report = str_replace(';', '', $report);
            $report = str_getcsv($report, "\n");
            $headers = str_getcsv($report[0], ',');
            $orders = [];
            for ($i = 1; $i < count($report); $i++) {
                $reportRow = str_getcsv($report[$i], ',');
                $order = [];
                for ($j = 0; $j < count($headers); $j++)
                    $order[$headers[$j]] = $reportRow[$j];
                $orders[] = [
                    $order['visit_id'],
                    $order['campaign_id'],
                    $order['gb_id'],
                    $order['phrase_id'],
                    $order['ad_id'],
                    $order['order_id'],
                    $order['profit'],
                    strtotime($order['date']),
                    $this->account->id
                ];
            }
            $orders = array_chunk($orders, $this->chunkSize);
            foreach ($orders as $chunk)
                $this->batchUpdate(
                    CrmOrder::tableName(),
                    [
                        'visit_id',
                        'campaign_id',
                        'adgroup_id',
                        'keyword_id',
                        'ad_id',
                        'order_id',
                        'revenue',
                        'date',
                        'account_id'
                    ],
                    $chunk
                );
            $task->increaseCompleteness($completenessRange);
        }
    }

    private function updateDirectConversionsFromCrm($date = 'yesterday')
    {
        $accountID = $this->account->id;
        $sql = sprintf(
            '
                SELECT
                    campaign_id,
                    adgroup_id,
                    keyword_id,
                    ad_id,
                    UNIX_TIMESTAMP(from_unixtime(date, %s)) order_date,
                    count(*) conversions,
                    sum(revenue) revenue
                FROM crm_order WHERE order_id <> 0 AND date >= %s AND date <= %s AND account_id = %s 
                group by campaign_id,
                    adgroup_id,
                    keyword_id,
                    ad_id,
                    order_date
            ',
            '\'%Y-%m-%d\'',
            strtotime($date . ' 00:00:00') - 7 * 86400,
            strtotime($date . ' 00:00:00'),
            $accountID
        );

        $directConversions = array_map(function ($dc) use ($accountID) {
            return [
                $dc['order_date'],
                $dc['order_date'],
                $accountID,
                $dc['campaign_id'],
                $dc['adgroup_id'],
                $dc['keyword_id'],
                $dc['ad_id'],
                $dc['revenue'],
                Statistics::STATE_RAW,
                $dc['conversions'],
                time(),
                time()
            ];
        }, Yii::$app->db->createCommand($sql)->queryAll());

        if (count($directConversions) > 0) {
            $directConversions = array_chunk($directConversions, $this->chunkSize);
            foreach ($directConversions as $chunk) {
                $this->batchUpdate(Statistics::tableName(), [
                    'start_date',
                    'end_date',
                    'account_id',
                    'campaign_source_id',
                    'adgroup_source_id',
                    'keyword_source_id',
                    'ad_source_id',
                    'crm_revenue',
                    'state',
                    'crm_conversions',
                    'created_at',
                    'updated_at',
                ], $chunk);
                Yii::$app->db->close();
            }
            $this->updateStatisticsInnerIDs();
        }
    }

    private function updateAssociativeConversions($date)
    {
        $accountID = $this->account->id;
        $sql = sprintf('SELECT visit_id,
		                            sum(order_id = 0) no_orders,
		                            sum(order_id <> 0) orders
                              FROM crm_order
                              WHERE date >= %s AND date <= %s AND account_id = %s
                              GROUP BY visit_id
                              HAVING no_orders > 0 AND orders > 0',
                        strtotime($date) - 30 * 86400,
                        strtotime($date),
                        $accountID);
        $visits = array_map(function ($row) {
            return '\'' . $row['visit_id'] . '\'';
        }, Yii::$app->db->createCommand($sql)->queryAll());
        $sql = sprintf(
            'SELECT * FROM crm_order 
              WHERE visit_id IN (%s) and date > %s and account_id = %s order by visit_id, date desc',
            implode(',', $visits),
            strtotime('30 days ago'),
            $accountID);
        $report = Yii::$app->db
            ->createCommand($sql)
            ->queryAll();
        $visits = [];
        foreach ($report as $visit)
            $visits[$visit['visit_id']][] = $visit;

        $assocConversions = [];
        foreach ($visits as $visit) {
            $visitAssocConversions = [];
            $orderID = 0;
            foreach ($visit as $conversion) {
                if ($conversion['order_id'] != $orderID && $conversion['order_id'] == 0)
                    $visitAssocConversions[] = [
                        'campaign_id' => $conversion['campaign_id'],
                        'adgroup_id' => $conversion['adgroup_id'],
                        'keyword_id' => $conversion['keyword_id'],
                        'ad_id' => $conversion['ad_id'],
                        'date' => $conversion['date'],
                    ];
                elseif ($conversion['order_id'] != $orderID)
                    $orderID = $conversion['order_id'];
            }

            $uniqueVisitConversions = [];
            foreach ($visitAssocConversions as $conversion)
                $uniqueVisitConversions[
                    $conversion['campaign_id'] .
                    $conversion['adgroup_id'] .
                    $conversion['keyword_id'] .
                    $conversion['ad_id'] .
                    $conversion['date']
                ] = $conversion;

            $visitAssocConversions = [];
            foreach ($uniqueVisitConversions as $conversion)
                $visitAssocConversions[] = $conversion;

            $assocConversions = array_merge($assocConversions, $uniqueVisitConversions);
        }

        $countedAssocConversions = [];
        foreach ($assocConversions as $conversion)
            if (array_key_exists(
                $conversion['campaign_id'] .
                $conversion['adgroup_id'] .
                $conversion['keyword_id'] .
                $conversion['ad_id'] .
                $conversion['date']
                , $countedAssocConversions
            )) {
                $conversion['conversions'] += 1;
            }
            else {
                $conversion['conversions'] = 1;
                $countedAssocConversions[
                    $conversion['campaign_id'] .
                    $conversion['adgroup_id'] .
                    $conversion['keyword_id'] .
                    $conversion['ad_id'] .
                    $conversion['date']
                ] = $conversion;
            }

        if (count($countedAssocConversions) > 0) {
            $assocConversions = array_map(function ($conversion) use ($accountID) {
                $date = strtotime(date('d.m.o', $conversion['date']));
                return [
                    $date,
                    $date,
                    $accountID,
                    $conversion['campaign_id'],
                    $conversion['adgroup_id'],
                    $conversion['keyword_id'],
                    $conversion['ad_id'],
                    $conversion['conversions'],
                    Statistics::STATE_RAW,
                    time(),
                    time()
                ];
            }, $countedAssocConversions);
            $assocConversions = array_chunk($assocConversions, $this->chunkSize);
            foreach ($assocConversions as $chunk) {
                $this->batchUpdate(Statistics::tableName(), [
                    'start_date',
                    'end_date',
                    'account_id',
                    'campaign_source_id',
                    'adgroup_source_id',
                    'keyword_source_id',
                    'ad_source_id',
                    'crm_assoc_conversions',
                    'state',
                    'created_at',
                    'updated_at',
                ], $chunk);
            }
            $this->updateStatisticsInnerIDs();
        }
    }

    /***
     * Записывает ставки в таблицу Bid
     * @param $bids
     * @param bool $setContextBid
     * @param int $sourceType
     * @param bool $taskID
     */
    public function updateKeywordBids($bids, $setContextBid = false, $sourceType = Bid::SOURCE_TYPE_ADVERT,
                                      $taskID = false)
    {
        $bids = array_map(function ($bid) use ($sourceType, $setContextBid, $taskID) {
            $bidRaw = [
                $bid['KeywordId'],
                $bid['Bid'],
                Bid::SOURCE_TYPE_ADVERT,
                Bid::STATE_RAW,
                $this->account->id,
                time(),
                time(),
                0,
                0
            ];
            if ($setContextBid)
                $bidRaw[] = $bid['ContextBid'];
            if ($taskID !== false)
                $bidRaw[] = $taskID;
            return $bidRaw;
        }, $bids);

        $bids = array_chunk($bids, $this->chunkSize);
        Yii::$app->db->close();
        $bidFields = [
            'keyword_source_id',
            'bid_search',
            'source_type',
            'state',
            'account_id',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by'
        ];
        $keywordFields = [
            'source_id',
            'bid_search',
            'account_id',
            'updated_at'
        ];
        if ($setContextBid) {
            $bidFields[] = 'bid_context';
            $keywordFields[] = 'bid_context';
        }
        if ($taskID !== false)
            $bidFields[] = 'task_id';
        foreach ($bids as $chunk) {
            Yii::$app->db->createCommand()->batchInsert(
                Bid::tableName(),
                $bidFields,
                $chunk
            )->execute();
            $keywordChunk = [];
            foreach ($chunk as $row) {
                $tmp = [
                    $row[0],
                    $row[1],
                    $row[4],
                    time(),
                ];
                if ($setContextBid)
                    $tmp[] = $row[9];
                $keywordChunk[] = $tmp;
            }
            $this->batchUpdate(Keyword::tableName(), $keywordFields, $keywordChunk);
        }

        Yii::$app->db->createCommand(
            'update bid b
              left join keyword k on b.keyword_source_id = k.source_id and b.account_id = k.account_id
              set b.keyword_id = k.id, b.state = :stateSet
              where b.state = :stateFilter', [
            'stateSet' => Bid::STATE_ACTIVE,
            'stateFilter' => Bid::STATE_RAW
        ])->execute();
    }

    /***
     * Обновляет ставки и ставки аукциона переданных ключевых слов $keywords
     * @param $keywords
     */
    public function updateBids($keywords, $updateAuctionOnly = false)
    {
        if ($keywords) {
            $keywords = array_map(function ($keyword) {
                if (is_array($keyword))//да, это костыль
                    return $keyword['source_id'];
                return $keyword->source_id;
            }, $keywords);
            $keywords = array_chunk($keywords, 10000);
            foreach ($keywords as $chunk) {
                $bids = $this->service->getBids($chunk, [], [], $updateAuctionOnly);
                if (!$updateAuctionOnly)
                    $this->updateKeywordBids($bids);
                $this->updateAuctionBids($bids);
            }
        }
    }

    public function getMetrikaCounters()
    {
        $ret = [];
        $api = new Metrika($this->account->stat_token);
        $counters = $api->getCounters();
        if ($counters)
            foreach ($counters as $counter)
                $ret[] = [
                    'name' => $counter['name'],
                    'id' => $counter['id']
                ];
        return $ret;
    }
}