<?php

namespace common\models\structure;

use common\models\Statistics;

/**
 * This is the model class for table "ad".
 *
 * @property integer $id
 * @property string $source_id
 * @property string $title
 * @property string $type
 * @property integer $adgroup_id
 * @property integer $campaign_id
 * @property integer $account_type
 * @property integer $state
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $adgroup_source_id
 * @property integer $account_id
 * @property string $href
 * @property string $sitelink_set_source_id
 *
 * @property Adgroup $adgroup
 * @property Campaign $campaign
 * @property Statistics[] $statistics
 * @property Sitelink[] $sitelinks
 */
class Ad extends Layer
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id', 'title', 'type', 'account_type', 'state', 'adgroup_source_id'], 'required'],
            [['source_id', 'adgroup_id', 'campaign_id', 'account_type', 'state', 'created_at', 'updated_at', 'created_by', 'updated_by', 'adgroup_source_id', 'account_id', 'sitelink_set_source_id'], 'integer'],
            [['title', 'type'], 'string', 'max' => 255],
            [['href'], 'string', 'max' => 1024],
            [['source_id', 'account_type'], 'unique', 'targetAttribute' => ['source_id', 'account_type'], 'message' => 'The combination of Source ID and Account Type has already been taken.'],
            [['adgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Adgroup::className(), 'targetAttribute' => ['adgroup_id' => 'id']],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campaign::className(), 'targetAttribute' => ['campaign_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'title' => 'Title',
            'type' => 'Type',
            'adgroup_id' => 'Adgroup ID',
            'campaign_id' => 'Campaign ID',
            'account_type' => 'Account Type',
            'state' => 'State',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'adgroup_source_id' => 'Adgroup Source ID',
            'account_id' => 'Account ID',
            'href' => 'Href',
            'sitelink_set_source_id' => 'Sitelink Set Source ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdgroup()
    {
        return $this->hasOne(Adgroup::className(), ['id' => 'adgroup_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatistics()
    {
        return $this->hasMany(Statistics::className(), ['ad_id' => 'id']);
    }

    public function getSitelinks(){
        return $this->hasMany(Sitelink::className(), ['set_source_id' => 'sitelink_set_source_id']);
    }
}
