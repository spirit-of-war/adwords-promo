<?php

namespace common\models\structure;

/**
 * This is the model class for table "adgroup".
 *
 * @property integer $id
 * @property string $source_id
 * @property string $name
 * @property string $type
 * @property integer $campaign_id
 * @property integer $account_type
 * @property integer $state
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $campaign_source_id
 * @property integer $account_id
 *
 * @property Ad[] $ads
 * @property Campaign $campaign
 * @property Keyword[] $keywords
 * @property NegativeKeyword[] $negativeKeywords
 */
class Adgroup extends Layer
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adgroup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id', 'name', 'type', 'account_type', 'state', 'campaign_source_id'], 'required'],
            [['source_id', 'campaign_id', 'account_type', 'state', 'created_at', 'updated_at', 'created_by', 'updated_by', 'campaign_source_id', 'account_id'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
            [['source_id', 'account_type'], 'unique', 'targetAttribute' => ['source_id', 'account_type'], 'message' => 'The combination of Source ID and Account Type has already been taken.'],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campaign::className(), 'targetAttribute' => ['campaign_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'name' => 'Name',
            'type' => 'Type',
            'campaign_id' => 'Campaign ID',
            'account_type' => 'Account Type',
            'state' => 'State',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'campaign_source_id' => 'Campaign Source ID',
            'account_id' => 'Account ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ad::className(), ['adgroup_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this->hasMany(Keyword::className(), ['adgroup_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNegativeKeywords()
    {
        return $this->hasMany(NegativeKeyword::className(), ['adgroup_id' => 'id']);
    }
}
