<?php

namespace common\models\structure;

use common\models\Account;
use common\models\strategy\RuleCampaign;
use common\models\Statistics;

/**
 * This is the model class for table "campaign".
 *
 * @property integer $id
 * @property string $source_id
 * @property string $name
 * @property string $type
 * @property string $source_state
 * @property string $status
 * @property string $currency
 * @property string $status_payment
 * @property integer $account_type
 * @property integer $state
 * @property integer $account_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Ad[] $ads
 * @property Adgroup[] $adgroups
 * @property Account $account
 * @property Keyword[] $keywords
 * @property NegativeKeyword[] $negativeKeywords
 * @property RuleCampaign[] $ruleCampaigns
 * @property Statistics[] $statistics
 */
class Campaign extends Layer
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id', 'name', 'type', 'source_state', 'status', 'currency', 'status_payment', 'account_type', 'state', 'account_id'], 'required'],
            [['source_id', 'account_type', 'state', 'account_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'type', 'source_state', 'status', 'currency', 'status_payment'], 'string', 'max' => 255],
            [['source_id', 'account_type'], 'unique', 'targetAttribute' => ['source_id', 'account_type'], 'message' => 'The combination of Source ID and Account Type has already been taken.'],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['account_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'name' => 'Name',
            'type' => 'Type',
            'source_state' => 'Source State',
            'status' => 'Status',
            'currency' => 'Currency',
            'status_payment' => 'Status Payment',
            'account_type' => 'Account Type',
            'state' => 'State',
            'account_id' => 'Account ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAds()
    {
        return $this->hasMany(Ad::className(), ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdgroups()
    {
        return $this->hasMany(Adgroup::className(), ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this->hasMany(Keyword::className(), ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNegativeKeywords()
    {
        return $this->hasMany(NegativeKeyword::className(), ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleCampaigns()
    {
        return $this->hasMany(RuleCampaign::className(), ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatistics()
    {
        return $this->hasMany(Statistics::className(), ['campaign_id' => 'id']);
    }
}
