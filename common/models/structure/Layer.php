<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 01.09.16
 * Time: 15:46
 */

namespace common\models\structure;

use common\models\BehaviorModel;

abstract class Layer extends BehaviorModel
{
    const LAYER_CAMPAIGNS = 0;
    const LAYER_ADGROUPS = 1;
    const LAYER_KEYWORDS = 3;
    const LAYER_ADS = 2;
    const LAYER_NEGATIVE_KETWORDS = 4;
    const LAYER_SITELINK = 5;

    const LAYER_NAME_CAMPAIGN = 'campaign';
    const LAYER_NAME_ADGROUP = 'adgroup';
    const LAYER_NAME_KEYWORD = 'keyword';
    const LAYER_NAME_AD = 'ad';
    const LAYER_NAME_COMPANY = 'company';
    const LAYER_NAME_ACCOUNT = 'account';
    const LAYER_NAME_RULE = 'rule';
    const LAYER_NAME_LIMIT = 'limit';
    const LAYER_NAME_REPORT = 'report';
    const LAYER_NAME_SETTING = 'update';
    const LAYER_NAME_DEAD_LINK = 'view-dead-links';

    public static function getNumberOfLayerName($name)
    {
        if ($name == Layer::LAYER_NAME_AD)
            return Layer::LAYER_ADS;
        elseif ($name == Layer::LAYER_NAME_KEYWORD)
            return Layer::LAYER_KEYWORDS;
        elseif ($name == Layer::LAYER_NAME_ADGROUP)
            return Layer::LAYER_ADGROUPS;
        else
            return Layer::LAYER_CAMPAIGNS;
    }
}