<?php

namespace common\models\structure;

/**
 * This is the model class for table "sitelink".
 *
 * @property string $set_source_id
 * @property string $title
 * @property string $href
 * @property string $description
 */
class Sitelink extends Layer
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sitelink';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['set_source_id'], 'integer'],
            [['title', 'href'], 'required'],
            [['title', 'href', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'set_source_id' => 'Set Source ID',
            'title' => 'Title',
            'href' => 'Href',
            'description' => 'Description',
        ];
    }
}
