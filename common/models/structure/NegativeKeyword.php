<?php

namespace common\models\structure;

/**
 * This is the model class for table "negative_keyword".
 *
 * @property integer $id
 * @property integer $source_id
 * @property integer $adgroup_source_id
 * @property string $keyword
 * @property integer $campaign_id
 * @property integer $adgroup_id
 * @property integer $state
 * @property integer $account_type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $account_id
 *
 * @property Adgroup $adgroup
 * @property Campaign $campaign
 */
class NegativeKeyword extends Layer
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'negative_keyword';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id', 'adgroup_source_id', 'keyword', 'state', 'account_type'], 'required'],
            [['source_id', 'adgroup_source_id', 'campaign_id', 'adgroup_id', 'state', 'account_type', 'created_at', 'updated_at', 'created_by', 'updated_by', 'account_id'], 'integer'],
            [['keyword'], 'string', 'max' => 255],
            [['adgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Adgroup::className(), 'targetAttribute' => ['adgroup_id' => 'id']],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campaign::className(), 'targetAttribute' => ['campaign_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'adgroup_source_id' => 'Adgroup Source ID',
            'keyword' => 'Keyword',
            'campaign_id' => 'Campaign ID',
            'adgroup_id' => 'Adgroup ID',
            'state' => 'State',
            'account_type' => 'Account Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'account_id' => 'Account ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdgroup()
    {
        return $this->hasOne(Adgroup::className(), ['id' => 'adgroup_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }
}
