<?php

namespace common\models\structure;

use Yii;

/**
 * This is the model class for table "bid_auction".
 *
 * @property integer $id
 * @property string $keyword_source_id
 * @property string $keyword_id
 * @property integer $p11_price
 * @property integer $p11_bid
 * @property integer $p12_price
 * @property integer $p12_bid
 * @property integer $p13_price
 * @property integer $p13_bid
 * @property integer $p21_price
 * @property integer $p21_bid
 * @property integer $p24_price
 * @property integer $p24_bid
 * @property integer $state
 * @property integer $account_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class BidAuction extends \common\models\structure\Layer
{
    const POSITION_P11 = 0;
    const POSITION_P12 = 1;
    const POSITION_P13 = 2;
    const POSITION_P21 = 3;
    const POSITION_P24 = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bid_auction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keyword_source_id', 'keyword_id', 'p11_price', 'p11_bid', 'p12_price', 'p12_bid', 'p13_price', 'p13_bid', 'p21_price', 'p21_bid', 'p24_price', 'p24_bid', 'state', 'account_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['p11_price', 'p11_bid', 'p12_price', 'p12_bid', 'p13_price', 'p13_bid', 'p21_price', 'p21_bid', 'p24_price', 'p24_bid', 'state', 'account_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword_source_id' => 'Keyword Source ID',
            'keyword_id' => 'Keyword ID',
            'p11_price' => 'P11 Price',
            'p11_bid' => 'P11 Bid',
            'p12_price' => 'P12 Price',
            'p12_bid' => 'P12 Bid',
            'p13_price' => 'P13 Price',
            'p13_bid' => 'P13 Bid',
            'p21_price' => 'P21 Price',
            'p21_bid' => 'P21 Bid',
            'p24_price' => 'P24 Price',
            'p24_bid' => 'P24 Bid',
            'state' => 'State',
            'account_id' => 'Account ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public static function getActualBids($keywordIds)
    {
        $keywordIds = implode(',', $keywordIds);
        $sql = sprintf('select keyword_id, keyword_source_id,
              (select p11_price from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p11_price,
              (select p12_price from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p12_price,
              (select p13_price from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p13_price,
              (select p21_price from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p21_price,
              (select p24_price from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p24_price,
              (select p11_bid from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p11_bid,
              (select p12_bid from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p12_bid,
              (select p13_bid from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p13_bid,
              (select p21_bid from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p21_bid,
              (select p24_bid from bid_auction bs where bm.account_id = bs.account_id 
                      and bm.keyword_id = bs.keyword_id and max(bm.created_at) = bs.created_at) p24_bid
              from bid_auction bm where keyword_id in (%s) group by keyword_id', $keywordIds);
        return BidAuction::findBySql($sql)->all();
    }
}
