<?php

namespace common\models\structure;

use common\models\Statistics;

/**
 * This is the model class for table "keyword".
 *
 * @property integer $id
 * @property string $source_id
 * @property string $adgroup_source_id
 * @property string $keyword
 * @property string $name
 * @property integer $campaign_id
 * @property integer $adgroup_id
 * @property integer $state
 * @property integer $account_type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property double $bid_context
 * @property double $bid_search
 * @property double $average_position
 * @property integer $account_id
 * @property integer $auction_p11_price
 * @property integer $auction_p12_price
 * @property integer $auction_p13_price
 * @property integer $auction_p21_price
 * @property integer $auction_p24_price
 * @property integer $auction_p11_bid
 * @property integer $auction_p12_bid
 * @property integer $auction_p13_bid
 * @property integer $auction_p21_bid
 * @property integer $auction_p24_bid
 * @property string $negtive
 *
 * @property Adgroup $adgroup
 * @property Campaign $campaign
 * @property Statistics[] $statistics
 */
class Keyword extends Layer
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'keyword';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source_id', 'adgroup_source_id', 'keyword', 'state', 'account_type'], 'required'],
            [['source_id', 'adgroup_source_id', 'campaign_id', 'adgroup_id', 'state', 'account_type', 'created_at', 'updated_at', 'created_by', 'updated_by', 'account_id'], 'integer'],
            [['bid_context', 'bid_search', 'average_position'], 'number'],
            [['keyword'], 'string', 'max' => 255],
            [['negative'], 'string', 'max' => 4096],
            [['source_id', 'account_type'], 'unique', 'targetAttribute' => ['source_id', 'account_type'], 'message' => 'The combination of Source ID and Account Type has already been taken.'],
            [['adgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Adgroup::className(), 'targetAttribute' => ['adgroup_id' => 'id']],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campaign::className(), 'targetAttribute' => ['campaign_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source_id' => 'Source ID',
            'adgroup_source_id' => 'Adgroup Source ID',
            'keyword' => 'Keyword',
            'campaign_id' => 'Campaign ID',
            'adgroup_id' => 'Adgroup ID',
            'state' => 'State',
            'account_type' => 'Account Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'bid_context' => 'Bid Context',
            'bid_search' => 'Bid Search',
            'average_position' => 'Average Position',
            'account_id' => 'Account ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdgroup()
    {
        return $this->hasOne(Adgroup::className(), ['id' => 'adgroup_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatistics()
    {
        return $this->hasMany(Statistics::className(), ['keyword_id' => 'id']);
    }

    public function __get($name)
    {
        if ($name == 'name')
            return $this->keyword;
        else
            return parent::__get($name);
    }
}
