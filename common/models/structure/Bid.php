<?php

namespace common\models\structure;

use common\models\Account;
use Yii;

/**
 * This is the model class for table "bid".
 *
 * @property integer $id
 * @property string $keyword_source_id
 * @property integer $keyword_id
 * @property integer $bid_search
 * @property integer $bid_context
 * @property integer $source_type
 * @property integer $state
 * @property integer $account_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $task_id
 */
class Bid extends \common\models\structure\Layer
{
    const SOURCE_TYPE_USER = 0;
    const SOURCE_TYPE_ADVERT = 1;
    const SOURCE_TYPE_RULE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keyword_source_id', 'keyword_id', 'bid_search', 'bid_context', 'source_type', 'state', 'account_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'task_id'], 'integer'],
            [['keyword_id', 'source_type', 'state', 'account_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword_source_id' => 'Keyword Source ID',
            'keyword_id' => 'Keyword ID',
            'bid_search' => 'Bid Search',
            'bid_context' => 'Bid Context',
            'source_type' => 'Source Type',
            'state' => 'State',
            'account_id' => 'Account ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
