<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 28.06.16
 * Time: 11:19
 */

namespace common\models;


trait ConvertDateTrait
{
    /**
     * @param string|int $date
     * @param string $format
     * @return bool|string
     */
    public function convertDateFormat($date, $format = 'o-m-d')
    {
        if (is_string($date))
            $date = strtotime($date);
        return date($format, $date);
    }

    public function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
}