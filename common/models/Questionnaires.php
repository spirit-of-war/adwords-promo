<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "questionnaires".
 *
 * @property integer $id
 * @property integer $monthly_budget
 * @property integer $max_cost_conversion
 * @property integer $advise_cost_conversion
 * @property string $context_advertising_type
 * @property integer $plannde_conversion_amount
 * @property string $commodity
 * @property string $commodity_advantages
 * @property string $company_advantages
 * @property string $sales
 * @property string $delivery_condition
 * @property string $message
 * @property string $fio
 * @property string $phone
 * @property string $site_address
 * @property string $advertising_district
 * @property string $advertising_systems
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Questionnaires extends \common\models\BehaviorModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questionnaires';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['monthly_budget', 'max_cost_conversion', 'advise_cost_conversion', 'plannde_conversion_amount', 'user_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['user_id'], 'required'],
            [['context_advertising_type', 'commodity', 'commodity_advantages', 'company_advantages', 'sales', 'delivery_condition', 'message', 'site_address', 'advertising_district', 'advertising_systems', 'fio','phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'monthly_budget' => 'Monthly Budget',
            'max_cost_conversion' => 'Max Cost Conversion',
            'advise_cost_conversion' => 'Advise Cost Conversion',
            'context_advertising_type' => 'Context Advertising Type',
            'plannde_conversion_amount' => 'Plannde Conversion Amount',
            'commodity' => 'Commodity',
            'commodity_advantages' => 'Commodity Advantages',
            'company_advantages' => 'Company Advantages',
            'sales' => 'Sales',
            'delivery_condition' => 'Delivery Condition',
            'message' => 'Message',
            'fio' => 'FIO',
            'phone' => 'Phone',
            'site_address' => 'Site Address',
            'advertising_district' => 'Advertising District',
            'advertising_systems' => 'Advertising Systems',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeValidate() {
        if(is_array($this->advertising_systems)){
            $this->advertising_systems = implode(",", $this->advertising_systems);
        }
        return true;
    }
}
