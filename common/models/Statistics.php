<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "statistics".
 *
 * @property integer $id
 * @property double $sum_search
 * @property double $sum_context
 * @property integer $shows_search
 * @property integer $shows_context
 * @property integer $clicks_search
 * @property integer $clicks_context
 * @property double $session_depth_search
 * @property double $session_depth_context
 * @property double $goal_conversion_search
 * @property double $goal_conversion_context
 * @property double $goal_cost_search
 * @property double $goal_cost_context
 * @property integer $campaign_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $ad_id
 * @property integer $start_date
 * @property integer $end_date
 * @property integer $keyword_id
 * @property integer $state
 * @property integer $account_type
 * @property integer $stat_conversions
 * @property integer $crm_assoc_conversions
 * @property Ad $ad
 * @property Campaign $campaign
 * @property integer crm_conversions
 */
class Statistics extends \common\models\BehaviorModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shows_search', 'shows_context', 'clicks_search', 'clicks_context', 'campaign_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'ad_id', 'start_date', 'end_date', 'state', 'crm_conversions', 'stat_conversions', 'crm_assoc_conversions'], 'integer'],
            [['start_date', 'end_date', 'state'], 'required'],
            [['sum_search', 'sum_context', 'session_depth_search', 'session_depth_context', 'goal_conversion_search', 'goal_conversion_context', 'goal_cost_search', 'goal_cost_context'], 'number'],
            [['ad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ad::className(), 'targetAttribute' => ['ad_id' => 'id']],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campaign::className(), 'targetAttribute' => ['campaign_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stat_date' => 'Stat Date',
            'sum_search' => 'Sum Search',
            'sum_context' => 'Sum Context',
            'shows_search' => 'Shows Search',
            'shows_context' => 'Shows Context',
            'clicks_search' => 'Clicks Search',
            'clicks_context' => 'Clicks Context',
            'session_depth_search' => 'Session Depth Search',
            'session_depth_context' => 'Session Depth Context',
            'goal_conversion_search' => 'Goal Conversion Search',
            'goal_conversion_context' => 'Goal Conversion Context',
            'goal_cost_search' => 'Goal Cost Search',
            'goal_cost_context' => 'Goal Cost Context',
            'campaign_id' => 'Campaign ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'ad_id' => 'Ad ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAd()
    {
        return $this->hasOne(Ad::className(), ['id' => 'ad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }
}
