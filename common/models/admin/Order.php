<?php

namespace common\models\admin;
use  \yii\db\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\swiftmailer\Mailer;

class Order extends ActiveRecord
{
	const TYPE_ORDER = 0;
	const TYPE_CALL = 1;
	const TYPE_CONTACT = 2;

	public function rules()
	{
		return [
            //[['name', 'phone', 'site', 'email'], 'required','message'=>'Введите значение для {attribute}.'],
			//[['name'], 'required','message'=>'Введите Фамилию Имя Отчество.'],
//			[['phone'], 'required','message'=>'Введите номер мобильного телефона.'],
			//[['site'], 'required','message'=>'Введите URL вашего сайта.'],
			//[['email'], 'required','message'=>'Введите ваш email.'],
			[['created_at'], 'integer'],
            [['phone'], 'string', 'max' => 255],
            [['phone'], 'match', 'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message' => 'номер телефона должен быть в другом формате']
        ];
	}

	public static function tableName()
    {
        return 'order';
    }
	
    public function sendEmail($model)
    {
		$emailTo =  Yii::$app->params['notificationEmail'];
		$subject = 'Новая заявка';
        $transport = \Yii::$app->mailer->transport;
        $mailer    = new Mailer();
        $mailer->setTransport($transport);

        return $mailer->compose('orderNotification', [
            'model' => $model,
        ])
            ->setFrom($transport->getUsername())
            ->setTo($emailTo)
            ->setSubject($subject)
            ->send();

    }
}