<?php

namespace common\models\admin;
use  \yii\db\ActiveRecord;
use common\behaviors\Slug;
use Yii;
use yii\base\Model;

class Page extends ActiveRecord
{
	const TYPE_PAGE = 0;
	const TYPE_NEWS = 1;
	const TYPE_BLOG = 2;

	const PUBLISH = 1;
	const DRAW = 0;
	
	const IS_SEO_ONLY_TRUE = 1;
	const IS_SEO_ONLY_FALSE = 0;

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => Slug::className(),
                'in_attribute' => 'h1',
                'out_attribute' => 'slug',
                'translit' => true
            ]
        ];
    }

	public function rules() {
		return [
            //[['name', 'phone', 'site', 'email'], 'required','message'=>'Введите значение для {attribute}.'],
			//[['name'], 'required','message'=>'Введите Фамилию Имя Отчество.'],
			[['h1'], 'required','message'=>'Введите Заголовок для статьи'],
			[['title'], 'required','message'=>'Введите SEO title для статьи'],
			
			[['announcement'], 'required','message'=>'Введите анонс для статьи'],
			[['body'], 'required','message'=>'Введите текст статьи'],
			[['type'], 'required','message'=>'Укажите тип статьи статьи'],

			[['keywords'], 'required','message'=>'Введите SEO keywords для статьи'],
			[['description'], 'required','message'=>'Введите SEO description для статьи'],
			[['icon'], 'file', 'extensions' => 'png, jpg'],
			[['slug'], 'unique', 'message'=>'Такой ЧПУ уже существует'],
            [['active', 'slug', 'partial_name'], 'string', 'max' => 255],
			//[['site'], 'required','message'=>'Введите URL вашего сайта.'],
			//[['email'], 'required','message'=>'Введите ваш email.'],
			//[['created_at'], 'integer'],
            //[['name', 'phone', 'site', 'email'], 'string', 'max' => 255],
        ];
	}
	public static function tableName()
    {
        return 'page';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'announcement' => 'Анонс',
            'body' => 'Текст',
            'created' => 'Создано',
            'icon' => 'Изображение',
            'type' => 'Тип статьи',
            'h1' => 'заголовок',
            'keywords' => 'keywords',
            'description' => 'description',
            'url' => 'Url',
            'is_seo_only' => 'is_seo_only',
            'active' => 'Активность',
            'slug' => 'Slug',
        ];
    }

	public static function getTanslate($id) 
	{
		if($id ==self::TYPE_PAGE) {
			$result = 'Страница';
		} elseif($id == self::TYPE_NEWS) {
			$result = 'Новость';
		} elseif($id == self::TYPE_BLOG) {
            $result = 'Блог';
        }
		return $result;
	}
	
	public static function getStatus($active) 
	{
		if($active == self::PUBLISH) {
			$result = 'Опубликовано';
		} else {
			$result = 'Черновик';
		}
		return $result;
	}

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $typeNames = [
                self::TYPE_NEWS => 'news',
                self::TYPE_BLOG => 'blog',
            ];
            if (isset($this->type) && array_key_exists($this->type, $typeNames)) {
                $this->url = $typeNames[$this->type];

                if (isset($this->slug)) {
                    $this->url .= '/' . $this->slug;
                } else {
                    $this->url .= '/' . $this->id;
                }

            } else {
                $this->url = (isset($this->slug) ? $this->slug : '');
            }

            return parent::beforeSave($insert);
        }
        return false;
    }
}