<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 28.06.16
 * Time: 16:10
 */

namespace common\models\service\yandex;

/**
 * Class Metrika provides access for interactions with API.
 * @package common\models\service
 */
class Metrika extends Base
{
    private $counter;

    const ATTRIBUTION_FIRST = 'first';
    const ATTRIBUTION_LAST = 'last';
    const ATTRIBUTION_PREV = 'prev';
    const ATTRIBUTION_LASTSIGN = 'lastsign';

    public function __construct($token, $counter = false)
    {
        $this->counter = $counter;
        parent::__construct($token);
    }

    /**
     * Sends request to Yandex Metrika API.
     * 
     * @param $service
     * @param $params
     * @param $expectedKey
     * 
     * @return array|false
     */
    private function sendRequest($service, $params = [], $expectedKey)
    {
        if ($this->counter !== false)
            $params['id'] = $this->counter;
        $params['oauth_token'] = $this->token;
        $query = [];
        foreach ($params as $key => $value) {
            if (is_array($value))
                $value = implode(',', $value);
            $query[] = $key . '=' . $value;
        }
        $query = implode('&', $query);
        $url = \Yii::$app->params['services']['yandex']['metrika']['url'] . $service . '?' . $query;

        if ( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $result = curl_exec($curl);
            $result = json_decode($result, true);
            if (array_key_exists($expectedKey, $result)) {
                return $result;
            } else if (array_key_exists('errors', $result)) {
                $result = $result['errors'];
                foreach ($result as $error) {
                    \Yii::error(sprintf("Yandex Metrika API \n Service: %s\n Error: %s %s",
                        $url,
                        $error['error_type'],
                        $error['message']
                    ));
                }
            } else {
                \Yii::error(sprintf("Yandex Metrika API Unexpected response\n Service: %s\n Response: %s",
                    $url,
                    var_export($result, true)
                ));
            }
            curl_close($curl);
        }
        return false;
    }

    public function getReport($goalID, $startDate = 'yesterday', $endDate = 'yesterday', $offset = 1, $limit = 100,
                              $attribution = self::ATTRIBUTION_LASTSIGN)
    {
        $params = [
            'metrics' => [
                'ym:s:goal<goal_id>reaches',
            ],
            'dimensions' => [
                'ym:s:<attribution>DirectBannerGroup',
                'ym:s:<attribution>DirectPhraseOrCond',
                'ym:s:<attribution>DirectClickBanner',
            ],
            'offset' => $offset,
            'limit' => $limit,
            'date1' => $this->convertDateFormat($startDate),
            'date2' => $this->convertDateFormat($endDate),
            'goal_id' => $goalID,
            'group' => 'Day',
            'attribution' => $attribution
        ];

        return $this->sendRequest('stat/v1/data', $params, 'data');
    }

    public function getCounters()
    {
        $params = [
            'status' => 'Active'
        ];
        $expectedKey = 'counters';
        $counters = $this->sendRequest('management/v1/counters', $params, $expectedKey);
        if ($counters !== false)
            return $counters[$expectedKey];
        return false;
    }

    public function getGoals($counterID)
    {
        $url = sprintf('/management/v1/counter/%s/goals', $counterID);
        $expectedKey = 'goals';
        $goals = $this->sendRequest($url, [], $expectedKey);
        if ($goals !== false)
            return $goals[$expectedKey];
        return false;
    }
}