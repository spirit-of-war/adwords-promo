<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 29.06.16
 * Time: 11:48
 */

namespace common\models\service\yandex;

use Yii;
use common\models\ConvertDateInterface;
use common\models\ConvertDateTrait;

abstract class Base implements ConvertDateInterface
{
    use ConvertDateTrait;

    protected $token = '';

    function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Returns token from Yandex.Oauth by supplied code.
     *
     * @param $code
     * @return mixed
     */
    public static function getToken($code)
    {
        $query = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => Yii::$app->params['services']['yandex']['direct']['appID'],
            'client_secret' => Yii::$app->params['services']['yandex']['direct']['appPassword']
        ];

        $result = '';
        $query = http_build_query($query);

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, Yii::$app->params['services']['yandex']['oauth']['url'] . 'token');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $query);
            $result = curl_exec($curl);
            curl_close($curl);
        }
        $result = json_decode($result, true);
        if (array_key_exists('access_token', $result)) {
            return $result;
        } else {
            Yii::error('Unexpected response:' . var_export($result, true), 'Yandex.Oauth');
        }
        return false;
    }

    /**
     * Gets login from Yandex.Passport API.
     *
     * @param $token
     * @return bool|string
     */
    public static function getLogin($token)
    {
        $result = file_get_contents(
            sprintf('%sinfo?format=json&oauth_token=%s',
                Yii::$app->params['services']['yandex']['passport']['url'],
                $token)
        );
        $result = json_decode($result, true);
        if (array_key_exists('login', $result)) {
            return $result['login'];
        } else {
            Yii::error('Unexpected response while getting Login from Yandex. Url:' .
                Yii::$app->params['services']['yandex']['passport']['url'], 'Yandex.Passport');
        }
        return false;
    }
}