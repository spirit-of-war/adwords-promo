<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 21.06.16
 * Time: 13:00
 */

namespace common\models\service\yandex;

/**
 * Class provides access for Yandex Direct API.
 * @package common\models\service
 */
class Direct extends Base
{
    const CAMPAIGN_SERVICE = 'campaigns';
    const ADGROUP_SERVICE = 'adgroups';
    const AD_SERVICE = 'ads';
    const KEYWORD_SERVICE = 'keywords';
    const SITELINKS_SERVICE = 'sitelinks';
    const CHANGES_SERVICE = 'changes';
    const BIDS_SERVICE = 'bids';
    const CLIENTS_SERVICE = 'clients';

    private $clientLogin = false;

    public function __construct($token, $clientLogin = false)
    {
        $this->clientLogin = $clientLogin;
        parent::__construct($token);
    }

    /**
     * Sends a POST request to Yandex Direct API version 5.
     *
     * @param array $params
     * @param string $service
     * @param string $returnKey
     * @param string $method
     *
     * @return array|false
     */
    private function sendRequestV5($params, $service = '', $returnKey = '', $method = 'get')
    {
        $postData = [
            'method' => $method,
            'params' => $params
        ];

        $opts = ['http' =>
            [
                'method'  => 'POST',
                'header' =>
                    "Authorization: Bearer $this->token\nContent-Type: application/json; charset=utf-8"
                    . ($this->clientLogin !== false ? "\nClient-Login: " . $this->clientLogin : ''),
                'content' => json_encode($postData),
            ]
        ];
        $service = \Yii::$app->params['services']['yandex']['direct']['url'] . $service;

        $context  = stream_context_create($opts);
        $result = file_get_contents($service, false, $context);
        $result = json_decode($result, true);

        $this->log($service, $opts, $result);

        if (array_key_exists('result', $result)) {
            if ($returnKey == '')
                return $result['result'];
            if (array_key_exists($returnKey, $result['result']))
                return $result['result'][$returnKey];
            else
                return [];
        } else if (array_key_exists('error', $result)) {
            $result = $result['error'];
            \Yii::error(sprintf("Yandex Direct API V5\n Service: %s\n Context: %s\n Error: %s %s",
                $service,
                var_export($opts, true),
                $result['error_string'],
                $result['error_detail']
            ));
        } else {
            \Yii::error(sprintf("Yandex Direct API V5 Unexpected response\n Service: %s\n Context: %s\n Response: %s",
                $service,
                var_export($opts, true),
                var_export($result, true)
            ));
        }

        return false;
    }

    /**
     * Sends a POST request to Yandex Direct API version 4.
     *
     * @param array $params
     * @param string $method
     *
     * @return array|false
     */
    private function sendRequestV4($params, $method)
    {
        $postData = [
            'method' => $method,
            'param' => $params,
            'token' => $this->token
        ];

        $opts = ['http' =>
            [
                'method'  => 'POST',
                'header' => "Content-Type: application/json; charset=utf-8",
                'content' => json_encode($postData),
            ]
        ];
        $service = \Yii::$app->params['services']['yandex']['direct']['url-v4'];

        $context  = stream_context_create($opts);
        $result = file_get_contents($service, false, $context);
        $result = json_decode($result, true);

        $this->log($service, $opts, $result);

        if (array_key_exists('data', $result))
            return $result['data'];
        else if ((array_key_exists('error_str', $result)))
            \Yii::error(sprintf("Yandex Direct API V4\n Service: %s\n Context: %s\n Error: %s %s",
                $service,
                var_export($opts, true),
                $result['error_str'],
                $result['error_detail']
            ));
        else
            \Yii::error(sprintf("Yandex Direct API V4 Unexpected response\n Service: %s\n Context: %s\n Response: %s",
                $service,
                var_export($opts, true),
                var_export($result, true)
            ));

        return false;
    }

    private function log($service, $opts, $result)
    {
        $logInfo = json_encode([
                time() => [
                    'url' => $service,
                    'opts' => $opts,
                    'response' => $result
                ]
            ]) . ',';

        $logPath = \Yii::getAlias('@runtime')
            . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'direct.log.' . date('d-m-o');
        if (is_dir($logPath) == false)
            mkdir($logPath);

        $files = glob($logPath . '/*');
        if ($files)
            foreach ($files as $existedFile)
                if (filesize($existedFile) < 20000000) {
                    $handle = fopen($existedFile, 'a');
                    fwrite($handle, $logInfo);
                    fclose($handle);
                    return;
                }

        file_put_contents($logPath . '/' . date('H-i-s'), $logInfo);
    }

    /**
     * Sends a request for getting Campaigns.
     *
     * @return array|false
     */
    public function getCampaigns()
    {
        $params = [
            'SelectionCriteria' => [
                'States' => ['ON'],
                'Statuses' => ['ACCEPTED'],
            ],
            'FieldNames' => [
                'Id', 'Type', 'Name', 'State', 'Status', 'StatusPayment', 'Currency', 'Statistics'
            ]
        ];
        return $this->sendRequestV5($params, self::CAMPAIGN_SERVICE, 'Campaigns');
    }

    /**
     * Sends a request for getting Adgroups.
     *
     * @param array $campaignIds
     *
     * @return array|false
     */
    public function getAdgroups($campaignIds)
    {
        $params = [
            'SelectionCriteria' => [
                'CampaignIds' => $campaignIds,
                'Statuses' => ['ACCEPTED']
            ],
            'FieldNames' => [
                'Id', 'Type', 'Name', 'CampaignId'
            ]
        ];
        return $this->sendRequestV5($params, self::ADGROUP_SERVICE, 'AdGroups');
    }

    /**
     * Sends a request for getting Ads.
     *
     * @param array $campaignIds
     *
     * @return array|false
     */
    public function getAds($campaignIds)
    {
        $params = [
            'SelectionCriteria' => [
                'CampaignIds' => $campaignIds,
                'States' => ['ON'],
                'Statuses' => ['ACCEPTED']
            ],
            'FieldNames' => [
                'Id', 'Type', 'AdGroupId'
            ],
            'TextAdFieldNames' => [
                'Title', 'Href', 'SitelinkSetId'
            ],
            'MobileAppAdFieldNames' => [
                'Title'
            ],
        ];
        return $this->sendRequestV5($params, self::AD_SERVICE, 'Ads');
    }

    /**
     * @param array $campaignIds
     * @return array
     */
    public function getKeywords($campaignIds)
    {
        $params = [
            'SelectionCriteria' => [
                'CampaignIds' => $campaignIds,
                'States' => ['ON'],
                'Statuses' => ['ACCEPTED']
            ],
            'FieldNames' => [
                'Id', 'Keyword', 'AdGroupId', 'Bid', 'ContextBid'
            ],
        ];
        return $this->sendRequestV5($params, self::KEYWORD_SERVICE, 'Keywords');
    }

    public function getAdsStat($campaignId, $startDate = 'yesterday', $endDate = 'yesterday', $currency = 'RUB')
    {
        $startDate = $this->convertDateFormat($startDate);
        $endDate = $this->convertDateFormat($endDate);
        $params = [
            'CampaignID' => $campaignId,
            'StartDate' => $startDate,
            'EndDate' => $endDate,
            'GroupByColumns' => [
                'clPhrase', 'clROI'
            ],
            'Currency' => $currency
        ];
        return $this->sendRequestv4($params, 'GetBannersStat');
    }

    /**
     * Sends a request for getting Summary Statistics.
     *
     * @param array $campaignIds
     * @param string $startDate
     * @param string $endDate
     *
     * @return array|false
     */
    public function getSummaryStat($campaignIds, $startDate = 'yesterday', $endDate = 'yesterday')
    {
        $startDate = $this->convertDateFormat($startDate);
        $endDate = $this->convertDateFormat($endDate);
        $params = [
            'CampaignIDS' => $campaignIds,
            'StartDate' => $startDate,
            'EndDate' => $endDate
        ];
        return $this->sendRequestV4($params, 'GetSummaryStat');
    }

    /**
     * Gets a list of preparing and prepared reports.
     *
     * @return array|false
     */
    public function getReportList()
    {
        return $this->sendRequestV4([], 'GetReportList');
    }

    /**
     * @param int $reportId
     */
    public function deleteReport($reportId)
    {
        $params = [$reportId];
        $this->sendRequestv4($params, 'DeleteReport');
    }

    /**
     * @param int $campaignId
     * @param int|string $startDate
     * @param int|string $endDate
     * @return array|false
     */
    public function createNewReport($campaignId, $startDate = 'yesterday', $endDate = 'yesterday')
    {
        $startDate = $this->convertDateFormat($startDate);
        $endDate = $this->convertDateFormat($endDate);
        $params = [
            'CampaignID' => $campaignId,
            'StartDate' => $startDate,
            'EndDate' => $endDate,
            'GroupByColumns' => [
                'clPhrase', 'clROI'
            ]
        ];
        return $this->sendRequestv4($params, 'CreateNewReport');
    }

    public function getBalance($campaignIds)
    {
        $params = [
            $campaignIds
        ];
        return $this->sendRequestv4($params, 'GetBalance');
    }

    /**
     * @param $ads array with this structure: ['id' => x, 'href' => y], x - source_id, y - string
     * @return array|false
     */
    public function updateAds($ads)
    {
        $ads = array_map(function ($ad) {
            return [
                'Id' => $ad['id'],
                'TextAd' => [
                    'Href' => $ad['href'],
                    'SitelinkSetId' => $ad['sitelinkSetId']
                ]
            ];
        }, $ads);
        $params = [
            'Ads' => $ads
        ];
        return $this->sendRequestV5($params, self::AD_SERVICE, 'UpdateResults', 'update');
    }

    /**
     * @param array $siteLinkSetIds
     * @return array|false
     */
    public function getSitelinks($siteLinkSetIds)
    {
        $params = [
            'SelectionCriteria' => [
                'Ids' => $siteLinkSetIds
            ],
            'FieldNames' => [
                'Id', 'Sitelinks'
            ]
        ];
        return $this->sendRequestV5($params, self::SITELINKS_SERVICE, 'SitelinksSets', 'get');
    }

    public function addSitelinks($sitelinks)
    {
        $params = [
            'SitelinksSets' => [
                $sitelinks
            ]
        ];
        return $this->sendRequestV5($params, self::SITELINKS_SERVICE, 'AddResults', 'add');
    }

    public function deleteSitelinks($sitelinkIds)
    {
        $params = [
            'SelectionCriteria' => [
                'Ids' => $sitelinkIds
            ]
        ];
        return $this->sendRequestV5($params, self::SITELINKS_SERVICE, 'DeleteResults', 'delete');
    }

    public function checkDictionaries()
    {
        $params = new \stdClass();
        return $this->sendRequestV5($params, self::CHANGES_SERVICE, 'Timestamp', 'checkDictionaries');
    }

    public function checkCampaigns($timestamp)
    {
        $params = [
            'Timestamp' => $timestamp
        ];
        return $this->sendRequestV5($params, self::CHANGES_SERVICE, '', 'checkCampaigns');
    }

    public function check($campaignIds, $timestamp)
    {
        $params = [
            'Timestamp' => $timestamp,
            'FieldNames' => ['CampaignIds', 'AdGroupIds', 'AdIds']
        ];
        if ($campaignIds)
            $params['CampaignIds'] = $campaignIds;
        return $this->sendRequestV5($params, self::CHANGES_SERVICE, '', 'check');
    }

    public function suspendKeywords($ids)
    {
        $params = [
            'SelectionCriteria' => [
                'Ids' => $ids
            ]
        ];
        return $this->sendRequestV5($params, self::KEYWORD_SERVICE, 'SuspendResults', 'suspend');
    }

    public function suspendCampaigns($ids)
    {
        $params = [
            'SelectionCriteria' => [
                'Ids' => $ids
            ]
        ];
        return $this->sendRequestV5($params, self::CAMPAIGN_SERVICE, 'SuspendResults', 'suspend');
    }

    public function getBids($keywordIds = [], $adgroupIds = [], $campaignIds = [], $auctionOnly = false)
    {
        $params = [
            'SelectionCriteria' => [],
            'FieldNames' => [
                'KeywordId', 'AuctionBids'
            ]
        ];
        if ($auctionOnly === false)
            $params['FieldNames'] = array_merge($params['FieldNames'], ['Bid', 'ContextBid']);

        if ($keywordIds)
            $params['SelectionCriteria']['KeywordIds'] = $keywordIds;
        if ($adgroupIds)
            $params['SelectionCriteria']['AdGroupIds'] = $adgroupIds;
        if ($campaignIds)
            $params['SelectionCriteria']['CampaignIds'] = $campaignIds;

        return $this->sendRequestV5($params, self::BIDS_SERVICE, 'Bids');
    }

    public function setBid($id, $bidSearch = false, $bidContext = false)
    {
        $bid = [
            'KeywordId' => $id
        ];
        if ($bidSearch !== false)
            $bid['Bid'] = $bidSearch;
        if ($bidContext !== false)
            $bid['ContextBid'] = $bidContext;
        $params = [
            'Bids' => [$bid]
        ];

        return $this->sendRequestV5($params, self::BIDS_SERVICE, 'SetResults', 'set')[0];
    }

    public function setBids($keywordBids)
    {
        $bids = array_map(function ($keywordBid) {
            $bid = [
                'KeywordId' => $keywordBid['KeywordId'],
                'Bid' => $keywordBid['Bid'],
            ];

            if (array_key_exists('ContextBid', $keywordBid))
                $bid['ContextBid'] = $keywordBid['ContextBid'];

            return $bid;
        }, $keywordBids);

        $params = [
            'Bids' => $bids
        ];

        return $this->sendRequestV5($params, self::BIDS_SERVICE, 'SetResults', 'set');
    }

    public function getClientsList()
    {
        $params = [
            'Filter' => [
                'StatusArch' => 'No'
            ]
        ];

        return $this->sendRequestV4($params, 'GetClientsList');
    }
}