<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 29.11.16
 * Time: 16:28
 */

namespace common\models\service\google;

use AdWordsUser;
use Selector;
use CustomerSyncSelector;
use OrderBy;
use Predicate;
use DateTimeRange;
use AdGroupAd;
use Ad;
use AdGroupAdOperation;
use BiddableAdGroupCriterion;
use Criterion;
use CpcBid;
use Money;
use BiddingStrategyConfiguration;
use AdGroupCriterionOperation;
use \ReportUtils;
use yii\helpers\VarDumper;

/***
 * Class V201609
 * @package common\models\service\google
 * @property AdWordsUser $user
 */
class V201609 extends Adwords
{
    public function __construct($account)
    {
        $this->version = 'v201609';
        parent::__construct($account);
    }

    public function getSync($campaignIds)
    {
        $service = $this->user->GetService(self::SERVICE_SYNC, $this->version);

        $dateTimeRange = new DateTimeRange();
        $dateTimeRange->min = date('Ymd his', strtotime('-1 day'));
        $dateTimeRange->max = date('Ymd his');

        $selector = new CustomerSyncSelector();
        $selector->dateTimeRange = $dateTimeRange;
        $selector->campaignIds = $campaignIds;

        return $service->get($selector);
    }

    public function getCampaigns()
    {
        $service = $this->user->GetService(self::SERVICE_CAMPAIGN, $this->version);

        $selector = new Selector();
        $selector->fields = [
            'Id',
            'Name',
            'AdvertisingChannelType',
            'CampaignStatus',
            'ServingStatus'
        ];
        $selector->ordering[] = new OrderBy('Name', 'ASCENDING');

        $data = $service->get($selector);

        $campaigns = [];
        foreach ($data->entries as $rawCampaign) {
            $campaigns[] = [
                'CampaignId' => $rawCampaign->id,
                'CampaignName' => $rawCampaign->name,
                'AdvertisingChannelType' => $rawCampaign->advertisingChannelType,
                'CampaignStatus' => $rawCampaign->status,
                'ServingStatus' => $rawCampaign->servingStatus
            ];
        }

        return $campaigns;
    }

    public function getAdgroups($campaignIds)
    {
        $service = $this->user->GetService(self::SERVICE_ADGROUP, $this->version);

        $selector = new Selector();
        $selector->fields = [
            'Id',
            'Name',
            'CampaignId'
        ];
        $selector->predicates[] = new Predicate('CampaignId', 'IN', $campaignIds);

        $data = $service->get($selector);

        $adgroups = [];
        foreach ($data->entries as $rawAdgroup) {
            $adgroups[] = [
                'AdGroupId' => $rawAdgroup->id,
                'AdGroupName' => $rawAdgroup->name,
                'BiddingStrategyType' => '',
                'CampaignId' => $rawAdgroup->campaignId,
            ];
        }

        return $adgroups;
    }

    public function getAds($campaignIds)
    {
        $service = $this->user->GetService(self::SERVICE_AD, $this->version);

        $selector = new Selector();
        $selector->fields = [
            'Id',
            'HeadlinePart1',
        ];
        $selector->predicates[] = new Predicate('CampaignId', 'IN', $campaignIds);

        $data = $service->get($selector);

        $ads = [];
        foreach ($data->entries as $rawAd) {
            $ads[] = [
                'Id' => $rawAd->ad->id,
                'Headline' => $rawAd->ad->headlinePart1,
                'AdType' => $rawAd->ad->AdType,
                'AdGroupId' => $rawAd->adGroupId
            ];
        }

        return $ads;
    }

    public function getKeywords($campaignId)
    {
        $service = $this->user->GetService(self::SERVICE_KEYWORDS, $this->version);

        $selector = new Selector();
        $selector->fields = [
            'Id',
            'Text',
            'BaseAdGroupId'
        ];
        $selector->predicates[] = new Predicate('CampaignId', 'EQUALS', $campaignId);
        $selector->predicates[] = new Predicate('CriteriaType', 'EQUALS', 'KEYWORD');

        $data = $service->get($selector);

        $keywords = [];
        if ($data->entries)
            foreach ($data->entries as $rawKeyword)
                $keywords[] = [
                    'Id' => $rawKeyword->criterion->id,
                    'Criteria' => $rawKeyword->criterion->text,
                    'AdGroupId' => $rawKeyword->baseAdGroupId
                ];

        return $keywords;
    }

    public function setUtmContent($campaignIds, $task, $completenessRange)
    {
        require_once 'Google/Api/Ads/Common/Util/ErrorUtils.php';

        $utm = [//не хватает двух параметров 'block' => '{position_type}', 'added' => '{addphrases}',
            'utm_source' => 'google',
            'utm_medium' => 'cpc',
            'campaign_id' => '{campaignid}',
            'gb_id' => '{adgroupid}',
            'phrase_id' => '{targetid}',
            'ad_id' => '{creative}',
            'banner_id' => '{creative}',
            'utm_term' => '{keyword}',
            'type' => '{network}',
            'pos' => '{adposition}'
        ];

        $parseUrl = function ($url) use ($utm) {
            $initialParams = parse_url($url, PHP_URL_QUERY);
//            $initialParams = str_replace('?', '&', $initialParams); - если случайно затесался второй вопрос,
//                                                                      но вдруг он является частью значения параметра?
            $params = [];
            parse_str($initialParams, $params);
            $allowedParams = [];
            $utm = array_keys($utm);
            foreach ($params as $key => $val)
                if (!in_array($key, $utm))
                    $allowedParams[$key] = $val;

            $allowedParams = array_map(function ($key, $value) {
                return $key . '=' . $value;
            }, array_keys($allowedParams), array_values($allowedParams));
            $allowedParams = implode('&', $allowedParams);

            $url = str_replace($initialParams, $allowedParams, $url);

            if (parse_url($url, PHP_URL_QUERY) == null && substr($url, -1) == '?')
                return substr($url, 0, -1);

            return $url;
        };

        $trackingTemplate = array_map(function ($key, $value) {
            return $key . '=' . $value;
        }, array_keys($utm), array_values($utm));
        $trackingTemplate = '{lpurl}?' . implode('&', $trackingTemplate);

        $service = $this->user->GetService(self::SERVICE_AD, $this->version);

        $selector = new Selector();
        $selector->fields = [
            'Id',
            'CreativeFinalUrls',
            'CreativeFinalMobileUrls',
            'CreativeFinalAppUrls',
        ];

        $operations = [];
        foreach ($campaignIds as $campaignId) {
            $selector->predicates[] = new Predicate('CampaignId', 'EQUALS', $campaignId);
//            $selector->predicates[] = new Predicate('AdGroupId', 'EQUALS', 15109950789);

            $data = $service->get($selector);

            foreach ($data->entries as $adGroupAd) {
                $adToDelete = new AdGroupAd();
                $adToDelete->adGroupId = $adGroupAd->adGroupId;
                $adToDelete->ad = new Ad($adGroupAd->ad->id);

                $removeOperation = new AdGroupAdOperation();
                $removeOperation->operand = $adToDelete;
                $removeOperation->operator = 'REMOVE';

                $adToChange = $adGroupAd->ad;
                $adToChange->id = null;
                $adToChange->trackingUrlTemplate = $trackingTemplate;

                $checkUrls = function ($urls) use ($parseUrl) {
                    $checkedUrls = [];
                    if (is_array($urls))
                        foreach ($urls as $url)
                            $checkedUrls[] = $parseUrl($url);
                    else
                        return $urls;

                    return $checkedUrls;
                };

                $adToChange->finalUrls = $checkUrls($adToChange->finalUrls);
                $adToChange->finalMobileUrls = $checkUrls($adToChange->finalMobileUrls);
                $adToChange->finalAppUrls = $checkUrls($adToChange->finalAppUrls);

                $newAdGroupAd = new AdGroupAd();
                $newAdGroupAd->adGroupId = $adGroupAd->adGroupId;
                $newAdGroupAd->ad = $adToChange;

                $addOperation = new AdGroupAdOperation();
                $addOperation->operand = $newAdGroupAd;
                $addOperation->operator = 'ADD';

                $operations[] = $removeOperation;
                $operations[] = $addOperation;
            }

            $policyFailOperations = [];
            try {
                $service->mutate($operations);
            } catch (\SoapFault $fault) {
                $errors = \ErrorUtils::GetApiErrors($fault);
                if (sizeof($errors) == 0) {
                    throw $fault;
                }
                foreach ($errors as $error) {
                    if ($error->ApiErrorType == 'PolicyViolationError' && $error->isExemptable) {
                        $operationIndex = \ErrorUtils::GetSourceOperationIndex($error);
                        $operation = $operations[$operationIndex];
                        $operation->exemptionRequests[] = new \ExemptionRequest($error->key);
                        $policyFailOperations[] = $operation;
                    } else {
                        throw $fault;
                    }
                }
            }

            if (sizeof($policyFailOperations) > 0) {
                $result = $service->mutate($policyFailOperations);
            }

            $task->increaseCompleteness($completenessRange / count($campaignIds));
        }
    }

    public function getBids($keywordIds)
    {
        $service = $this->user->GetService(self::SERVICE_KEYWORDS, $this->version);

        $selector = new Selector();
        $selector->fields = [
            'Id',
            'CpcBid',
            'FirstPageCpc',
            'TopOfPageCpc',
            'FirstPositionCpc'
        ];
        $selector->predicates[] = new Predicate('Id', 'IN', $keywordIds);
        $selector->predicates[] = new Predicate('CriteriaType', 'EQUALS', 'KEYWORD');

        $data = $service->get($selector);

        $keywordsBid = [];
        if ($data->entries)
            foreach ($data->entries as $rawKeyword)
                $keywordsBid[] = [
                    'KeywordId' => $rawKeyword->criterion->id,
                    'Bid' => $rawKeyword->biddingStrategyConfiguration->bids[0]->bid->microAmount,
                    'firstPageCpc' =>
                        $rawKeyword->firstPageCpc == null ? null : $rawKeyword->firstPageCpc->amount->microAmount,
                    'topOfPageCpc' =>
                        $rawKeyword->topOfPageCpc == null ? null : $rawKeyword->topOfPageCpc->amount->microAmount,
                    'firstPositionCpc' =>
                        $rawKeyword->firstPositionCpc == null ? null : $rawKeyword->firstPositionCpc->amount->microAmount
                ];

        return $keywordsBid;
    }

    public function setBids($keywordBids)
    {
        $service = $this->user->GetService(self::SERVICE_KEYWORDS, $this->version);

        $operations = [];
        foreach ($keywordBids as $keywordBid) {
            $adGroupCriterion = new BiddableAdGroupCriterion();
            $adGroupCriterion->adGroupId = $keywordBid['AdGroupId'];
            $adGroupCriterion->criterion = new Criterion($keywordBid['KeywordId']);

            $bid = new CpcBid();
            $bid->bid =  new Money($keywordBid['Bid']);
            $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
            $biddingStrategyConfiguration->bids[] = $bid;

            $adGroupCriterion->biddingStrategyConfiguration = $biddingStrategyConfiguration;

            $operation = new AdGroupCriterionOperation();
            $operation->operand = $adGroupCriterion;
            $operation->operator = 'SET';

            $operations[] = $operation;
        }

        return $service->mutate($operations);
    }

    public function getAdsStat($campaignIds, $startDate = 'yesterday', $endDate = 'yesterday')
    {
        require_once 'Google/Api/Ads/AdWords/Util/v201609/ReportUtils.php';

        $dateRange = sprintf('%d,%d', date('Ymd', strtotime($startDate)), date('Ymd', strtotime($endDate)));
        $selectFields = [
            'CampaignId',
            'AdGroupId',
            'Id',
            'CriterionId',
            'Cost',
            'Impressions',
            'Clicks',
            'ConversionValue',
            'AveragePosition'
        ];
        $reportQuery = sprintf(
            'SELECT %s FROM AD_PERFORMANCE_REPORT WHERE CampaignId IN [%s] AND CriterionType = KEYWORD DURING %s',
            implode(',', $selectFields),
            implode(',', $campaignIds),
            $dateRange
        );

        $options = array('version' => $this->version);
        $options['skipReportHeader'] = true;
        $options['skipColumnHeader'] = true;
        $options['skipReportSummary'] = true;

        $fileName = \Yii::getAlias('@runtime')
            . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR
            . 'adwords.log.stat.' . $campaignIds[0] . '.' . date('d-m-o');
        $reportUtils = new ReportUtils();
        $reportUtils->DownloadReportWithAwql($reportQuery, $fileName, $this->user, 'CSV', $options);

        return file_get_contents($fileName);
    }

    function GetReportFieldsExample()
    {
        $service = $this->user->GetService('ReportDefinitionService', $this->version);

        $reportType = 'AD_PERFORMANCE_REPORT';

        $reportDefinitionFields = $service->getReportFields($reportType);

        // Display results.
        printf("The report type '%s' contains the following fields:\n", $reportType);
        foreach ($reportDefinitionFields as $reportDefinitionField) {
            printf('  %s (%s)', $reportDefinitionField->fieldName,
                $reportDefinitionField->fieldType);
            if (isset($reportDefinitionField->enumValues)) {
                printf(' := [%s]', implode(', ', $reportDefinitionField->enumValues));
            }
            print "\n";
        }
    }
}