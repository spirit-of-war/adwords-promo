<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 29.11.16
 * Time: 16:34
 */

namespace common\models\service\google;

use Yii;
use AdWordsUser;
use Google_Client;
use ManagedCustomerLink;
use LinkOperation;

/***
 * Class AdwordsService
 * @package common\models\service\google
 * @property AdWordsUser $user
 */
abstract class Adwords
{
    const SERVICE_CAMPAIGN = 'CampaignService';
    const SERVICE_ADGROUP = 'AdGroupService';
    const SERVICE_AD = 'AdGroupAdService';
    const SERVICE_KEYWORDS = 'AdGroupCriterionService';
    const SERVICE_SYNC = 'CustomerSyncService';

    protected $version;
    protected $adwordsUser;
    protected $googleClient;
    protected $accessToken;
    protected $clientCustomerId;

    public function __construct($account)
    {
        $this->clientCustomerId = $account->client_customer_id;

        if ($account->advert_token != null) {
            if($account->advert_token_expires > time())
            {
                $this->accessToken = $account->advert_token;
            }
            else
            {
                $client = $this->getGoogleClient();
                $client->refreshToken($account->refresh_token);
                $newToken = $client->getAccessToken();
                $account->advert_token = $newToken['access_token'];
                $account->advert_token_expires = (string)(time() + $newToken['expires_in']);
                $account->save();
                $this->accessToken = $newToken['access_token'];
            }
        }
    }

    private function getUser()
    {
        if ($this->adwordsUser == null) {
            $developerToken = Yii::$app->params['services']['google']['adwords']['developerToken'];
            $userAgent = Yii::$app->params['services']['google']['adwords']['userAgent'];
            $oauth2Info = [
                'client_id' => Yii::$app->params['services']['google']['adwords']['clientId'],
                'client_secret'=> Yii::$app->params['services']['google']['adwords']['clientSecret'],
//                'access_token' => $this->access_token
            ];

//            $this->adwordsUser =
//                new AdWordsUser(null, $developerToken, $userAgent, $this->clientCustomerId, null, $oauth2Info);

            $this->adwordsUser =
                new AdWordsUser(null, $developerToken, $userAgent, null, null, $oauth2Info);
        }
        return $this->adwordsUser;
    }

    public function getGoogleClient()
    {
        if ($this->googleClient == null) {
            $this->googleClient = new Google_Client();
            $this->googleClient->setClientId(Yii::$app->params['services']['google']['adwords']['clientId']);
            $this->googleClient->setClientSecret(Yii::$app->params['services']['google']['adwords']['clientSecret']);
            $this->googleClient->setRedirectUri(Yii::$app->params['services']['google']['adwords']['redirectUrl']);
            $this->googleClient->setAccessType('offline');
            $this->googleClient->setApprovalPrompt('force');
            $this->googleClient->setScopes([
                'https://www.googleapis.com/auth/analytics',
                'https://www.googleapis.com/auth/adwords',
                'https://www.googleapis.com/auth/userinfo.profile'
            ]);
        }
        return $this->googleClient;
    }

    public function getAuthUrl()
    {
        $OAuth2Handler = $this->getUser()->GetOAuth2Handler();
        return $OAuth2Handler->GetAuthorizationUrl(
            $this->getUser()->GetOAuth2Info(),
            Yii::$app->params['services']['google']['adwords']['redirectUrl'],
            true);
    }

    public function getOAuth2Info($code)
    {
        $OAuth2Handler = $this->getUser()->GetOAuth2Handler();
        $accessToken = $OAuth2Handler->GetAccessToken(
            $this->getUser()->GetOAuth2Info(),
            $code,
            Yii::$app->params['services']['google']['adwords']['redirectUrl']);
        $this->getUser()->SetOAuth2Info($accessToken);

        return $this->getUser()->GetOAuth2Info();
    }

    public abstract function getCampaigns();
    public abstract function getAdgroups($campaignIds);
    public abstract function getAds($campaignIds);
    public abstract function getKeywords($campaignId);
}