<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property integer $object_id
 * @property double $completeness
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $error_details
 */
class Task extends BehaviorModel
{
    const TYPE_UPDATE_STRUCTURE = 0;
    const TYPE_UPDATE_STATISTICS = 1;
    const TYPE_RULE = 2;
    const TYPE_UPDATE_SITELINKS = 3;
    const TYPE_LIMIT = 4;
    const TYPE_UPDATE_UTM = 5;

    const STATUS_RUNNING = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_ERROR = 3;

    const PRIORITY_MIN = 0;
    const PRIORITY_LOW = 25;
    const PRIORITY_MEDIUM = 50;
    const PRIORITY_HIGH = 75;
    const PRIORITY_MAX = 100;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'status'], 'required'],
            [['type', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by', 'object_id'], 'integer'],
            [['completeness'], 'number'],
            [['error_details'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @param float $completeness
     * @param bool $failed if task is failed
     */
    public function increaseCompleteness($completeness, $failed = false, $errorDetails = false)
    {
        $this->completeness += $completeness;

        if ((string)$this->completeness == '100' || $failed) {
//            $this->redis->executeCommand('del', [sprintf('%s:%s:%s', $this->type, $this->object_id, $this->id)]);
            $this->status = $failed ? self::STATUS_ERROR : self::STATUS_COMPLETED;
            if ($failed && $errorDetails !== false) {
                if (strlen($errorDetails) > 1024) {
                    $errorFile = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . time()
                        . '.error';
                    file_put_contents($errorFile, $errorDetails);
                    $errorDetails = sprintf('Full error see in %s %s', $errorFile, $errorDetails);
                    $errorDetails = substr($errorDetails, 0, 1024);
                }
                $this->error_details = $errorDetails;
            }
            $this->update();
        } else {
//            $this->redis->executeCommand('set', [
//                sprintf('%s:%s:%s', $this->type, $this->object_id, $this->id),
//                $this->completeness
//            ]);
        }

        if (Yii::$app->className() == 'yii\console\Application')
            if ($failed)
                echo 'Error' . PHP_EOL;
            else
                echo 'Progress: ' . round($this->completeness, 2) . "%        " . PHP_EOL;
    }

    public static function getRunningTasks($types = [], $json = true)
    {
        $tmp = new Task();
        $types = array_map(function ($type) {
            return (integer)$type;
        }, $types);
        $types = '[' . implode(',', $types) . ']';
        $tasks = $tmp->redis->executeCommand('keys', [$types]);

        $runningTasks = [];
        foreach ($tasks as $task)
            $runningTasks[$task] = $tmp->redis->executeCommand('get', [$task]);

        if ($json) {
            $tasks = [];
            foreach ($runningTasks as $task)
                $tasks[] = [
                    [
                        'id' => $task->id,
                        'completeness' => $task->completeness,
                        'status' => $task->status
                    ]
                ];
        } else {
            return $runningTasks;
        }
    }

    function save($runValidation = true, $attributeNames = null)
    {
        if (Yii::$app->className() == 'yii\console\Application') {
            if(self::TYPE_UPDATE_STRUCTURE == $this->type)
                echo 'Structure updating';
            elseif(self::TYPE_UPDATE_STATISTICS == $this->type)
                echo 'Statistics updating';
            elseif(self::TYPE_RULE == $this->type)
                echo 'Rule execution';
            elseif(self::TYPE_UPDATE_SITELINKS == $this->type)
                echo 'Sitelinks updating';
            elseif(self::TYPE_LIMIT == $this->type)
                echo 'Limit execution';
            elseif(self::TYPE_UPDATE_UTM == $this->type)
                echo 'Utm updating';

            echo ', object id: ' . $this->object_id . PHP_EOL;
        }


        return parent::save($runValidation, $attributeNames);
    }
}
