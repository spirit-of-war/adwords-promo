<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_UNCONFIRMED = 40;

	const ROLE_USER = 20;
	const ROLE_ADMIN = 30;

    public $fio;
    public $phone;
    public $site_address;
    public $password_repeat;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_UNCONFIRMED],
			['role', 'default', 'value' => self::ROLE_USER],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_UNCONFIRMED]],
			['role', 'in', 'range' => [self::ROLE_USER,self::ROLE_ADMIN]],

        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByAuthKey($key)
    {
        return static::findOne(['auth_key' => $key, 'status' => self::STATUS_UNCONFIRMED]);
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
	public static function isUser($username)
	{
		if (static::findOne(['username' => $username, 'role' => self::ROLE_USER])) {
			return true;
		} else {
			return false;
		}
	}
	public static function isUserAdmin($username)
	{
		if (static::findOne(['username' => $username, 'role' => self::ROLE_ADMIN])) {
			return true;
		} else {
			return false;
		}
	}

    /**
     * Indicates whether current user has a specified account
     * @param $accountID
     * @return bool
     */
    public static function hasAccount($userID, $accountID)
    {
        $accountQuantity = Yii::$app->db->createCommand(
            'select count(*) from account a inner join company c where c.user_id = :userID and a.id = :accountID',
            [
                'userID' => $userID,
                'accountID' => $accountID
            ]
        )->queryScalar();
        return (integer)$accountQuantity > 0;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $questionnaires = new Questionnaires();
            $questionnaires->fio = $this->fio;
            $questionnaires->phone = $this->phone;
            $questionnaires->site_address = $this->site_address;
            $questionnaires->user_id = $this->getPrimaryKey();
            $questionnaires->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getQuestionnaires()
    {
        return $this->hasOne(Questionnaires::className(), ['user_id' => 'id']);
    }
}
