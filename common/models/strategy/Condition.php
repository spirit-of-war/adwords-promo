<?php

namespace common\models\strategy;

use Yii;

/**
 * This is the model class for table "condition".
 *
 * @property integer $id
 * @property integer $rule_id
 * @property integer $disjunction
 * @property string $index
 * @property string $action
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $period
 * @property integer $startDate
 * @property integer $endDate
 *
 * @property Rule $rule
 */
class Condition extends \common\models\BehaviorModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'condition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rule_id', 'disjunction', 'index', 'action', 'value', 'period'], 'required'],
            [['rule_id', 'disjunction', 'created_at', 'updated_at', 'created_by', 'updated_by', 'period'], 'integer'],
            [['index', 'action', 'value'], 'string', 'max' => 255],
            [['rule_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rule::className(), 'targetAttribute' => ['rule_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'disjunction' => 'Disjunction',
            'index' => 'Index',
            'action' => 'Action',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'period' => 'Period',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRule()
    {
        return $this->hasOne(Rule::className(), ['id' => 'rule_id']);
    }

    public function __get($name)
    {
        if ($name == 'startDate')
            return strtotime('today') - $this->period * 86400;
        elseif ($name == 'endDate')
            return strtotime('today');

        return parent::__get($name);
    }
}
