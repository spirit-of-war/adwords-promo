<?php

namespace common\models\strategy;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RuleSearch represents the model behind the search form about `common\models\strategy\Rule`.
 */
class RuleSearch extends Rule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'layer', 'frequency', 'account_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'action', 'weekday', 'monthday', 'instant_start', 'onceday', 'state', 'position'], 'integer'],
            [['name'], 'safe'],
            [['change'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'layer' => $this->layer,
            'frequency' => $this->frequency,
            'change' => $this->change,
            'account_id' => $this->account_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'action' => $this->action,
            'weekday' => $this->weekday,
            'monthday' => $this->monthday,
            'instant_start' => $this->instant_start,
            'onceday' => $this->onceday,
            'state' => [Rule::STATE_ACTIVE, Rule::STATE_SUSPENDED],
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
