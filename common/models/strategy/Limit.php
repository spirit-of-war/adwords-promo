<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 24.10.16
 * Time: 13:36
 */

namespace common\models\strategy;


use common\models\structure\Ad;
use common\models\structure\Layer;
use common\models\Task;
use yii\base\Exception;
use yii\swiftmailer\Mailer;
use common\models\structure\Campaign;
use common\models\structure\Adgroup;
use common\models\structure\Keyword;

class Limit extends Rule
{
    const ACTION_STOP = 0;
    const ACTION_NOTIFICATION = 4;

    const TYPE = 1;

    public $layerConditions;

    public function save($runValidation = true, $attributeNames = null)
    {
        parent::save($runValidation, $attributeNames);
        if (!$this->errors
            && $this->layerConditions != null
            && ($this->layer == Layer::LAYER_ADGROUPS || $this->layer == Layer::LAYER_KEYWORDS)) {

            RuleCampaign::deleteAll([
                'rule_id' => $this->id
            ]);

            if (!$this->isNewRecord)
                Condition::deleteAll([
                    'rule_id' => $this->id,
                    'index' => '-1'
                ]);

            foreach ($this->layerConditions as $raw) {
                $condition = new Condition([
                    'index' => '-1',
                    'action' => (string)$raw['sign'],
                    'value' => $raw['value'],
                    'period' => -1,
                    'disjunction' => -1,
                    'rule_id' => $this->id
                ]);
                $condition->save();
            }
        } elseif ($this->layer == Layer::LAYER_CAMPAIGNS && !$this->isNewRecord) {
            Condition::deleteAll([
                'rule_id' => $this->id,
                'index' => '-1'
            ]);
        }
    }

    public function getSql()
    {
        if ($this->layer == Layer::LAYER_CAMPAIGNS)
            return parent::getSql();

        $nameConditions = $this->hasMany(Condition::className(), ['rule_id' => 'id'])->where('`index` = -1')->all();
        $nameField = $this->layer == Layer::LAYER_KEYWORDS ? 'keyword' : 'name';

        $conds = [];
        foreach ($nameConditions as $condition)
            if ($condition->action == 0)
                $conds[] = "lower($nameField)=lower('$condition->value')";
            elseif ($condition->action == 1)
                $conds[] = "lower($nameField)<>lower('$condition->value')";
            elseif ($condition->action == 2)
                $conds[] = "$nameField like '%$condition->value%'";
            elseif ($condition->action == 3)
                $conds[] = "$nameField not like '%$condition->value%'";
            elseif ($condition->action == 4)
                $conds[] = "$nameField like '$condition->value%'";
            elseif ($condition->action == 5)
                $conds[] = "$nameField like '%$condition->value'";
        $conds = implode(' and ', $conds);

        return parent::getSql() . ' and ' . $conds;
    }

    public function getConditionElements($limit = false, $offset = false)
    {
        if ($this->layer == Layer::LAYER_CAMPAIGNS)
            return Campaign::findBySql($this->getSql(false, $limit, $offset))->all();
        elseif ($this->layer == Layer::LAYER_ADGROUPS)
            return Adgroup::findBySql($this->getSql(false, $limit, $offset))->all();
        elseif ($this->layer == Layer::LAYER_KEYWORDS)
            return Keyword::findBySql($this->getSql(false, $limit, $offset))->all();
        elseif ($this->layer == Layer::LAYER_ADS)
            return Ad::findBySql($this->getSql(false, $limit, $offset))->all();
    }

    public function execute()
    {
        $task = new Task([
            'type' => Task::TYPE_LIMIT,
            'status' => Task::STATUS_RUNNING,
            'object_id' => $this->id,
            'completeness' => 1
        ]);
        $task->save();

        try {
            $condElements = $this->getConditionElements();

            if ($condElements)
                if ($this->action == self::ACTION_NOTIFICATION) {
                    $this->sendNotification($condElements, $task);
                }
                elseif ($this->action == self::ACTION_STOP) {
                    if ($this->layer == Layer::LAYER_CAMPAIGNS)
                        $this->account->export->suspendCampaigns($condElements, $this->id);
                    elseif ($this->layer == Layer::LAYER_KEYWORDS)
                        $this->account->export->suspendKeywords($condElements, $this->id);
                    elseif ($this->layer == Layer::LAYER_ADGROUPS)
                        $this->account->export->suspendAdgroups($condElements, $this->id);
                    $this->sendNotification($condElements);
                }

            $task->increaseCompleteness(99);
        } catch (Exception $e) {
            $task->increaseCompleteness(0, true, $e->getMessage());
        }
    }

    private function sendNotification($condElements, $task)
    {
        $rows = [];
        foreach ($condElements as $element)
            $rows[] = [
                $this->id,
                $this->layer,
                $element->source_id,
                $this->action,
                $task->id,
                time(),
                time()
            ];
        \Yii::$app->db->createCommand()->batchInsert(
            LimitResult::tableName(),
            ['rule_id', 'layer', 'source_id', 'action', 'task_id', 'created_at', 'updated_at'],
            $rows)->execute();

        $transport = \Yii::$app->mailer->transport;
        $mailer = new Mailer();
        $mailer->setTransport($transport);

        $mailer
            ->compose('limitNotification-text', [
                'userName' => $this->account->company->name,
                'limit' => $this,
                'condElements' => $condElements
            ])
            ->setFrom($transport->getUsername())
            ->setTo($this->account->company->user->email)
            ->setSubject('Лимит на PROFITPANDA.RU')
            ->send();
    }

    public function getLayerConds()
    {
        $conds = $this->hasMany(Condition::className(), ['rule_id' => 'id'])->where('`index` = -1')->all();
        $conds = array_map(function ($cond) {
            return [
                'id' => $cond['id'],
                'sign' => $cond['action'],
                'value' => $cond['value']
            ];
        }, $conds);

        return json_encode($conds);
    }
}