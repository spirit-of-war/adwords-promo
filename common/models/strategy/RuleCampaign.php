<?php

namespace common\models\strategy;

use common\models\BehaviorModel;
use common\models\structure\Campaign;

/**
 * This is the model class for table "rule_campaign".
 *
 * @property integer $id
 * @property integer $rule_id
 * @property integer $campaign_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Campaign $campaign
 * @property Rule $rule
 */
class RuleCampaign extends BehaviorModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rule_campaign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rule_id', 'campaign_id'], 'required'],
            [['rule_id', 'campaign_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campaign::className(), 'targetAttribute' => ['campaign_id' => 'id']],
            [['rule_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rule::className(), 'targetAttribute' => ['rule_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'campaign_id' => 'Campaign ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRule()
    {
        return $this->hasOne(Rule::className(), ['id' => 'rule_id']);
    }
}
