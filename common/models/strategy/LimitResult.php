<?php

namespace common\models\strategy;

use Yii;

/**
 * This is the model class for table "limit_result".
 *
 * @property integer $id
 * @property integer $rule_id
 * @property integer $layer
 * @property string $source_id
 * @property integer $action
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $task_id
 *
 * @property Rule $rule
 */
class LimitResult extends \common\models\BehaviorModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'limit_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rule_id', 'layer', 'source_id', 'action', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['rule_id', 'layer', 'source_id', 'action', 'created_at', 'updated_at', 'created_by', 'updated_by', 'task_id'], 'integer'],
            [['rule_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rule::className(), 'targetAttribute' => ['rule_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule_id' => 'Rule ID',
            'layer' => 'Layer',
            'source_id' => 'Source ID',
            'action' => 'Action',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRule()
    {
        return $this->hasOne(Rule::className(), ['id' => 'rule_id']);
    }
}
