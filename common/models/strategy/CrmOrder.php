<?php

namespace common\models\strategy;

use Yii;

/**
 * This is the model class for table "crm_order".
 *
 * @property string $visit_id
 * @property string $campaign_id
 * @property string $adgroup_id
 * @property string $keyword_id
 * @property string $ad_id
 * @property integer $order_id
 * @property integer $date
 */
class CrmOrder extends \common\models\BehaviorModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id', 'adgroup_id', 'keyword_id', 'ad_id', 'order_id', 'date'], 'integer'],
            [['visit_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'visit_id' => 'Visit ID',
            'campaign_id' => 'Campaign ID',
            'adgroup_id' => 'Adgroup ID',
            'keyword_id' => 'Keyword ID',
            'ad_id' => 'Ad ID',
            'order_id' => 'Order ID',
            'date' => 'Date',
        ];
    }
}
