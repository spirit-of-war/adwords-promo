<?php

namespace common\models\strategy;

use common\models\Account;
use common\models\structure\Ad;
use common\models\structure\Adgroup;
use common\models\structure\Campaign;
use common\models\Statistics;
use common\models\structure\Keyword;
use common\models\structure\Layer;
use Yii;
use common\models\Task;

/**
 * This is the model class for table "rule".
 *
 * @property integer $id
 * @property string $name
 * @property integer $layer
 * @property integer $frequency
 * @property double $change
 * @property integer $account_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $action
 * @property integer $instant_start
 * @property integer $weekday
 * @property integer $monthday
 * @property integer $onceday
 * @property integer $state
 * @property integer $min_bid
 * @property integer $max_bid
 * @property string $sql
 *
 * @property Condition[] $conditions
 * @property Account $account
 * @property RuleCampaign[] $ruleCampaigns
 */
class Rule extends \common\models\BehaviorModel
{
    const TYPE = 0;

    const FREQ_DAY = 0;
    const FREQ_WEEK = 1;
    const FREQ_MONTH = 2;
    const FREQ_ONCE = 3;

    const ACTION_CHANGE_PERCENT = 1;
    const ACTION_CHANGE_CONSTANT = 2;
    const ACTION_CHANGE_POSITION = 3;

    const WEEKDAY_SUNDAY = 0;
    const WEEKDAY_MONDAY = 1;
    const WEEKDAY_TUESDAY = 2;
    const WEEKDAY_WEDNSDAY = 3;
    const WEEKDAY_THURSDAY = 4;
    const WEEKDAY_FRIDAY = 5;
    const WEEKDAY_SATTURDAY = 6;

    public $conditions = [];
    public $campaignIds = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['layer', 'frequency', 'account_id', 'conditions', 'action', 'instant_start', 'state'], 'required'],
            [['layer', 'frequency', 'account_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'action', 'weekday', 'monthday', 'instant_start', 'onceday', 'state', 'position', 'type'], 'integer'],
            [['change', 'min_bid', 'max_bid'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['account_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'layer' => 'Layer',
            'frequency' => 'Frequency',
            'change' => 'Change',
            'account_id' => 'Account ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConditions($json = false)
    {
        $conditions = $this->hasMany(Condition::className(), ['rule_id' => 'id'])->where('disjunction > -1');

        if ($json) {
            $json = [];
            foreach ($conditions->all() as $condition) {
                if (!array_key_exists($condition->disjunction, $json))
                    $json[$condition->disjunction] = [
                        'id' => $condition->disjunction,
                    ];
                $json[$condition->disjunction]['conds'][] = [
                    'id' => $condition->id,
                    'index' => $condition->index,
                    'action' => $condition->action,
                    'value' => $condition->value,
                    'period' => $condition->period,
                ];
            }
            $json = array_values($json);

            return json_encode($json);
        }

        return $conditions;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleCampaigns()
    {
        return $this->hasMany(RuleCampaign::className(), ['rule_id' => 'id']);
    }

    public function getCampaigns($json = false)
    {
        $campaigns = $this->getRuleCampaigns()->all();
        if (count($campaigns) > 0) {
            $campaigns = array_map(function ($campaign) {
                return $campaign->campaign_id;
            }, $campaigns);
            $campaigns = Campaign::findAll([
                'id' => $campaigns,
                'account_id' => $this->account_id
            ]);
        } else {
            $campaigns = Campaign::findAll([
                'account_id' => $this->account_id
            ]);
        }
        if ($json) {
            $campaigns = array_map(function ($campaign) {
                return $campaign->id;
            }, $campaigns);
            return json_encode($campaigns);
        } else {
            return $campaigns;
        }
    }

    public function getConditionElements($limit = false, $offset = false)
    {
        return Yii::$app->db->createCommand($this->getSql(false, $limit, $offset))->queryAll();
    }

    public function getConditionElementsQuantity()
    {
        return Yii::$app->db->createCommand($this->getSql(true))->queryScalar();
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        parent::save($runValidation, $attributeNames);
        if (!$this->errors) {
            array_map(function ($cond) {
                $cond->delete();
            }, $this->getConditions()->all());

            foreach ($this->conditions as $disjunction) {
                foreach ($disjunction['conds'] as $conjunction) {
                    $condition = new Condition([
                        'index' => $conjunction['index'],
                        'action' => $conjunction['action'],
                        'value' => $conjunction['value'],
                        'period' => $conjunction['period'],
                        'disjunction' => $disjunction['id'],
                        'rule_id' => $this->id
                    ]);
                    $condition->save();
                }
            }

            if (!$this->isNewRecord)
                RuleCampaign::deleteAll([
                    'rule_id' => $this->id
                ]);
            $campaigns = Campaign::findAll([
                'account_id' => $this->account_id,
                'id' => $this->campaignIds
            ]);
            $allCampaigns = Campaign::findAll([
                'account_id' => $this->account_id,
            ]);
            if (count($campaigns) != count($allCampaigns)) {
                $ruleCamps = [];
                foreach ($campaigns as $campaign)
                    $ruleCamps[] = [
                        $this->id,
                        $campaign->id
                    ];
                Yii::$app->db->createCommand()->batchInsert(
                    RuleCampaign::tableName(),
                    ['rule_id', 'campaign_id'],
                    $ruleCamps
                )->execute();
            }
        }
    }

    protected function getSql($getQuantityOnly = false, $limit = false, $offset = false)
    {
        $where = [];
        foreach ($this->getConditions()->all() as $condition) {
            if (!array_key_exists($condition->disjunction, $where))
                $where[$condition->disjunction] = [];
            $expression = Index::getIndex($condition['index'])['expression'];
            $expression = sprintf(
                '(select %s from %s s where s.%s_id = c.id and start_date >= %s and end_date <= %s) %s %s',
                $expression,
                Statistics::tableName(),
                Keyword::tableName(),
                $condition->startDate,
                $condition->endDate,
                $condition->action,
                $condition->value
            );
            $where[$condition->disjunction][] = $expression;
        }
        $where = array_map(function ($disjunction) {
            return implode(' and ', $disjunction);
        }, $where);
        $where = implode(PHP_EOL . ' or ' . PHP_EOL, $where);

        $campaignIdField = $this->layer == Layer::LAYER_CAMPAIGNS ? 'id' : 'campaign_id';

        $campaigns = $this->getRuleCampaigns()->all();
        if ($campaigns) {
            $campaigns = array_map(function ($campaign) {
                return $campaign->campaign_id;
            }, $campaigns);
            $campaigns = $campaigns ? 'c.' . $campaignIdField . ' in (' . implode(',', $campaigns) . ') and' : '';
        } else {
            $campaigns = '';
        }

        return sprintf('select %s from %s c where c.account_id = %s and %s (%s) %s %s',
            $getQuantityOnly ? 'count(*)' : 'id, source_id',
            Keyword::tableName(),
            $this->account_id,
            $campaigns,
            $where,
            $limit !== false ? 'limit ' . $limit : '',
            $offset !== false ? 'offset ' . $offset : '');
    }

    public function delete()
    {
        $ruleCampaigns = array_map(function ($ruleCampaign) {
            return $ruleCampaign->id;
        }, $this->getRuleCampaigns()->all());
        Yii::$app->db->createCommand()->delete(RuleCampaign::tableName(), ['id' => $ruleCampaigns])->execute();

        $conds = array_map(function ($cond) {
            return (int)$cond->id;
        }, $this->getConditions()->all());
        Yii::$app->db->createCommand()->delete(Condition::tableName(), ['id' => $conds])->execute();

        return parent::delete();
    }

    public function execute()
    {
        $task = new Task([
            'type' => Task::TYPE_RULE,
            'status' => Task::STATUS_RUNNING,
            'object_id' => $this->id
        ]);
        $task->save();

        $q = $this->getConditionElementsQuantity();

        if ($q > 0) {
            $offset = 0;
            $limit = 5000;
            $steps = ceil($q / $limit);
            for ($i = 0; $i < $steps; $i++) {
                $elements = $this->getConditionElements($limit, $offset);

                if ($this->action == self::ACTION_CHANGE_CONSTANT)
                    $this->account->export->setBidsConstantValue($elements, $this->change, $task->id);
                elseif ($this->action == self::ACTION_CHANGE_PERCENT)
                    $this->account->export->setBidsByPercent($elements, $this->change, $task->id, $this->min_bid, $this->max_bid);
                elseif ($this->action == self::ACTION_CHANGE_POSITION)
                    $this->account->export->setBidsByPosition($this->position, $elements, $this->change, $task->id,
                        $this->min_bid, $this->max_bid);

                $offset = $offset + $limit;
            }
        }

        $task->increaseCompleteness(100);
    }

    public static function findAll($condition)
    {
        if (is_array($condition) && !array_key_exists('type', $condition))
            $condition['type'] = static::TYPE;

        return parent::findAll($condition);
    }
}
