<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 08.08.16
 * Time: 14:52
 */

namespace common\models\strategy;


use common\models\Account;

class Index
{
    public static $tables = [
        'c' => 'campaign',
        's' => 'statistics'
    ];

    public static $actionNumeric = [
        '>', '>=', '=', '!=', '<=', '<'
    ];

    public static $actionString = ['%%', '=', '!='];

    public static function getAll()
    {
        return [
            [
                'group' => 'traffic',
                'title' => 'Трафик',
                'indexes' => [
                    [
                        'name' => 'clicks',
                        'title' => 'Клики',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(sum(s.clicks_search + s.clicks_context), 0)'
                    ],
                    [
                        'name' => 'shows',
                        'title' => 'Показы',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(sum(s.shows_search + s.shows_context), 0)'
                    ],
                    [
                        'name' => 'ctr',
                        'title' => 'CTR, %',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(sum(s.clicks_search + s.clicks_context) / sum(s.shows_search + s.shows_context) * 100, 0)'
                    ],
                    [
                        'name' => 'sum',
                        'title' => 'Затраты',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(sum(s.sum_search + s.sum_context), 0)'
                    ],
                    [
                        'name' => 'avarage_clicks_cost',
                        'title' => 'Средняя цена клика',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(sum(s.sum_search + s.sum_context) / sum(s.clicks_search + s.clicks_context), 0)'
                    ],
                    [
                        'name' => 'average_position',
                        'title' => 'Средняя позиция',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(sum(s.sum_search + s.sum_context) / sum(s.clicks_search + s.clicks_context), 0)',
                        'available' => [Account::TYPE_GOOGLE]
                    ]
                ]
            ],
            [
                'group' => 'metrika',
                'title' => 'Метрика',
                'indexes' => [
                    [
                        'name' => 'metrika_conversions_first',
                        'title' => 'Конверсии (первый переход)',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.metrika_conversions_first), 0)'
                    ],
                    [
                        'name' => 'metrika_conversions_last',
                        'title' => 'Конверсии (последний переход)',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.metrika_conversions_last), 0)'
                    ],
                    [
                        'name' => 'metrika_conversions_lastsign',
                        'title' => 'Конверсии (последний значимый переход)',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.metrika_conversions_lastsign), 0)'
                    ],
                    [
                        'name' => 'metrika_conversions_cost_first',
                        'title' => 'Стоимость конверсии (первый переход)',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.sum_search + s.sum_context) / SUM(s.metrika_conversions_first), 0)'
                    ],
                    [
                        'name' => 'metrika_conversions_cost_last',
                        'title' => 'Стоимость конверсии (последний переход)',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.sum_search + s.sum_context) / SUM(s.metrika_conversions_last), 0)'
                    ],
                    [
                        'name' => 'metrika_conversions_cost_lastsign',
                        'title' => 'Стоимость конверсии (последний значимый переход)',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.sum_search + s.sum_context) / SUM(s.metrika_conversions_lastsign), 0)'
                    ],
                    [
                        'name' => 'metrika_revenue',
                        'title' => 'Выручка',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.revenue), 0)'
                    ],
                    [
                        'name' => 'metrika_roi',
                        'title' => 'ROI, %',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL((IFNULL(SUM(s.revenue), 0) - IFNULL(SUM(s.sum_search + s.sum_context), 0)) / (sum(s.sum_search + s.sum_context)) * 100, 0)'
                    ],
                    [
                        'name' => 'metrika_profit',
                        'title' => 'Прибыль',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.revenue), 0) - IFNULL(SUM(s.sum_search + s.sum_context), 0)'
                    ]
                ]
            ],
            [
                'group' => 'crm',
                'title' => 'CRM',
                'indexes' => [
                    [
                        'name' => 'crm_conversions',
                        'title' => 'Количество конверсий',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.crm_conversions), 0)'
                    ],
                    [
                        'name' => 'crm_conversions_cost',
                        'title' => 'Стоимость конверсий',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.sum_search + s.sum_context) / SUM(s.crm_conversions), 0)'
                    ],
                    [
                        'name' => 'crm_revenue',
                        'title' => 'Выручка',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(sum(s.crm_revenue), 0)'
                    ],
                    [
                        'name' => 'crm_roi',
                        'title' => 'ROI, %',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL((IFNULL(SUM(s.crm_revenue), 0) - IFNULL(SUM(s.sum_search + s.sum_context), 0)) / sum(s.sum_search + s.sum_context) * 100, 0)'
                    ],
                    [
                        'name' => 'crm_profit',
                        'title' => 'Прибыль',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(SUM(s.crm_revenue), 0) - IFNULL(SUM(s.sum_search + s.sum_context), 0)'
                    ],
                    [
                        'name' => 'crm_assoc_conversions',
                        'title' => 'Ассоциированные конверсии',
                        'action' => self::$actionNumeric,
                        'expression' => 'IFNULL(sum(s.crm_assoc_conversions), 0)'
                    ]
                ]
            ]
        ];
    }

    /**
     * @param bool $json
     * @param bool|array|string $indexNames
     * @return array|string
     */
    public static function getIndexes($json = false, $indexNames = false)
    {
        $indexes = self::getAll();

        if (is_string($indexNames)) {
            foreach ($indexes as $group)
                foreach ($group['indexes'] as $index)
                    if ($index['name'] == $indexNames)
                        return $index;
        } elseif (is_array($indexNames)) {
            $indexesRaw = [];
            foreach ($indexes as $group)
                foreach ($group['indexes'] as $index)
                    if (array_search($index['name'], $indexNames) !== false)
                        $indexesRaw[] = $index;
            $indexes = $indexesRaw;
        }

        return $json ? json_encode($indexes) : $indexes;
    }

    public static function getIndex($name)
    {
        foreach (self::getAll() as $group)
            foreach ($group['indexes'] as $index)
                if ($index['name'] == $name)
                    return $index;

        return null;
    }

    public static function getGroup($indexName)
    {
        $indexes = self::getAll();
        foreach ($indexes as $group)
            foreach ($group['indexes'] as $index)
                if ($index['name'] == $indexName)
                    return $group;
        return null;
    }

    /***
     * Возвращает название индекса в формате "Группа: Индекс"
     * @param $indexName
     * @return string
     */
    public static function getFullName($indexName)
    {
        $indexes = self::getAll();
        foreach ($indexes as $group)
            foreach ($group['indexes'] as $index)
                if ($index['name'] == $indexName)
                    return $group['title'] . ': ' . $index['title'];
    }

    public static function getExpression($indexName)
    {
        $indexName = self::getIndex($indexName);
        if ($indexName !== null)
            return $indexName['expression'];

        return null;
    }

    public static function getAllByAccountType($type)
    {
        $indexesTmp = [];
        foreach (self::getAll() as $group) {
            $groupTmp = $group;
            $groupTmp['indexes'] = [];
            foreach ($group['indexes'] as $index)
                if (array_key_exists('available', $index) && in_array($type, $index['available'])
                    || !array_key_exists('available', $index))
                    $groupTmp['indexes'][] = $index;
            $indexesTmp[] = $groupTmp;
        }

        return $indexesTmp;
    }

    public static function getAllInJson()
    {
        return json_encode(self::getAll());
    }

    public static function getStringForSql($indexes)
    {
        $indexesTmp = [];
        foreach ($indexes as $indexName)
            $indexesTmp[$indexName] = self::getExpression($indexName);

        $indexStr = array_map(function ($key, $val) {
            return $val . ' ' . $key;
        }, array_keys($indexesTmp), array_values($indexesTmp));

        return implode(',', $indexStr);
    }
}