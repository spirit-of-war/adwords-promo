<?php

namespace common\models\strategy;

use common\models\Account;

/**
 * This is the model class for table "report_setting".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $layer
 * @property string $field
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Account $account
 */
class ReportSetting extends \common\models\BehaviorModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'layer', 'field'], 'required'],
            [['account_id', 'layer', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['field'], 'string', 'max' => 255],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['account_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'layer' => 'Layer',
            'field' => 'Field',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'account_id']);
    }
}
