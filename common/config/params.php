<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
	'adminName' => 'Анатолий',
	'domain' => 'http://profitpanda.ru/',
	'domainAdmin' => 'http://admin.profitpanda.ru/',
	'defaultSEOTitle' => '| ProfitPanda',
    'user.passwordResetTokenExpire' => 3600,
    'services' => [
        'yandex' => [
            'direct' => array_merge([
                    'url' => 'https://api.direct.yandex.com/json/v5/',
                    'url-v4' => 'https://api.direct.yandex.ru/live/v4/json/',
                    'utm' => '&campaign_id={campaign_id}&adgroup_id={gbid}&keyword_id={phrase_id}&ad_id={ad_id}&position={position}'
                ],
                require(__DIR__ . '/directAccess.php')
            ),
            'metrika' => [
                'url' => 'https://api-metrika.yandex.ru/'
            ],
            'oauth' => [
                'url' => 'https://oauth.yandex.ru/'
            ],
            'passport' => [
                'url' => 'https://login.yandex.ru/'
            ]
        ],
        'google' => [
            'adwords' => [
                'userAgent' => 'Panda',
                'developerToken' => 'x7_qB-rlUo46SN8tdKCSFg',
                'trackingUrlTemplate' => 'utm_source=google&utm_medium=cpc&network={network}&placement={placement}&position={adposition}&adid={creative}&match={matchtype}&keyword={keyword}&gid={adgroupid}&campaignid={campaignid}',


                'clientId' => '176515811443-flq6if50ga6c6i2gs7f5s1nnmkg1fe06.apps.googleusercontent.com',
                'clientSecret' => '9g6lxc9ARHr85_6dZL9HOzQF',
                'redirectUrl' => 'http://admin.profitpanda.ru/company/get-token-adwords',
            ]
        ]
    ]
];