<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;
$baseUrl = str_replace('/backend/web', '', (new Request)->getBaseUrl());
$domainName = 'profitpanda.ru';
return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gii' => 'yii\gii\Module',
        'reportico' => [
            'class' => 'reportico\reportico\Module' ,
            'controllerMap' => [
                'reportico' => 'reportico\reportico\controllers\ReporticoController',
                'mode' => 'reportico\reportico\controllers\ModeController',
                'ajax' => 'reportico\reportico\controllers\AjaxController',
            ]
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'components' => [
        'request' => [
            'baseUrl' => $baseUrl,
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => ".".$domainName,
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => true,
                'path' => '/',
                'domain' => ".".$domainName,
            ],
        ],
        'session' => [
            'cookieParams' => [
                'domain' => ".".$domainName,
                'path' => '/',
                'httpOnly' => true,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'account/check-dead-links' => 'account/check-dead-links',
                'account/view-dead-links' => 'account/view-dead-links',
                'account/<dimension:\S+>/<accountID:\d+>' => 'account/view',
                'company/<id:\d+>/<tab:\S+>' => 'company/view',
                'company/<id:\d+>' => 'company/view',
                'company/update/<id:\d+>' => 'company/update',
                'company/delete/<id:\d+>' => 'company/delete',
                'admin/stat/<accountID:\d+>' => 'admin/stat',
                'api/get-report/<dimension:\S+>/<accountID:\d+>' => 'api/get-report',
                'api/get-menu-items/<parentType:\S+>/<parentId:\d+>' => 'api/get-menu-items',
				'admin/page/<id:\d+>' => 'admin/page',
				'admin/page/ckupload' => 'admin/ckupload',
				'admin/page/add' => 'admin/page-add',
				'admin/page/update/<id:\d+>' => 'admin/page-update',
            ],
        ],
    ],
    'params' => $params,
    'layout' => 'gantelella'
];
