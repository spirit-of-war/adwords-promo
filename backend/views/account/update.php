<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\widgets\Panel;
use backend\widgets\TabMenu;
use common\models\structure\Layer;

/* @var $this yii\web\View */
/* @var $account common\models\Account */

$this->title = 'Настройки';
$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;
$this->registerJs('var counters = ' . json_encode($counters) . ';', \yii\web\View::POS_HEAD);
$this->registerJs('var selectedGoals = ' . json_encode($selectedGoals) . ';', \yii\web\View::POS_HEAD);
$this->registerJsFile('/js/account/update.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerCssFile('/css/account/update.css');
?>
<?php Panel::begin([
    'header' => $this->title,
]);
echo TabMenu::widget(['account' => $account, 'dimension' => Layer::LAYER_NAME_SETTING])
?>
<div class="account-update">

    <div class="col-md-6 col-xs-12">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal form-label-left']]); ?>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Счетчик</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="form-control counters" name="counter">
                    <?php
                    foreach ($counters as $counter)
                        printf(
                            '<option value="%s">%s</option>',
                            $counter['id'],
                            $counter['id'] . ' - ' . $counter['name']
                        );
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Цели</label>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="goals"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success">Сохранить</button>
            </div>
        </div>
        <?php
        ActiveForm::end();
        ?>
    </div>

</div>
<?php Panel::end();?>
