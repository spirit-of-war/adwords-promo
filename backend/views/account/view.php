<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 15.07.16
 * Time: 13:01
 */

/* @var $account common\models\Account */
/* @var $dimension string */

use yii\helpers\Url;
use backend\widgets\Panel;
use backend\widgets\TabMenu;
use common\models\structure\Layer;
use backend\assets\DateRangeAsset;
use backend\assets\GridAsset;
use common\models\strategy\Index;
use backend\assets\TunableChartsAsset;
use backend\assets\jQueryUIAsset;
use yiister\gentelella\widgets\grid\GridView;

$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

GridAsset::register($this);
TunableChartsAsset::register($this);
jQueryUIAsset::register($this);
$this->registerCssFile('/css/account/view.css');
$this->registerCssFile('/css/tableFieldsSelector.css');
$this->registerJsFile('/js/account/view.js', ['depends' => 'yii\web\YiiAsset']);

DateRangeAsset::register($this);
$this->registerCss('
.daterangepicker.opensright .ranges, 
.daterangepicker.opensright .calendar, 
.daterangepicker.openscenter .ranges, .daterangepicker.openscenter .calendar {float: left}
');
$this->registerJs("

$('.daterange').daterangepicker({
        locale: {
          format: 'DD-MM-YYYY'
        },
    'ranges': {$dateRange['ranges']},
    'alwaysShowCalendars': true,
    'startDate': '{$dateRange['startDate']}',
    'endDate': '{$dateRange['endDate']}',
    'minDate': '{$dateRange['minDate']}',
    'maxDate': '{$dateRange['maxDate']}',
});

$('.daterange').on('apply.daterangepicker', function(ev, picker) {
    $('input[name=\"startDate\"]').val(picker.startDate.format('DD-MM-YYYY'));
    $('input[name=\"endDate\"]').val(picker.endDate.format('DD-MM-YYYY'));
    $('.table-form').submit();
});;
");
$js = sprintf(
    'var accountID = %s;
     var view = "%s";',
    $account->id,
    $dimension);
$this->registerJs($js, \yii\web\View::POS_HEAD);

$gridViewFields = [
    [
        'attribute' => 'clicks',
        'label' => 'Трафик: Клики',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['clicks'], 2);
        }
    ],
    [
        'attribute' => 'shows',
        'label' => 'Трафик: Показы',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['shows'], 2);
        }
    ],
    [
        'attribute' => 'ctr',
        'label' => 'Трафик: CTR, %',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['ctr'], 2);
        }
    ],
    [
        'attribute' => 'sum',
        'label' => 'Трафик: Затраты',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['sum'], 2);
        }
    ],
    [
        'attribute' => 'avarage_clicks_cost',
        'label' => 'Трафик: Ср. цена клика',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['avarage_clicks_cost'], 2);
        }
    ],
    [
        'attribute' => 'average_position',
        'label' => 'Трафик: Ср. позиция',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['average_position'], 2);
        }
    ],

    [
        'attribute' => 'metrika_revenue',
        'label' => 'Метрика: Доход',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_revenue'], 2);
        }
    ],
    [
        'attribute' => 'metrika_conversions_first',
        'label' => 'Метрика: Конверсии (первый переход)',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_conversions_first'], 2);
        }
    ],
    [
        'attribute' => 'metrika_conversions_last',
        'label' => 'Метрика: Конверсии (последний переход)',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_conversions_last'], 2);
        }
    ],
    [
        'attribute' => 'metrika_conversions_lastsign',
        'label' => 'Метрика: Конверсии (последний значимый переход)',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_conversions_lastsign'], 2);
        }
    ],
    [
        'attribute' => 'metrika_conversions_cost_first',
        'label' => 'Метрика: Стоимость конверсии (первый переход)',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_conversions_cost_first'], 2);
        }
    ],
    [
        'attribute' => 'metrika_conversions_cost_last',
        'label' => 'Метрика: Стоимость конверсии (последний переход)',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_conversions_cost_last'], 2);
        }
    ],
    [
        'attribute' => 'metrika_conversions_cost_lastsign',
        'label' => 'Метрика: Стоимость конверсии (последний значимый переход)',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_conversions_cost_lastsign'], 2);
        }
    ],
    [
        'attribute' => 'metrika_roi',
        'label' => 'Метрика: ROI, %',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_roi'], 2);
        }
    ],
    [
        'attribute' => 'metrika_profit',
        'label' => 'Метрика: Прибыль',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['metrika_roi'], 2);
        }
    ],

    [
        'attribute' => 'crm_conversions',
        'label' => 'CRM: Конверсии',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['crm_conversions'], 2);
        }
    ],
    [
        'attribute' => 'crm_conversions_cost',
        'label' => 'CRM: Стоимость конверсий',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['crm_conversions_cost'], 2);
        }
    ],
    [
        'attribute' => 'crm_revenue',
        'label' => 'CRM: Выручка',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['crm_revenue'], 2);
        }
    ],
    [
        'attribute' => 'crm_roi',
        'label' => 'CRM: ROI, %',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['crm_roi'], 2);
        }
    ],
    [
        'attribute' => 'crm_profit',
        'label' => 'CRM: Прибыль',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['crm_profit'], 2);
        }
    ],
    [
        'attribute' => 'crm_assoc_conversions',
        'label' => 'CRM: Ассоциированные конверсии',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['crm_assoc_conversions'], 2);
        }
    ]
];

$this->registerCss('
.filter-field {
    width: 100%;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}');

Panel::begin();
?>
<input type="hidden" id="accountId" value="<?= $account->id ?>">
<div class="" role="tabpanel" data-example-id="togglable-tabs">

    <?= TabMenu::widget(['account' => $account, 'dimension' => $dimension]) ?>

    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
            <div class="chart-fields-selector">
                <i class="fa fa-wrench"></i>
                <div class="chart-fields-selector-indexes">
                    <?php
                    foreach (Index::getAllByAccountType($account->advert_type) as $group)
                        if ($group['group'] != 'common')
                            foreach ($group['indexes'] as $index)
                                printf(
                                    '<div class="checkbox"><label><input type="checkbox" value="%s" %s> %s
                                                </label></div>',
                                    $index['name'],
                                    in_array($index['name'], $chartSettings) ? 'checked' : '',
                                    $group['title'] . ': ' . $index['title']
                                );
                    ?>
                    <a class="btn save">Сохранить</a>
                    <a class="btn cancel">Отмена</a>
                </div>
            </div>
            <div id="googleChartsPlaceholder"></div>
            <form class="table-form">
                <div class="">
                    <div class="input-prepend input-group">
                        <span class="add-on input-group-addon daterange-icon">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" style="width: 200px" name="dates" class="form-control active daterange">

                        <input type="hidden" name="startDate" class="filter-field" value="<?=$dateRange['startDate'] ?>">
                        <input type="hidden" name="endDate" class="filter-field" value="<?=$dateRange['startDate'] ?>">
                        <input type="hidden" class="filter-field" name="ul_id">
                    </div>
                    <?= \yii\helpers\Html::a(' Очистить все фильтры', [
                        'account/view',
                        'dimension' => $dimension,
                        'accountID' => $account->id,
                        'clean' => 'all'
                    ], ['class' => 'clearFilters']) ?>
                </div>
                <div class="table-fields-selector">
                    <i class="fa fa-wrench"></i>
                    <div class="table-fields-selector-indexes">
                        <ul>
                            <?php
                            foreach ($reportSettings as $field) {
                                $index = Index::getIndex($field);
                                printf(
                                    '<li class="checkbox"><label><input type="checkbox" checked value="%s" %s> %s
                                                    <i class="fa fa-reorder"></i></label></li>',
                                    $index['name'],
                                    $index['name'] == 'clicks' ? 'checked' : '',
                                    Index::getGroup($field)['title'] . ': ' . $index['title']
                                );
                            }
                            foreach (Index::getAllByAccountType($account->advert_type) as $group) {
                                foreach ($group['indexes'] as $index) {
                                    if (!in_array($index['name'], $reportSettings))
                                        printf(
                                            '<li class="checkbox"><label><input type="checkbox" value="%s"> %s
                                                    <i class="fa fa-reorder"></i></label></li>',
                                            $index['name'],
                                            $group['title'] . ': ' . $index['title']
                                        );
                                }
                            }
                            ?>
                        </ul>
                        <a class="btn save">Сохранить</a>
                        <a class="btn cancel">Отмена</a>
                    </div>
                </div>
                <div class="report-container">
                <?= $this->render($dimension, [
                    'account' => $account,
                    'dataProvider' => $dataProvider,
                    'gridViewFields' => $gridViewFields,
                    'totalRow' => $totalRow,
                    'totalFilterRow' => $totalFilterRow,
                    'reportSettings' => $reportSettings
                ]) ?>
                </div>
                <div class="rows-quantity">
                    Выводить <select class="per-page" name="per-page">
                        <option value="10">10 строк</option>
                        <option value="30">30 строк</option>
                        <option value="50">50 строк</option>
                        <option value="100">100 строк</option>
                        <option value="200">200 строк</option>
                        <option value="500">500 строк</option>
                        <option value="1000">1000 строк</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
Panel::end();

function getGridView($dataProvider, $columns, $reportSettings, $view, $totalRow, $totalFilterRow)
{
    $displayColumns = [];
    $indexes = [];
    foreach (Index::getAll() as $group)
        foreach ($group['indexes'] as $index)
            $indexes[] = $index['name'];
    foreach ($columns as $column)
        if (!in_array($column['attribute'], $indexes))
            $displayColumns[] = $column;

    $columns = array_merge(
        $displayColumns,
        array_map(function ($field) use ($columns) {
            foreach ($columns as $column)
                if ($column['attribute'] == $field)
                    return $column;
        }, $reportSettings)
    );

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'pager' => [
            'firstPageLabel' => '|«',
            'lastPageLabel'  => '»|'
        ],
    ]);

    filterFields($columns, $view);
    addTotalRows($columns, $totalRow, $view, 'Итого');
    if (array_diff($totalRow, $totalFilterRow))
        addTotalRows($columns, $totalFilterRow, $view, 'Итого по фильтрации');
}

function filterFields($fields, $view)
{
    $js = "$('<tr></tr>').appendTo('.dataTable thead');";
    foreach ($fields as $field) {
        $td = sprintf('<td><input type="text" class="filter-field" placeholder="%s" name="%s" /></td>',
            is_array($field) && array_key_exists('label', $field) ? $field['label'] : $field,
            is_array($field) && array_key_exists('attribute', $field) ? $field['attribute'] : $field);
        $js .= sprintf("$('%s').appendTo('.dataTable thead tr:nth-child(2)');", $td);
    }

    $js .= "
    
    var query = location.search.substr(1);
    var result = {};
    query.split('&').forEach(function(part) {
    if (part) {
        var item = part.split('=');
        result[item[0]] = decodeURIComponent(item[1]);
    }
    });
    
    
    
    $.each(result, function(key, value) {
        if (key != 'dates')
            $('[name=\"' + key + '\"]').val(value);
    });
    
    $('.per-page').on('change', function() { $('.table-form').submit(); });
    
    $('.filter-field').on('keypress', function(e) {
        if(e.keyCode == 13)
            $('.table-form').submit();
    });
     
    ";
    $view->registerJs($js);

}

function addTotalRows($fields, $totalRow, $view, $caption = 'Итого')
{
    $totalRowRaw = [];
    foreach ($fields as $field)
        if (array_key_exists($field['attribute'], $totalRow))
            $totalRowRaw[$field['attribute']] = $totalRow[$field['attribute']];
    $totalRow = $totalRowRaw;

    $emptyFields = count($fields) - count($totalRow);
    $emptyFields = sprintf('<td colspan="%s">%s:</td>', $emptyFields, $caption);
    $fields = array_map(function ($field) use($totalRow) {
        if (array_key_exists($field['attribute'], $totalRow))
            return '<td>' . round($totalRow[$field['attribute']], 2) . '</td>';
    }, $fields);
    $totalRow = sprintf('<tr class="total-row">%s %s</tr>', $emptyFields, implode('', $fields));
    $totalRow = sprintf("$('%s').appendTo('.dataTable thead');", $totalRow);

    $view->registerJs($totalRow);
}