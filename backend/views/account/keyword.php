<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 15.07.16
 * Time: 14:23
 */

use common\models\Account;
use yii\helpers\Html;
use backend\assets\PNotify;
use yii\web\View;

/* @var $account common\models\Account */

$this->registerJs('var accountID = ' . $account->id . ';', View::POS_HEAD);
$keywordsJS = $account->advert_type == Account::TYPE_YANDEX ? 'yandex' : 'google';
$this->registerJsFile(sprintf('/js/account/keyword-%s.js', $keywordsJS), ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('/js/account/keyword.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerCssFile('/css/account/keyword.css');
PNotify::register($this);

$ulFields = [];
$adgroupField = [
    'attribute' => 'adgroup_name',
    'label' => 'Группа'
];
$campaignField = [
    'attribute' => 'campaign_name',
    'label' => 'Кампания'
];
if (Yii::$app->session->get('campaignID', false) !== false)
    $ulFields = [$adgroupField];
elseif (Yii::$app->session->get('campaignID', false) === false && Yii::$app->session->get('adgroupID', false) === false)
    $ulFields = [$adgroupField, $campaignField];

$adwordsFields = [];

if (isset($account['advert_type']) && intval($account['advert_type']) === Account::TYPE_GOOGLE) {
    $adwordsFields[] = [
        'attribute' => 'average_position',
        'label' => 'Трафик: Ср. позиция',
        'format' => 'raw',
        'value' => function ($row) {
            return round($row['average_position'], 2);
        }
    ];
}

$bids = [
    [
        'attribute' => 'bid_search',
        'label' => 'Ставка' . ($account->advert_type == Account::TYPE_YANDEX ? ' на поиске' : ''),
        'format' => 'raw',
        'value' => function ($row) {
            return
                sprintf(
                    '<input type="text" value="%s" class="bid-changer bid-changer-%s" data-keyword="%s"
                        data-bidtype="search" />
                     <div class="auction-bids-selector auction-bids-selector-%s" data-keyword="%s"></div>',
                $row['bid_search'] / 1000000,
                $row['id'], $row['id'], $row['id'], $row['id']);
        }
    ]
];
if ($account->advert_type == Account::TYPE_YANDEX)
    $bids[] = [
        'attribute' => 'bid_context',
        'label' => 'Ставка в РСЯ',
        'format' => 'raw',
        'value' => function ($row) {
            return
                sprintf('<input type="text" value="%s" class="bid-changer" data-keyword="%s" data-bidtype="context" />',
                    $row['bid_context'] / 1000000, $row['id']);
        }
    ];

$columns = array_merge([
    [
        'attribute' => 'source_id',
        'label' => 'ID'
    ],
    [
        'attribute' => 'keyword',
        'label' => 'Ключевое слово',
        'format' => 'raw',
        'value' => function ($keyword) {
            $common = $keyword['keyword'];
            $negative = $keyword['negative'];
            $negative = $negative ? '<a class="negative-show" data-keyword="' . $keyword['id'] . '">Минус-слова</a>'
                            . '<div class="negative negative-' . $keyword['id'] . '">
                                <div><i class="fa fa-close"></i></div>' . $negative . '</div>' : '';
            return $common . $negative
                . '<div class="auction-bids auction-bids-' . $keyword['id'] . '"data-keyword="' . $keyword['id'] .
                  '"></div>';
        }
    ]
], $ulFields, $adwordsFields, $bids, $gridViewFields);

getGridView($dataProvider, $columns, $reportSettings, $this, $totalRow, $totalFilterRow);