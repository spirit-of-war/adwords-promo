<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 15.07.16
 * Time: 12:36
 */

use yii\helpers\Html;

$columns = array_merge([
    [
        'attribute' => 'source_id',
        'label' => 'ID'
    ],
    [
        'attribute' => 'name',
        'label' => 'Кампания',
        'format' => 'raw',
        'value' => function ($campaign) {
            return Html::a($campaign['name'], [
                'account/view',
                'dimension' => 'adgroup',
                'accountID' => $campaign['account_id'],
                'ul_id' => $campaign['id']
            ]);
        }
    ]
], $gridViewFields);

getGridView($dataProvider, $columns, $reportSettings, $this, $totalRow, $totalFilterRow);