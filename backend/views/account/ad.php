<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 15.07.16
 * Time: 14:23
 */

use common\models\Account;

/* @var $account common\models\Account */

$ulFields = [];
$adgroupField = [
    'attribute' => 'adgroup_name',
    'label' => 'Группа'
];
$campaignField = [
    'attribute' => 'campaign_name',
    'label' => 'Кампания'
];
if (Yii::$app->session->get('campaignID', false) !== false)
    $ulFields = [$adgroupField];
elseif (Yii::$app->session->get('campaignID', false) === false && Yii::$app->session->get('adgroupID', false) === false)
    $ulFields = [$adgroupField, $campaignField];

$columns = array_merge(
    [
        [
            'attribute' => 'source_id',
            'label' => 'ID'
        ],
        [
            'attribute' => 'title',
            'label' => 'Объявление'
        ]
    ],
    $ulFields,
    $gridViewFields);

getGridView($dataProvider, $columns, $reportSettings, $this, $totalRow, $totalFilterRow);