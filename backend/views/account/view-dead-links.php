<?php
use yii\helpers\Html;
use \yii\helpers\Url;
use backend\widgets\Panel;
use backend\widgets\TabMenu;
use common\models\structure\Layer;

$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

$lastCheckedDate = ($account->dead_link_checked ? date('d m Y H:i',$account->dead_link_checked) : 'не проводилась');
$message = '';
if($state == 0){
 $message = 'Нет новых ссылок на проверку';
}

Panel::begin([
    'header' => $this->title,
]);
echo TabMenu::widget(['account' => $account, 'dimension' => Layer::LAYER_NAME_DEAD_LINK]);
?>

<div>
 <?php echo $message ?>
</div>
<br>
<div>
 Дата последней проверки: <?php echo $lastCheckedDate ?>
</div>
<br>
<?php
if($isProcess){
 echo '<div>Проверка началась, пожалуйста дождитесь окончания...</div>';
} else {
 echo Html::a('Начать проверку', [
     'account/check-dead-links',
     'accountID' => $account->id
 ], ['class' => 'btn btn-primary']);
} ?>

<br>
<br>


<?php echo $widget;

Panel::end();


