<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 15.07.16
 * Time: 13:35
 */

$columns = array_merge([
    [
        'attribute' => 'source_id',
        'label' => 'ID'
    ],
    [
        'attribute' => 'name',
        'label' => 'Группа',
        'format' => 'raw',
        'value' => function ($adgroup) {
            return \yii\helpers\Html::a($adgroup['name'], [
                'account/view',
                'dimension' => 'keyword',
                'accountID' => $adgroup['account_id'],
                'ul_id' => $adgroup['id']
            ]);
        }
    ],
    [
        'attribute' => 'ul_name',
        'label' => 'Кампания'
    ]
], $gridViewFields);

getGridView($dataProvider, $columns, $reportSettings, $this, $totalRow, $totalFilterRow);