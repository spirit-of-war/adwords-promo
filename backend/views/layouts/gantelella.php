<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use \yiister\gentelella\widgets\Menu;

$bundle = yiister\gentelella\assets\Asset::register($this);
\backend\assets\AppAsset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-md">
<?php $this->beginBody(); ?>
<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><i class="fa fa-paw"></i> <span>profitpanda.ru</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_info">
                        <span>Добро пожаловать,</span>
                        <?= Yii::$app->user->identity->username ?>
                    </div>
                    <div class="clear"></div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <?php
                        if (array_key_exists('leftMenuItems', $this->params))
                            echo Menu::widget([
                                'items' => $this->params['leftMenuItems']['items']
                            ]);
                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->

            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="/img/text.png" alt="">
                                <?= Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->username ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li>
                                    <?= Html::a('<i class="fa fa-sign-out pull-right"></i> Выйти',
                                        '/site/logout', [
                                        'data' => [
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
                <div class="page-title">

                    <div class="title_left">
                        <div>
                            <?php
                            if (isset($this->params['breadcrumbs'])) {
                                $lastcrumb = end($this->params['breadcrumbs']);
                                foreach ($this->params['breadcrumbs'] as $crumb) {
                                    if ($crumb == $lastcrumb) {
                                        if (is_array($crumb))
                                            echo $crumb['label'];
                                        else
                                            echo $crumb;
                                    } else {
                                        echo Html::a($crumb['label'], $crumb['url']) . ' / ';
                                    }
                                }
                            }
                            ?>
                        </div>
                        <?php
                        if (Yii::$app->session->get('campaignID', false) || Yii::$app->session->get('adgroupID', false)) {
                            $lastcrumb = end($this->params['breadcrumbs']);
                            echo Html::tag('h3', is_array($lastcrumb) ? $lastcrumb['label'] : $lastcrumb);
                        }
                        ?>
                    </div>
                    <div class="title_right"></div>
                </div>
            <div class="clearfix"></div>

            <?= $content ?>
        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>