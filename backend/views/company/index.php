<?php

use yii\helpers\Html;
use yiister\gentelella\widgets\grid\GridView;
use yiister\gentelella\widgets\Panel;
use backend\controllers\AccountController;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проекты';
$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

Panel::begin(['header' => '']);
echo Html::a('Добавить', ['create'], ['class' => 'btn btn-primary']);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'bordered' => false,
    'summary' => false,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Название',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data->name, ['view', 'id' => $data->id]);
            }
        ],
        [
            'attribute' => 'website',
            'format' => 'url',
        ],
        [
            'format' => 'raw',
            'value' => function($data) {
                return Html::a('<span class="fa fa-edit"></span>',
                    ['company/update', 'id' => $data->id]);
            }
        ]
    ],
]);
Panel::end();