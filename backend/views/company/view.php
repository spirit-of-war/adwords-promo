<?php

use yiister\gentelella\widgets\Panel;
use \yii\helpers\Url;
use \common\models\Account;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use \backend\controllers\AccountController;
use \yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $direct common\models\Account */

$this->title = $model->name;
$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;
$this->registerCssFile('/css/company/view.css');
$this->registerJsFile('/js/company/view.js', ['depends' => 'yii\web\YiiAsset']);

Panel::begin(['header' => $model->name]);
?>
    <table class="project">
        <tr>
            <th></th>
            <th>Система контекстной рекламы</th>
            <th>Система веб-аналитики</th>
            <th>CRM-система</th>
        </tr>
        <tr>
            <td>
                <button type="button" class="btn btn-primary">Начать работу</button>
            </td>
            <td>
                <?php if ($direct != null) { ?>
                    <div class="well alert-success">
                        Подключен Яндекс.Директ: <b><?= $direct->advert_login ?></b>
                        <a
                            data-confirm="Вы действительно хотите отключить аккаунт?"
                            href="<?= Url::to(['company/unbind-direct', 'id' => $direct->id])?>">Отключить</a>
                        <?php if ($directClientList !== false) {
                            echo '<div>Данный аккаунт является агентством, аккаунт клиента: ';
                            Modal::begin([
                                'header' => '<h2>Выбор клиента Яндекс.Директ</h2>',
                                'toggleButton' => [
                                    'label' => in_array($direct->client_login, [null, '']) ? 'Выбрать'
                                        : $direct->client_login,
                                    'tag' => 'a'
                                ],
                                'id' => 'directModal'
                            ]);
                            ?>

                            <?php ActiveForm::begin(['action' => 'save-login']); ?>
                            <input type="hidden" value="<?= $direct->id ?>" name="id">
                            <p>Аккаунт Яндекс.Директ: <b><?= $direct->advert_login ?></b>
                                является агентством, выберите клиента:</p>
                            <?php foreach ($directClientList as $client) {
                                printf('<div><label><input type="radio" name="directClient" value="%s" %s> %s</label></div>',
                                    $client,
                                    $client == $direct->client_login ? 'checked' : '',
                                    $client);
                            } ?>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                            <?php ActiveForm::end(); ?>

                            <?php
                            Modal::end();
                            echo '</div>';

                            if (Yii::$app->session->getFlash('systemType', false) === Account::SYSTEM_TYPE_CONTEXT)
                                $this->registerJs('$(\'#directModal\').modal(\'toggle\')');

                        } ?>
                    </div>
                <?php } else { ?>
                    <div class="well">
                        <a class="connect" href="<?= Url::to(['company/connect-direct', 'id' => $model->id])?>">
                            Подключить Яндекс.Директ</a>
                    </div>
                <?php } ?>
            </td>
            <td>
                <?php if ($direct != null && $direct->stat_login != null) { ?>
                    <div class="well alert-success">
                        <?php if ($direct->stat_type == Account::TYPE_YANDEX) { ?>

                            <div>
                                Подключена Яндекс.Метрика: <b><?= $direct->advert_login ?></b>
                                <a
                                    data-confirm="Вы действительно хотите отключить аккаунт?"
                                    href="<?= Url::to(['company/unbind-metrika', 'id' => $direct->id])?>">Отключить</a>
                            </div>

                            <div>
                                Счетчик:
                                <?php
                                Modal::begin([
                                    'header' => '<h2>Выбор счетчика Яндекс.Метрика</h2>',
                                    'toggleButton' => [
                                        'label' => $direct->counter == null ? 'выбрать'
                                            : $direct->counter . ' (' . count($directSelectedGoals) . ' целей)',
                                        'tag' => 'a'
                                    ],
                                    'id' => 'metrikaModal'
                                ]);
                                ?>

                                <?php ActiveForm::begin(['action' => 'save-goals']); ?>
                                <input type="hidden" value="<?= $direct->id ?>" name="id">
                                Счетчик: <select name="counter" class="counter">
                                    <?php foreach ($directMetrikaCounters as $counter) {
                                        printf('<option value="%s" %s>%s</option>',
                                            $counter['id'],
                                            $counter['id'] == $direct->counter ? 'selected' : '',
                                            $counter['id'] . ' - ' . $counter['name']);
                                    } ?>
                                </select>
                                <div class="goals-selector">
                                    Цели:
                                    <?php foreach ($directMetrikaCounters as $counter) { ?>
                                        <div class="goals goals-<?= $counter['id'] ?>">
                                            <?php foreach ($counter['goals'] as $goal) {
                                                printf('<div><label>
                                                    <input type="checkbox" name="goal[]" value="%s" %s /> %s</label></div>',
                                                    $goal['id'],
                                                    in_array($goal['id'], $directSelectedGoals) ? 'checked' : '',
                                                    $goal['name']);
                                            } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                <?php ActiveForm::end(); ?>

                                <?php
                                Modal::end();

                                if (Yii::$app->session->getFlash('systemType', false) === Account::SYSTEM_TYPE_WEBANALYTICS)
                                    $this->registerJs('$(\'#metrikaModal\').modal(\'toggle\')');

                                ?>
                            </div>

                        <?php } else {
                            echo 'Google.Analytics: ' . $direct->advert_login;
                        } ?>
                    </div>
                <?php } else { ?>
                    <div class="well">
                        <?php if ($direct == null) { ?>
                            Сначала подключите Яндекс.Директ
                        <?php } else { ?>
                            <a class="connect" href="<?= Url::to(['company/connect-metrika', 'accountId' => $direct->id])?>">
                                Подключить Яндекс.Метрика</a>
                            или
                            <a class="connect">Подключить Google.Analytics</a>
                        <?php } ?>

                    </div>
                <?php } ?>
            </td>
            <td rowspan="2">
                <div class="well <?= in_array($model->sales_report_url, ['', null]) ? '' : 'alert-success' ?>">
                    <?php
                    Modal::begin([
                        'header' => '<h2>Подключение CRM</h2>',
                        'toggleButton' => [
                            'label' => in_array($model->sales_report_url, ['', null]) ? 'Подключить'
                                : 'Адрес отчета: ' . $model->sales_report_url,
                            'tag' => 'a'
                        ],
                    ]);
                    ?>
                    <p>Для использования данных из CRM-системы, введите адрес,
                        по которому возможно получать ежедневную выгрзку статистики:</p>

                    <?php ActiveForm::begin(['action' => 'save-crm']); ?>
                    <input type="hidden" value="<?= $model->id ?>" name="id">
                    <input type="text" name="crm" class="crm-field"
                       placeholder="https://mywebsite.ru/crm/export_orders?someparameters"
                       value="<?= in_array($model->sales_report_url, ['', null]) ? '' : $model->sales_report_url ?>">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <?php ActiveForm::end(); ?>

                    <?php
                    Modal::end();
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <button type="button" class="btn btn-primary">Начать работу</button>
            </td>
            <td>
                <div class="well">
                <?php
                if ($adwords == null) {
                    if ($customerId != null) {
                        printf('<p>Введен Customer ID: %s,<br>ожидайте подключения</p>', $customerId);
                        echo Html::a('Проверить',
                            [
                                'company/connect-adwords',
                                'id' => $model->id
                            ]);
                        echo Html::a('Отвязать',
                            [
                                'company/unbind-customer-id',
                                'id' => $model->id,
                                'customerId' => $customerId
                            ]);
                    } else {
                        Modal::begin([
                            'header' => '<h2>Подключение Google.AdWords</h2>',
                            'toggleButton' => [
                                'label' => 'Подключить Google.AdWords',
                                'tag' => 'a'
                            ],
                        ]);
                        ?>
                        <p>Введите десятизначный номер Customer ID:</p>

                        <?php ActiveForm::begin(['action' => 'save-customer-id']); ?>
                        <input type="hidden" value="<?= $model->id ?>" name="id">
                        <input type="text" name="customerId" class="crm-field" placeholder="123-456-7890">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                        <?php
                        ActiveForm::end();
                        Modal::end();
                    }
                }
                ?>
                </div>
            </td>
            <td>
                <div class="well">
                    <a class="connect">Подключить Яндекс.Метрика</a>
                    или
                    <a class="connect">Подключить Google.Analytics</a>
                </div>
            </td>
        </tr>
    </table>
<?php
Panel::end();