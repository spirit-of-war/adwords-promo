<?php

use backend\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

Panel::begin(['header' => 'Добавить проект']);
echo $this->render('_form', ['model' => $model]);
Panel::end();