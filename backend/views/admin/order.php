<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 02.09.16
 * Time: 10:39
 */

use yiister\gentelella\widgets\grid\GridView;

$this->params['leftMenuItems'] = $leftMenuItems;

echo GridView::widget([
    'dataProvider' => $dataProvider,
	'columns' =>[
		//['class' => 'yii\grid\SerialColumn'],
        [
            'header' => 'ID',
            'attribute' => 'id',
        ],
        [
            'header' => 'ФИО',
            'attribute' => 'name',
        ],
		[
            'header' => 'Телефон',
            'attribute' => 'phone',
        ],
        [
            'header' => 'Сайт',
            'attribute' => 'site',
        ],
		[
            'header' => 'Email',
            'attribute' => 'email',
        ],
        [
            'header' => 'Дата создания',
            'attribute' => 'created_at',
        ],
		[
			'header' => 'Тип',
            'attribute' => 'type',
		]
	]
]);