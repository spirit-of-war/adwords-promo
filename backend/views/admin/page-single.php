<?php
//use moonland\tinymce\TinyMCE;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yiister\gentelella\widgets\Panel;
use common\models\admin\Page;

 $domain = \Yii::$app->params['domain'];
 

 if($action == 'add') {
//	 $model->type = Page::TYPE_BLOG;
	 $typeAction = 'Статья добавление.';
	 $buttText = 'Создать';
 } else {
	$pageLink = '<a href="'.$domain.$model->url.'" style="text-decoration:underline;">'.$model->h1.'</a>';
	$typeAction = 'редактирование статьи - '.$pageLink;
 	$buttText = 'Обновить';
 }
 
 Panel::begin(['header' => $typeAction]);
	$form = ActiveForm::begin([
		'id' => 'admin-page-form',
		'action' => '/admin/page/'.$action,
		'options' => ['enctype' => 'multipart/form-data']
	]);
	?>

	<?php
		echo $form->field($model, 'h1')->label('Заголовок статьи(H1)')->textInput(['placeholder' => 'Заголовок']);
		echo $form->field($model, 'slug')->label('ЧПУ')->textInput(['placeholder' => 'Оставьте поле пустым чтобы сгенерировать из заголовка']);
		echo $form->field($model, 'type')->label('Тип статьи')->dropDownList([
			Page::TYPE_NEWS => Page::getTanslate(Page::TYPE_NEWS),
			Page::TYPE_BLOG => Page::getTanslate(Page::TYPE_BLOG),
			Page::TYPE_PAGE => Page::getTanslate(Page::TYPE_PAGE),
		]);

		if($model->is_seo_only == Page::IS_SEO_ONLY_FALSE) {
			echo $form->field($model, 'announcement')->label('Анонс')->widget(CKEditor::className());
			echo $form->field($model, 'body')->label('Текст статьи')->widget(CKEditor::className());
			if($model->type !== Page::TYPE_PAGE ) {

				if($model->icon!='') {
					$url = \Yii::$app->params['domain'].'img/news/'.$model->icon;
					echo "<img src='".$url."' style='max-width:200px;'>";
					$labelFileInput = 'Обновить иконку для статьи';
				} else {
					echo "Нет иконки у новости";
					$labelFileInput = 'Загрузить иконку для статьи';
				}
				
				echo $form->field($model, 'icon')->label($labelFileInput)->fileInput();
			}
		}
		if($action == 'add') {
			$model->title = \Yii::$app->params['defaultSEOTitle'];
		}
		echo $form->field($model, 'title')->label('Title')->textInput(['placeholder' => 'Title']);
		echo $form->field($model, 'keywords')->label('Keywords')->textInput(['placeholder' => 'Keywords']);
		echo $form->field($model, 'description')->label('Description')->textInput(['placeholder' => 'Description']);
				
		if($model->is_seo_only == Page::IS_SEO_ONLY_FALSE || $action == 'add') {
			echo $form->field($model, 'active')->label('Опубликовать')->checkbox(); 			
		}
		
		echo Html::submitButton($buttText, ['class' => 'btn btn-primary', 'name' => 'admin-page-update']);
		ActiveForm::end();
		Panel::end();
