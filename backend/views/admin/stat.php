<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 02.09.16
 * Time: 10:39
 */

use yiister\gentelella\widgets\grid\GridView;
use yii\bootstrap\Html;

echo Html::dropDownList('accounts', null, $accounts, [
    'class' => 'accounts-selectbox',
    'onchange' => 'console.log($(".accounts-selectbox option:selected"))'
//    'onchange' => 'location.href = location.origin + "admin/stat/"'
]);

echo '<p style="margin-top: 50px">Last downloading of statistics was ' . $lastDownload .' days ago.';
echo GridView::widget([
    'dataProvider' => $statDataProvider,
]);