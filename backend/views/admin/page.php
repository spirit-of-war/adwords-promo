<?php
use yiister\gentelella\widgets\grid\GridView;
use yii\bootstrap\Html;
use common\models\admin\Page;
?>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:right;">
		<a class="btn btn-primary" href="/admin/page/add">Добавить статью</a>
	</div>
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<?php
			echo GridView::widget([
				'dataProvider' => $dataProvider,
				'columns' =>[
					//['class' => 'yii\grid\SerialColumn'],
					[
						'header' => 'ID',
						'attribute' => 'id',
					],
					[
						'header' => 'Заголовок(H1)',
						'attribute' => 'h1',
					],
					[
						'header' => 'Анонс',
						'value' => function($data){
							return mb_strimwidth($data['announcement'], 0, 50, "...");
						}
						//'attribute' => 'announcement',
					],
					[
						'header' => 'Тип страницы',
						'value' => function($data){
							return Page::getTanslate($data['type']);
						}
					],
					[
						'header' => 'Статус',
						'value' => function($data){
							return Page::getStatus($data['active']);
						}
					],
					[
						'header' => 'Действия',
						'label' => 'Ссылка',
						'format' => 'raw',
						'value' => function($data){

							/*if($data['type'] == Page::TYPE_PAGE) {
								$url = $data['url'];
							} else {
								$url = 'news/'.$data['id'];
							}*/

							$url = '/admin/page/'.$data['id'];

							return Html::a(
								'Редактировать',
								$url,
								[
//									'title' => $data['header'],
									'target' => '_blank'
								]
							);
						}
					],
				]
			]);
		?>
	</div>
</div>