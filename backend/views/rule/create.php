<?php

use common\models\strategy\Rule;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\strategy\Rule */
/* @var $accounts \common\models\Account */

$this->title = 'Добавить правило';
$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

echo $this->render('_form', [
    'account' => $account
]);