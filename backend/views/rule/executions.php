<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 25.08.16
 * Time: 12:05
 */

use yii\widgets\LinkPager;

$this->registerCssFile('/css/rule/executions.css');
$this->registerJsFile('/js/rule/executions.js', ['depends' => 'yii\web\YiiAsset']);
?>
<div class="accordion">
    <?php foreach ($tasks as $task) { ?>
    <div class="panel">
        <a class="panel-heading" data-id="<?= $task['id'] ?>">

                <i class="fa fa-angle-double-down"></i>
                <?= date('d.m.o H:i', $task['date']) ?>
                Изменено ключевых слов: <?= $task['executions'] ?>

        </a>
        <div id="execution-<?= $task['id'] ?>" class="panel-collapse collapse" style="display: none;" data-downloaded="false">
            <div class="panel-body">
                Загрузка...
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<?php
echo LinkPager::widget([
    'pagination' => $pages,
]);
