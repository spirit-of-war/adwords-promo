<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 25.08.16
 * Time: 12:06
 */

use common\models\Account;
use common\models\strategy\Rule;
use common\models\structure\Layer;
use \common\models\structure\BidAuction;

?>
<table class="table">
    <tr><td>Название:</td><td class="check-name"><?= isset($rule) ? $rule->name : '' ?></td></tr>
    <tr>
        <td>Аккаунт:</td>
        <td class="check-account">
            <?php
            if (isset($rule)) {
                echo $rule->account->advert_type == Account::TYPE_YANDEX ? 'Яндекс.Директ' : 'Google.AdWords';
                echo ': ' . $rule->account->advert_login;
            }
            ?>
        </td>
    </tr>
    <tr>
        <td>Кампании:</td>
        <td class="check-campaigns">
            <?php
            if (isset($rule)) {
                $campaigns = $rule->getCampaigns();
                echo (count($campaigns) == $allCampsQuantity ? 'Выбраны все' : 'Выбрано ' . count($campaigns)) . ':';
                echo '<div class="campaigns">';
                $show = '';
                if (count($campaigns) > 0) {
                    foreach ($campaigns as $campaign)
                        $show .= $campaign->source_id . ' - ' . $campaign->name . '<br>';
                } else {
                    $show = 'Все';
                }
                echo $show;
                echo '</div>';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td>Частота применения:</td>
        <td class="check-freq">
            <?php
            if (isset($rule)) {
                if ($rule->frequency == Rule::FREQ_ONCE) {
                    echo 'Один раз: ' . ($rule->instant_start ? '' : date('d.m.o', $rule->onceday));
                } elseif ($rule->frequency == Rule::FREQ_DAY) {
                    echo 'Каждый день';
                }
                elseif ($rule->frequency == Rule::FREQ_WEEK) {
                    echo 'Каждую неделю ';
                    if ($rule->weekday == Rule::WEEKDAY_MONDAY)
                        echo 'в понедельник';
                    elseif ($rule->weekday == Rule::WEEKDAY_TUESDAY)
                        echo 'во вторник';
                    elseif ($rule->weekday == Rule::WEEKDAY_WEDNSDAY)
                        echo 'в среду';
                    elseif ($rule->weekday == Rule::WEEKDAY_THURSDAY)
                        echo 'в четверг';
                    elseif ($rule->weekday == Rule::WEEKDAY_FRIDAY)
                        echo 'в пятницу';
                    elseif ($rule->weekday == Rule::WEEKDAY_SATTURDAY)
                        echo 'в субботу';
                    elseif ($rule->weekday == Rule::WEEKDAY_SUNDAY)
                        echo 'в воскресенье';
                }
                elseif ($rule->frequency == Rule::FREQ_MONTH) {
                    echo 'Один раз в месяц: ' . $rule->monthday . ' числа';
                }

                if ($rule->instant_start)
                    if ($rule->frequency == Rule::FREQ_ONCE)
                        echo '<br>Правило запущено после создания ' . date('d.m.o', $rule->created_at);
                    else
                        echo '<br>Первый раз правило запущено после создания ' . date('d.m.o', $rule->created_at);
            }
            ?>
        </td>
    </tr>
    <tr>
        <td>Условия:</td>
        <td class="check-rules">
            <div id="condition-container"></div>
        </td>
    </tr>
    <?php
    //это будет для Google.Adwords
//    <tr>
//        <td>Область применения:</td>
//        <td class="check-layer">
//            <?php
//            if (isset($rule)) {
//                if ($rule->layer == Layer::LAYER_CAMPAIGNS)
//                    echo 'Кампании';
//                elseif ($rule->layer == Layer::LAYER_ADGROUPS)
//                    echo 'Группы';
//                elseif ($rule->layer == Layer::LAYER_ADS)
//                    echo 'Объявления';
//                elseif ($rule->layer == Layer::LAYER_KEYWORDS)
//                    echo 'Ключевые слова';
//            }
//
//</td>-->
//<!--    </tr>-->
    ?>
    <tr>
        <td>Действие:</td>
        <td class="check-action">
            <?php
            if (isset($rule)) {
                if ($rule->action == Rule::ACTION_CHANGE_CONSTANT)
                    echo 'Установка значения ставки: ' . $rule->change;
                elseif ($rule->action == Rule::ACTION_CHANGE_PERCENT)
                    echo 'Изменение значения ставки на ' . $rule->change . '%';
                elseif ($rule->action == Rule::ACTION_CHANGE_POSITION)
                    echo 'Изменение ставки до позиции "'
                        .  ($rule->position == BidAuction::POSITION_P11 ? 'Первое спецразмещение' : '')
                        .  ($rule->position == BidAuction::POSITION_P12 ? 'Второе спецразмещение' : '')
                        .  ($rule->position == BidAuction::POSITION_P13 ? 'Вход в спецразмещение' : '')
                        .  ($rule->position == BidAuction::POSITION_P21 ? 'Первое место' : '')
                        .  ($rule->position == BidAuction::POSITION_P24 ? 'Вход в гарантию' : '')
                        . '" + ' . $rule->change . '%';
            }
            ?>
        </td>
    </tr>
    <tr class="check-tr-min-bid"
        <?= isset($rule) && $rule->action == Rule::ACTION_CHANGE_CONSTANT ? 'style="display:none"' : '' ?>>
        <td>Минимальная ставка:</td>
        <td class="check-min-bid"><?= isset($rule) && $rule->min_bid != null ? $rule->min_bid : 'не уставновлена' ?></td>
    </tr>
    <tr class="check-tr-max-bid"
        <?= isset($rule) && $rule->action == Rule::ACTION_CHANGE_CONSTANT ? 'style="display:none"' : '' ?>>
        <td>Максимальная ставка:</td>
        <td class="check-max-bid"><?= isset($rule) && $rule->max_bid != null ? $rule->max_bid : 'не уставновлена' ?></td>
    </tr>
</table>
