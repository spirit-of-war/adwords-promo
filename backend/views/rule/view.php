<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 23.08.16
 * Time: 12:20
 */

use backend\widgets\Panel;
use common\models\strategy\Rule;
use common\models\strategy\Index;
use yii\web\View;
use backend\assets\Conditions;
use yii\helpers\Html;
use backend\assets\PNotify;

/* @var $rule common\models\strategy\Rule */
/* @var $company common\models\Company */

$check = isset($check) ? $check : false;

if (isset($rule) && !$check) {
    $this->title = 'Правило: ' . $rule->name;
    $this->params['leftMenuItems'] = $leftMenuItems;
    $this->params['breadcrumbs'] = $breadcrumbs;

    $this->registerCssFile('/css/rule/view.css');
    $this->registerJs("var indexes = " . Index::getAllInJson(). ";", View::POS_END);
    $this->registerJs('var existingConds = ' . (isset($rule) ? $rule->getConditions(true) : '[]') . ';', View::POS_HEAD);

    Conditions::register($this);

    Panel::begin(['header' => $this->title]);
}

if ($ruleExecuted) {
    PNotify::register($this);
    $this->registerJs('new PNotify({
            title: \'Правило выполнено\',
            text: \'Результат вы можете посмотреть на вкладке "История запусков".\',
            type: \'success\',
            styling: \'bootstrap3\'
        });');
}

?>
    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="<?= $showExecutions ? '' : 'active'?>" aria-expanded="true">
                <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false">Инфо</a>
            </li>
            <li role="presentation" class="<?= $showExecutions ? 'active' : ''?>">
                <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab">История запусков</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade <?= $showExecutions ? '' : 'active in'?>" id="tab_content1" aria-labelledby="home-tab">
                <div>
                    <?php
                    echo Html::a('Редактировать',
                        [
                            'rule/update',
                            'id' => $rule->id
                        ],
                        [
                            'class' => 'btn btn-primary'
                        ]);
                    echo Html::a('Удалить',
                        [
                            'rule/delete',
                            'id' => $rule->id
                        ],
                        [
                            'data' => [
                                'method' => 'post',
                                'confirm' => 'Вы действительно хотите удалить правило?'
                            ],
                            'class' => 'btn btn-danger'
                        ]);
                    echo Html::a('Исполнить',
                        [
                            'rule/execute',
                            'id' => $rule->id
                        ],
                        [
                            'data' => [
                                'method' => 'post',
                                'confirm' => 'Вы действительно хотите запустить правило?'
                            ],
                            'class' => 'btn btn-success'
                        ]);
                    $toggleAction = $rule->state == Rule::STATE_SUSPENDED ? 'Возобновить' : 'Приостановить';
                    echo Html::a($toggleAction,
                        [
                            'rule/toggle',
                            'id' => $rule->id
                        ],
                        [
                            'data' => [
                                'method' => 'post',
                                'confirm' => 'Вы действительно хотите ' . mb_strtolower($toggleAction) . ' правило?'
                            ],
                            'class' => 'btn btn-warning'
                        ]);
                    echo $this->render('info', [
                        'rule' => $rule,
                        'allCampsQuantity' => $allCampsQuantity
                    ]);
                    ?>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade <?= $showExecutions ? 'active in' : ''?>" id="tab_content2" aria-labelledby="profile-tab">
                <?= $this->render('executions', [
                    'rule' => $rule,
                    'tasks' => $tasks,
                    'pages' => $pages
                ]); ?>
            </div>
        </div>
    </div>
<?php
if (isset($rule) && !$check)
    Panel::end();