<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 16.08.16
 * Time: 10:58
 */

use yii\helpers\Html;
use common\models\structure\Layer;
use \common\models\strategy\Rule;
use backend\widgets\TabMenu;
use backend\widgets\Panel;

$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

Panel::begin();

echo TabMenu::widget(['account' => $account, 'dimension' => Layer::LAYER_NAME_RULE]);

echo Html::a('Добавить', [
    'rule/create',
    'accountID' => $account->id
], ['class' => 'btn btn-primary']);

?>
<div class="grid-view">
<table class="table dataTable table-striped">
    <thead>
        <tr><th>Название</th><th>Применение</th><th>Статус</th><th></th></tr>
    </thead>
    <tbody>
    <?php
    foreach ($rules as $rule) {

        $layer = '';
        if ($rule->layer == Layer::LAYER_CAMPAIGNS)
            $layer = 'Кампании';
        elseif ($rule->layer == Layer::LAYER_ADGROUPS)
            $layer = 'Группы';
        elseif ($rule->layer == Layer::LAYER_ADS)
            $layer = 'Объявления';
        elseif ($rule->layer == Layer::LAYER_KEYWORDS)
            $layer = 'Ключевые слова';

        $frequency = '';
        if ($rule->frequency == Rule::FREQ_DAY)
            $frequency = 'Каждый день';
        elseif ($rule->frequency == Rule::FREQ_WEEK)
            $frequency = 'Каждую неделю';
        elseif ($rule->frequency == Rule::FREQ_MONTH)
            $frequency = 'Каждый месяц';

        $state = '';
        if ($rule->state == Rule::STATE_ACTIVE)
            $state = '<span class="glyphicon glyphicon-play" title="Активно" aria-hidden="true"></span>';
        elseif ($rule->state == Rule::STATE_SUSPENDED)
            $state = '<span class="glyphicon glyphicon-pause" title="Приостановлено" aria-hidden="true"></span>';

        printf('<tr>
                    <td><a href="/rule/view?id=%s">%s</a></td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>
                        <a href="/rule/delete?id=%s" data-method="post"
                                        data-confirm="Вы действительно хотите удалить данное правило?">
                            <span class="fa fa-remove"></span></a>
                    </td>
                </tr>',
            $rule->id,
            $rule->name,
            $frequency,
            $state,
            $rule->id,
            $rule->id
        );
    }

    ?>
    </tbody>
</table>
</div>
<?php Panel::end();