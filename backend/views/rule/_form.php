<?php

use backend\assets\SmartWizard;
use backend\widgets\Panel;
use \common\models\structure\Layer;
use backend\assets\Switchery;
use \backend\assets\Select2;
use common\models\Account;
use common\models\strategy\Rule;
use common\models\strategy\Index;
use \backend\assets\Conditions;
use yii\web\View;
use yii\widgets\ActiveForm;
use \backend\assets\DateRangeAsset;
use backend\assets\CampaignSelector;
use \common\models\structure\BidAuction;

/* @var $this yii\web\View */
/* @var $model common\models\strategy\Rule */
/* @var $rule common\models\strategy\Rule */
/* @var $account \common\models\Account */

SmartWizard::register($this);
Switchery::register($this);
Select2::register($this);
Conditions::register($this);
DateRangeAsset::register($this);
CampaignSelector::register($this);

$this->registerCssFile('/css/rule/create.css');
$this->registerJsFile('/js/rule/create.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJs("var indexes = " . Index::getAllInJson() . ";", View::POS_END);
$this->registerJs('var existingConds = ' . (isset($rule) ? $rule->getConditions(true) : '[]') . ';', View::POS_HEAD);
$this->registerJs('var selectedCamps = ' . (isset($rule) ? $rule->getCampaigns(true) : '[]') . ';', View::POS_HEAD);

Panel::begin(['header' => $this->title]);
?>
<?php ActiveForm::begin(['options' => ['class' => 'condition-form']]) ?>
    <input type="hidden" value="<?= isset($rule) ? $rule->account_id : $account->id ?>" name="account" id="account">
    <?php if (isset($rule)) { ?>
        <input type="hidden" value="<?= $rule->id ?>" name="ruleID">
    <?php } ?>
    <div id="wizard" class="form_wizard wizard_horizontal">
        <ul class="wizard_steps">
            <li>
                <a href="#step-1">
                    <span class="step_no">1</span>
                    <span class="step_descr">Шаг 1<br /><small>Основное</small></span>
                </a>
            </li>
            <li>
                <a href="#step-2">
                    <span class="step_no">2</span>
                    <span class="step_descr">Шаг 2<br /><small>Частота</small></span>
                </a>
            </li>
            <li>
                <a href="#step-3">
                    <span class="step_no">3</span>
                    <span class="step_descr">Шаг 3<br /><small>Условия</small></span>
                </a>
            </li>
            <li>
                <a href="#step-4">
                    <span class="step_no">4</span>
                    <span class="step_descr">Шаг 4<br /><small>Действия</small></span>
                </a>
            </li>
            <li>
                <a href="#step-5">
                    <span class="step_no">5</span>
                    <span class="step_descr">Шаг 5<br /><small>Проверка</small></span>
                </a>
            </li>
        </ul>
        <div id="step-1">
            <div class="form-horizontal form-label-left">
                <div class="form-group rule-name">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Название правила*
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="name" required="required"
                               class="form-control col-md-7 col-xs-12 rule-name"
                               value="<?= isset($rule) ? $rule->name : '' ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Кампании</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="hidden" name="campaigns" class="rule-campaigns">
                        <div id="campaignHolder"></div>
                    </div>
                </div>
            </div>

        </div>
        <div id="step-2">
            <div class="form-horizontal form-label-left">
                <div class="form-group rule-frequency">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Применять</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" name="frequency">
                            <option <?= isset($rule) && $rule->frequency == Rule::FREQ_ONCE ? 'selected' : '' ?>
                                value="<?= Rule::FREQ_ONCE ?>">Один раз</option>
                            <option <?= isset($rule) && $rule->frequency == Rule::FREQ_DAY ? 'selected' : '' ?>
                                value="<?= Rule::FREQ_DAY ?>">Каждый день</option>
                            <option  <?= isset($rule) && $rule->frequency == Rule::FREQ_WEEK ? 'selected' : '' ?>
                                value="<?= Rule::FREQ_WEEK ?>">Раз в неделю</option>
                            <option <?= isset($rule) && $rule->frequency == Rule::FREQ_MONTH ? 'selected' : '' ?>
                                value="<?= Rule::FREQ_MONTH ?>">Раз в месяц</option>
                        </select>
                    </div>
                </div>
                <div class="form-group rule-weekday"
                    <?= isset($rule) && $rule->frequency == Rule::FREQ_WEEK ? '' : 'style="display:none"'?>>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" name="weekday">
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_MONDAY ? 'selected' : '' ?>
                                value="<?= Rule::WEEKDAY_MONDAY ?>">Понедельник</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_TUESDAY ? 'selected' : '' ?>
                                value="<?= Rule::WEEKDAY_TUESDAY ?>">Вторник</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_WEDNSDAY ? 'selected' : '' ?>
                                value="<?= Rule::WEEKDAY_WEDNSDAY ?>">Среда</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_THURSDAY ? 'selected' : '' ?>
                                value="<?= Rule::WEEKDAY_THURSDAY ?>">Четверг</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_FRIDAY ? 'selected' : '' ?>
                                value="<?= Rule::WEEKDAY_FRIDAY ?>">Пятница</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_SATTURDAY ? 'selected' : '' ?>
                                value="<?= Rule::WEEKDAY_SATTURDAY ?>">Суббота</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_SUNDAY ? 'selected' : '' ?>
                                value="<?= Rule::WEEKDAY_SUNDAY ?>">Воскресенье</option>
                        </select>
                    </div>
                </div>
                <div class="form-group rule-monthday"
                    <?= isset($rule) && $rule->frequency == Rule::FREQ_MONTH ? '' : 'style="display:none"'?>>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" min="1" max="31" name="monthday" class="form-control"
                               value="<?= isset($rule) && $rule->monthday ? $rule->monthday : '1' ?>">
                        <span class="form-control suffix">числа каждого месяца</span>
                    </div>
                </div>
                <div class="form-group rule-once"
                    <?= isset($rule) && $rule->frequency == Rule::FREQ_ONCE
                        || !isset($rule) ? '' : 'style="display:none"'?>>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control has-feedback-left" id="onceCal" name="onceday"
                                <?= isset($rule) && $rule->instant_start ? 'disabled' : '' ?>
                                value="<?= isset($rule) && $rule->onceday > 0 ? date('d.m.o', $rule->onceday) : '' ?>"
                                placeholder="Дата запуска" aria-describedby="inputSuccess2Status3">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                    </div>
                </div>
                <div class="form-group rule-instant_start">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="">
                            <label>
                                <input type="checkbox" class="js-switch"
                                    <?= isset($rule) && $rule->instant_start ? 'checked="checked"' : '' ?>
                                       data-switchery="true" name="instantStart">&nbsp;запустить после создания
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-9 col-sm-9 col-xs-12">Правило запускается приблизительно в 5:00 по МСК.</div>
                </div>
            </div>
        </div>
        <div id="step-3">
            <input type="hidden" name="conditions" id="conditions"
                   value='<?= isset($rule) ? $rule->getConditions(true) : '' ?>'>
            <div id="condition-container"></div>
        </div>
        <div id="step-4">
            <div class="form-horizontal form-label-left">
                <?php
                //это будет нужно для google adwords
//                  <div class="form-group rule-layer">
//                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Действия применять для </label>
//                            <div class="col-md-6 col-sm-6 col-xs-12">
//                                <select class="form-control" name="layer">
//                                    <option
//                        <?= isset($rule) && $rule->layer == Layer::LAYER_KEYWORDS ? 'selected' : '' ? >
//                        value="--><?//= Layer::LAYER_KEYWORDS ? ><!--">Ключевых слов</option>-->
//                        <option-->
//                            --><?//= isset($rule) && $rule->layer == Layer::LAYER_ADS ? 'selected' : '' ? >
//                            value="-->--><?////= Layer::LAYER_ADS ? ><!--<!--">Объявлений</option>-->-->
//                        <option-->
//                            --><?//= isset($rule) && $rule->layer == Layer::LAYER_ADGROUPS ? 'selected' : '' ? >
//                            value="--><?//= Layer::LAYER_ADGROUPS ? ><!--">Групп</option>-->
//                        <option-->-->
//                            --><?//= isset($rule) && $rule->layer == Layer::LAYER_CAMPAIGNS ? 'selected' : '' ? >
//                            value="--><?//= Layer::LAYER_CAMPAIGNS ? ><!--">Кампаний</option>-->
//                        </select>-->
//                    </div>-->
//                </div>-->
                ?>
                <div class="form-group rule-action">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Действие</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control action-select" name="action">
                            <option
                                <?= isset($rule) && $rule->action == Rule::ACTION_CHANGE_CONSTANT ? 'selected' : '' ?>
                                value="<?= Rule::ACTION_CHANGE_CONSTANT ?>">Установить определенное значение ставки
                            </option>
                            <option
                                <?= isset($rule) && $rule->action == Rule::ACTION_CHANGE_PERCENT ? 'selected' : '' ?>
                                value="<?= Rule::ACTION_CHANGE_PERCENT ?>">Изменение ставки на процент
                            </option>
                            <?php if ($account->advert_type == Account::TYPE_YANDEX) { ?>
                                <option
                                    <?= isset($rule) && $rule->action == Rule::ACTION_CHANGE_POSITION ? 'selected' : '' ?>
                                    value="<?= Rule::ACTION_CHANGE_POSITION ?>">Изменение ставки до нужной позиции + процент
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group rule-position" style="
                    <?= isset($rule) && $rule->action == Rule::ACTION_CHANGE_POSITION ? '' : 'display: none' ?>">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Позиция</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control action-position" name="position">
                            <option
                                <?= isset($rule) && $rule->position == BidAuction::POSITION_P11 ? 'selected' : '' ?>
                                value="<?= BidAuction::POSITION_P11 ?>">Первое спецразмещение
                            </option>
                            <option
                                <?= isset($rule) && $rule->position == BidAuction::POSITION_P12 ? 'selected' : '' ?>
                                value="<?= BidAuction::POSITION_P12 ?>">Второе спецразмещение
                            </option>
                            <option
                                <?= isset($rule) && $rule->position == BidAuction::POSITION_P13 ? 'selected' : '' ?>
                                value="<?= BidAuction::POSITION_P13 ?>">Вход в спецразмещение
                            </option>
                            <option
                                <?= isset($rule) && $rule->position == BidAuction::POSITION_P21 ? 'selected' : '' ?>
                                value="<?= BidAuction::POSITION_P21 ?>">Первое место
                            </option>
                            <option
                                <?= isset($rule) && $rule->position == BidAuction::POSITION_P24 ? 'selected' : '' ?>
                                value="<?= BidAuction::POSITION_P24 ?>">Вход в гарантию
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group change">
                    <label class="control-label change-label col-md-3 col-sm-3 col-xs-12" for="change">
                        <?php if (isset($rule)) {
                            if ($rule->action == Rule::ACTION_CHANGE_CONSTANT)
                                echo 'Значение ставки';
                            elseif ($rule->action == Rule::ACTION_CHANGE_PERCENT
                                    || $rule->action == Rule::ACTION_CHANGE_POSITION)
                                echo 'Процент';
                        } else {
                            echo 'Значение ставки';
                        } ?>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="change" name="change" required="required"
                               value="<?= isset($rule) ? $rule->change : ''?>" class="form-control col-md-7 col-xs-12">
                        <?php
                        $showPercentCaption = isset($rule)
                            && in_array($rule->action, [Rule::ACTION_CHANGE_PERCENT, Rule::ACTION_CHANGE_POSITION]);
                        ?>
                        <span class="percent-caption" style="display: <?= $showPercentCaption ? 'inline' : 'none'?>">
                            * для повышения ставки на 25%, укажите "25",
                            для понижения ставки на 25%, укажите "-25"</span>
                    </div>
                </div>
                <div class="form-group min-bid">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Минимальная ставка</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="minBid" name="minBid" required="required"
                               value="<?= isset($rule) ? $rule->min_bid : ''?>" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>
                <div class="form-group max-bid">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Максимальная ставка</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="maxBid" name="maxBid" required="required"
                               value="<?= isset($rule) ? $rule->max_bid : ''?>" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>
            </div>
        </div>
        <div id="step-5">
            <?php
            $step5 = [
                'check' => true
            ];
            if (isset($rule)) {
                $step5['rule'] = $rule;
                $step5['allCampsQuantity'] = $allCampsQuantity;
            }

            echo $this->render('info', $step5);
            ?>
        </div>
        </div>
    </div>

<?php
ActiveForm::end();
Panel::end();