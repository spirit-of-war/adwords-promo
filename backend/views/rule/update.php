<?php

/* @var $this yii\web\View */
/* @var $model common\models\strategy\Rule */

$this->title = $model->name;
$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

echo $this->render('_form', [
    'account' => $account,
    'rule' => $model,
    'allCampsQuantity' => $allCampsQuantity
]);