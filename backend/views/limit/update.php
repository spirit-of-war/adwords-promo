<?php

/* @var $this yii\web\View */
/* @var $model common\models\strategy\Rule */

$this->title = $model->name;
$this->params['breadcrumbs'] = $breadcrumbs;

echo $this->render('_form', [
    'accounts' => $accounts,
    'rule' => $model,
    'allCampsQuantity' => $allCampsQuantity
]);