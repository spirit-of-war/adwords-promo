<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 23.08.16
 * Time: 12:20
 */

use backend\widgets\Panel;
use common\models\strategy\Rule;
use common\models\strategy\Index;
use yii\web\View;
use backend\assets\Conditions;
use yii\helpers\Html;

/* @var $rule common\models\strategy\Rule */
/* @var $company common\models\Company */

$check = isset($check) ? $check : false;

if (isset($rule) && !$check) {
    $this->title = 'Правило: ' . $rule->name;
    $this->params['breadcrumbs'] = $breadcrumbs;

    $this->registerCssFile('/css/rule/view.css');
    $this->registerJs("var indexes = " . Index::getAllInJson() . ";", View::POS_END);
    $this->registerJs('var existingConds = ' . (isset($rule) ? $rule->getConditions(true) : '[]') . ';', View::POS_HEAD);

    Conditions::register($this);

    Panel::begin(['header' => $this->title]);
}
?>
    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active" aria-expanded="true">
                <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false">Инфо</a>
            </li>
<!--            <li role="presentation" class="">-->
<!--                <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab">История запусков</a>-->
<!--            </li>-->
        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                <div>
                    <?php
                    echo Html::a('Редактировать',
                        [
                            'limit/update',
                            'id' => $rule->id
                        ],
                        [
                            'class' => 'btn btn-primary'
                        ]);
                    echo Html::a('Удалить',
                        [
                            'limit/delete',
                            'id' => $rule->id
                        ],
                        [
                            'data' => [
                                'method' => 'post',
                                'confirm' => 'Вы действительно хотите удалить правило?'
                            ],
                            'class' => 'btn btn-danger'
                        ]);
                    echo $this->render('info', [
                        'rule' => $rule,
                        'allCampsQuantity' => $allCampsQuantity
                    ]);
                    ?>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

            </div>
        </div>
    </div>
<?php
if (isset($rule) && !$check)
    Panel::end();