<?php

use backend\assets\SmartWizard;
use backend\widgets\Panel;
use backend\assets\Switchery;
use \backend\assets\Select2;
use common\models\strategy\Rule;
use common\models\strategy\Index;
use \backend\assets\Conditions;
use yii\web\View;
use yii\widgets\ActiveForm;
use \backend\assets\DateRangeAsset;
use backend\assets\CampaignSelector;
use common\models\strategy\Limit;
use common\models\structure\Layer;
use \backend\assets\LayerFilter;

/* @var $this yii\web\View */
/* @var $model common\models\strategy\Rule */
/* @var $rule common\models\strategy\Limit */
/* @var $accounts \common\models\Account */

SmartWizard::register($this);
Switchery::register($this);
Select2::register($this);
Conditions::register($this);
DateRangeAsset::register($this);
CampaignSelector::register($this);
LayerFilter::register($this);

$this->registerCssFile('/css/rule/conditions.css');
$this->registerCssFile('/css/rule/create.css');
$this->registerJsFile('/js/limit/create.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJs("var indexes = " . Index::getAllInJson() . ";", View::POS_END);
$this->registerJs('var existingConds = ' . (isset($rule) ? $rule->getConditions(true) : '[]') . ';', View::POS_HEAD);
$this->registerJs('var selectedCamps = ' . (isset($rule) ? $rule->getCampaigns(true) : '[]') . ';', View::POS_HEAD);
$this->registerJs('var keywordsConds = '
    . (isset($rule) && $rule->layer == Layer::LAYER_KEYWORDS ? $rule->getLayerConds() : '[]') . ';', View::POS_HEAD);
$this->registerJs('var adgroupsConds = '
    . (isset($rule) && $rule->layer == Layer::LAYER_ADGROUPS ? $rule->getLayerConds() : '[]') . ';', View::POS_HEAD);

Panel::begin(['header' => $this->title]);
?>
<?php ActiveForm::begin(['options' => ['class' => 'condition-form']]) ?>
    <input type="hidden" value="<?= isset($rule) ? $rule->account_id : $account->id ?>" name="account" id="account">
<?php if (isset($rule)) { ?>
    <input type="hidden" value="<?= $rule->id ?>" name="ruleID">
<?php } ?>
    <div id="wizard" class="form_wizard wizard_horizontal">
        <ul class="wizard_steps">
            <li>
                <a href="#step-1">
                    <span class="step_no">1</span>
                    <span class="step_descr">Шаг 1<br /><small>Основное</small></span>
                </a>
            </li>
            <li>
                <a href="#step-2">
                    <span class="step_no">2</span>
                    <span class="step_descr">Шаг 2<br /><small>Частота</small></span>
                </a>
            </li>
            <li>
                <a href="#step-3">
                    <span class="step_no">3</span>
                    <span class="step_descr">Шаг 3<br /><small>Условия</small></span>
                </a>
            </li>
            <li>
                <a href="#step-4">
                    <span class="step_no">4</span>
                    <span class="step_descr">Шаг 4<br /><small>Действия</small></span>
                </a>
            </li>
            <li>
                <a href="#step-5">
                    <span class="step_no">5</span>
                    <span class="step_descr">Шаг 5<br /><small>Проверка</small></span>
                </a>
            </li>
        </ul>
        <div id="step-1">
            <div class="form-horizontal form-label-left">
                <div class="form-group rule-name">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Название лимита
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="name" required="required"
                               class="form-control col-md-7 col-xs-12 rule-name"
                               value="<?= isset($rule) ? $rule->name : '' ?>">
                    </div>
                </div>

                <div class="form-group rule-layer">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Область применения</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" name="layer">
                            <option
                                <?= isset($rule) && $rule->layer == Layer::LAYER_CAMPAIGNS ? 'selected' : '' ?>
                                value="<?= Layer::LAYER_CAMPAIGNS ?>">Кампании</option>
                            <option
                                <?= isset($rule) && $rule->layer == Layer::LAYER_KEYWORDS ? 'selected' : '' ?>
                                    value="<?= Layer::LAYER_KEYWORDS ?>">Ключевые слова</option>
                        </select>
                    </div>
                </div>

                <div class="form-group rule-campaigns">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Кампании</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="hidden" name="campaigns" class="rule-campaigns">
                        <div id="campaignHolder"></div>
                    </div>
                </div>

                <div class="form-group rule-groups">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Группы объявлений</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="hidden" name="adgroupConditions" id="ruleGroups">
                        <div id="groupsSelectorHolder"></div>
                    </div>
                </div>

                <div class="form-group rule-keywords">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Ключевые слова</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="hidden" name="keywordsConditions" id="ruleKeywords">
                        <div id="keywordsSelectorHolder"></div>
                    </div>
                </div>
            </div>

        </div>
        <div id="step-2">
            <div class="form-horizontal form-label-left">
                <div class="form-group rule-frequency">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Применять</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" name="frequency">
                            <option <?= isset($rule) && $rule->frequency == Rule::FREQ_DAY ? 'selected' : '' ?>
                                    value="<?= Rule::FREQ_DAY ?>">Каждый день</option>
                            <option  <?= isset($rule) && $rule->frequency == Rule::FREQ_WEEK ? 'selected' : '' ?>
                                    value="<?= Rule::FREQ_WEEK ?>">Раз в неделю</option>
                            <option <?= isset($rule) && $rule->frequency == Rule::FREQ_MONTH ? 'selected' : '' ?>
                                    value="<?= Rule::FREQ_MONTH ?>">Раз в месяц</option>
                        </select>
                    </div>
                </div>
                <div class="form-group rule-weekday"
                    <?= isset($rule) && $rule->frequency == Rule::FREQ_WEEK ? '' : 'style="display:none"'?>>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" name="weekday">
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_MONDAY ? 'selected' : '' ?>
                                    value="<?= Rule::WEEKDAY_MONDAY ?>">Понедельник</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_TUESDAY ? 'selected' : '' ?>
                                    value="<?= Rule::WEEKDAY_TUESDAY ?>">Вторник</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_WEDNSDAY ? 'selected' : '' ?>
                                    value="<?= Rule::WEEKDAY_WEDNSDAY ?>">Среда</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_THURSDAY ? 'selected' : '' ?>
                                    value="<?= Rule::WEEKDAY_THURSDAY ?>">Четверг</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_FRIDAY ? 'selected' : '' ?>
                                    value="<?= Rule::WEEKDAY_FRIDAY ?>">Пятница</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_SATTURDAY ? 'selected' : '' ?>
                                    value="<?= Rule::WEEKDAY_SATTURDAY ?>">Суббота</option>
                            <option <?= isset($rule) && $rule->weekday == Rule::WEEKDAY_SUNDAY ? 'selected' : '' ?>
                                    value="<?= Rule::WEEKDAY_SUNDAY ?>">Воскресенье</option>
                        </select>
                    </div>
                </div>
                <div class="form-group rule-monthday"
                    <?= isset($rule) && $rule->frequency == Rule::FREQ_MONTH ? '' : 'style="display:none"'?>>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" min="1" max="31" name="monthday" class="form-control"
                               value="<?= isset($rule) && $rule->monthday ? $rule->monthday : '1' ?>">
                        <span class="form-control suffix">числа каждого месяца</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="step-3">
            <input type="hidden" name="conditions" id="conditions"
                   value='<?= isset($rule) ? $rule->getConditions(true) : '' ?>'>
            <div id="condition-container"></div>
        </div>
        <div id="step-4">
            <div class="form-horizontal form-label-left">
                <div class="form-group rule-action">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Действие</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control action-select" name="action">
                            <option
                                <?= isset($rule) && $rule->action == Limit::ACTION_NOTIFICATION ? 'selected' : '' ?>
                                    value="<?= Limit::ACTION_NOTIFICATION ?>">Уведомление на почту
                            </option>
                            <option
                                <?= isset($rule) && $rule->action == Limit::ACTION_STOP ? 'selected' : '' ?>
                                    value="<?= Limit::ACTION_STOP ?>">Остановка рекламы
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-group rule-email">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        * Будет отправлено уведомление на почту:
                        <span class="account-email"><?= Yii::$app->user->identity->email ?></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="step-5">
            <?php
            $step5 = [
                'check' => true
            ];
            if (isset($rule)) {
                $step5['rule'] = $rule;
                $step5['allCampsQuantity'] = $allCampsQuantity;
            }

            echo $this->render('info', $step5);
            ?>
        </div>
    </div>
    </div>

<?php
ActiveForm::end();
Panel::end();