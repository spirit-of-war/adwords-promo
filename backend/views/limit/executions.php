<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 25.08.16
 * Time: 12:05
 */

$this->registerCssFile('/css/rule/executions.css');
?>
<div class="accordion" role="tablist" aria-multiselectable="true">
    <?php foreach ($tasks as $task) { ?>
    <div class="panel">
        <a class="panel-heading collapsed" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1"
           href="#execution<?= $task[0]['id'] ?>" aria-expanded="false" aria-controls="collapseOne">
            <h4 class="panel-title"><?= date('d.m.o H:i', $task[0]['created_at']) ?></h4>
        </a>
        <div id="execution<?= $task[0]['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"
             aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Кампания</th>
                            <th>Ключевое слово</th>
                            <th>Установлено значение</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($task as $execution) { ?>
                        <tr>
                            <td><?= $execution['campaign'] ?></td>
                            <td><?= $execution['keyword_source_id'] . ' - ' . $execution['keyword'] ?></td>
                            <td>
                                <?= isset($execution['bid_search'])
                                    ? sprintf('На поиске: %s. ', $execution['bid_search'] / 1000000) : '' ?>
                                <?= isset($execution['bid_search'])
                                    ? sprintf('В рекламной сети: %s.', $execution['bid_context'] / 1000000) : '' ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
