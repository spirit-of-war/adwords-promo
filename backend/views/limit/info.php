<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 25.08.16
 * Time: 12:06
 */

use common\models\Account;
use common\models\strategy\Rule;
use common\models\structure\Layer;
use \common\models\structure\BidAuction;
use \common\models\strategy\Limit;
use common\models\strategy\Condition;

?>
<table class="table">
    <tr><td>Название:</td><td class="check-name"><?= isset($rule) ? $rule->name : '' ?></td></tr>
    <tr>
        <td>Область применения:</td>
        <td class="check-layer">
            <?php
            if (isset($rule)) {
                if ($rule->layer == Layer::LAYER_CAMPAIGNS)
                    echo 'Кампании';
                elseif ($rule->layer == Layer::LAYER_ADGROUPS)
                    echo 'Группы';
                elseif ($rule->layer == Layer::LAYER_ADS)
                    echo 'Объявления';
                elseif ($rule->layer == Layer::LAYER_KEYWORDS)
                    echo 'Ключевые слова';
            }
            ?>
        </td>
    </tr>
    <?php if (isset($rule) && $rule->layer == Layer::LAYER_CAMPAIGNS || !isset($rule)) { ?>
        <tr class="check-campaigns-tr">
            <td>Кампании:</td>
            <td class="check-campaigns">
                <?php
                if (isset($rule)) {
                    $campaigns = $rule->getCampaigns();
                    echo (count($campaigns) == $allCampsQuantity ? 'Выбраны все' : 'Выбрано ' . count($campaigns)) . ':';
                    echo '<div class="campaigns">';
                    $show = '';
                    if (count($campaigns) > 0) {
                        foreach ($campaigns as $campaign)
                            $show .= $campaign->source_id . ' - ' . $campaign->name . '<br>';
                    } else {
                        $show = 'Все';
                    }
                    echo $show;
                    echo '</div>';
                }
                ?>
            </td>
        </tr>
    <?php } ?>
    <?php if (isset($rule) && $rule->layer == Layer::LAYER_ADGROUPS || !isset($rule)) { ?>
        <tr class="check-groups">
            <td>Условия фильтрации групп:</td>
            <?php if (isset($rule)) { ?>
                <td><?php condsX($rule) ?></td>
            <?php } else {?>
                <td id="checkGroupsConds"></td>
            <?php } ?>
        </tr>
    <?php } ?>
    <?php if (isset($rule) && $rule->layer == Layer::LAYER_KEYWORDS || !isset($rule)) { ?>
        <tr class="check-keywords">
            <td>Условия фильтрации ключевых слов:</td>
            <?php if (isset($rule)) { ?>
                <td><?php condsX($rule) ?></td>
            <?php } else {?>
                <td id="checkKeywordsConds"></td>
            <?php } ?>
        </tr>
    <?php } ?>
    <tr>
        <td>Частота применения:</td>
        <td class="check-freq">
            <?php
            if (isset($rule)) {
                if ($rule->frequency == Rule::FREQ_ONCE) {
                    echo 'Один раз: ' . ($rule->instant_start ? '' : date('d.m.o', $rule->onceday));
                } elseif ($rule->frequency == Rule::FREQ_DAY) {
                    echo 'Каждый день';
                }
                elseif ($rule->frequency == Rule::FREQ_WEEK) {
                    echo 'Каждую неделю ';
                    if ($rule->weekday == Rule::WEEKDAY_MONDAY)
                        echo 'в понедельник';
                    elseif ($rule->weekday == Rule::WEEKDAY_TUESDAY)
                        echo 'во вторник';
                    elseif ($rule->weekday == Rule::WEEKDAY_WEDNSDAY)
                        echo 'в среду';
                    elseif ($rule->weekday == Rule::WEEKDAY_THURSDAY)
                        echo 'в четверг';
                    elseif ($rule->weekday == Rule::WEEKDAY_FRIDAY)
                        echo 'в пятницу';
                    elseif ($rule->weekday == Rule::WEEKDAY_SATTURDAY)
                        echo 'в субботу';
                    elseif ($rule->weekday == Rule::WEEKDAY_SUNDAY)
                        echo 'в воскресенье';
                }
                elseif ($rule->frequency == Rule::FREQ_MONTH) {
                    echo 'Один раз в месяц: ' . $rule->monthday . ' числа';
                }

                if ($rule->instant_start)
                    if ($rule->frequency == Rule::FREQ_ONCE)
                        echo '<br>Правило запущено после создания ' . date('d.m.o', $rule->created_at);
                    else
                        echo '<br>Первый раз правило запущено после создания ' . date('d.m.o', $rule->created_at);
            } else {
                echo 'Каждый день';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td>Условия:</td>
        <td class="check-rules">
            <div id="condition-container"></div>
        </td>
    </tr>
    <tr>
        <td>Действие:</td>
        <td class="check-action">
            <?php
            if (isset($rule)) {
                if ($rule->action == Limit::ACTION_NOTIFICATION)
                    echo 'Уведомление на почту: ' . $rule->account->company->user->email;
                elseif ($rule->action == Limit::ACTION_STOP)
                    echo 'Остановка рекламы с уведомлением на почту: ' . $rule->account->company->user->email;
            }
            ?>
        </td>
    </tr>
</table>

<?php
function condsX ($rule) {
    $conds = $rule->hasMany(Condition::className(), ['rule_id' => 'id'])->where('`index` = -1')->all();

    if ($conds)
        foreach ($conds as $cond) {
            $action = '';
            if ($cond->action == 0)
                $action = 'Равно';
            else if ($cond->action == 1)
                $action = 'Не равно';
            else if ($cond->action == 2)
                $action = 'Содержит';
            else if ($cond->action == 3)
                $action = 'Не содержит';
            else if ($cond->action == 4)
                $action = 'Начинается с';
            else if ($cond->action == 5)
                $action = 'Заканчивается на';

            echo "<div>$action $cond->value</div>";
        }
    else
        echo '<div>Все элементы</div>';
};