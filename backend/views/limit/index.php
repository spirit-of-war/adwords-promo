<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 16.08.16
 * Time: 10:58
 */

use yii\helpers\Html;
use common\models\structure\Layer;
use backend\widgets\TabMenu;
use backend\widgets\Panel;

$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

Panel::begin();

echo TabMenu::widget(['account' => $account, 'dimension' => Layer::LAYER_NAME_LIMIT]);
echo Html::a('Добавить', [
    'limit/create',
    'accountID' => $account->id
], ['class' => 'btn btn-primary']);

?>

<div class="grid-view">
<table class="table dataTable table-striped">
    <thead>
        <tr><th>Название</th><th>Применение</th><th></th></tr>
    </thead>
    <tbody>
    <?php
    foreach ($rules as $rule) {

        $layer = '';
        if ($rule->layer == Layer::LAYER_CAMPAIGNS)
            $layer = 'Кампании';
        elseif ($rule->layer == Layer::LAYER_ADGROUPS)
            $layer = 'Группы';
        elseif ($rule->layer == Layer::LAYER_ADS)
            $layer = 'Объявления';
        elseif ($rule->layer == Layer::LAYER_KEYWORDS)
            $layer = 'Ключевые слова';

        printf('<tr>
                    <td><a href="/limit/view?id=%s">%s</a></td>
                    <td>%s</td>
                    <td>
                        <a href="/limit/delete?id=%s" data-method="post"
                                        data-confirm="Вы действительно хотите удалить данный лимит?">
                            <span class="fa fa-remove"></span></a></td></tr>',
            $rule->id,
            $rule->name,
            $layer,
            $rule->id,
            $rule->id
        );
    }

    ?>
    </tbody>
</table>
</div>

<?php Panel::end();