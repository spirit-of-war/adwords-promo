<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 22.12.16
 * Time: 12:46
 */

use backend\assets\GoogleChartsAsset;
use yii\bootstrap\ActiveForm;
use backend\widgets\fieldsSelector\FieldsSelector;
use \backend\controllers\ReportController;
use backend\widgets\Panel;
use backend\widgets\TabMenu;
use common\models\structure\Layer;

$this->params['leftMenuItems'] = $leftMenuItems;
$this->params['breadcrumbs'] = $breadcrumbs;

GoogleChartsAsset::register($this);
$this->registerCssFile('/css/report/index.css');
$this->registerJsFile('/js/report/index.js', ['depends' => 'backend\assets\GoogleChartsAsset']);

Panel::begin();
echo TabMenu::widget(['account' => $account, 'dimension' => Layer::LAYER_NAME_REPORT]);
echo '<h1>Показатели эффективности</h1>';

$getmonthFullName = function ($shortName) {
    if ($shortName == 'янв') return 'Январь';
    if ($shortName == 'фев') return 'Февраль';
    if ($shortName == 'март') return 'Март';
    if ($shortName == 'апр') return 'Апрель';
    if ($shortName == 'май') return 'Май';
    if ($shortName == 'июнь') return 'Июнь';
    if ($shortName == 'июль') return 'Июль';
    if ($shortName == 'авг') return 'Август';
    if ($shortName == 'сен') return 'Сентябрь';
    if ($shortName == '') return '';
    if ($shortName == '') return '';
    if ($shortName == '') return '';
};

?>

<div id="w0" class="x_panel">

    <div class="row">
        <?php ActiveForm::begin(['options' => ['class' => 'form-horizontal form-label-left settings-form']]); ?>
            <input type="hidden" name="accountID" value="<?= $account->id ?>">
            <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Период</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <select class="form-control" name="period">
                                <option <?= $period == ReportController::PERIOD_WEEK ? 'selected' : '' ?>
                                        value="<?= ReportController::PERIOD_WEEK ?>">Неделя</option>
                                <option <?= $period == ReportController::PERIOD_SEVEN_DAYS ? 'selected' : '' ?>
                                        value="<?= ReportController::PERIOD_SEVEN_DAYS ?>">Последние 7 дней</option>
                                <option <?= $period == ReportController::PERIOD_MONTH ? 'selected'  : '' ?>
                                        value="<?= ReportController::PERIOD_MONTH ?>">Месяц</option>
                                <option <?= $period == ReportController::PERIOD_THIRTY_DAYS ? 'selected'  : '' ?>
                                        value="<?= ReportController::PERIOD_THIRTY_DAYS ?>">Последние 30 дней</option>
                                <option <?= $period == ReportController::PERIOD_YEAR ? 'selected'  : '' ?>
                                        value="<?= ReportController::PERIOD_YEAR ?>">Год</option>
                            </select>
                        </div>
                    </div>

            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary">Обновить отчет</button>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="clear"></div>
    <div class="x_title">
        <?php echo
        FieldsSelector::widget([
            'selectedFields' => $chartSettings,
            'account' => $account,
            'className' => 'chart',
            'saveButtonCaption' => 'Выбрать',
            'onSave' => '
$(\'.chart-fields\').remove();
$(\'.tfs-chart input[type=checkbox]:checked\').map(function(i,e){
    $(\'.settings-form\')
        .append(\'<input type="hidden" name="chartFields[]" class="chart-fields" value="\' + $(e).val() + \'" />\');
});'
        ]);
        ?>
        <h2>Статистика по эффективности аккаунта</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="row">
            <?php
            foreach ($chartValues as $value) {
                $data = [
                    ['Период', $value['indexTitle']],
                    [$value['fpName'], $value['fpValue']],
                    [$value['ppName'], $value['ppValue']],
                    [$value['lpName'], $value['lpValue']]
                ];
            ?>
                <div class="col-md-4">
                    <div class="report-chart chart-<?= $value['indexName'] ?>" data-chart='<?= json_encode($data)?>'></div>
                </div>
            <?php } ?>
        </div>

    </div>
    
    <div class="clear"></div>
    <div class="x_title">
        <?php echo
        FieldsSelector::widget([
            'selectedFields' => $indexSettings,
            'account' => $account,
            'className' => 'indexes',
            'saveButtonCaption' => 'Выбрать',
            'onSave' => '
$(\'.index-fields\').remove();
$(\'.tfs-indexes input[type=checkbox]:checked\').map(function(i,e){
    $(\'.settings-form\')
        .append(\'<input type="hidden" name="indexFields[]" class="index-fields" value="\' + $(e).val() + \'" />\');
});'
        ]);
        ?>
        <h2>Результаты (<?= $indexValues[0]['lpName'] ?>):</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="row">
            <?php foreach ($indexValues as $value) { ?>
                <div class="col-md-55">
                    <div class="thumbnail">
                        <div class="caption"><b><?= $value['indexTitle'] ?></b></div>
                        <div class="value">
                            <?php
                            $val = explode('.', $value['lpValue']);
                            $beforeDot = $val[0];
                            $beforeDot = str_split($beforeDot);
                            $i = 0;
                            $concatDigit = [];
                            foreach (array_reverse($beforeDot) as $digit) {
                                $concatDigit[] = $digit;
                                $i++;
                                if ($i == 3) {
                                    $concatDigit[] = ' ';
                                    $i = 0;
                                }
                            }
                            $concatDigit = array_reverse($concatDigit);
                            echo implode($concatDigit) . (isset($val[1]) ? '.' . $val[1] : '');
                            ?>
                        </div>
                        <div class="diff">
                            <?php if ($value['diff'] == 0) { ?>
                                = 0 %
                            <?php } else { ?>
                                <span style="color: <?= $value['diff'] >= 1 ? 'green' : 'red' ?>;">
                                    <i class="fa fa-level-<?= $value['diff'] >= 1 ? 'up' : 'down' ?>"></i>
                                    <?= $value['diff'] ?> %
                                </span>
                            <?php } ?>
                            <br>к <?= $value['ppName'] ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div>
</div>
<?php Panel::end();