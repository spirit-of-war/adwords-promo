<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;
class SmartWizard extends AssetBundle
{
    public $sourcePath = '@bower/gentelella/vendors/jQuery-Smart-Wizard/';
    public $css = [
        'styles/smart_wizard.css'
    ];
    public $js = [
        'js/jquery.smartWizard.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
