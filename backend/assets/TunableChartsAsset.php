<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class TunableChartsAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/tunableCharts/';

    public $js = [
        'script.js'
    ];

    public $css = [
        'style.css'
    ];

    public $depends = [
        'backend\assets\GoogleChartsAsset'
    ];
}
