<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;
class LayerFilter extends AssetBundle
{
    public $sourcePath = '@app/widgets/layerFilter/';
    public $js = [
        'script.js'
    ];
    public $jsOptions = [
        'type' => 'text/babel'
    ];
    public $css = [
        'style.css'
    ];
    public $depends = [
        'backend\assets\React',
    ];
}
