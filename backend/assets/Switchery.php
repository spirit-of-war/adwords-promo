<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 08.08.16
 * Time: 10:19
 */

namespace backend\assets;

use yii\web\AssetBundle;

class Switchery extends AssetBundle
{
    public $sourcePath = '@bower/gentelella/vendors/switchery/dist/';
    public $css = [
        'switchery.min.css'
    ];
    public $js = [
        'switchery.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}