<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 08.08.16
 * Time: 10:19
 */

namespace backend\assets;

use yii\web\AssetBundle;

class PNotify extends AssetBundle
{
    public $sourcePath = '@bower/gentelella/vendors/pnotify/dist/';
    public $css = [
        'pnotify.css',
        'pnotify.buttons.css',
        'pnotify.nonblock.css',
    ];
    public $js = [
        'pnotify.js',
        'pnotify.buttons.js',
        'pnotify.nonblock.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}