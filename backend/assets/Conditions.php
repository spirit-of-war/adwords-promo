<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;
class Conditions extends AssetBundle
{
    public $sourcePath = '@app/widgets/conditions/';
    public $js = [
        'script.js'
    ];
    public $jsOptions = [
        'type' => 'text/babel'
    ];
    public $css = [
        'style.css'
    ];
    public $depends = [
        'backend\assets\React',
    ];
}
