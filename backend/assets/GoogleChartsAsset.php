<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class GoogleChartsAsset extends AssetBundle
{
    public $js = [
        'https://www.gstatic.com/charts/loader.js',
    ];

    public $depends = [
        'backend\assets\AppAsset'
    ];
}
