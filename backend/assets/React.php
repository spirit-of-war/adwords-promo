<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;
class React extends AssetBundle
{
    public $js = [
        'https://npmcdn.com/react@15.3.0/dist/react.js',
        'https://npmcdn.com/react-dom@15.3.0/dist/react-dom.js',
        'https://npmcdn.com/babel-core@5.8.38/browser.min.js',
    ];
}
