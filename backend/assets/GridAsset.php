<?php

namespace backend\assets;

use yii\web\AssetBundle;

class GridAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/grid.css',
    ];
}
