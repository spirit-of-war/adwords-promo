<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;
class CampaignSelector extends AssetBundle
{
    public $sourcePath = '@app/widgets/campaignSelector/';
    public $js = [
        'script.js'
    ];
    public $jsOptions = [
        'type' => 'text/babel'
    ];
    public $css = [
        'style.css'
    ];
    public $depends = [
        'backend\assets\React',
    ];
}
