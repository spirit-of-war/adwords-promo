<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 08.08.16
 * Time: 10:19
 */

namespace backend\assets;

use yii\web\AssetBundle;

class Select2 extends AssetBundle
{
    public $sourcePath = '@bower/gentelella/vendors/select2/dist/';
    public $css = [
        'css/select2.min.css'
    ];
    public $js = [
        'js/select2.full.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}