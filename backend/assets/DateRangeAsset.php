<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 03.08.16
 * Time: 10:28
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Asset for date range input control.
 */
class DateRangeAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/daterangepicker/';
    public $css = [
        'style.css',
    ];
    public $js = [
        'moment.min.js',
        'script.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
