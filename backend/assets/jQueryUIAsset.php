<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class jQueryUIAsset extends AssetBundle
{
    public $js = [
        'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
    ];

    public $depends = [
        'backend\assets\AppAsset',
    ];
}
