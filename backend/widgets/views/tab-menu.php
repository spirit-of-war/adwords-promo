<?php
use common\models\structure\Layer;
use yii\helpers\Url;

use yii\widgets\Menu;

//$dimension = '';
?>

<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_CAMPAIGN ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'account/view',
        'dimension' => Layer::LAYER_NAME_CAMPAIGN,
        'accountID' => $account->id])
    ?>" aria-expanded="<?= $dimension == Layer::LAYER_NAME_CAMPAIGN ? 'true' : 'false' ?>">Кампании</a>
</li>
<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_ADGROUP ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'account/view',
        'dimension' => Layer::LAYER_NAME_ADGROUP,
        'accountID' => $account->id])
    ?>" aria-expanded="<?= $dimension == Layer::LAYER_NAME_ADGROUP ? 'true' : 'false' ?>">Группы</a>
</li>
<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_KEYWORD ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'account/view',
        'dimension' => Layer::LAYER_NAME_KEYWORD,
        'accountID' => $account->id, ])
    ?>" aria-expanded="<?= $dimension == Layer::LAYER_NAME_KEYWORD ? 'true' : 'false' ?>">Слова</a>
</li>
<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_AD ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'account/view',
        'dimension' => Layer::LAYER_NAME_AD,
        'accountID' => $account->id])
    ?>" aria-expanded="<?= $dimension == Layer::LAYER_NAME_AD ? 'true' : 'false' ?>">Объявления</a>
</li>

<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_RULE ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'rule/index',
        'accountID' => $account->id])
    ?>">Правила</a>
</li>
<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_LIMIT ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'limit/index',
        'accountID' => $account->id])
    ?>">Лимиты</a>
</li>
<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_REPORT ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'report/index',
        'accountId' => $account->id])
    ?>">Отчеты</a>
</li>
<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_SETTING ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'account/update',
        'id' => $account->id])
    ?>">Настройки</a>
</li>
<li role="presentation" class="<?= $dimension == Layer::LAYER_NAME_DEAD_LINK ? 'active' : '' ?>">
    <a href="<?= Url::to([
        'account/view-dead-links',
        'accountID' => $account->id])
    ?>">Проверка ссылок</a>
</li>
</ul>