/**
 * Created by savchenko on 07.11.16.
 */
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    $('.chart-fields-selector-indexes').hide();
    var indexes = [];
    $('.chart-fields-selector-indexes input:checked').each(function(key, element) {
        indexes.push($(element).val());
    });
    $.ajax({
        url: '/chart',
        method: 'POST',
        data: {
            fields: indexes,
            accountID: accountID,
            dates: $('.daterange').val()
        },
        success: function(data) {
            data = $.parseJSON(data);
            data = google.visualization.arrayToDataTable(data);
            var options = {
                legend: { position: 'right' },
                focusTarget: 'category',
                pointSize: 3
            };
            var chart = new google.visualization.LineChart(document.getElementById('googleChartsPlaceholder'));
            chart.draw(data, options);
        }
    });
}

$('.chart-fields-selector i').on('click', function () {
    $('.chart-fields-selector-indexes').toggle();
});

$('.chart-fields-selector .cancel').on('click', function () {
    $('.chart-fields-selector-indexes').hide();
});

$('.chart-fields-selector .save').on('click', function () {
    drawChart();

    var indexes = [];
    $('.chart-fields-selector-indexes input:checked').each(function(key, element) {
        indexes.push($(element).val());
    });
    $.ajax({
        url: '/chart/save-settings',
        method: 'POST',
        data: {
            fields: indexes,
            accountID: accountID,
            layer: view
        }
    });
});