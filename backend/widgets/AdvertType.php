<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 11.07.16
 * Time: 18:50
 */

namespace backend\widgets;

class AdvertType extends YandexServiceType
{
    public function run()
    {
        $this->login = $this->account->advert_login;
        $this->token = $this->account->advert_token;
        return parent::run();
    }
}