/**
 * Created by savchenko on 26.10.16.
 */

var Condition = React.createClass({
    getInitialState: function () {
        return ({
            sign: this.props.cond.sign,
            value: this.props.cond.value
        });
    },
    changeSign: function (e) {
        this.setState({
            sign: e.target.value
        });
        this.props.cond.sign = e.target.value;
        this.props.onChange();
    },
    changeValue: function (e) {
        this.setState({
            value: e.target.value
        });
        this.props.cond.value = e.target.value;
        this.props.onChange();
    },
    remove: function () {
        this.props.onRemove(this.props.cond.id);
    },
    render: function() {
        return (
            <div className="name-conditions-item">
                <div className="name-conditions-item-control">
                    <select value={this.state.sign} onChange={this.changeSign}>
                        <option value={0}>Равно</option>
                        <option value={1}>Не равно</option>
                        <option value={2}>Содержит</option>
                        <option value={3}>Не содержит</option>
                        <option value={4}>Начинается с</option>
                        <option value={5}>Заканчивается на</option>
                    </select>
                </div>
                <div className="name-conditions-item-control">
                    <input type="text" value={this.state.value} onChange={this.changeValue} />
                </div>
                <div className="name-conditions-item-control">
                    <div className="name-conditions-item-remove" onClick={this.remove}>
                        <i className="fa fa-remove"></i></div>
                </div>
                <div className="clear"></div>
            </div>
        );
    }
});

var ConditionList = React.createClass({
    getInitialState: function () {
        return ({
            conds: this.props.conds,
            hiddenConds: [],
            selectAll: this.props.conds.length == 0
        });
    },
    addCondition: function () {
        var id = this.state.conds.length > 0 ? this.state.conds[this.state.conds.length - 1].id + 1 : 0;
        this.state.conds.push({
            id: id,
            sign: 0,
            value: ''
        });
        this.state.selectAll = false;
        this.setState(this.state);
    },
    remove: function (id) {
        for (var i = 0; i < this.state.conds.length; i++)
            if (this.state.conds[i].id == id)
                this.state.conds.splice(i, 1);

        this.setState({
            conds: this.state.conds,
            selectAll: this.state.conds.length == 0
        });
    },
    serialize: function () {
        console.log(134);
        if (document.getElementById(this.props.serializeField) != null)
            document.getElementById(this.props.serializeField).value = JSON.stringify(this.state.conds).replace('[]', '');

        if (document.getElementById(this.props.checkField) != null) {
            var conds;
            if (this.state.conds.length > 0)
                conds = this.state.conds.map(function (cond) {
                    var sign = '';
                    if (cond.sign == 0)
                        sign = 'Равно';
                    else if (cond.sign == 1)
                        sign = 'Не равно';
                    else if (cond.sign == 2)
                        sign = 'Содержит';
                    else if (cond.sign == 3)
                        sign = 'Не содержит';
                    else if (cond.sign == 4)
                        sign = 'Начинается с';
                    else if (cond.sign == 5)
                        sign = 'Заканчивается на';

                    return (<div key={cond.id}>{sign} {cond.value}</div>);
                });
            else
                conds = <span>все элементы</span>;
            conds = <div>{conds}</div>;
            ReactDOM.render(conds, document.getElementById(this.props.checkField));
        }
    },
    componentDidMount: function () {
        this.serialize();
    },
    componentDidUpdate: function () {
        this.serialize();
    },
    selectAll: function (e) {
        var newState = {
            selectAll: !this.state.selectAll,
            conds: !this.state.selectAll ? [] : this.state.hiddenConds,
            hiddenConds: !this.state.selectAll ? this.state.conds : []
        };
        this.setState(newState);
    },
    render: function() {
        var conds = this.state.conds.map(function (cond) {
            return <Condition key={cond.id} cond={cond} onRemove={this.remove} onChange={this.serialize} />
        }.bind(this));
        return (
            <div className="name-conditions">
                {conds}
                <div onClick={this.addCondition} className="name-conditions-add clear"
                    style={{display: this.state.selectAll ? 'none' : 'block'}}>Добавить условие</div>
                <input type="checkbox" checked={this.state.selectAll} onClick={this.selectAll} /> все элементы
            </div>
        );
    }
});

ReactDOM.render(
    <ConditionList serializeField="ruleKeywords" checkField="checkKeywordsConds" conds={keywordsConds} />,
    document.getElementById('keywordsSelectorHolder')
);

ReactDOM.render(
    <ConditionList serializeField="ruleGroups" checkField="checkGroupsConds"  conds={adgroupsConds} />,
    document.getElementById('groupsSelectorHolder')
);