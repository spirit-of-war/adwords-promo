var Item = React.createClass({
    getInitialState: function () {
        var indexes = [];
        var title = '';
        for (var i = 0; i < this.props.indexes.length; i++) {
            var options = [];
            var group = this.props.indexes[i];
            for (var j = 0; j < group.indexes.length; j++) {
                var index = this.props.indexes[i].indexes[j];
                options.push(<option key={Math.random()} value={index.name}>{group.title}: {index.title}</option>);
                if (this.props.cond.index == index.name)
                    title = group.title + ': ' + index.title;
            }
            indexes.push(<optgroup key={Math.random()} label={group.title}>{options}</optgroup>);
        }

        var actions;
        for (var i = 0; i < this.props.indexes.length; i++)
            for (var j = 0; j < this.props.indexes[i].indexes.length; j++)
                if (this.props.indexes[i].indexes[j].name == this.props.cond.index)
                    actions = this.props.indexes[i].indexes[j].action.map(function (act) {
                        return (<option key={Math.random()}>{act}</option>);
                    });

        return ({
            index: this.props.cond.index,
            title: title,
            action: this.props.cond.action,
            value: this.props.cond.value,
            period: this.props.cond.period,
            indexes: indexes,
            actions: actions
        });
    },
    changeIndex: function (e) {
        var actions, action, title;
        for (var i = 0; i < this.props.indexes.length; i++)
            for (var j = 0; j < this.props.indexes[i].indexes.length; j++)
                if (this.props.indexes[i].indexes[j].name == e.target.value) {
                    title = this.props.indexes[i].title + ': ' + this.props.indexes[i].indexes[j].title;
                    actions = this.props.indexes[i].indexes[j].action.map(function (act) {
                        return (<option key={Math.random()} value={act}>{act}</option>);
                    });
                    action = this.props.indexes[i].indexes[j].action[0];
                }

        this.setState({
            index: e.target.value,
            actions: actions,
            action: action,
            title: title,
        });
        this.props.cond.index = e.target.value;
        this.props.serialize();
    },
    changeAction: function (e) {
        this.setState({action: e.target.value});
        this.props.cond.action = e.target.value;
        this.props.serialize();
    },
    changeValue: function (e) {
        this.setState({value: e.target.value});
        this.props.cond.value = e.target.value;
        this.props.serialize();
    },
    remove: function () {
        this.props.remove(this.props.cond);
    },
    render: function () {
        return (
            <div className="condition-group-item">
                <div className="condition-group-item-control">
                    <div className="condition-group-item-and">И</div>
                </div>
                <div className="condition-group-item-control">
                    <span>{this.state.title}</span>
                    <select value={this.state.index} onChange={this.changeIndex}>{this.state.indexes}</select>
                </div>
                <div className="condition-group-item-control">
                    <span>{this.state.action}</span>
                    <select value={this.state.action} onChange={this.changeAction}>{this.state.actions}</select>
                </div>
                <div className="condition-group-item-control">
                    <span>{this.state.value}</span>
                    <input placeholder="Значение" value={this.state.value} onChange={this.changeValue} />
                </div>
                <div className="condition-group-item-control">
                    <div className="condition-group-item-remove" onClick={this.remove}>
                        <i className="fa fa-remove"></i></div>
                </div>
            </div>
        );
    }
});

var Group = React.createClass({
    getInitialState: function () {
        return ({
            conds: this.props.group.conds,
            period: this.props.group.conds[0].period
        });
    },
    addAnd: function () {
        var id = this.state.conds[this.state.conds.length - 1].id + 1;
        this.state.conds.push({
            id: id,
            index: this.props.indexes[0].indexes[0].name,
            action: this.props.indexes[0].indexes[0].action[0],
            value: '',
            period: this.state.period
        });
        this.setState(this.state);
        this.props.serialize();
    },
    removeCondition: function (cond) {
        for (var i = 0; i < this.state.conds.length; i++)
            if (cond.id == this.state.conds[i].id)
                this.state.conds.splice(i, 1);

        if (this.state.conds.length == 0)
            this.props.remove(this.props.group)
        else
            this.setState(this.state);
        this.props.serialize();
    },
    changePeriod: function (e) {
        this.setState({
            period: e.target.value
        });
        for (var i = 0; i < this.props.group.conds.length; i++)
            this.props.group.conds[i].period = e.target.value
        this.props.serialize();
    },
    render: function () {
        var items = this.state.conds.map(function (cond) {
            return <Item key={cond.id} cond={cond} indexes={this.props.indexes} remove={this.removeCondition}
                        serialize={this.props.serialize} />
        }.bind(this));
        return (
            <div className=".condition-or">
                <div className="condition-or-label">ИЛИ</div>
                <div className="condition-group">
                    <div className="condition-group-period">На основе статистики за последние&nbsp;
                        <span>{this.state.period}</span>
                        <input type="number" min="1" max="999" value={this.state.period}
                               onChange={this.changePeriod} /> дней
                    </div>
                    <div className="condition-group-items">
                        {items}
                    </div>
                    <div className="condition-group-item-add" onClick={this.addAnd}>Добавить И</div>
                </div>
            </div>
        );
    }
});

var ConditionList = React.createClass({
    getInitialState: function () {
        var conds = [];
        if (this.props.conds.length == 0)
            conds.push(this.newOr());
        else
            conds = this.props.conds;

        return ({
            conds: conds
        });
    },
    addOr: function () {
        this.state.conds.push(this.newOr());
        this.setState(this.state);
        this.serialize();
    },
    newOr: function () {
        var id = 1;
        if (this.state != null && this.state.conds.length > 0)
            id = this.state.conds[this.state.conds.length - 1].id + 1;
        return {
            id: id,
            conds: [{
                id: 1,
                index: this.props.indexes[0].indexes[0].name,
                title: this.props.indexes[0].indexes[0].title,
                action: this.props.indexes[0].indexes[0].action[0],
                value: '',
                period: 7
            }]
        };
    },
    componentWillMount: function () {
        this.serialize();
    },
    removeGroup: function (group) {
        for (var i = 0; i < this.state.conds.length; i++)
            if (group.id == this.state.conds[i].id)
                this.state.conds.splice(i, 1);
        this.setState(this.state);
        this.serialize();
    },
    serialize: function () {
        if (document.getElementById('conditions') != null)
            document.getElementById('conditions').value = JSON.stringify(this.state.conds);
        setTimeout(function () {
            $('.check-rules').html($('#condition-container').html());
        }, 100);
    },
    render: function () {
        var groups = this.state.conds.map(function (group) {
            return <Group key={group.id} group={group} indexes={this.props.indexes} remove={this.removeGroup}
                          serialize={this.serialize} />;
        }.bind(this));
        return (
            <div>
                {groups}
                <div className="condition-group-item-add condition-group-item-add-or"
                     onClick={this.addOr}>Добавить ИЛИ</div>
            </div>
        );
    }
});

ReactDOM.render(
    <ConditionList indexes={indexes} conds={existingConds} />,
    document.getElementById('condition-container')
);