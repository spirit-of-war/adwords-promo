<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 11.07.16
 * Time: 19:11
 */

namespace backend\widgets;

use yii\base\Widget;
use common\models\Account;
use yii\helpers\Html;
use backend\controllers\AccountController;

abstract class YandexServiceType extends Widget
{
    public $account;
    public $showLink = true;

    protected $yandexServiceName = 'Яндекс.Директ';
    protected $googleServiceName = 'Google AdWords';

    protected $login = '';
    protected $token = '';

    public function run()
    {
        $type = $this->account->stat_type == Account::TYPE_YANDEX ? $this->yandexServiceName : $this->googleServiceName;
        $login = $this->login == '' ? '' : ': ' . $this->login;
        $token = $this->token == '' ? ', ' . Html::a('токен не получен',
                ['account/token-request', 'accountID' => $this->account->id, 'type' => AccountController::TYPE_CONTEXT],
                [
                    'onmouseenter' => '$(this).text("получить токен")',
                    'onmouseleave' => '$(this).text("токен не получен")',
                ]) : '';
        if ($token === '' && $this->showLink)
            return Html::a($type . $login, [
                'account/view',
                'accountID' => $this->account->id,
                'dimension' => 'campaign'
            ]);
        else
            return $type . $login . $token;
    }
}