var Campaign = React.createClass({
    getInitialState: function () {
        return {
            selected: this.props.camp.selected
        };
    },
    select: function () {
        this.props.select(this.props.camp);
    },
    render: function () {
        return (
            <div
                className={this.props.selected ? 'campaign-selector-list-item-selected' : 'campaign-selector-list-item'}
                onClick={this.select}>
                <input type="checkbox" checked={this.props.selected ? 'checked' : ''} />{' '}
                {this.props.camp.sourceID} - {this.props.camp.name}
            </div>
        );
    }
});

var CampaignSelector = React.createClass({
    getInitialState: function () {
        $('.rule-campaigns').val(this.props.selectedCamps);
        return {
            camps: [],
            selectedCamps: this.props.selectedCamps,
            filteredCamps: [],
            filterText: '',
            selectAll: false,
            quantity: 0
        };
    },
    updateList: function () {
        $.ajax({
            url: "/api/get-campaigns?accountID=" + $('#account').val(),
        })
            .done(function(campaigns) {
                campaigns = JSON.parse(campaigns);
                campaigns = campaigns.map(function (camp) {
                    return {
                        id: camp.id,
                        sourceID: camp.sourceID,
                        name: camp.name,
                        selected: false,
                    };
                });
                this.setState({
                    camps: campaigns,
                    selectedCamps: this.props.selectedCamps,
                    filteredCamps: campaigns,
                    filterText: '',
                    selectAll: false,
                    quantity: 0,
                    showSelectedText: 'Показать выбранные'
                });
                $('.rule-campaigns').val(this.props.selectedCamps);
            }.bind(this));
    },
    updateListViaAccountSelect: function (e) {
        if (e.target.className.indexOf('account-select') != -1) {
            this.updateList();
        }
    },
    componentDidMount: function () {
        this.updateList();
        window.addEventListener('click', this.updateListViaAccountSelect);
    },
    filter: function (e) {
        var filterText = e.target.value;
        var filteredCamps = [];
        for (var i = 0; i < this.state.camps.length; i++) {
            var camp = this.state.camps[i];
            if ($.trim(filterText) == ''
                || camp.name.toLowerCase().indexOf($.trim(filterText.toLowerCase())) != -1
                || camp.sourceID.indexOf($.trim(filterText)) != -1)
                filteredCamps.push(camp);
        }
        this.setState({
            filteredCamps: filteredCamps,
            filterText: filterText,
            selectAll: false,
            allCamps: undefined,
            showSelectedText: 'Показать выбранные'
        });

    },
    selectAll: function () {
        var selectAll = this.state.selectAll != true;
        if (selectAll) {
            var selectedCamps = this.state.filteredCamps.map(function (camp) {
                return camp.id;
            });
            selectedCamps = this.state.selectedCamps.concat(selectedCamps);
            selectedCamps = $.unique(selectedCamps);
        } else {
            selectedCamps = [];
        }

        this.setState({
            selectedCamps: selectedCamps,
            selectAll: selectAll
        });
        $('.rule-campaigns').val(selectedCamps);
        this.step5(selectedCamps);
    },
    select: function (camp) {
        var pos = this.state.selectedCamps.indexOf(camp.id);
        if (pos == -1)
            this.state.selectedCamps.push(camp.id);
        else
            this.state.selectedCamps.splice(pos, 1);

        this.setState(this.state);
        $('.rule-campaigns').val(this.state.selectedCamps);
        this.step5(this.state.selectedCamps);
    },
    step5: function (selectedCamps) {
        var step5Camps = [];
        for (var i = 0; i < this.state.filteredCamps.length; i++)
            if (selectedCamps.indexOf(this.state.filteredCamps[i].id) >= 0)
                step5Camps.push(this.state.filteredCamps[i]);
        var html = step5Camps.map(function (camp) {
            return '<div>' + camp.sourceID + ' - ' + camp.name + '</div>';
        });
        html = html.join('');
        html = 'Выбрано ' + step5Camps.length + ': <div class="campaigns">' + html + '</div>';
        $('.check-campaigns').html(html);
    },
    clear: function () {
        this.setState({
            filteredCamps: this.state.camps,
            selectedCamps: [],
            filterText: ''
        });
    },
    showSelected: function () {
        if (this.state.allCamps == undefined) {
            var camps = [];
            var allCamps = this.state.filteredCamps;

            for (var i = 0; i < this.state.camps.length; i++)
                if (this.state.selectedCamps.indexOf(this.state.camps[i].id) >= 0)
                    camps.push(this.state.camps[i]);

            this.setState({
                filteredCamps: camps,
                allCamps: allCamps,
                showSelectedText: 'Показать все'
            });
        } else {
            this.setState({
                filteredCamps: this.state.allCamps,
                allCamps: undefined,
                showSelectedText: 'Показать выбранные'
            });
        }
    },
    render: function () {

        var camps = this.state.filteredCamps.map(function (camp) {
            var selected = this.state.selectedCamps.indexOf(camp.id) > -1;
            return (<Campaign key={camp.id} camp={camp} select={this.select} selected={selected} />);
        }.bind(this));
        return (
            <div className="campaign-selector">
                <div className="campaign-selector-filter">
                    <input type="text" placeholder="Фильтр по названию или идентификатору"
                           onChange={this.filter} value={this.state.filterText} />
                </div>
                <div className="campaign-selector-all" onClick={this.selectAll}>
                    <input type="checkbox" checked={this.state.selectAll} /> Выбрать все</div>
                <div className="campaign-selector-list-items">
                    {camps}
                </div>
                <div className="campaign-selector-summary">
                    Выбрано {this.state.selectedCamps.length}
                    {this.state.selectedCamps.length > 0 ? <span>: </span>: ''}
                    {this.state.selectedCamps.length > 0 ? <span className="clear" onClick={this.clear}>Очистить</span>: ''}
                    {this.state.selectedCamps.length > 0 ?
                                <span className="show-selected"
                                        onClick={this.showSelected}>{this.state.showSelectedText}</span>: ''}
                </div>
            </div>
        );
    }
});

ReactDOM.render(
    <CampaignSelector selectedCamps={selectedCamps} />,
    document.getElementById('campaignHolder')
);