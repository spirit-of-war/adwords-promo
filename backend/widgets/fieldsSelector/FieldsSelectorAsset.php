<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 23.12.16
 * Time: 22:02
 */

namespace backend\widgets\fieldsSelector;

use yii\web\AssetBundle;

class FieldsSelectorAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/fieldsSelector/views/';
    public $js = [
        'script.js'
    ];
    public $css = [
        'style.css'
    ];
    public $depends = [
        'backend\assets\jQueryUIAsset'
    ];
}