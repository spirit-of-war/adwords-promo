<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 23.12.16
 * Time: 22:03
 */

namespace backend\widgets\fieldsSelector;

use yii\web\AssetBundle;

class FieldsSelectorSaveDefaultAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/fieldsSelector/views/';
    public $js = [
        'saveDefault.js'
    ];
    public $depends = [
        'backend\widgets\fieldsSelector\FieldsSelectorAsset'
    ];
}