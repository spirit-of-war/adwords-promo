<?php

namespace backend\widgets\fieldsSelector;

use yii\base\Exception;
use yii\base\Widget;

class FieldsSelector extends Widget
{
    public $savePath, $selectedFields, $advertType, $account, $onSave, $className, $saveButtonCaption;


    public function init()
    {
        parent::init();

        if ($this->selectedFields === null || $this->account === null)
            throw new Exception('Please specify the savePath, selectedFields and advertType');
    }

    public function run()
    {
        return $this->render('index', [
            'savePath' => $this->savePath,
            'selectedFields' => $this->selectedFields,
            'advertType' => $this->advertType,
            'account' => $this->account,
            'onSave' => $this->onSave,
            'className' => 'tfs-' . ($this->className === null ? rand() : $this->className),
            'saveButtonCaption' => $this->saveButtonCaption === null ? 'Сохранить' : $this->saveButtonCaption
        ]);
    }
}