/**
 * Created by savchenko on 22.12.16.
 */

$('.table-fields-selector i').on('click', function (e) {
    $('.table-fields-selector-indexes').hide();
    $(
        $(e.target).parent().children('.table-fields-selector-indexes')
    ).show();
});

$('.table-fields-selector .cancel').on('click', function () {
    $('.table-fields-selector-indexes').hide();
});

$('.table-fields-selector-indexes ul').sortable();