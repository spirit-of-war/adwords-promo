/**
 * Created by savchenko on 22.12.16.
 */

$('.table-fields-selector .save').on('click', function (e) {
    var fields = [];
    $(e.target).parent().find('ul input[type=checkbox]:checked').each(function(key, element) {
        fields.push($(element).val());
    });
    $.ajax({
        url: $(e.target).parent().data('save-path'),
        method: 'POST',
        data: {
            fields: fields,
            accountID: accountID,
        },
        success: function(data) {
            if ($.parseJSON(data) == true)
                location.reload();
        }
    });
});