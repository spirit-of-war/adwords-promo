<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 23.12.16
 * Time: 16:12
 */

use common\models\strategy\Index;
use backend\widgets\fieldsSelector\FieldsSelectorAsset;
use \backend\widgets\fieldsSelector\FieldsSelectorSaveDefaultAsset;

FieldsSelectorAsset::register($this);
if ($onSave === null)
    FieldsSelectorSaveDefaultAsset::register($this);
else
    $this->registerJs('$(\'.' . $className . ' .save\').on(\'click\', function(){'
        . $onSave
        . '$(\'.' . $className . ' .table-fields-selector-indexes\').hide()'
        . '});');

$this->registerJs('var accountID = ' . $account->id . ';', \yii\web\View::POS_HEAD);
?>

<div class="table-fields-selector <?= $className ?>">
    <i class="fa fa-wrench"></i>&nbsp;
    <div class="table-fields-selector-indexes" data-save-path="<?= $savePath ?>">
        <ul>
            <?php
            foreach ($selectedFields as $field) {
                $index = Index::getIndex($field);
                printf(
                    '<li class="checkbox"><label><input type="checkbox" checked value="%s" %s> %s
                                                    <i class="fa fa-reorder"></i></label></li>',
                    $index['name'],
                    $index['name'] == 'clicks' ? 'checked' : '',
                    Index::getGroup($field)['title'] . ': ' . $index['title']
                );
            }
            foreach (Index::getAllByAccountType($account->advert_type) as $group) {
                foreach ($group['indexes'] as $index) {
                    if (!in_array($index['name'], $selectedFields))
                        printf(
                            '<li class="checkbox"><label><input type="checkbox" value="%s"> %s
                                                    <i class="fa fa-reorder"></i></label></li>',
                            $index['name'],
                            $group['title'] . ': ' . $index['title']
                        );
                }
            }
            ?>
        </ul>
        <a class="btn save"><?= $saveButtonCaption ?></a>
        <a class="btn cancel">Отмена</a>
    </div>
</div>
