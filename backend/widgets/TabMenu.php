<?php
namespace backend\widgets;;

use common\models\Account;
use yii\base\Widget;
use yii\helpers\Html;

class TabMenu extends Widget
{
    public $account;
    public $dimension;

    public function init()
    {
        parent::init();
        if ($this->account === null) {
            $this->account =  Account::findOne(1);
        }
        if ($this->dimension === null) {
            $this->dimension =  '';
        }
    }

    public function run()
    {
        return $this->render('tab-menu', ['account' => $this->account, 'dimension' => $this->dimension]);
    }
}