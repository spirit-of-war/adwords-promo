<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 11.07.16
 * Time: 19:10
 */

namespace backend\widgets;

class StatType extends YandexServiceType
{
    protected $yandexServiceName = 'Яндекс.Метрика';
    protected $googleServiceName = 'Google Analytics';

    public function run()
    {
        $this->login = $this->account->stat_login;
        $this->token = $this->account->stat_token;
        return parent::run();
    }
}