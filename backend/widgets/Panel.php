<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 13.07.16
 * Time: 13:16
 */

namespace backend\widgets;

use rmrevin\yii\fontawesome\component\Icon;
use yii\helpers\Html;

class Panel extends \yiister\gentelella\widgets\Panel
{
    public $editLink = false;

    public function initTools()
    {
        if ($this->editLink !== false)
            $this->tools[] = Html::a(new Icon('wrench'), $this->editLink);

        parent::initTools();
    }
}