<?php
namespace backend\controllers;

use common\models\Account;
use common\models\structure\Layer;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $accountID = Yii::$app->db->createCommand('select a.id from account a
                                              inner join company c on a.company_id = c.id
                                              inner join user u on c.user_id = u.id
                                              where u.id = :userID and a.state = :state limit 1',
            [
                ':userID' => Yii::$app->user->id,
                ':state' => Account::STATE_ACTIVE
        ])->queryScalar();

        if ($accountID !== false)
            return $this->redirect(
                Url::to([
                    'account/view',
                    'dimension' => Layer::LAYER_NAME_CAMPAIGN,
                    'accountID' => (int)$accountID
                ])
            );
        else
            return $this->redirect(
                Url::to(['company/index'])
            );
    }

    public function actionLogin()
    {
        $this->layout = false;
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    protected function getBreadcrumbs($view, $model)
    {
        return [];
    }
}