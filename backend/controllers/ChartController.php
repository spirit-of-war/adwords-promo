<?php

namespace backend\controllers;

use common\models\strategy\Index;
use \yii\web\Controller;

class ChartController extends Controller implements SettingsInterface
{
    use SettingsTrait;

    const SETTING_TYPE = 1;

    public function actionIndex()
    {
        $indexes = \Yii::$app->request->post('fields');
        $accountID = \Yii::$app->request->post('accountID');
        $getDate = function ($index) {
            $date = \Yii::$app->request->post('dates');
            $date = trim(explode(' - ', $date)[$index]);
            return strtotime($date);
        };
        $startDate = $getDate(0);
        $endDate = $getDate(1);

        $headers = array_map(function ($index) {
            return Index::getFullName($index);
        }, $indexes);
        $data[] = array_merge(['date'], $headers);

        $sql = array_map(function ($index) {
            return Index::getIndex($index)['expression'] . ' ' . $index;
        }, $indexes);
        $sql = implode(',', $sql);
        $sql = "select from_unixtime(start_date, '%d.%m.%Y') date, {$sql} from statistics s 
                        where start_date >= {$startDate} and end_date <= {$endDate}
                              and s.account_id = {$accountID} and s.campaign_id is not null 
                        group by start_date";
        $rawData = \Yii::$app->db->createCommand($sql)->queryAll();

        foreach ($rawData as $row) {
            $dataRow = [
                $row['date']
            ];
            foreach ($indexes as $index)
                $dataRow[] = round((float)$row[$index], 2);
            $data[] = $dataRow;
        }

        return json_encode($data);
    }

    public function actionSaveSettings()
    {
        $this->saveSettings(self::SETTING_TYPE);
    }
}

