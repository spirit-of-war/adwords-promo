<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 10.12.16
 * Time: 14:39
 */

namespace backend\controllers;


interface SettingsInterface
{
    public function saveSettings($type);
}