<?php

namespace backend\controllers;

use common\models\strategy\Index;
use common\models\Account;
use common\models\structure\Layer;
use Yii;

class ReportController extends BaseController implements SettingsInterface
{
    use SettingsTrait;

    const REPORT_INDEX_SETTINGS = 2;
    const REPORT_CHART_SETTINGS = 3;

    const PERIOD_WEEK = 1;
    const PERIOD_SEVEN_DAYS = 2;
    const PERIOD_MONTH = 3;
    const PERIOD_THIRTY_DAYS = 4;
    const PERIOD_YEAR = 5;

    public function actionIndex($accountId)
    {
        $period = Yii::$app->request->post('period', self::PERIOD_WEEK);

        if (Yii::$app->request->method == 'POST') {
            $indexFields = Yii::$app->request->post('indexFields', false);
            if ($indexFields !== false)
                $this->saveSettings(self::REPORT_INDEX_SETTINGS, $indexFields);

            $chartFields = Yii::$app->request->post('chartFields', false);
            if ($chartFields !== false)
                $this->saveSettings(self::REPORT_CHART_SETTINGS, $chartFields);
        }

        $indexSettings = $this->getSettings($accountId, Layer::LAYER_CAMPAIGNS, self::REPORT_INDEX_SETTINGS);
        if (count($indexSettings) == 0)
            $indexSettings = ['clicks', 'shows', 'sum'];

        $chartSettings = $this->getSettings($accountId, Layer::LAYER_CAMPAIGNS, self::REPORT_CHART_SETTINGS);
        if (count($chartSettings) == 0)
            $chartSettings = ['clicks', 'shows', 'sum'];

        return $this->render('index', [
            'account' => Account::findOne($accountId),
            'indexSettings' => $indexSettings,
            'chartSettings' => $chartSettings,
            'indexValues' => $this->getValues($indexSettings, $period, $accountId),
            'chartValues' => $this->getValues($chartSettings, $period, $accountId, true),
            'period' => $period
        ]);
    }

    private function getValues($indexes, $period, $accountId, $firstPeriod = false)
    {
        $indexStr = Index::getStringForSql($indexes);
        $getCertainPeriodValues = function ($startDate, $endDate) use ($indexStr, $accountId) {
            $sql = sprintf(
                'SELECT %s FROM statistics s
              WHERE account_id = %s AND start_date >= unix_timestamp(\'%s\') AND end_date < unix_timestamp(\'%s\')',
                $indexStr,
                $accountId,
                date('c', $startDate),
                date('c', $endDate)
            );
            return Yii::$app->db->createCommand($sql)->queryAll()[0];
        };

        $getMonthName = function ($number) {
            if ($number == '01') return 'янв';
            if ($number == '02') return 'фев';
            if ($number == '03') return 'март';
            if ($number == '04') return 'апр';
            if ($number == '05') return 'май';
            if ($number == '06') return 'июнь';
            if ($number == '07') return 'июль';
            if ($number == '08') return 'авг';
            if ($number == '09') return 'сен';
            if ($number == '10') return 'окт';
            if ($number == '11') return 'ноя';
            if ($number == '12') return 'дек';
        };

        if (in_array($period, [self::PERIOD_WEEK, self::PERIOD_SEVEN_DAYS, self::PERIOD_THIRTY_DAYS])) {
            $days = 7;
            if ($period == self::PERIOD_WEEK) {
                $lpStartDate = strtotime('Monday last week 00:00:00');
            } elseif ($period == self::PERIOD_SEVEN_DAYS) {
                $lpStartDate = strtotime('7 days ago 00:00:00');
            } else {
                $lpStartDate = strtotime('30 days ago 00:00:00');
                $days = 30;
            }

            $lpEndDate = strtotime('+' . $days . ' days', $lpStartDate);
            $ppStartDate = strtotime('-' . $days . ' days', $lpStartDate);
            $ppEndDate = strtotime('-' . $days . ' days', $lpEndDate);
            $fpStartDate = strtotime('-' . $days . ' days', $ppStartDate);
            $fpEndDate = strtotime('-' . $days . ' days', $ppEndDate);

            $fpName = date('d', $fpStartDate) . ' ' . $getMonthName(date('m', $fpStartDate))
                . ' - ' . date('d', strtotime('-1 day', $fpEndDate)) . ' ' . $getMonthName(date('m', $fpEndDate));
            $ppName = date('d', $ppStartDate) . ' ' . $getMonthName(date('m', $ppStartDate))
                . ' - ' . date('d', strtotime('-1 day', $ppEndDate)) . ' ' . $getMonthName(date('m', $ppEndDate));
            $lpName = date('d', $lpStartDate) . ' ' . $getMonthName(date('m', $lpStartDate))
                . ' - ' . date('d', strtotime('-1 day', $lpEndDate)) . ' ' . $getMonthName(date('m', $lpEndDate));
        } elseif ($period == self::PERIOD_MONTH) {
            $lpStartDate = strtotime('first day of last month 00:00:00');
            $lpEndDate = strtotime('first day of this month 00:00:00');
            $ppStartDate = strtotime('-1 month', $lpStartDate);
            $ppEndDate =  strtotime('-1 month', $lpEndDate);
            $fpStartDate = strtotime('-1 month', $ppStartDate);
            $fpEndDate =  strtotime('-1 month', $ppEndDate);
            $fpName = $getMonthName(date('m', $fpStartDate));
            $ppName = $getMonthName(date('m', $ppStartDate));
            $lpName = $getMonthName(date('m', $lpStartDate));
        } elseif ($period == self::PERIOD_YEAR) {
            $lpStartDate = strtotime('last year january 1 00:00:00');
            $lpEndDate = strtotime('january 1 00:00:00');
            $ppStartDate = strtotime('-1 year', $lpStartDate);
            $ppEndDate =  strtotime('-1 year', $lpEndDate);
            $fpStartDate = strtotime('-1 year', $ppStartDate);
            $fpEndDate =  strtotime('-1 year', $ppEndDate);
            $fpName = date('Y', $fpStartDate) . ' г.';
            $ppName = date('Y', $ppStartDate) . ' г.';
            $lpName = date('Y', $lpStartDate) . ' г.';
        }

        $latestPeriod = $getCertainPeriodValues($lpStartDate, $lpEndDate);
        $previousPeriod = $getCertainPeriodValues($ppStartDate, $ppEndDate);
        if ($firstPeriod !== false)
            $firstPeriod = $getCertainPeriodValues($fpStartDate, $fpEndDate);

        $data = [];
        foreach ($indexes as $indexName) {
            $row = [
                'indexName' => $indexName,
                'indexTitle' => Index::getFullName($indexName),
                'lpValue' => round($latestPeriod[$indexName], 2),
                'ppValue' => round($previousPeriod[$indexName], 2),
                'diff' => $previousPeriod[$indexName] == 0 ? 0 : round(
                    ($latestPeriod[$indexName] / $previousPeriod[$indexName] - 1) * 100, 2
                ),
                'fpName' => $fpName,
                'ppName' => $ppName,
                'lpName' => $lpName,
            ];
            if ($firstPeriod !== false)
                $row['fpValue'] = round($firstPeriod[$indexName], 2);

            $data[] = $row;
        }

        return $data;
    }

    public function getBreadcrumbs($view, $model)
    {
        $breadcrumbs = parent::getBreadcrumbs($view, $model);
        $breadcrumbs[] = [
            'label' => 'Отчеты'
        ];
        return $breadcrumbs;
    }
}

