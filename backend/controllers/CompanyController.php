<?php

namespace backend\controllers;

use common\models\Account;
use common\models\Goal;
use common\models\service\yandex\Metrika;
use Yii;
use common\models\Company;
use common\models\CompanySearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $params = Yii::$app->request->queryParams;
        $params['CompanySearch']['user_id'] = Yii::$app->user->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $direct = Account::find()->where(
            'company_id = ' . $id .
            ' and advert_type = ' . Account::TYPE_YANDEX .
            ' and advert_login is not null and advert_login <> \'\''
        )->one();

        $directMetrikaCounters = [];
        $directSelectedGoals = [];
        $directClientList = false;
        if ($direct != null) {
            $counters = $direct->import->getMetrikaCounters();
            $metrika = new Metrika($direct->stat_token);
            $directMetrikaCounters = array_map(function ($counter) use ($metrika) {
                return [
                    'id' => $counter['id'],
                    'name' => $counter['name'],
                    'goals' => $metrika->getGoals($counter['id'])
                ];
            }, $counters);
            $directSelectedGoals = Goal::find()->select('number')->where(['account_id' => $direct->id])->column();

            $directClientList = $direct->import->getClientList();
        }

        $adwords = Account::find()->where(
            'company_id = ' . $id .
            ' and advert_type = ' . Account::TYPE_GOOGLE .
            ' and advert_login is not null and advert_login <> \'\''
        )->one();
        $customerId = Account::find()
            ->select('client_customer_id')
            ->where(
                'company_id = ' . $id .
                ' and advert_type = ' . Account::TYPE_GOOGLE .
                ' and client_customer_id is not null and client_customer_id <> \'\''
            )->scalar();


        return $this->render('view', [
            'model' => $this->findModel($id),
            'direct' => $direct,
            'adwords' => $adwords,
            'customerId' => $customerId,
            'directMetrikaCounters' => $directMetrikaCounters,
            'directSelectedGoals' => $directSelectedGoals,
            'directClientList' => $directClientList
        ]);
    }

    /**
     * Creates a new Company model.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Company();
        $model->user_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getBreadcrumbs($view, $model)
    {
        $breadcrumbs = [
            [
                'label' => 'Проекты',
                'url' => Url::to(['/company/index'])
            ]
        ];
        if ($view == 'index')
            return $breadcrumbs;
        elseif ($view == 'create')
            $breadcrumbs[] = 'Добавить';
        elseif ($view == 'view')
            $breadcrumbs[] = $model['model']->name;
        elseif ($view == 'connectCrm') {
            $breadcrumbs[] = [
                'label' => $model['model']->name,
                'url' => Url::to([
                    'company/view',
                    'id' => $model['model']->id
                ])
            ];
            $breadcrumbs[] = 'Подключить CRM';
        }
        elseif ($view == 'update') {
            $breadcrumbs[] = [
                'label' => $model['model']->name,
                'url' => Url::to([
                    'company/view',
                    'id' => $model['model']->id
                ])
            ];
            $breadcrumbs[] = 'Обновить';
        }

        return $breadcrumbs;
    }

    public function actionSaveLogin()
    {
        $accountId = Yii::$app->request->post('id', false);
        $login = Yii::$app->request->post('directClient', false);

        $account = Account::findOne($accountId);
        $account->client_login = $login;
        $account->update();

        return $this->redirect([
            'company/view',
            'id' => $account->company_id
        ]);
    }

    public function actionSaveGoals()
    {
        $accountId = Yii::$app->request->post('id', false);
        $counter = Yii::$app->request->post('counter', false);
        $goals = Yii::$app->request->post('goal', false);
        $account = Account::findOne($accountId);
        $account->counter = $counter;
        $account->update();

        Goal::deleteAll(['account_id' => $accountId]);
        foreach ($goals as $goal) {
            $goal = new Goal([
                'number' => $goal,
                'account_id' => $account->id
            ]);
            $goal->save();
        }

        return $this->redirect([
            'company/view',
            'id' => $account->company_id
        ]);
    }

    public function actionSaveCrm()
    {
        $companyId = Yii::$app->request->post('id', false);
        $crmUrl = Yii::$app->request->post('crm', false);
        $company = Company::findOne(['id' => $companyId]);
        $company->sales_report_url = trim($crmUrl);
        $company->update();

        return $this->redirect([
            'company/view',
            'id' => $companyId
        ]);
    }

    private function tokenRequestYandex($type, $accountId)
    {
        Yii::$app->session->set('accountId', $accountId);
        Yii::$app->session->set('type', $type);
        if ($type == Account::SYSTEM_TYPE_CONTEXT || $type == Account::SYSTEM_TYPE_WEBANALYTICS)
            Yii::$app->response->redirect(
                sprintf('%sauthorize?response_type=code&client_id=%s',
                    Yii::$app->params['services']['yandex']['oauth']['url'],
                    Yii::$app->params['services']['yandex']['direct']['appID'])
            );
    }

    public function actionConnectDirect($id)
    {
        $company = Company::findOne($id);//$company = $this->findModel($id);
        $account = Account::findOne([
            'company_id'  => $company->id,
            'advert_type' => Account::TYPE_YANDEX
        ]);
        if ($account == null) {
            $account                    = new Account();
            $account->company_id        = $company->id;
            $account->stat_type         = -1;
            $account->is_account_common = 0;
            $account->state = Account::STATE_RAW;
            $account->advert_type = Account::TYPE_YANDEX;
            $account->save();
        }

        $this->tokenRequestYandex(Account::SYSTEM_TYPE_CONTEXT, $account->id);
    }

    public function actionConnectMetrika($accountId)
    {
        $account = Account::findOne($accountId);
        if ($account == null || $account->company->user_id != Yii::$app->user->id)
            throw new NotFoundHttpException('Cтраница не найдена.');

        $account->stat_type = Account::TYPE_YANDEX;
        $account->update();

        $this->tokenRequestYandex(Account::SYSTEM_TYPE_WEBANALYTICS, $account->id);
    }

    public function actionUnbindDirect($id)
    {
        $account = Account::findOne($id);
        $account->advert_login = '';
        $account->client_login = '';
        $account->advert_token = '';
        $account->advert_token_expires = '';
        $account->update();

        return $this->redirect([
            'company/view',
            'id' => $account->company_id
        ]);
    }

    public function actionUnbindMetrika($id)
    {
        $account = Account::findOne($id);
        $account->stat_type = -1;
        $account->stat_login = '';
        $account->stat_token = '';
        $account->stat_token_expires = '';
        $account->counter = null;
        $account->update();

        return $this->redirect([
            'company/view',
            'id' => $account->company_id
        ]);
    }

    public function actionConnectAdwords($id)
    {
        $company = Company::findOne($id);//$company = $this->findModel($id);
        $account = Account::findOne([
            'company_id'  => $company->id,
            'advert_type' => Account::TYPE_GOOGLE
        ]);
        if ($account == null) {
            $account                    = new Account();
            $account->company_id        = $company->id;
            $account->stat_type         = -1;
            $account->is_account_common = 0;
            $account->state = Account::STATE_RAW;
            $account->advert_type = Account::TYPE_GOOGLE;
            $account->save();
        }

        $this->tokenRequestGoogle(Account::SYSTEM_TYPE_CONTEXT, $account->id);
    }

    public function tokenRequestGoogle($type, $accountId)
    {
        Yii::$app->session->set('accountId', $accountId);
        Yii::$app->session->set('type', $type);
        $account = Account::findOne($accountId);
        $authUrl = $account->import->service->getAuthUrl();
        Yii::$app->response->redirect($authUrl);
    }

    public function actionSaveCustomerId()
    {
        $companyId = Yii::$app->request->post('id', false);
        $company = Company::findOne($companyId);
        $customerId = Yii::$app->request->post('customerId', '');
        $customerId = str_replace('-', '', $customerId);
        $account = Account::findOne([
            'company_id' => $companyId,
            'advert_type' => Account::TYPE_GOOGLE
        ]);
        if ($account == null) {
            $account = new Account();
            $account->company_id = $company->id;
            $account->advert_type = Account::TYPE_GOOGLE;
            $account->stat_type = -1;
            $account->is_account_common = 0;
            $account->state = Account::STATE_RAW;
        }
        $account->client_customer_id = $customerId;
        $account->save();

        return $this->redirect([
            'company/view',
            'id' => $companyId
        ]);
    }

    public function actionUnbindCustomerId($id, $customerId)
    {
        $company = Company::findOne($id);

        $account = Account::findOne([
            'company_id' => $company->id,
            'client_customer_id' => $customerId
        ]);
        if ($account != null) {
            $account->client_customer_id = '';
            $account->save();
        }

        return $this->redirect([
            'company/view',
            'id' => $id
        ]);
    }

    public function actionGetTokenAdwords($code)
    {
        die;
        $accountId = Yii::$app->session->get('accountId', false);
        if ($accountId !== false) {
            $account = Account::findOne($accountId);
            $oauthInfo = $account->import->service->getOAuth2Info($code);
            VarDumper::dump($oauthInfo, 10, true);
            $account->advert_token = $oauthInfo['access_token'];
            if (array_key_exists('refresh_token', $oauthInfo))
                $account->refresh_token = $oauthInfo['refresh_token'];
            $account->advert_token_expires = (string)($oauthInfo['expires_in'] + time());
            $account->save();
        }
    }
}
//После того как вы укажете, какие аккаунты вы хотите связать,
//их владельцы смогут принять или отклонить ваш запрос в разделе "Доступ к аккаунту" в интерфейсе AdWords.
//Чтобы проверить статус запросов, выберите "Ещё не принятые приглашения" в меню в левой части панели управления.