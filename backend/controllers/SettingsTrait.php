<?php
/**
 * Created by PhpStorm.
 * User: savchenko
 * Date: 10.12.16
 * Time: 14:39
 */

namespace backend\controllers;

use Yii;
use common\models\structure\Layer;
use common\models\User;
use common\models\strategy\ReportSetting;

trait SettingsTrait
{
    public function saveSettings($type, $fields = null)
    {
        $accountID = Yii::$app->request->post('accountID', false);
        if ($fields === null)
            $fields = Yii::$app->request->post('fields', false);
        $layer = Layer::getNumberOfLayerName(Yii::$app->request->post('layer', false));

        if (User::hasAccount(Yii::$app->user->id, $accountID)) {
            $reportSettings = [];
            foreach ($fields as $field)
                $reportSettings[] = [$accountID, $layer, $field, $type];
            $oldSettings = array_map(
                function ($setting) {
                    return $setting->id;
                },
                ReportSetting::findAll([
                    'account_id' => $accountID,
                    'layer' => $layer,
                    'type' => $type
                ])
            );
            $saved = Yii::$app->db->createCommand()->batchInsert(
                ReportSetting::tableName(), ['account_id', 'layer', 'field', 'type'], $reportSettings
            )->execute();
            if ($saved > 0) {
                ReportSetting::deleteAll(['id' => $oldSettings]);
                return json_encode(true);
            } else {
                return json_encode(false);
            }
        }
    }

    public function getSettings($accountID, $layer, $type)
    {
        $getLayerFields = function ($fields, $layer) {
            $layerFields = [];
            foreach ($fields as $field)
                if ($field->layer == $layer)
                    $layerFields[] = $field->field;
            return $layerFields;
        };

        $fields = ReportSetting::findAll([
            'account_id' => $accountID,
            'type' => $type
        ]);

        $layerFields = $getLayerFields($fields, $layer);
        if ($layerFields)
            return $layerFields;

        if ($layer != Layer::LAYER_CAMPAIGNS) {
            $layerFields = $getLayerFields($fields, Layer::LAYER_CAMPAIGNS);
            if ($layerFields)
                return $layerFields;
        }

        return [];
    }
}