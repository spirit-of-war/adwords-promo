<?php
namespace backend\controllers;

use common\models\Account;
use common\models\Company;
use common\models\structure\Adgroup;
use common\models\structure\Campaign;
use common\models\structure\Layer;
use common\models\User;
use Yii;
use yii\web\Controller;
use yii\helpers\Url;

/**
 * Class BaseController
 * @package backend\controllers
 */
class BaseController extends Controller
{
    public $layout = 'gantelella';

    /**
     * @param string $view
     * @param array $model
     *
     * @return string
     */
    public function render($view, $model = [])
    {
        $model['leftMenuItems'] = $this->getLeftMenuItems($model);
        $model['breadcrumbs'] = $this->getBreadcrumbs($view, $model);

        return parent::render($view, $model);
    }

    /**
     * @param array $model
     * @param $view
     * @return array
     */
    protected function getBreadcrumbs($view, $model)
    {
        if(isset($model['account'])){
            $account = $model['account'];
            $result = [
                [
                    'label' => 'Проекты',
                    'url' => Url::to(['/company/index'])
                ],
                [
                    'label' => $account->company->name,
                    'url' => Url::to([
                        '/company/view',
                        'id' => $account->company_id
                    ])
                ],
                [
                    'label' => $account->name,
                    'url' => Url::to([
                        '/account/view',
                        'dimension' => Layer::LAYER_NAME_CAMPAIGN,
                        'accountID' => $account->id
                    ])
                ]

            ];
        } else {
            $result = [];
        }

        return $result;
    }

    /**
     * Получаем элементы меню левой навигации
     *
     * @param array $model
     * @return array
     */
    private function getLeftMenuItems($model)
    {
        $currentAccountId = isset($model['account']['id']) ? $model['account']['id'] : 0;
        $menuItems = [
            'items' => $this->getCompanyMenuItems(
                isset($model['ulId']) && $model['ulId'] ? $model['ulId'] : $currentAccountId,
                isset($model['dimension']) ? $model['dimension'] : ''
            ),
        ];

        $username = User::findIdentity(\Yii::$app->user->id);

        if (User::isUserAdmin($username)) {
            $menuItems['items'][] = [
                "label" => "Админка",
                "url" => '#',
                "icon" => "edit",
                "items" => [
                    [
                        'label' => 'Заявки',
                        'url' => '/admin/order',
                    ],
                    [
                        'label' => 'Статьи',
                        'url' => '/admin/page',
                    ],
                ],
            ];
        }

        return $menuItems;
    }

    /**
     * Получаем самые главные элементы левой навигации
     *
     * @return array
     */
    private function getRootCompanyItems()
    {
        $menuItems = [];
        $clients = Company::findAll(['user_id' => Yii::$app->user->id]);

        if (count($clients) == 0) {
            $menuItems[] = [
                "label" => "Нет проектов",
                "url" => '/company/create',
                "icon" => "child",
            ];
        } else {
            $clientItems = [];
            foreach ($clients as $client) {
                $accounts = [];
                foreach ($client->accounts as $account)
                    if ($account->state == Account::STATE_ACTIVE)
                        $accounts[] = [
                            'label' => $account->advert_login,
                            'options' => [
                                'data-id' => $account->id,
                                'data-type' => Layer::LAYER_NAME_ACCOUNT,
                            ],
                            'url' => [
                                'account/view',
                                'accountID' => $account->id,
                                'dimension' => Layer::LAYER_NAME_CAMPAIGN,
                            ],
                        ];

                if (count($accounts) == 1)
                    $clientItems[] = [
                        'label' => $client->name,
                        'url' => $accounts[0]['url'],
                    ];
                elseif (count($accounts) > 1)
                    $clientItems[] = [
                        'label' => $client->name,
                        'active' => false,
                        'items' => $accounts,
                        'url' => [
                            'company/view',
                            'id' => $client->id,
                        ],
                        'options' => [
                            'data-id' => $client->id,
                            'data-type' => Layer::LAYER_NAME_COMPANY,
                            'class' => ['my-custom-menu'],
                        ],
                    ];
                else
                    $clientItems[] = [
                        'label' => $client->name,
                        'url' => ['company/view', 'id' => $client->id],
                    ];
            }

            if (count($clientItems) > 5)
                $menuItems[] = [
                    "label" => "Проекты",
                    "url" => '#',
                    "icon" => "child",
                    "items" => $clientItems
                ];
            else
                foreach ($clientItems as $clientItem)
                    $menuItems[] = array_merge([
                        'icon' => 'child',
                    ], $clientItem);
        }

        return $menuItems;
    }

    /**
     * Получаем элементы меню для компаний
     *
     * @param int $parentId
     * @param string $dimension
     * @param array $children
     * @param int $id
     * @return array
     */
    private function getCompanyMenuItems($parentId, $dimension, $id = 0, $children = [])
    {
        $dependencyChildren = $this->getChildrenDependency();
        $dependencyParents = $this->getParentsDependency();
        $items = [];
        $nextParentId = 0;

        switch ($dimension) {
            case Layer::LAYER_NAME_COMPANY:
                $items = Company::find()->select([
                    'id',
                    'label' => 'name',
                    'parent_id' => 'user_id',
                ])->where([
                    'state' => 1,
                    'user_id' => Yii::$app->user->id
                ])->asArray()->all();
                break;
            case Layer::LAYER_NAME_ACCOUNT:
                // Подгружаем смежные элементы
                $items = Account::find()->select([
                    'id',
                    'label' => 'advert_login',
                    'account_id' => 'id',
                ])->where(['company_id' => $parentId, 'state' => 1])->asArray()->all();

                // Узнаем следующего родителя
                $nextParentId = Account::find()->select([
                    'company_id',
                ])->where(['id' => $parentId])->scalar();
                break;
            case Layer::LAYER_NAME_CAMPAIGN:
                // Подгружаем смежные элементы
                $items = Campaign::find()->select([
                    'id',
                    'label' => 'name',
                    'account_id',
                ])->where(['account_id' => $parentId, 'state' => 1])->asArray()->all();

                // Узнаем следующего родителя
                $nextParentId = Account::find()->select([
                    'company_id',
                ])->where(['id' => $parentId])->scalar();
                break;
            case Layer::LAYER_NAME_ADGROUP:
                // Подгружаем смежные элементы
                $items = Adgroup::find()->select([
                    'id',
                    'label' => 'name',
                    'account_id',
                ])->where(['campaign_id' => $parentId, 'state' => 1])->asArray()->all();

                // Узнаем следующего родителя
                $nextParentId = Campaign::find()->select([
                    'account_id',
                ])->where(['id' => $parentId])->scalar();
                break;
            case Layer::LAYER_NAME_KEYWORD:
                $nextParentId = Adgroup::find()->select([
                    'campaign_id',
                ])->where(['id' => $parentId])->scalar();
                break;
            default:
                return $this->getRootCompanyItems();
        }

        foreach ($items as &$item) {
            if (isset($dependencyChildren[$dimension])) {
                $item['url'] = sprintf(
                    '/account/%s/%s%s',
                    $dependencyChildren[$dimension],
                    $item['account_id'],
                    $dimension == Layer::LAYER_NAME_ADGROUP || $dimension == Layer::LAYER_NAME_CAMPAIGN ? '?ul_id=' . $item['id'] : ''
                );
            } else {
                $item['url'] = sprintf('/company/%s', $item['id']);
                $item['icon'] = 'child';
            }

            $item['options'] = [
                'data-id' => $item['id'],
                'data-type' => $dimension,
            ];

            if ($item['id'] == $id) {
                $item['items'] = $children;
                $item['options']['data-current'] = true;

                if ($dimension == Layer::LAYER_NAME_COMPANY) {
                    $item['options']['class'] = ['my-custom-menu'];
                    $item['options']['data-holder'] = 'left_menu';
                }
            }
        }

        $id = $parentId;

        if (isset($dependencyParents[$dimension])) {
            // Рекурсивно подгружаем от нижних ветвей к верхним
            return $this->getCompanyMenuItems($nextParentId, $dependencyParents[$dimension], $id, $items);
        } else {
            return $items;
        }
    }

    /**
     * Возвращает список зависимых имен объектов от дочернего к родительскому
     *
     * @return array
     */
    private function getParentsDependency()
    {
        return [
            Layer::LAYER_NAME_ACCOUNT => Layer::LAYER_NAME_COMPANY,
            Layer::LAYER_NAME_CAMPAIGN => Layer::LAYER_NAME_ACCOUNT,
            Layer::LAYER_NAME_ADGROUP => Layer::LAYER_NAME_CAMPAIGN,
            Layer::LAYER_NAME_KEYWORD => Layer::LAYER_NAME_ADGROUP,
            Layer::LAYER_NAME_AD => Layer::LAYER_NAME_ADGROUP,
        ];
    }

    /**
     * Возвращает список зависимых имен объектов от родительскому к дочернего
     *
     * @return array
     */
    private function getChildrenDependency()
    {
        return [
            Layer::LAYER_NAME_ACCOUNT => Layer::LAYER_NAME_CAMPAIGN,
            Layer::LAYER_NAME_CAMPAIGN => Layer::LAYER_NAME_ADGROUP,
            Layer::LAYER_NAME_ADGROUP => Layer::LAYER_NAME_KEYWORD,
        ];
    }
}