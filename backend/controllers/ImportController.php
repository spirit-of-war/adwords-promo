<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;

class ImportController extends Controller
{
    public function actionStatistics($accountID, $from = 'yesterday', $to = 'yesterday')
    {
        $command = sprintf('php %s %s \'%s\' \'%s\'',
            Yii::getAlias('@app') . '/../yii import/statistics-manual',
            $accountID,
            $from,
            $to);
        exec($command);
    }

    public function actionStructure($accountID)
    {
        $command = sprintf('php %s %s',
            Yii::getAlias('@app') . '/../yii import/structure-manual',
            $accountID);
        exec($command);
    }
}
