<?php

namespace backend\controllers;

use common\models\DeadLink;
use common\models\Goal;
use common\models\strategy\Index;
use common\models\structure\Campaign;
use common\models\structure\Adgroup;
use common\models\structure\Ad;
use common\models\structure\Keyword;
use common\models\service\yandex\Direct;
use common\models\Company;
use common\models\Statistics;
use common\models\structure\Layer;
use common\models\FileLink;
use Yii;
use common\models\Account;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use common\models\service\yandex\Metrika;
use yii\data\SqlDataProvider;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/**
 * AccountController implements the CRUD actions for Account model.
 */
class AccountController extends BaseController implements SettingsInterface
{
    use SettingsTrait;

    const TYPE_WEBANALYTICS = 0;
    const TYPE_CONTEXT      = 1;

    const SETTING_TYPE = 0;

    /**
     * Finds the Account model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Account the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Account::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single Account model.
     *
     * @param string  $dimension
     * @param integer $accountID
     *
     * @return mixed
     */
    public function actionView($dimension = Layer::LAYER_NAME_CAMPAIGN, $accountID)
    {
        if (!isset($_GET['clean'])) {
            $startDate = isset($_GET['startDate']) ? $_GET['startDate'] : Yii::$app->session->get('startDate');
            $startDate = $startDate ?: date('d-m-Y', strtotime('7 days ago'));
            $endDate   = isset($_GET['endDate']) ? $_GET['endDate'] : Yii::$app->session->get('endDate');
            $endDate   = $endDate ?: date('d-m-Y', strtotime('1 days ago'));
            Yii::$app->session->set('startDate', $startDate);
            Yii::$app->session->set('endDate', $endDate);
        } else {
            $startDate = date('d-m-Y', strtotime('7 days ago'));
            $endDate   = date('d-m-Y', strtotime('1 days ago'));
            Yii::$app->session->remove('endDate');
            Yii::$app->session->remove('startDate');
        }

        $availableDates = Yii::$app->db->createCommand(
            'select from_unixtime(min(start_date), GET_FORMAT(DATE,\'EUR\')) min, 
                    from_unixtime(max(start_date), GET_FORMAT(DATE,\'EUR\')) max
              from statistics where (shows_search is not null or shows_context is not null) and account_id ='
            . $accountID)->queryAll()[0];

        $dateRange = [
            'startDate' => $startDate,
            'endDate'   => $endDate,
            'minDate'   => $availableDates['min'],
            'maxDate'   => $availableDates['max'],
            'ranges'    => json_encode([
                'Вчера'             => [
                    date('d.m.Y', strtotime('1 days ago')),
                    date('d.m.Y', strtotime('1 days ago'))
                ],
                'Последние 7 дней'  => [
                    date('d.m.Y', strtotime('7 days ago')),
                    date('d.m.Y', strtotime('1 days ago'))
                ],
                'Последние 14 дней' => [
                    date('d.m.Y', strtotime('14 days ago')),
                    date('d.m.Y', strtotime('1 days ago'))
                ],
                'Последние 30 дней' => [
                    date('d.m.Y', strtotime('30 days ago')),
                    date('d.m.Y', strtotime('1 days ago'))
                ],
                'Этот месяц'        => [
                    date('d.m.Y', strtotime('first day of this month')),
                    date('d.m.Y', strtotime('1 days ago'))
                ],
            ])
        ];
        $startDate = strtotime($dateRange['startDate']);
        $endDate   = strtotime($dateRange['endDate']) + 86399;

        $account = $this->findModel($accountID);
        $fields  = [];
        if ($dimension == Layer::LAYER_NAME_CAMPAIGN) {
            $fields['name'] = 'd.name';
            $tableName      = Campaign::tableName();
            $ulSql          = '';

            Yii::$app->session->remove('campaignID');
            Yii::$app->session->remove('adgroupID');
        } elseif ($dimension == Layer::LAYER_NAME_ADGROUP) {
            $fields    = [
                'name'    => 'd.name',
                'ul_name' => 'ul.name',
                'ul_id'   => 'ul.id'
            ];
            $tableName = Adgroup::tableName();
            $ulSql     = 'left join campaign ul on d.campaign_id = ul.id';

            if (Yii::$app->request->get('ul_id', false))
                Yii::$app->session->set('campaignID', Yii::$app->request->get('ul_id'));
            if (Yii::$app->session->get('adgroupID', false)) {
                $campaignID = Adgroup::findOne(Yii::$app->session->get('adgroupID'))->campaign_id;
                Yii::$app->session->set('campaignID', $campaignID);
                Yii::$app->session->remove('adgroupID');
            }
        } elseif ($dimension == Layer::LAYER_NAME_AD) {
            $fields    = ['title' => 'd.title'];
            $tableName = Ad::tableName();

            if (Yii::$app->session->get('campaignID', false) !== false) {
                $fields['adgroup_name'] = 'g.name';
                $ulSql                  = 'left join adgroup g on d.adgroup_id = g.id';
            } elseif (Yii::$app->session->get('campaignID', false) === false
                && Yii::$app->session->get('adgroupID', false) === false
            ) {
                $fields['adgroup_name']  = 'g.name';
                $fields['campaign_name'] = 'c.name';
                $ulSql                   = 'left join adgroup g on d.adgroup_id = g.id left join campaign c on d.campaign_id = c.id';
            } else {
                $ulSql = '';
            }
        } else {//keyword
            $fields    = [
                'keyword'          => 'd.keyword',
                'negative'         => 'negative',
                'bid_context'      => 'd.bid_context',
                'bid_search'       => 'd.bid_search',
                'average_position' => 'd.average_position',
            ];
            $tableName = Keyword::tableName();

            if (Yii::$app->request->get('ul_id', false)) {
                Yii::$app->session->remove('campaignID');
                Yii::$app->session->set('adgroupID', Yii::$app->request->get('ul_id'));
            }

            if (Yii::$app->session->get('campaignID', false) !== false) {
                $fields['adgroup_name'] = 'ul.name';
                $ulSql                  = 'left join adgroup ul on d.adgroup_id = ul.id';
            } elseif (Yii::$app->session->get('campaignID', false) === false
                && Yii::$app->session->get('adgroupID', false) === false
            ) {
                $fields['adgroup_name']  = 'ul.name';
                $fields['campaign_name'] = 'c.name';
                $ulSql                   = 'left join adgroup ul on d.adgroup_id = ul.id left join campaign c on d.campaign_id = c.id';
            } else {
                $ulSql = 'left join adgroup ul on d.adgroup_id = ul.id';
            }
        }

        $fields = array_merge([
            'account_id' => 'd.account_id',
            'id'         => 'd.id',
            'source_id'  => 'd.source_id',
        ], $fields);

        $calculatedFields = [
            'clicks'              => Index::getExpression('clicks'),
            'shows'               => Index::getExpression('shows'),
            'ctr'                 => Index::getExpression('ctr'),
            'sum'                 => Index::getExpression('sum'),
            'avarage_clicks_cost' => Index::getExpression('avarage_clicks_cost'),
            'average_position'    => Index::getExpression('average_position'),

            'metrika_conversions_first'         => Index::getExpression('metrika_conversions_first'),
            'metrika_conversions_last'          => Index::getExpression('metrika_conversions_last'),
            'metrika_conversions_lastsign'      => Index::getExpression('metrika_conversions_lastsign'),
            'metrika_conversions_cost_first'    => Index::getExpression('metrika_conversions_cost_first'),
            'metrika_conversions_cost_last'     => Index::getExpression('metrika_conversions_cost_last'),
            'metrika_conversions_cost_lastsign' => Index::getExpression('metrika_conversions_cost_lastsign'),
            'metrika_revenue'                   => Index::getExpression('metrika_revenue'),
            'metrika_roi'                       => Index::getExpression('metrika_roi'),
            'metrika_profit'                    => Index::getExpression('metrika_profit'),

            'crm_conversions'       => Index::getExpression('crm_conversions'),
            'crm_conversions_cost'  => Index::getExpression('crm_conversions_cost'),
            'crm_revenue'           => Index::getExpression('crm_revenue'),
            'crm_roi'               => Index::getExpression('crm_roi'),
            'crm_profit'            => Index::getExpression('crm_profit'),
            'crm_assoc_conversions' => Index::getExpression('crm_assoc_conversions')
        ];

        $getSelectFields = function ($fields) {
            $selectFields = array_map(function ($key, $val) {
                return $val . ' ' . $key;
            }, array_keys($fields), array_values($fields));

            return implode(',', $selectFields);
        };

        $ulId     = isset($_GET['ul_id']) && strlen($_GET['ul_id']) > 0 ? $_GET['ul_id'] : 0;
        $where    = $ulId > 0 ? ' and ul.id=' . $_GET['ul_id'] : '';
        $totalRow = sprintf('select %s from %s d left join %s s on s.%s_id = d.id
                                                          and s.start_date >= %s and s.end_date <= %s %s
                        where d.account_id = %d %s',
            $getSelectFields($calculatedFields),
            $tableName,
            Statistics::tableName(),
            $tableName,
            $startDate,
            $endDate,
            $ulSql,
            $accountID,
            $where);
        $totalRow = Yii::$app->db->createCommand($totalRow)->queryAll();
        $totalRow = $totalRow ? $totalRow[0] : $totalRow;

        $fields = array_merge($calculatedFields, $fields);

        $selectFields = $getSelectFields($fields);

        $where  = [];
        $having = [];
        foreach ($_GET as $key => $val)
            if (array_key_exists($key, $fields) && $_GET[$key]) {
                if (filter_input(INPUT_GET, $key, FILTER_VALIDATE_INT)
                    || filter_input(INPUT_GET, $key, FILTER_VALIDATE_FLOAT)
                )
                    if (strpos($fields[$key], 'sum(') !== false || strpos($fields[$key], 'avg(') !== false)
                        $having[] = $key . '=' . $val;
                    else
                        $where[] = $fields[$key] . '=' . $val;
                elseif (strlen($val) > 1 && in_array(substr($val, 0, 1), ['<', '>', '=']))
                    if (strpos($fields[$key], 'sum(') !== false || strpos($fields[$key], 'avg(') !== false)
                        $having[] = $key . $val;
                    else
                        $where[] = $fields[$key] . $val;
                else
                    if (strpos($fields[$key], 'sum(') !== false || strpos($fields[$key], 'avg(') !== false)
                        $having[] = $key . ' like \'%' . $val . '%\'';
                    else
                        $where[] = $fields[$key] . ' like \'%' . $val . '%\'';
            }

        if (Yii::$app->session->get('campaignID', false))
            $where[] = 'd.campaign_id = ' . Yii::$app->session->get('campaignID');
        if (Yii::$app->session->get('adgroupID', false))
            $where[] = 'd.adgroup_id = ' . Yii::$app->session->get('adgroupID');

        $where  = implode(' and ', $where);
        $having = implode(' and ', $having);

        $sql        = sprintf('select %s from %s d left join %s s on s.%s_id = d.id
                                                          and s.start_date >= %s and s.end_date <= %s %s 
                        where d.account_id = %d %s
                        group by d.id %s',
            $selectFields,
            $tableName,
            Statistics::tableName(),
            $tableName,
            $startDate,
            $endDate,
            $ulSql,
            $accountID,
            ($where ? 'and ' . $where : ''),
            ($having ? 'having ' . $having : ''));
        $totalCount = Yii::$app->db->createCommand($sql)->execute();

        $calculatedFields['ctr']                               = 'IFNULL(sum(clicks) / sum(shows) * 100, 0)';
        $calculatedFields['metrika_conversions_cost_first']    = 'IFNULL(SUM(sum) / SUM(metrika_conversions_first), 0)';
        $calculatedFields['metrika_conversions_cost_last']     = 'IFNULL(SUM(sum) / SUM(metrika_conversions_last), 0)';
        $calculatedFields['metrika_conversions_cost_lastsign'] = 'IFNULL(SUM(sum) / SUM(metrika_conversions_lastsign), 0)';
        $calculatedFields['metrika_roi']                       = 'IFNULL((IFNULL(SUM(metrika_revenue), 0) - IFNULL(SUM(sum), 0)) / (sum(sum)), 0)';
        $calculatedFields['crm_conversions_cost']              = 'IFNULL(SUM(sum) / SUM(crm_conversions), 0)';
        $calculatedFields['crm_roi']                           = 'IFNULL((IFNULL(SUM(crm_revenue), 0) - IFNULL(SUM(sum), 0)) / sum(sum), 0)';
        $calculatedFields['avarage_clicks_cost']               = 'IFNULL(sum(sum) / sum(clicks), 0)';
        $selectFields                                          = array_map(function ($key, $expression) {
            if (in_array($key,
                [
                    'ctr',
                    'metrika_conversions_cost_first',
                    'metrika_conversions_cost_last',
                    'metrika_conversions_cost_lastsign',
                    'metrika_roi',
                    'crm_conversions_cost',
                    'crm_roi',
                    'avarage_clicks_cost'
                ]))
                return $expression . ' ' . $key;

            return 'sum(' . $key . ') ' . $key;
        }, array_keys($calculatedFields), array_values($calculatedFields));
        $selectFields                                          = implode(',', $selectFields);
        $totalFilterRow                                        = sprintf('select %s from (%s) report',
            $selectFields,
            $sql);
        $totalFilterRow                                        = Yii::$app->db->createCommand($totalFilterRow)->queryAll();
        $totalFilterRow                                        = $totalFilterRow ? $totalFilterRow[0] : $totalFilterRow;

        $dataProvider = new SqlDataProvider([
            'sql'        => $sql,
            'totalCount' => $totalCount,
            'sort'       => [
                'attributes' => array_keys($fields)
            ],
            'pagination' => [
                'pageSize' => Yii::$app->request->getQueryParam('per-page') ?: 10,
            ],
        ]);

        $chartSettings =
            $this->getSettings($accountID, Layer::getNumberOfLayerName($dimension), ChartController::SETTING_TYPE);
        if (count($chartSettings) == 0)
            $chartSettings[] = 'clicks';

        return $this->render('view', [
            'account'        => $account,
            'ulId'           => $ulId,
            'dimension'      => $dimension,
            'dataProvider'   => $dataProvider,
            'dateRange'      => $dateRange,
            'totalRow'       => $totalRow,
            'totalFilterRow' => $totalFilterRow,
            'reportSettings' =>
                $this->getSettings($accountID, Layer::getNumberOfLayerName($dimension), AccountController::SETTING_TYPE),
            'chartSettings'  => $chartSettings
        ]);
    }

    /**
     * Updates an existing Account model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($_POST) {
            Account::updateAll(['counter' => $_POST['counter']], ['id' => $id]);
            if (array_key_exists('goal', $_POST)) {
                Goal::deleteAll(['account_id' => $id]);
                foreach ($_POST['goal'] as $goal)
                    (new Goal([
                        'account_id' => $id,
                        'number'     => $goal
                    ]))->save();
            }

        }

        $api      = new Metrika($model->stat_token);
        $counters = $api->getCounters();

        if ($counters !== false) {
            $selectedGoals = array_map(
                function ($number) {
                    return (int)$number;
                },
                Goal::find()->select('number')->where(['account_id' => $id])->column()
            );

            $goals       = [];
            $countersRaw = [];
            foreach ($counters as $counter) {
                $goals         = array_merge($goals, $api->getGoals($counter['id']));
                $countersRaw[] = [
                    'id'    => $counter['id'],
                    'name'  => $counter['name'],
                    'goals' => $goals,
                ];
            }

            return $this->render('update', [
                'account'       => $model,
                'counters'      => $countersRaw,
                'selectedGoals' => $selectedGoals
            ]);
        } else {
            //TODO: сделать получение токена
        }
    }

    /**
     * Redirects to Yandex page where user must grant access to his data.
     *
     * @param $accountID
     *
     * @throws NotFoundHttpException
     */
    public function actionTokenRequest($type, $accountID)
    {
        Yii::$app->session->set('accountID', $accountID);
        Yii::$app->session->set('type', $type);
        $account = $this->findModel($accountID);
        if ($type == self::TYPE_CONTEXT && $account->advert_type == Account::TYPE_YANDEX
            || $type == self::TYPE_WEBANALYTICS && $account->stat_type == Account::TYPE_YANDEX
        ) {
            Yii::$app->response->redirect(
                sprintf('%sauthorize?response_type=code&client_id=%s',
                    Yii::$app->params['services']['yandex']['oauth']['url'],
                    Yii::$app->params['services']['yandex']['direct']['appID'])
            );
        } elseif ($type == self::TYPE_CONTEXT && $account->advert_type == Account::TYPE_GOOGLE ||
            $type == self::TYPE_WEBANALYTICS && $account->stat_type == Account::TYPE_GOOGLE
        ) {
//            if ($type == self::TYPE_CONTEXT) {
//                $adwords = new Adwords(\Yii::$app->params['services']['google']['adwords']['PANDA']['id']);
//                $adwords->createInvitation($account->client_customer_id);
//            }
//            Yii::$app->session->set('service', Account::TYPE_GOOGLE);
//            $authUrl = Adwords::getAuthUrl();
//            Yii::$app->response->redirect($authUrl);
        }
    }

    /**
     * Handles request from Yandex and requests token.
     *
     * @param $code
     */
    public function actionGetToken($code)
    {
        $accountID = Yii::$app->session->get('accountId');
        $type      = Yii::$app->session->get('type');
        $account   = Account::findOne(['id' => $accountID]);

        if (Yii::$app->session->get('service') == Account::TYPE_GOOGLE) {
//            $tokens = Adwords::getGoogleAccessToken($code);
//
//            $login = Adwords::getUserInfo($tokens['access_token']);
//
//            $expiresIn = (int)time() + $tokens['expires_in'];
//            if ($account->is_account_common) {
//                $account->advert_token = (string)$tokens['access_token'];
//                $account->stat_token = (string)$tokens['access_token'];
//                $account->refresh_token = (string)$tokens['refresh_token'];
//                $account->stat_token_expires = (string)$expiresIn;
//                $account->advert_token_expires = (string)$expiresIn;
//                $account->advert_login = $login['name'];
//                $account->stat_login = $login['name'];
//            } elseif ($type == self::TYPE_CONTEXT) {
//                $account->advert_token = (string)$tokens['access_token'];
//                $account->advert_token_expires = (string)$expiresIn;
//                $account->refresh_token = (string)$tokens['refresh_token'];
//                $account->advert_login = $login['name'];
//            } elseif ($type == self::TYPE_WEBANALYTICS) {
//                $account->stat_token = (string)$tokens['access_token'];
//                $account->stat_token_expires = (string)$expiresIn;
//                $account->refresh_token = (string)$tokens['refresh_token'];
//                $account->stat_login = $login['name'];
//            }
//            $account->save();
//            if ($type == self::TYPE_CONTEXT) {
//                $Adwords = new Adwords($account->id);
//                $Adwords->acceptInnivitation($account->client_customer_id);//7263790380
//            }
        } else {
            $token = Direct::getToken($code);
            $login = Direct::getLogin($token['access_token']);
            if ($type == self::TYPE_CONTEXT) {
                $account->advert_token = (string)$token['access_token'];
                $account->advert_token_expires = (string)$token['expires_in'];
                $account->advert_login         = $login;
                $account->state                = Account::STATE_ACTIVE;
            } else {
                $account->stat_token         = (string)$token['access_token'];
                $account->stat_token_expires = (string)$token['expires_in'];
                $account->stat_login         = $login;
            }
            $account->update();

            Yii::$app->session->setFlash('systemType', $type);
        }

        Yii::$app->response->redirect([
            'company/view',
            'id' => $account->company_id,
        ]);
    }

    public function actionSaveReportSettings()
    {
        return $this->saveSettings(self::SETTING_TYPE);
    }

    public function getBreadcrumbs($view, $model)
    {
        if ($view == 'create') {
            return [
                [
                    'label' => 'Проекты',
                    'url' => Url::to(['/company/index'])
                ],
                [
                    'label' => $model['company']->name,
                    'url'   => Url::to([
                        '/company/view',
                        'id' => $model['company']->id
                    ])
                ],
                'Добавить аккаунт'
            ];
        } else {
            $breadcrumbs = parent::getBreadcrumbs($view, $model);
            if ($view == 'update') {
                $breadcrumbs[] = 'Настройки';
            } elseif (Yii::$app->session->get('campaignID', false)) {
                $campaign      = Campaign::findOne(Yii::$app->session->get('campaignID'));
                $breadcrumbs[] = $campaign->name;
            } elseif (Yii::$app->session->get('adgroupID', false)) {
                $adgroup       = Adgroup::findOne(Yii::$app->session->get('adgroupID'));
                $breadcrumbs[] = [
                    'label' => $adgroup->campaign->name,
                    'url'   => Url::to([
                        'account/view',
                        'dimension' => Layer::LAYER_NAME_ADGROUP,
                        'accountID' => $adgroup->account_id
                    ])
                ];
                $breadcrumbs[] = $adgroup->name;
            }

            return $breadcrumbs;
        }
    }

    public function actionViewDeadLinks($accountID)
    {
        $account = $this->findModel($accountID);

        $query = DeadLink::find()->where(['account_name' => $account->advert_login]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],
            'id',
            'title',
            'href',
            'source_id',
            'ad_group_name',
            'campaign_name',
            'account_name',
            [
                'attribute'=>'created_at',
                'label'=>'Дата',
                'vAlign'=>'middle',
                'width'=>'190px',
                'value'=>function ($model) {
                    return date('d m Y H:m:s', $model->created_at);
                },
                'format'=>'raw'
            ],
        ];

        $widget = ExportMenu::widget([
                'dataProvider' => $provider,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Export All',
                    'class' => 'btn btn-default'
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) . "<hr>\n".
            GridView::widget([
                'dataProvider' => $provider,
                'columns' => $gridColumns,
                'exportConfig' => [
                    GridView::CSV => ['label' => 'Save as CSV'],
                    GridView::TEXT => [ 'label' => 'Save as TEXT'],
                    GridView::EXCEL => [ 'label' => 'Save as EXCEL'],
                    GridView::JSON => [ 'label' => 'Save as JSON'],
                ]
            ]);

        $isProcess = (FileLink::findOne(['account_id' => $accountID]) ? true : false);


        $state = (isset($_REQUEST['state']) ? $_REQUEST['state'] : 1);
        return $this->render('view-dead-links',
            [
                'widget' => $widget,
                'account' => $account,
                'isProcess' => $isProcess,
                'state' => $state
            ]);
    }

    public function actionCheckDeadLinks($accountID)
    {

        $account = $this->findModel($accountID);

        if(FileLink::findOne(['account_id' => $accountID])){
            //вывести флэш сообщение что обработка началась
            return $this->redirect(array('/account/view-dead-links', 'accountID' => $account->id),302);
        }

        if($account->dead_link_checked == 0){
            $direct = $account->export;
            $rows   = $direct->getPrepareUrls();
//            $elem                       = end($rows);
//            $elem2                      = end($rows);
//            $elem['url_after_prepare']  = 'https://abakan.rus-buket.ru/1';
//            $elem2['url_after_prepare'] = 'http://yaa123123123123.ru';
//            $rows[]                     = $elem;
//            $rows[]                     = $elem2;
        } else {
            $direct = $account->import;
            $rows = $direct->getPrepareLinks();

        }

        if(isset($rows) && count($rows)>0){
            $this->writeToFile($rows, $account);
            $state = '1';
        } else {
            $state = '0';
            $account->dead_link_checked = time();
            $account->save();
        }

        return $this->redirect(array('/account/view-dead-links', 'accountID' => $account->id, 'state' => $state),302);
    }



    private function writeToFile($array, $account)
    {
        if(count($array) == 0) return false;

        $lastFile = FileLink::find()
            ->orderBy('id DESC')
            ->limit(1)
            ->one();

        if ($lastFile) {
            $newFileName = intval($lastFile->file_name) + 1;
        } else {
            $newFileName = 1;
        }
        $filePath = realpath('../../console/files/');
        $fileDir  = $filePath . '/' . $newFileName;

        while (file_exists($fileDir)) {
            $newFileName++;
            $fileDir = $filePath . '/' . $newFileName;
        }
        $fp = fopen($fileDir, "w");
        chmod($fileDir, 0777);

        foreach ($array as $output) {
            fwrite($fp, json_encode($output) . "\r\n");
        }

        fclose($fp);

        if (file_exists($fileDir)) {
            $fileLink            = new FileLink();
            $fileLink->file_path = $filePath;
            $fileLink->file_name = strval($newFileName);
            $fileLink->account_id = $account->id;
            $fileLink->save();
        }
    }


}
