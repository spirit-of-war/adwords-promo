<?php

namespace backend\controllers;

use common\models\strategy\Limit;
use common\models\structure\Layer;
use Yii;
use common\models\strategy\Rule;
use yii\web\NotFoundHttpException;
use common\models\structure\Campaign;

/**
 * RuleController implements the CRUD actions for Rule model.
 */
class LimitController extends RuleController
{
    protected $type = Limit::TYPE;
    protected $header = 'Лимиты';

    protected function saveRule($rule)
    {
        if ($rule->isNewRecord)
            $rule = new Limit();

        $conditions = json_decode(Yii::$app->request->post('conditions'), true);
        $layerConditions = '';
        if (Yii::$app->request->post('layer') == Layer::LAYER_KEYWORDS)
            $layerConditions = Yii::$app->request->post('keywordsConditions');
        elseif (Yii::$app->request->post('layer') == Layer::LAYER_ADGROUPS)
            $layerConditions = Yii::$app->request->post('adgroupConditions');
        $layerConditions = json_decode($layerConditions, true);

        $rule->attributes = [
            'name' => Yii::$app->request->post('name'),
            'account_id' => Yii::$app->request->post('account'),
            'frequency' => Yii::$app->request->post('frequency'),
            'weekday' => Yii::$app->request->post('weekday'),
            'monthday' => Yii::$app->request->post('monthday'),
            'instant_start' => 0,
            'conditions' => $conditions,
            'layer' => Yii::$app->request->post('layer'),
            'action' => Yii::$app->request->post('action'),
            'state' => Rule::STATE_ACTIVE,
            'type' => Limit::TYPE,
        ];
        $rule->layerConditions = $layerConditions;
        $rule->campaignIds = array_map('intval', explode(',', Yii::$app->request->post('campaigns')));
        $rule->save();

        return $this->redirect([
            'limit/view',
            'id' => $rule->id
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Limit::findOne(['id' => $id, 'type' => $this->type])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single Rule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $rule = $this->findModel($id);
        $company = $rule->account->company;
        $campaignsQuantity = Campaign::findAll([
            'state' => Campaign::STATE_ACTIVE,
            'account_id' => $rule->account_id
        ]);
        $allCampsQuantity = count($campaignsQuantity);

        return $this->render('view', [
            'rule' => $rule,
            'company' => $company,
            'allCampsQuantity' => $allCampsQuantity,
            'account' => $rule->account,
        ]);
    }
}
