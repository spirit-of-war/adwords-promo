<?php

namespace backend\controllers;

use common\models\Account;
use common\models\service\yandex\Metrika;
use common\models\structure\Adgroup;
use common\models\structure\BidAuction;
use common\models\structure\Campaign;
use common\models\structure\Keyword;
use common\models\structure\Layer;
use yii\filters\VerbFilter;
use common\models\Task;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class ApiController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'upload' => ['post'],
                ],
            ],
        ];
    }

    public function actionGetCounters($accountID)
    {
        $account = Account::findOne(['id' => $accountID]);
        $ret = [];
        if ($account) {
            $api = new Metrika($account->stat_token);
            $counters = $api->getCounters();
            foreach ($counters as $counter)
                $ret['counters'][] = [
                    'name' => $counter['name'],
                    'id' => $counter['id']
                ];
            echo json_encode($ret);
        }
    }

    public function actionTask($id)
    {
        $task = Task::findOne($id);
        $task = [
            'id' => $task->id,
            'completeness' => $task->completeness,
            'status' => $task->status
        ];
        return json_encode($task);
    }

    public function actionTasks($accountID)
    {
        return Task::getRunningTasks([
            Task::TYPE_UPDATE_STRUCTURE,
            Task::TYPE_UPDATE_STATISTICS
        ]);
    }

    public function actionGetCampaigns($accountID)
    {
        $campaigns = Campaign::findAll([
            'account_id' => $accountID,
            'state' => Campaign::STATE_ACTIVE
        ]);
        $campaigns = array_map(function ($campaign) {
            return [
                'id' => $campaign->id,
                'sourceID' => $campaign->source_id,
                'name' => $campaign->name
            ];
        }, $campaigns);
        echo json_encode($campaigns);
    }

    public function actionGetAuctionBids()
    {
        $keywordIds = Yii::$app->request->post('keywordIds', false);
        $keywordIds = array_map(function ($keyword) {
            return (int)$keyword;
        }, $keywordIds);

        if (filter_var(Yii::$app->request->post('update', false), FILTER_VALIDATE_BOOLEAN)) {
            $keywords = Keyword::findAll($keywordIds);
            if ($keywords) {
                $account = Account::findOne($keywords[0]->account_id);
                $account->import->updateBids($keywords);
            }
        }

        $keywords = Keyword::findAll($keywordIds);
        $keywords = array_map(function ($keyword) {
            return [
                'keyword_id' => $keyword->id,
                'p11_bid' => $keyword->auction_p11_bid,
                'p11_price' => $keyword->auction_p11_price,
                'p12_bid' => $keyword->auction_p12_bid,
                'p12_price' => $keyword->auction_p12_price,
                'p13_bid' => $keyword->auction_p13_bid,
                'p13_price' => $keyword->auction_p13_price,
                'p21_bid' => $keyword->auction_p21_bid,
                'p21_price' => $keyword->auction_p21_price,
                'p24_bid' => $keyword->auction_p24_bid,
                'p24_price' => $keyword->auction_p24_price,
            ];
        }, $keywords);
        return json_encode($keywords);
    }

    public function actionSetBid()
    {
        $keywordID = Yii::$app->request->post('keywordID');
        $accountID = Yii::$app->request->post('accountID');
        $key = 'bid_' . Yii::$app->request->post('type');
        $value = Yii::$app->request->post('value') * 1000000;

        $account = Account::findOne($accountID);
        $keyword = Keyword::findOne($keywordID);
        $updateResult = $account->export->setBid($keyword,
                                    ($key == 'bid_search' ? $value : false), ($key == 'bid_context' ? $value : false));
        if ($updateResult)
            return json_encode(true);
        else
            return json_encode(false);
    }

    /**
     * Получаем дочерние элементы для меню
     *
     * @param string $parentType
     * @param int $parentId
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetMenuItems($parentType, $parentId)
    {
        switch ($parentType) {
            case Layer::LAYER_NAME_COMPANY:
                echo json_encode([
                    'items' => Account::find()->select([
                        'id',
                        'name' => 'advert_login',
                    ])->where(['company_id' => $parentId, 'state' => 1])->asArray()->all(),
                    'type' => Layer::LAYER_NAME_ACCOUNT,
                    'url' => '/account/campaign/',
                ]);
                break;
            case Layer::LAYER_NAME_ACCOUNT:
                echo json_encode([
                    'items' => Campaign::find()->select([
                        'id',
                        'name',
                    ])->where(['account_id' => $parentId, 'state' => 1])->asArray()->all(),
                    'url' => '/account/adgroup/',
                    'type' => Layer::LAYER_NAME_CAMPAIGN,
                ]);
                break;
            case Layer::LAYER_NAME_CAMPAIGN:
                echo json_encode([
                    'items' => Adgroup::find()->select([
                        'id',
                        'name',
                    ])->where(['campaign_id' => $parentId, 'state' => 1])->asArray()->all(),
                    'url' => '/account/keyword/',
                    'type' => Layer::LAYER_NAME_ADGROUP,
                ]);
                break;
            default:
                throw new BadRequestHttpException("the type of parentId it's not processing");
                break;
        }
    }
}