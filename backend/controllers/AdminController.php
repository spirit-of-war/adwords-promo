<?php
namespace backend\controllers;

use common\models\Account;
use common\models\User;
use common\models\admin\Page;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use \DateTime;
use yii\data\SqlDataProvider;

class AdminController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
		
        return [
			'access' => [
			   'class' => AccessControl::className(),
			   'rules' => [
				   [
					   'actions' => ['order','page','page-add','page-update','stat', 'ckupload'],
					   'allow' => true,
					   'roles' =>  ['@'],
					   'matchCallback' => function ($rule, $action) {
							return User::isUserAdmin(Yii::$app->user->identity->username);
						}
				   ],
			   ],
		   ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($this->action->id == 'ckupload')
        {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);

    }

    public function actionStat($accountID)
    {
        $sql = sprintf('SELECT distinct from_unixtime(start_date), from_unixtime(end_date), start_date, count(*)
                    FROM statistics where account_id = %s group by start_date order by start_date desc',
            $accountID);
        $totalCount = Yii::$app->db->createCommand($sql)->execute();
        $statDataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => Yii::$app->request->getQueryParam('per-page') ?: 10,
            ],
        ]);

        $lastDate = Yii::$app->db->createCommand(
            'select from_unixtime(start_date) from statistics where account_id = :accountID
              order by start_date desc limit 1',
            ['accountID' => $accountID])->queryScalar();
        $lastDate = new DateTime($lastDate);
        $today = new DateTime('today');
        $diff = $today->diff($lastDate);

        $accounts = Account::findAll([
            'state' => Account::STATE_ACTIVE
        ]);
        $accounts = array_map(function ($account) {
            return [$account->id => $account->name];
        }, $accounts);

        return $this->render('stat', [
            'statDataProvider' => $statDataProvider,
            'lastDownload' => $diff->days,
            'accounts' => $accounts
        ]);
    }
	public function actionOrder(){
		$sql = 'SELECT id,name,phone,site,email,from_unixtime(created_at) as created_at, CASE WHEN type = 1 THEN "Заказан звонок" ELSE "Заявка с сайта" END as type FROM `order` ORDER BY created_at DESC';
		$totalCount = Yii::$app->db->createCommand($sql)->execute();
		$dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => Yii::$app->request->getQueryParam('per-page') ?: 10,
            ],
        ]);

		return $this->render('order',[
			'dataProvider' => $dataProvider
		]);
	}

	public function actionPageAdd() {
		if(isset($_REQUEST['Page'])) {
			$model = new Page();
            $model->load(Yii::$app->request->post());
			$model->created = time();

            if(isset($_FILES['Page']['name']['icon']) && $_FILES['Page']['name']['icon'] != '') {
                $model->icon = $this->uploadFile($model);
            }
			$model->save();
			$id = $model->getPrimaryKey();
			$this->redirect('/admin/page/'.$id);
		}
		return $this->render('page-single',[
			'model' => new Page(),
			'action' => 'add'
		]);
	}
	
	private function uploadFile(Page $model = null) {
		$uploadDir = 'frontend/web/img/news';
		$uploadDir2 = Yii::getAlias('@app'.'/../'.$uploadDir);

		if(isset($model) && $model->icon!='') {
		    $file = $uploadDir2.'/'.$model->icon;
		    if(!file_exists($file)) return false;
			unlink($file);
		}
		$filename = $_FILES['Page']['name']['icon'];
		$filePath = $_FILES['Page']['tmp_name']['icon'];
				
		$uploadFile = time().'_'.$filename;
				
		$uploadPath = $uploadDir2.'/'.$uploadFile;
		move_uploaded_file($filePath, $uploadPath);
		return $uploadFile;
	}

	public function actionCkupload()
    {
        $uploadDir = 'frontend/web/img/news/';

        $callback = Yii::$app->request->get('CKEditorFuncNum');
        $file_name = time() . '_' . $_FILES['upload']['name'];
        $file_name_tmp = $_FILES['upload']['tmp_name'];
        $file_new_name = Yii::getAlias('@app'.'/../'.$uploadDir);
        $full_path = $file_new_name.$file_name;
        $http_path = Yii::$app->params['domain'].'img/news/'.$file_name;
        $error = '';
        if( move_uploaded_file($file_name_tmp, $full_path) ) {
        } else {
            $error = 'Some error occured please try again later';
            $http_path = '';
        }
        return "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(".$callback.",  \"".$http_path."\", \"".$error."\" );</script>";
    }
	
	public function actionPageUpdate($id) {
		if($orderForm = Yii::$app->request->post('Page')) {
			$model = Page::findOne(['id' => $id]);

			if(isset($_FILES['Page']['name']['icon']) && $_FILES['Page']['name']['icon'] != '') {
				$model->icon = $this->uploadFile($model);
			}

			$model->h1 = $orderForm['h1'];
			$model->title = $orderForm['title'];
			$model->keywords = $orderForm['keywords'];
			$model->description = $orderForm['description'];
            $model->type = $orderForm['type'];
            $model->slug = $orderForm['slug'];

			if($model->is_seo_only == Page::IS_SEO_ONLY_TRUE) {
				$model->active = Page::PUBLISH;
				$model->announcement = '-';
				$model->body = '-';
			} else {
				$model->active = $orderForm['active'];
				$model->announcement = $orderForm['announcement'];
				$model->body = $orderForm['body'];
			}
			$model->created = time();

			$model->save();

			$this->redirect(['page', 'id' => $model->id]);
		}
	}
	
	public function actionPage($id = null){
        $model = [];
		if($id!=null) {
			$page = Page::findOne(['id' => $id]);
			return $this->render('page-single',[
				'model' => $page,
				'action' => 'update/'.$page['id']
			]);
			
		} else {
			$sql = 'SELECT id,title,announcement,body,created,icon,type,h1,keywords,description,url,active FROM page ORDER BY created DESC';
			$totalCount = Yii::$app->db->createCommand($sql)->execute();
			$dataProvider = new SqlDataProvider([
				'sql' => $sql,
				'totalCount' => $totalCount,
				'pagination' => [
					'pageSize' => Yii::$app->request->getQueryParam('per-page') ?: 10,
				],
			]);

			return $this->render('page',[
				'model' => $model,
				'dataProvider' => $dataProvider
			]);
		}
	}
}
