<?php

namespace backend\controllers;

use common\models\Account;
use common\models\structure\Campaign;
use common\models\Company;
use common\models\strategy\Index;
use common\models\structure\Layer;
use Yii;
use common\models\strategy\Rule;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Task;

/**
 * RuleController implements the CRUD actions for Rule model.
 */
class RuleController extends BaseController
{
    protected $type = Rule::TYPE;
    protected $header = 'Правила';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($accountID)
    {
        Yii::$app->session->set('testttt', 'yep');
        $account = Account::findOne($accountID);

        $rules = Rule::findAll([
            'account_id' => $accountID,
            'state' => [Rule::STATE_ACTIVE, Rule::STATE_SUSPENDED],
            'type' => $this->type
        ]);

        return $this->render('index', [
            'rules' => $rules,
            'account' => $account
        ]);
    }

    /**
     * Displays a single Rule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $rule = $this->findModel($id);
        $company = $rule->account->company;
        $campaignsQuantity = Campaign::findAll([
            'state' => Campaign::STATE_ACTIVE,
            'account_id' => $rule->account_id
        ]);
        $allCampsQuantity = count($campaignsQuantity);

        $taskQuantity = Task::find()->where([
            'object_id' => $id,
            'type' => Task::TYPE_RULE,
            'status' => Task::STATUS_COMPLETED
        ])->count();

        $pageSize = 20;
        $pages = new Pagination(['totalCount' => $taskQuantity, 'pageSize' => $pageSize]);
        $showExecutions = Yii::$app->request->get('page', false) !== false;

        $tasks = Task::find()
            ->where([
                'object_id' => $id,
                'type' => Task::TYPE_RULE,
                'status' => Task::STATUS_COMPLETED])
            ->offset($pageSize * (Yii::$app->request->get('page', 1) - 1))
            ->limit($pageSize)
            ->orderBy('id desc')
            ->all();

        $taskExecutions = [];
        if ($tasks) {
            $taskIds = array_map(function ($task) {
                return $task->id;
            }, $tasks);

            $executionsSql = sprintf(
                'select t.id, count(*) q
              from task t
              inner join bid b on b.task_id = t.id
              inner join keyword k on b.keyword_id = k.id
              inner join campaign c on c.id = k.campaign_id
              where t.id in (%s)
              group by t.id', implode(',', $taskIds));
            $executions = Yii::$app->db->createCommand($executionsSql)->queryAll();

            foreach ($tasks as $task) {
                $taskExecutions[$task->id] = [
                    'id' => $task->id,
                    'date' => $task->created_at,
                    'executions' => 0
                ];
                foreach ($executions as $execution)
                    if ($task->id == $execution['id'])
                        $taskExecutions[$task->id]['executions'] = $execution['q'];
            }
        }

        return $this->render('view', [
            'rule' => $rule,
            'company' => $company,
            'allCampsQuantity' => $allCampsQuantity,
            'tasks' => $taskExecutions,
            'account' => $rule->account,
            'pages' => $pages,
            'showExecutions' => $showExecutions,
            'ruleExecuted' => Yii::$app->session->getFlash('ruleExecuted', false)
        ]);
    }

    public function actionGetRuleResult()
    {
        $taskId = Yii::$app->request->post('task', false);

        if ($taskId !== false) {
            $executionsSql = sprintf(
                'select b.keyword_source_id, b.bid_search, b.bid_context, k.keyword, c.name campaign
                  from task t
                  inner join bid b on b.task_id = t.id
                  inner join keyword k on b.keyword_id = k.id
                  inner join campaign c on c.id = k.campaign_id
                  where t.id = %s
                  order by campaign, t.created_at desc', $taskId);
            $executions = Yii::$app->db->createCommand($executionsSql)->queryAll();

            return json_encode($executions);
        }
    }

    /**
     * Creates a new Rule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($accountID)
    {
        if (Yii::$app->request->isPost) {
            $rule = new Rule();
            $this->saveRule($rule);
        }

        $model = new Rule();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'index' => new Index(),
                'account' => Account::findOne($accountID)
            ]);
        }
    }

    /**
     * Updates an existing Rule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->isPost) {
            $rule = $this->findModel($id);
            $this->saveRule($rule);
        }
        $campaignsQuantity = Campaign::findAll([
            'state' => Campaign::STATE_ACTIVE,
            'account_id' => $id
        ]);
        $allCampsQuantity = count($campaignsQuantity);

        $model = $this->findModel($id);
        $sql = sprintf('select * from %s where company_id = (select company_id from %s where id = %s)',
            Account::tableName(),
            Account::tableName(),
            $model->account_id
        );
        $accounts = Account::findBySql($sql)->all();
        $company = Company::findOne($model->account->company_id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'accounts' => $accounts,
                'company' => $company,
                'allCampsQuantity' => $allCampsQuantity,
                'account' => $model->account
            ]);
        }
    }

    public function actionToggle($id)
    {
        $rule = $this->findModel($id);
        if (in_array($rule->state, [Rule::STATE_SUSPENDED, Rule::STATE_ACTIVE])) {
            $rule->state = $rule->state == Rule::STATE_SUSPENDED ? Rule::STATE_ACTIVE : Rule::STATE_SUSPENDED;
            $rule->update(true, ['state']);
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    /**
     * Deletes an existing Rule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $account = Account::findOne($this->findModel($id)->account_id);

        $rule = $this->findModel($id);
        $rule->state = Rule::STATE_REMOVED_BY_USER;
        $rule->update(false);

        return $this->redirect([
            $this->type == Rule::TYPE ? 'rule/index' : 'limit/index',
            'accountID' => $account->id
        ]);
    }

    /**
     * Finds the Rule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rule::findOne(['id' => $id, 'type' => $this->type])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param Rule $rule
     * @return mixed
     */
    protected function saveRule($rule)
    {
        $conditions = json_decode(Yii::$app->request->post('conditions'), true);

        $rule->attributes = [
            'name' => Yii::$app->request->post('name'),
            'account_id' => Yii::$app->request->post('account'),
            'frequency' => Yii::$app->request->post('frequency'),
            'weekday' => Yii::$app->request->post('weekday'),
            'monthday' => Yii::$app->request->post('monthday'),
            'onceday' => (integer)strtotime(Yii::$app->request->post('onceday')),
            'instant_start' => Yii::$app->request->post('instantStart') == 'on' ? 1 : 0,
            'conditions' => $conditions,
            'layer' => Layer::LAYER_KEYWORDS,
            'change' =>  Yii::$app->request->post('change'),
            'action' =>  Yii::$app->request->post('action'),
            'state' => Rule::STATE_ACTIVE,
            'position' => Yii::$app->request->post('position'),
            'type' => Rule::TYPE,
            'min_bid' => Yii::$app->request->post('minBid'),
            'max_bid' => Yii::$app->request->post('maxBid'),
        ];
        $rule->campaignIds = array_map('intval', explode(',', Yii::$app->request->post('campaigns')));
        $rule->save();

        return $this->redirect([
            'rule/view',
            'id' => $rule->id
        ]);
    }

    public function actionExecute($id)
    {
        $rule = $this->findModel($id);
        $rule->execute();

        Yii::$app->session->setFlash('ruleExecuted');

        return $this->redirect([
            'rule/view',
            'id' => $id
        ]);
    }

    protected function getBreadcrumbs($view, $model)
    {
        $breadcrumbs = parent::getBreadcrumbs($view, $model);
        $breadcrumbs[] = [
            'label' => $this->header,
            'url' => Url::to([
                'rule/index',
                'accountID' => $model['account']->id
            ])
        ];
        if ($view == 'view') {
            $breadcrumbs[] = [
                'label' => $model['rule']->name,
                'url' => Url::to([
                    'rule/view',
                    'id' => $model['rule']->id
                ])
            ];
        }
        elseif ($view == 'create') {
            $breadcrumbs[] = 'Добавить';
        }
        elseif ($view == 'update') {
            $breadcrumbs[] = [
                'label' => $model['model']->name,
                'url' => Url::to([
                    'rule/view',
                    'id' => $model['model']->id
                ])
            ];
            $breadcrumbs[] = 'Редактирование';
        }
        return $breadcrumbs;
    }
}
