/**
 * Created by savchenko on 23.12.16.
 */

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(function () {
    $('.report-chart').map(function (i, e) {
        console.log($(e).data('chart'));
        // var data = $.parseJSON($(e).data('chart'));
        var data = $(e).data('chart');
        data = google.visualization.arrayToDataTable(data);
        var options = {
            legend: { position: 'top' },
            focusTarget: 'category',
            pointSize: 3
        };
        var chart = new google.visualization.LineChart(e);
        chart.draw(data, options);
    });
});

