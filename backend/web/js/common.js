var set_push_state = true,
    xhr;

const PATH = window.location.protocol + '//' + window.location.hostname + '/';

Common = {
    /*
     Возвращает текущий id аккаунта
     * */
    getAccountId: function () {
        return $('#accountId').val();
    },
    /*
     Нужна ли проверка ставки при клике на поле
     */
    checkBid: function () {
        return true;
    }
};

function sendAjax(btn_object, data, url, success_function, type, dataType) {
    var class_for_btn_press = 'btn_press';

    if (btn_object !== null) {
        if (btn_object.hasClass(class_for_btn_press)) {
            return;
        }

        btn_object.addClass(class_for_btn_press);
    }

    dataType = dataType || 'json';

    window.xhr = $.ajax({
        url: url,
        data: data,
        dataType: dataType,
        type: type,
        statusCode: {
            404: function (xhr) {
                console.error(JSON.parse(xhr.responseText)[404]);
            },
            400: function (xhr) {
                console.error(JSON.parse(xhr.responseText)[400]);
            },
            500: function () {
                console.error('Произошла непредвиденная ошибка на сервере');
            }
        },
        success: function (json) {
            success_function(json);
        },
        complete: function (xhr, status) {
            if (btn_object !== null) {
                btn_object.removeClass(class_for_btn_press);
            }

            set_push_state = true;
            window.xhr = null;
        }
    });

    return window.xhr;
}

function showCurrentMenuItemsRecursive(currentObj, isFirst) {
    isFirst = isFirst || false;
    if (currentObj.parents('[data-type]:first').length) {
        showCurrentMenuItemsRecursive(currentObj.parents('[data-type]:first'));
    }

    currentObj.addClass('my-active').parents('ul:first').addClass('active').css('display', 'block');

    if (isFirst) {
        currentObj.find('ul:first').css('display', 'block');
        $('[data-holder="left_menu"]').scrollTop(currentObj.offset().top - 200);
    }
}

$(function () {
    if ($('#sidebar-menu li[data-current]').length) {
        showCurrentMenuItemsRecursive($('#sidebar-menu li[data-current]:last'), true);
    }

    $('#sidebar-menu').off('click.view_children').on('click.view_children', '[data-type]', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var li_obj = $(this);

        // Если мы кликаем не по компании и вложенность детей не больше одного
        if (li_obj.data('type') !== 'company') {
            window.location.href = li_obj.find('a:first').attr('href');
            return;
        }

        li_obj.find('ul:first').toggle();
    });

    $('#sidebar-menu').off('click.get_children').on('click.get_children', '.fa-chevron-down', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
});