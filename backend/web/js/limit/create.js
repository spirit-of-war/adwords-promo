var updateMode = location.pathname == '/limit/update';

/* FIRST STEP */
    $('.rule-name').on('change', function (e) {
        $('.check-name').html(e.target.value);
    });
    $('.account-select').on('change', function (e) {
        $('.check-campaigns').html('');
        $('.check-account').html($('.account-select option:selected').text());
    });
    $('.check-account').html($('.account-select option:selected').text());
    $('.rule-layer select').on('change', function (e) {
        var show = '', hide = '';
        if (e.target.value == 0) {
            show = '.rule-campaigns, .check-campaigns-tr';
            hide = '.rule-keywords, .rule-groups, .check-keywords, .check-groups';
        } else if (e.target.value == 1) {
            show = '.rule-groups, .check-groups';
            hide = '.rule-keywords, .rule-campaigns, .check-keywords, .check-campaigns-tr';
        } else {
            show = '.rule-keywords, .check-keywords';
            hide = '.rule-campaigns, .rule-groups, .check-campaigns-tr, .check-groups';
        }
        $(show).show();
        $(hide).hide();
    });

    if (updateMode) {
        if ($('.rule-layer select').val() == 0)//campaigns
            $('.rule-keywords, .rule-groups').hide();
        else if ($('.rule-layer select').val() == 1)//groups
            $('.rule-keywords, .rule-campaigns').hide();
        else if ($('.rule-layer select').val() == 3)//keywords
            $('.rule-groups, .rule-campaigns').hide();
    }
    else {
        $('.rule-keywords, .rule-groups').hide();
        $('.check-groups, .check-keywords').hide();
    }

/* SECOND STEP */

    $('.rule-frequency select').on('change', function (e) {
        var show = '', hide = '', checkFreq = '';
        checkFreq = $('.rule-frequency option:selected').text();
        if (e.target.value == 0) {
            hide = '.rule-weekday, .rule-monthday, .rule-once';
        } else if (e.target.value == 1) {
            show = '.rule-weekday';
            hide = '.rule-monthday, .rule-once';
            checkFreq += ': ' + $('.rule-weekday option:selected').text();
        } else if (e.target.value == 2) {
            show = '.rule-monthday';
            hide = '.rule-weekday, .rule-once';
            checkFreq += ': ' + $('.rule-monthday input').val() + ' числа';
        } else if (e.target.value == 3) {
            show = '.rule-once';
            hide = '.rule-weekday, .rule-monthday';
        }
        $(show).show();
        $(hide).hide();
        $('.check-freq').html(checkFreq);
    });

    $('.rule-weekday select').on('change', function () {
        var checkFreq = $('.rule-frequency option:selected').text() + ': ' + $('.rule-weekday option:selected').text();
        $('.check-freq').html(checkFreq);
    });

    $('.rule-monthday input').on('change', function () {
        var checkFreq = $('.rule-frequency option:selected').text() + ': ' + $('.rule-monthday input').val() + ' числа';
        $('.check-freq').html(checkFreq);
    });

    $('.rule-monthday input').on('change', function (e) {
        var value = e.target.value;
        if (value < 1 || value instanceof String)
            e.target.value = 1;
        else if (value > 31)
            e.target.value = 31;
    });


/* THIRD STEP */
/* FOURTH STEP */

    $('.action-select').on('change', function (e) {
        if (e.target.value == 1) {
            $('.rule-position').hide();
            $('.change').show();
            $('.change-label').html('Процент');
        } else if (e.target.value == 2) {
            $('.rule-position').hide();
            $('.change').show();
            $('.change-label').html('Значение ставки');
        } else if (e.target.value == 3) {
            $('.rule-position').show();
            $('.change').show();
            $('.change-label').html('Процент');
        } else if (e.target.value == 0) {
            $('.change').hide();
            $('.rule-position').hide();
        }
    });

    $('.check-layer').html($('.rule-layer option:selected').text());
    $('.rule-layer select').on('change', function () {
        $('.check-layer').html($('.rule-layer option:selected').text());
    });

    $('.check-action').html(
        $('.rule-action option:selected').text() + ': ' + $('.account-email').html());
    $('.rule-action').on('change', function () {
        var addText = $('.rule-action select').val() == 0 ? ' с уведомлением на почту: ' : ': ';
        addText += $('.account-email').html();
        $('.check-action').html($('.rule-action option:selected').text() + addText);
    });

    $('.rule-layer select').on('change', function (e) {
        if (e.target.value == 0 || e.target.value == 1 || e.target.value == 2) {
            $('.action-select option[value=1], .action-select option[value=2], .action-select option[value=3],' +
                '.change').hide();
            $('.action-select').val(0);
        } else {
            $('.action-select option[value=1], .action-select option[value=2], .action-select option[value=3]').show();
        }
    });

    var validated = false;
    var validate = function (validateCondition, errorText) {
        if (validateCondition) {
            if (validated == false) {
                $('.actionBar').append(
                    '<div class="rule-validate rule-validate"><span class="label label-danger">'
                    + errorText
                    + '</span></div>'
                );
            }
            $('.rule-name input').focus();
            validated = true;
            return false;
        } else {
            $('.rule-validate').remove();
            validated = false;
            return true;
        }
    };
    var validateCurrentStep = function (context) {

        if (context.fromStep == 1)
            return validate($.trim($('.rule-name input').val()) == '', 'Введите название правила')
                && validate($('.rule-campaigns').val() == '' && $('.rule-layer select').val() == 0, 'Выберите кампании');
        else if (context.fromStep == 2)
            return true;
        else if (context.fromStep == 3) {
            var conds = JSON.parse($('#conditions').val());
            var empty = true;
            if (conds.length == 0) {
                return validate(true, 'Добавьте условия, при которых будет срабатывать лимит.');
            } else {
                for (var i = 0; i < conds.length; i++)
                    for (var j = 0; j < conds[i].conds.length; j++) {
                        if ($.trim(conds[i].conds[j].value) != '') {
                            empty = false;
                            break;
                        }
                    }
            }
            return validate(empty, 'Добавьте условия, при которых будет срабатывать лимит.');
        } else if (context.fromStep == 4) {
            return validate(
                $('.action-select').val() == 1 && $.trim($('#change').val()) == '',
                'Укажите %, на который изменять ставку при выполнении условий.');
        }
        return true;
    };

    $("#wizard").smartWizard({
        labelNext:'Далее', // label for Next button
        labelPrevious:'Назад', // label for Previous button
        labelFinish: updateMode ? 'Обновить' : 'Добавить',
        enableAllSteps: updateMode,
        enableFinishButton: updateMode,
        reverseButtonsOrder: false,
        keyNavigation: false,
        onShowStep: function () {
            $('.stepContainer').removeAttr('style');
        },
        onLeaveStep: function(e, context) {
            if (updateMode) {
                if (context.toStep == 5) {
                    $('#wizard .buttonNext').hide();
                } else if (context.fromStep == 5) {
                    $('#wizard .buttonNext').show();
                }
            } else {
                if (context.toStep == 5) {
                    $('#wizard .buttonNext').hide();
                    $('#wizard .buttonFinish').show();
                } else {
                    $('#wizard .buttonNext').show();
                    $('#wizard .buttonFinish').hide();
                }
        }
            return validateCurrentStep(context);
        },
        onFinish: function (e, context) {
            if (!validateCurrentStep(context))
                return false;
            if (!validate(
                    $.trim($('.rule-name input').val()) == '',
                    'Введите название правила')) {
                $("#wizard").smartWizard('goToStep', 1);
                return false;
            }
            if (!validate(
                    $('.all-campaigns').prop('checked') == false && $('.campaigns-select').val() == null,
                    'Выберите кампании или укажите, применять для всех')) {
                $("#wizard").smartWizard('goToStep', 2);
                return false;
            }
            if (!validate(
                    $('#conditions').val() == '',
                    'Добавьте условия, при которых будет выполняться правило')) {
                $("#wizard").smartWizard('goToStep', 3);
                return false;
            }
            if (!validate(
                    $('.action-select').val() == 1 && $.trim($('#change').val()) == '',
                    'Укажите %, на который изменять ставку при выполнении условий.'
                )) {
                $("#wizard").smartWizard('goToStep', 4);
                return false;
            }

            $('.condition-form').submit();
        }
    });

    $("#wizard").css('visibility', 'visible');
    $(".buttonNext").addClass("btn btn-primary");
    $(".buttonPrevious").addClass("btn btn-primary");
    $(".buttonFinish").addClass("btn btn-success");
    if (!updateMode)
        $('.buttonFinish').hide();