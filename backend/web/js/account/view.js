$('.table-fields-selector i').on('click', function () {
    $('.table-fields-selector-indexes').toggle();
});

$('.table-fields-selector .cancel').on('click', function () {
    $('.table-fields-selector-indexes').hide();
});

$('.table-fields-selector .save').on('click', function () {
    var fields = [];
    $('.table-fields-selector-indexes ul input[type=checkbox]:checked').each(function(key, element) {
        fields.push($(element).val());
    });
    $.ajax({
        url: '/account/save-report-settings',
        method: 'POST',
        data: {
            fields: fields,
            accountID: accountID,
            layer: view
        },
        success: function(data) {
            if ($.parseJSON(data) == true)
                location.reload();
        }
    });
});

$('.table-fields-selector-indexes ul').sortable();

$(".daterange-icon").hide();
var records = $('.summary').html();
$('.summary').html('');
$(".table-fields-selector").detach().appendTo('.summary');
$('.summary').append(' ' + records);
$(".daterange").detach().appendTo('.summary');
$(".daterange").css('display', 'inline-block');
$(".daterange").css('margin-left', '10px');
$(".daterange").css('margin-right', '10px');
$(".clearFilters").detach().appendTo('.summary');
