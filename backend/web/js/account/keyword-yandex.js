var getAuctionBids = function (keywords, updateFromYandex) {
    $.ajax({
        url: '/api/get-auction-bids',
        method: 'POST',
        dataType: 'json',
        data: {'keywordIds': keywords, 'update': updateFromYandex},
        success: function (data) {
            $.each(data, function (key, auction) {
                $('.auction-bids-' + auction.keyword_id).html(
                    '<table>' +
                    '<tr><td colspan="3">Цены позиций ' +
                    '<a href="https://direct.yandex.ru/tooltip.html?id=positions&lang=ru" target="_blank" title="Помощь">' +
                    '<i class="fa fa-question-circle"></i></a> ' +
                    '<a class="refresh" onclick="getAuctionBids([' + auction.keyword_id + '], true)" title="Обновить">' +
                    '   <i class="fa fa-refresh"></i></a>' +
                    '</td></tr>' +
                    '<tr><td>1-ое спецразмещение </td>' +
                    '<td>' + auction.p11_bid / 1000000 + '</td>' +
                    '<td>' + auction.p11_price / 1000000 + '</td></tr>' +
                    '<tr><td>2-ое спецразмещение </td>' +
                    '<td>' + auction.p12_bid / 1000000 + '</td>' +
                    '<td>' + auction.p12_price / 1000000 + '</td></tr>' +
                    '<tr><td>вход в спецразмещение </td>' +
                    '<td>' + auction.p13_bid / 1000000 + '</td>' +
                    '<td>' + auction.p13_price / 1000000 + '</td></tr>' +
                    '<tr><td>1-ое место </td>' +
                    '<td>' + auction.p21_bid / 1000000 + '</td>' +
                    '<td>' + auction.p21_price / 1000000 + '</td></tr>' +
                    '<tr><td>вход в гарантию </td>' +
                    '<td>' + auction.p24_bid / 1000000 + '</td>' +
                    '<td>' + auction.p24_price / 1000000 + '</td></tr>'
                    + '</table>'
                );
            });

            $.each(data, function (key, auction) {
                $('.auction-bids-selector-' + auction.keyword_id).html(
                    '<table>' +
                    '<tr data-bid="' + auction.p11_bid / 1000000 + '"><td>цена 1-го спецразмещения </td>' +
                    '<td>' + auction.p11_bid / 1000000 + '</td>' +
                    '<td>' + auction.p11_price / 1000000 + '</td></tr>' +
                    '<tr data-bid="' + auction.p12_bid / 1000000 + '"><td>цена 2-го спецразмещения </td>' +
                    '<td>' + auction.p12_bid / 1000000 + '</td>' +
                    '<td>' + auction.p12_price / 1000000 + '</td></tr>' +
                    '<tr data-bid="' + auction.p13_bid / 1000000 + '"><td>вход в спецразмещение </td>' +
                    '<td>' + auction.p13_bid / 1000000 + '</td>' +
                    '<td>' + auction.p13_price / 1000000 + '</td></tr>' +
                    '<tr data-bid="' + auction.p21_bid / 1000000 + '"><td>цена 1-го места </td>' +
                    '<td>' + auction.p21_bid / 1000000 + '</td>' +
                    '<td>' + auction.p21_price / 1000000 + '</td></tr>' +
                    '<tr data-bid="' + auction.p24_bid / 1000000 + '"><td>вход в гарантию </td>' +
                    '<td>' + auction.p24_bid / 1000000 + '</td>' +
                    '<td>' + auction.p24_price / 1000000 + '</td></tr>'
                    + '</table>'
                );
            });
        }
    });
};
