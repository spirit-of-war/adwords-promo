var getAuctionBids = function (keywords, updateFromYandex) {
    $.ajax({
        url: '/api/get-auction-bids',
        method: 'POST',
        dataType: 'json',
        data: {'keywordIds': keywords, 'update': updateFromYandex},
        success: function (data) {
            $.each(data, function (key, auction) {
                $('.auction-bids-' + auction.keyword_id).html(
                    '<table>' +
                    '<tr><td colspan="3">Цены позиций ' +
                    '<a class="refresh" onclick="getAuctionBids([' + auction.keyword_id + '], true)" title="Обновить">' +
                    '   <i class="fa fa-refresh"></i></a>' +
                    '</td></tr>' +
                    '<tr><td>Первая страница </td>' +
                    '<td>' + auction.p11_bid / 1000000 + '</td>' +
                    '<tr><td>Топ страницы </td>' +
                    '<td>' + auction.p12_bid / 1000000 + '</td>' +
                    '<tr><td>Первая позиция </td>' +
                    '<td>' + auction.p13_bid / 1000000 + '</td></tr>' +
                    '</table>'
                );
            });

            $.each(data, function (key, auction) {
                $('.auction-bids-selector-' + auction.keyword_id).html(
                    '<table>' +
                    '<tr data-bid="' + auction.p11_bid / 1000000 + '"><td>Первая страница </td>' +
                    '<td>' + auction.p11_bid / 1000000 + '</td></tr>' +
                    '<tr data-bid="' + auction.p12_bid / 1000000 + '"><td>Топ страницы </td>' +
                    '<td>' + auction.p12_bid / 1000000 + '</td></tr>' +
                    '<tr data-bid="' + auction.p13_bid / 1000000 + '"><td>Первая позиция </td>' +
                    '<td>' + auction.p13_bid / 1000000 + '</td></tr>'
                    + '</table>'
                );
            });
        }
    });
};
