var showGoals = function (e) {
    $('.goals').html('');
    for (var i = 0; i < counters.length; i++)
        for (var j = 0; j < counters[i]['goals'].length; j++) {
            var goal = counters[i]['goals'][j];
            var selected = selectedGoals.indexOf(goal['id']) != -1;
            $('.goals').append('<div class="checkbox"><label>'
                + '<input type="checkbox" name="goal[]" value="' + goal['id'] + '" '
                + (selected ? 'checked="checked"' : '') + '> ' + goal['name']
                + '</label></div>');
        }
};

$(showGoals());
$('.counters').on('change', showGoals());