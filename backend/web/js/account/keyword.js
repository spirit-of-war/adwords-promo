var keywords = [];
$.each($('.auction-bids'), function (key, val) {
    keywords.push($(val).data('keyword'));
}.bind(keywords));
getAuctionBids(keywords, true);
setInterval(function() {
    getAuctionBids(keywords, true);
}, 1000 * 60 * 5);

$('.auction-bids-selector').on('click', function (e) {
    $(e.target).parents('.auction-bids-selector').hide();
    var keywordID = $(e.target).parents('.auction-bids-selector').data('keyword');
    $('.bid-changer-' + keywordID).val($(e.target).parent().data('bid'));
});

var previousValue;
$('.bid-changer').on('focus', function (e) {
    previousValue = e.target.value;
    $('.auction-bids-selector-' + $(e.target).data('keyword')).show();
});
$('.bid-changer').on('focusout', function (e) {
    if (previousValue != false)
        e.target.value = previousValue;
});
$('.bid-changer').on('keypress', function (e) {
    if (e.which === 13) {
        var updatingNotice = new PNotify({
            title: 'Обновление ставки',
            text: 'Запрос на изменение ставки отправлен.',
            type: 'info',
            styling: 'bootstrap3'
        });
        previousValue = false;
        $(e.target).blur();
        var postData = {
            keywordID: $(e.target).data('keyword'),
            value: e.target.value,
            type: $(e.target).data('bidtype'),
            accountID: accountID
        };
        $.ajax({
            url: '/api/set-bid',
            method: 'POST',
            dataType: 'json',
            data: postData,
            success: function (data) {
                updatingNotice.remove();
                new PNotify({
                    title: data == true ? 'Ставка обновлена' : 'Ошибка',
                    text: data == true ? 'Изменение ставки прошло успешно!' : 'Ставку не уалось обновить.',
                    type: data == true ? 'success' : 'error',
                    styling: 'bootstrap3'
                });
            }.bind(updatingNotice)
        });
    }
});

$('.negative-show').on('click', function () {
    $('.negative-' + $(this).data('keyword')).show();
});
$('.negative i').on('click', function () {
    $(this).parents('.negative').hide()
});

$('body').on ('mousedown', function (e) {
    var container = $('.negative, .auction-bids-selector');
    if (!container.is(e.target) && container.has(e.target).length === 0)
        container.hide();
});