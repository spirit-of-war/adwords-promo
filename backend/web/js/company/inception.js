/**
 * Created by savchenko on 29.12.16.
 */

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var type = getUrlParameter('type');

$("#wizard").smartWizard({
    labelNext: 'Далее',
    labelPrevious: 'Назад',
    labelFinish: 'Добавить',
    enableAllSteps: false,
    enableFinishButton: false,
    reverseButtonsOrder: false,
    keyNavigation: false,
    onShowStep: function () {
        return true;
    },
    onLeaveStep: function(e, context) {
        if (context.fromStep == 1) {
            if ($('#isProjectCreated').val() || (type != undefined && (type == 1 || type == 0))) {
                return true;
            }
            if ($.trim($('#projectName').val()) == '') {
                return false;
            } else {
                $.ajax({
                    url: '/company/create',
                    method: 'POST',
                    dataType: 'json',
                    data: {'companyName': $('#projectName').val(), 'website': $('#website').val()},
                    success: function (data) {
                        $('#isProjectCreated').val(data);
                        $('#advertDirectLink').attr('href', '/account/connect-direct?companyId=' + data);
                        if (data > 0) {
                            $("#wizard").smartWizard('disableStep', 1);
                        } else {
                            alert('Произошла ошибка. Попробуйте перезагрузить страницу.');
                        }
                    }
                });
            }
        } if (context.fromStep == 2) {
            if ($('.advert-connect-result input').length > 0 && $('.advert-connect-result input:checked').length > 0) {
                $.ajax({
                    url: '/company/save-login',
                    method: 'POST',
                    dataType: 'json',
                    data: {'login': $('.advert-connect-result input:checked').val(), 'id': getUrlParameter('id')},
                    success: function (data) {
                        if (data == true) {
                            $('.advert-connect-result input').remove();
                            $("#wizard").smartWizard('goForward');
                            $("#wizard").smartWizard('disableStep', 2);
                        }
                        else {
                            alert('Произошла ошибка. Попробуйте перезагрузить страницу.');
                        }
                    }
                });
            } else {
                return true;
            }
        } if (context.fromStep == 3) {
            var goals = [];
            $('.counter-goals input:checked').map(function (i, goal) {
                goals.push(goal.value);
            });
            if ($('.counter-goals input:checked').length > 0) {
                $.ajax({
                    url: '/company/save-goals',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        'id': getUrlParameter('id'),
                        'counter': $('.webanal-connect-result select').val(),
                        'goals': goals
                    }
                });
            }
            return true;
        } if (context.fromStep == 4) {
            if ($('.crm-url').val() != '') {
                $.ajax({
                    url: '/company/save-crm',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        'id': getUrlParameter('id'),
                        'crmUrl': $('.crm-url').val(),
                    }
                });
            }
            return true;
        } else {
            return true;
        }
    }.bind(type),
    onFinish: function (e, context) {
        return true;
    }
});
$(".buttonNext").addClass("btn btn-primary");
$(".buttonPrevious").hide();
$(".buttonFinish").hide();


if (type != undefined && type == '1') {
    $("#wizard").smartWizard('goToStep', 2);
    $("#wizard").smartWizard('disableStep', 1);
    $('.advert-connect-link').hide();

    $.ajax({
        url: '/company/get-direct-client-list?id=' + getUrlParameter('id'),
        dataType: 'json',
        success: function (data) {
            if (data.isAgency != false) {
                $('.advert-connect-result')
                    .append('<div>Подключенный аккаунт Яндекс.Директ <b>' + data.login + '</b> является агентством, выберите клиента:</div>');
                data.clients.map(function (login) {
                    $('.advert-connect-result')
                        .append('<div><label><input type="radio" name="login" value="' + login + '"> ' + login + '</label></div>');
                });
                $('.advert-connect-result input:first').attr('checked', 'checked');
            } else {
                $('.advert-connect-result')
                    .append('<div>Подключен аккаунт Яндекс.Директ <b>' + data.login + '</b></div>');
            }
        }
    });

    $('#webanalDirectLink').attr('href', '/account/connect-metrika?companyId=' + getUrlParameter('id'));
}

if (type != undefined && type == '0') {
    $("#wizard").smartWizard('goToStep', 3);
    $("#wizard").smartWizard('disableStep', 1);
    $("#wizard").smartWizard('disableStep', 2);
    $('.webanal-connect-link').hide();

    $.ajax({
        url: '/company/get-metrika-counters?id=' + getUrlParameter('id'),
        dataType: 'json',
        success: function (data) {
            if (data.counters.length > 0) {
                $('.webanal-connect-result').append('<div>Подключен аккаунт Яндекс.Метрика <b>' + data.login + '</b></div>');
                $('.webanal-connect-result').append('<div><select><option>Выберите счетчик</option></select></div>');
                data.counters.map(function (counter) {
                    $('.webanal-connect-result select')
                        .append('<option value="' + counter.id + '">' + counter.name + '</option>');
                    $('.webanal-connect-result').append('<div style="display: none" class="counter-goals goals-'
                        + counter.id + '"></div>');
                    counter.goals.map(function (goal) {
                        $('.goals-' + counter.id).append('<div><label><input type="checkbox" value="' + goal.id + '"> '
                            + goal.name + '</label></div>');
                    });
                });

                $('.webanal-connect-result select').on('click', function (e) {
                    $('.stepContainer').removeAttr('style');
                    $('.counter-goals input').removeAttr('checked');
                    $('.counter-goals').hide();
                    $('.goals-' + e.target.value).show();
                });
            } else {
                $('.webanal-connect-result')
                    .append('<div>В подключенном аккаунте Яндекс.Метрика <b>' + data.login + '</b> нет счетчиков.</div>');
            }
        }
    });
}