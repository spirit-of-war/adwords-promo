/**
 * Created by savchenko on 29.12.16.
 */

$('select.counter').on('change', function (e) {
    $('.goals-selector input').removeAttr('checked');
    $('.goals').hide();
    $('.goals-' + e.target.value).show();
});

$('.goals-' + $('select.counter').val()).show();