/**
 * Created by savchenko on 19.01.17.
 */

$('.accordion a').on('click', function (e) {
    var id = $(e.target).data('id');
    $('#execution-' + id).toggle();
    var hasData = $('#execution-' + id).data('downloaded');
    console.log(hasData);
    if (hasData == false) {
        $('#execution-' + id).data('downloaded', true);
        $.ajax({
            url: 'get-rule-result',
            method: 'POST',
            data: {
                task: id,
            },
            success: function(data) {
                data = $.parseJSON(data);
                var table = '';
                for (var i = 0; i < data.length; i++) {
                    var e = data[i];
                    table += '<tr><td>' + e.campaign + '</td><td>' + e.keyword_source_id + '</td><td>' + e.keyword + '</td>'
                        + '<td>' + (e.bid_search == null ? '' : e.bid_search) + '</td>'
                        + '<td>' + (e.bid_context == null ? '' : e.bid_context) + '</td></tr>';
                }
                table = '<table class="table">' +
                    '<tr><th>Кампания</th><th>ID ключевого слова</th><th>Ключевое слово</th><th>Ставка на поиске</th><th>Ставка в РС</th></tr>'
                    + table + '</table>';
                $('#execution-' + id).html(table);
            }
        });
    }
});