var updateMode = location.pathname == '/rule/update';

/* FIRST STEP */
    $('.rule-name').on('change', function (e) {
        $('.check-name').html(e.target.value);
    });
    $('.account-select').on('change', function (e) {
        $('.check-campaigns').html('');
        $('.check-account').html($('.account-select option:selected').text());
    });
    $('.check-account').html($('.account-select option:selected').text());


/* SECOND STEP */

    $('.rule-frequency select').on('change', function (e) {
        var show = '', hide = '', checkFreq = '';
        checkFreq = $('.rule-frequency option:selected').text();
        if (e.target.value == 0) {
            hide = '.rule-weekday, .rule-monthday, .rule-once';
        } else if (e.target.value == 1) {
            show = '.rule-weekday';
            hide = '.rule-monthday, .rule-once';
            checkFreq += ': ' + $('.rule-weekday option:selected').text();
        } else if (e.target.value == 2) {
            show = '.rule-monthday';
            hide = '.rule-weekday, .rule-once';
            checkFreq += ': ' + $('.rule-monthday input').val() + ' числа';
        } else if (e.target.value == 3) {
            show = '.rule-once';
            hide = '.rule-weekday, .rule-monthday';
        }
        $(show).show();
        $(hide).hide();
        $('.check-freq').html(checkFreq);
    });

    $('.rule-weekday select').on('change', function () {
        var checkFreq = $('.rule-frequency option:selected').text() + ': ' + $('.rule-weekday option:selected').text();
        $('.check-freq').html(checkFreq);
    });

    $('.rule-monthday input').on('change', function () {
        var checkFreq = $('.rule-frequency option:selected').text() + ': ' + $('.rule-monthday input').val() + ' числа';
        $('.check-freq').html(checkFreq);
    });

    var tomorrow = new Date();
    tomorrow.setHours(24);
    $('#onceCal').daterangepicker({
        locale: {
            format: 'DD.MM.YYYY'
        },
        singleDatePicker: true,
        calender_style: "picker_3",
        minDate: (tomorrow.getDate() + '-' + (tomorrow.getMonth() + 1) + '-' + tomorrow.getFullYear())
    }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });

    $('.rule-instant_start input').on('change', function (e) {
        if ($(e.target).prop('checked'))
            $('.rule-once input').prop('disabled', true);
        else
            $('.rule-once input').prop('disabled', false);
    });

    $('.rule-monthday input').on('change', function (e) {
        var value = e.target.value;
        if (value < 1 || value instanceof String)
            e.target.value = 1;
        else if (value > 31)
            e.target.value = 31;
    });


/* THIRD STEP */
/* FOURTH STEP */

    $('.action-select').on('change', function (e) {
        if (e.target.value == 1) {
            $('.rule-position').hide();
            $('.change, .min-bid, .max-bid, .check-tr-min-bid, .check-tr-max-bid').show();
            $('.change-label').html('Процент');
            $('.percent-caption').show();
        } else if (e.target.value == 2) {
            $('.rule-position, .min-bid, .max-bid, .check-tr-min-bid, .check-tr-max-bid').hide();
            $('.change').show();
            $('.change-label').html('Значение ставки');
            $('.percent-caption').hide();
        } else if (e.target.value == 3) {
            $('.rule-position').show();
            $('.change, .min-bid, .max-bid, .check-tr-min-bid, .check-tr-max-bid').show();
            $('.change-label').html('Процент');
            $('.percent-caption').show();
        } else if (e.target.value == 0) {
            $('.change').hide();
            $('.rule-position').hide();
        }
    });

    $('.check-layer').html($('.rule-layer option:selected').text());
    $('.rule-layer select').on('change', function () {
        $('.check-layer').html($('.rule-layer option:selected').text());
    });

    $('.check-action').html(
        $('.rule-action option:selected').text() + ': ' + $('.change input').val());
    $('.rule-action option:selected, .change input').on('change', function () {
        $('.check-action').html(
            $('.rule-action option:selected').text() + ': ' + $('.change input').val());
    });

    $('.rule-layer select').on('change', function (e) {
        if (e.target.value == 0 || e.target.value == 1 || e.target.value == 2) {
            $('.action-select option[value=1], .action-select option[value=2], .action-select option[value=3],' +
                '.change').hide();
            $('.action-select').val(0);
        } else {
            $('.action-select option[value=1], .action-select option[value=2], .action-select option[value=3]').show();
        }
    });

    $('.change input').on('keypress', function (e) {
        if ([45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57].indexOf(e.keyCode) == -1)
            e.preventDefault();
    });

    if (updateMode && $('.action-select').val() == 2 || !updateMode)
        $('.min-bid, .max-bid, .check-tr-min-bid, .check-tr-max-bid').hide();

    $('#minBid').on('change', function (e) {
        $('.check-min-bid').html(e.target.value);
    });
    $('#maxBid').on('change', function (e) {
        console.log(123);
        $('.check-max-bid').html(e.target.value);
    });

    var validated = false;
    var validate = function (validateCondition, errorText) {
        if (validateCondition) {
            if (validated == false) {
                $('.actionBar').append(
                    '<div class="rule-validate rule-validate"><span class="label label-danger">'
                    + errorText
                    + '</span></div>'
                );
            }
            $('.rule-name input').focus();
            validated = true;
            return false;
        } else {
            $('.rule-validate').remove();
            validated = false;
            return true;
        }
    };
    var validateCurrentStep = function (context) {
        if (context.fromStep == 1)
            return validate(
                $.trim($('.rule-name input').val()) == '',
                'Введите название правила')
                && validate(
                    $('.rule-campaigns').val() == '',
                    'Выберите кампании');
        else if (context.fromStep == 2)
            return true;
        else if (context.fromStep == 3) {
            return validate(
                $('#conditions').val() == '' || $('#conditions').val() == '[]',
                'Добавьте условия, при которых будет выполняться правило');
        } else if (context.fromStep == 4) {
            return validate(
                $('.action-select').val() == 1 && $.trim($('#change').val()) == '',
                'Укажите %, на который изменять ставку при выполнении условий.');
        }
        return true;
    };

    $("#wizard").smartWizard({
        labelNext:'Далее', // label for Next button
        labelPrevious:'Назад', // label for Previous button
        labelFinish: updateMode ? 'Обновить' : 'Добавить',
        enableAllSteps: updateMode,
        enableFinishButton: updateMode,
        reverseButtonsOrder: false,
        keyNavigation: false,
        onShowStep: function () {
            $('.stepContainer').removeAttr('style');
        },
        onLeaveStep: function(e, context) {
            console.log(context);
            if (updateMode) {
                if (context.toStep == 5) {
                    $('#wizard .buttonNext').hide();
                } else if (context.fromStep == 5) {
                    $('#wizard .buttonNext').show();
                }
            } else {
                if (context.toStep == 5) {
                    $('#wizard .buttonNext').hide();
                    $('#wizard .buttonFinish').show();
                } else {
                    $('#wizard .buttonNext').show();
                    $('#wizard .buttonFinish').hide();
                }
            }
            return validateCurrentStep(context);
        },
        onFinish: function (e, context) {
            if (!validateCurrentStep(context))
                return false;
            if (!validate(
                    $.trim($('.rule-name input').val()) == '',
                    'Введите название правила')) {
                $("#wizard").smartWizard('goToStep', 1);
                return false;
            }
            if (!validate(
                    $('.all-campaigns').prop('checked') == false && $('.campaigns-select').val() == null,
                    'Выберите кампании или укажите, применять для всех')) {
                $("#wizard").smartWizard('goToStep', 2);
                return false;
            }
            if (!validate(
                    $('#conditions').val() == '',
                    'Добавьте условия, при которых будет выполняться правило')) {
                $("#wizard").smartWizard('goToStep', 3);
                return false;
            }
            if (!validate(
                    $('.action-select').val() == 1 && $.trim($('#change').val()) == '',
                    'Укажите %, на который изменять ставку при выполнении условий.'
                )) {
                $("#wizard").smartWizard('goToStep', 4);
                return false;
            }

            $('.condition-form').submit();
        }
    });

    $("#wizard").css('visibility', 'visible');
    $(".buttonNext").addClass("btn btn-primary");
    $(".buttonPrevious").addClass("btn btn-primary");
    $(".buttonFinish").addClass("btn btn-success");
    if (!updateMode)
        $('.buttonFinish').hide();