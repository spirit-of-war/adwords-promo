<?php
namespace frontend\models;

class Socials
{
	CONST VK = 'vk';
	CONST OK = 'ok';
	CONST FACEBOOK = 'facebook';
	CONST INSTAGRAM = 'instagram';
	CONST GOOGLE = 'google';
	CONST TWITTER = 'twitter';
							
	public function getData() {
		$socialsArr = [
			self::VK => [
				'href' => '',
				'name' => '',
				'src' => '/img/socials/vk.png'
			],
			self::OK => [
				'href' => '',
				'name' => '',
				'src' => '/img/socials/ok.png'
			],
			self::FACEBOOK => [
				'href' => '',
				'name' => '',
				'src' => '/img/socials/facebook.png'
			],
			self::INSTAGRAM => [
				'href' => '',
				'name' => '',
				'src' => '/img/socials/insta.png'			
			],
			self::GOOGLE => [
				'href' => '',
				'name' => '',
				'src' => '/img/socials/google.png'	
			],
			self::TWITTER => [
				'href' => '',
				'name' => '',
				'src' => '/img/socials/twitter.png'
			]
		];
	
		return $socialsArr;
	}
}