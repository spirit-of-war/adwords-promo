<?php
namespace frontend\models;

use common\models\Questionnaires;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $fio;
    public $phone;
    public $site_address;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            [['email', 'fio', 'phone', 'site_address'], 'required'],
            ['email', 'email'],
            [['email', 'fio', 'phone', 'site_address'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->fio = $this->fio;
        $user->phone = $this->phone;
        $user->site_address = $this->site_address;
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Адрес электронной почты',
            'username' => 'Имя пользователя',
            'fio' => 'Имя (ФИО)',
            'phone' => 'Телефон',
            'site_address' => 'Адрес сайта'
        ];
    }
}
