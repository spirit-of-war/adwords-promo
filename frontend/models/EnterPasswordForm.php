<?php
namespace frontend\models;

use common\models\Questionnaires;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class EnterPasswordForm extends Model
{

    public $password_repeat;
    public $password;
    public $user;

    function __construct(User $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'password_repeat'], 'required'],
            [['password', 'password_repeat'], 'string', 'min' => 6, 'max' => 21],
            ['password_repeat', 'compare', 'compareAttribute'=>'password'],
            [['password', 'password_repeat'], 'match', 'pattern' => '/^[a-zA-Z0-9_-]+$/', 'message' => 'пароль должен содержать только латинские символы']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function activateUser()
    {
        if (!$this->validate()) {
            return null;
        }

        $this->user->setPassword($this->password);
        $this->user->status = User::STATUS_ACTIVE;
        return $this->user->save() ? $this->user : null;
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'password_repeat' => 'Подтверждение пароля',
        ];
    }
}
