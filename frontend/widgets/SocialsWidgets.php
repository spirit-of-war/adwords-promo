<?php
namespace frontend\widgets;
use frontend\models\Socials;

class SocialsWidgets
{
	/*
	*  ['blockStyle'] = стиль блока
	*  ['itemStyle'] = стиль сущности
	*/
	public static function init($blockStyle = '', $itemStyle = '') {
		$socials = new Socials();
		$socialData = $socials->getData();

        $rows = '';
		foreach($socialData as $item) {
			$rows .='
				<div class="socials-item" style="'.$itemStyle.'">
					<a href="#">
						<img src="'.$item['src'].'">
					</a>
				</div>';
		}
		$widget = '<div class="socials" style="'.$blockStyle.'">'.$rows.'</div>';
		
		return $widget;
	}
}
