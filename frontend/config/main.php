<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
$domainName = 'profitpanda.ru';
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'baseUrl' => $baseUrl,
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => ".".$domainName,
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'path' => '/',
                'domain' => ".".$domainName,
            ],
        ],
        'session' => [
            'cookieParams' => [
                'path' => '/',
                'domain' => ".".$domainName,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
			'enableStrictParsing' => true,
            'rules' => [
				'/' => '',
				'login' => 'site/login',
				'signup' => 'site/signup',
				'activate-user' => 'site/activate-user',
				'logout' => 'site/logout',
				'call' => 'site/call',
				'connect' => 'site/template',
				'contacts' => 'site/template',
				'agency' => 'site/template',
				'news' => 'site/news',
				'services' => 'site/template',
				'about' => 'site/template',
				'blog'=>  'site/blog',
				'scopes' => 'site/template',
				'news/<alias:[\w_\/-]+>' => 'site/template',
				'blog/<alias:[\w_\/-]+>' => 'site/template',
				'questionnaires/create' => 'questionnaires/create',
//				'questionnaires/test' => 'questionnaires/test',
				//'services' => 'site/template',
				//'actionOrderACall' => 'site/orderACallSave'
            ],
        ],
		'seotools' => [
			'class' => 'jpunanua\seotools\Component',
		],
    ],
    'params' => $params,
];
