<?php
$domainName = 'adwords.dev'; // type this YOUR frontend serverName
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'awL_xXM6kmJgmOFxF_laluSQEPWHtt4s',
            'csrfCookie' => [
                'name' => '_csrf',
                'path' => '/',
                'domain' => '.'.$domainName,
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'path' => '/',
                'domain' => ".".$domainName,
            ],
        ],
        'session' => [
            'cookieParams' => [
                'path' => '/',
                'domain' => ".".$domainName,
            ],
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
