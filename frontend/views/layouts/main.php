<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
//use yii\helpers\CHtml;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Modal;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\SocialsWidgets;

use common\widgets\Alert;

/*$setCanonical = false;
$checkDb = true;
Yii::$app->seotools->setMeta(['title' => \Yii::t('title','A good title for this page')], $setCanonical, $checkDb);
*/
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<link href="img/logo-blue.png" rel="shortcut icon" type="image/x-icon" />
    <?php $this->head() ?>
	
</head>
<body>
<?php $this->beginBody() ?>


<div class="container mobile-main-menu visible-xs">
	<div class="row" style="padding-top:10px;padding-bottom:10px;">
		<div class="col-xs-12 visible-xs">
			<div class="logo-div">
				<a href="/" style="color:#40A5BD;">
					<img class="logo-image" src="/img/logo-blue.png">
					ProfitPanda
				</a>
			</div>
		</div>
	</div>
</div>
<div class="main-menu">
<nav class="navbar navbar-default" role="navigation" style="background:transparent;">
  <div class="container nav-bar-container" style="">
    <div class="navbar-header">
		<div style="float:left;width:80%;height:45px;padding-left:15px;" class="visible-xs">
			<div class="main-menu-d2-d1" style="font-size:24px;">
				8-800-505-03-81
			</div>
		</div>
		<div style="float:left;width:20%;height:45px;" class="visible-xs">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
		</div>
						
      
      <a class="navbar-brand" style="display:none;" href="#">Brand</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	 <!--div class="main-menu"-->
	 <div class="container">
		<div class="row">
				<div class="hidden-xs col-sm-5 col-md-3 col-lg-3">
					<div class="main-menu-d">
						<div class="logo-div" style="">
							<a href="/">
								<img class="logo-image" src="/img/logo.png">
								ProfitPanda
							</a>
						</div>
					</div>
				</div>
				<div class="hidden-xs col-sm-7 col-md-6 col-lg-6">
					<div class="main-menu-d2">
						<div class="main-menu-d2-d1" style="font-size:24px;">8-800-505-03-81</div>
						<div class="main-menu-d2-d2"> / </div>
						<div class="main-menu-d2-d3">
							<a class="order_a_call white" style="cursor:pointer;" data-toggle="modal" style="cursor:pointer;" data-target="#orderACall">Заказать звонок</a>
							
						</div>
					</div>
				</div>
				<div class="hidden-xs hidden-sm col-md-3 col-lg-3">
					<div class="main-menu-d3">
						<?php if(Yii::$app->user->isGuest){?>
						<div class="main-menu-d3-d1">
							<a href="/login" class="white">Войти</a>
						</div>
						<div class="main-menu-d3-d2">
							/ 
						</div>
						<div class="main-menu-d3-d3">
							<a href="/signup" class="white">Регистрация</a>
						</div>
						<?php } else {?>
							<div class="main-menu-d3-d1">
								<ul class="nav navbar-nav navbar-right">
									<li class="">
										<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<?= Yii::$app->user->identity->username ?>
											<span class=" fa fa-angle-down"></span>
										</a>
										<ul class="dropdown-menu dropdown-usermenu pull-right">
											<li>
												<?= Html::a('<i class="fa fa-sign-out pull-right"></i> Личный кабинет',
													Yii::$app->params['domainAdmin'], [
													]) ?>
											</li>
											<li>
												<?= Html::a('<i class="fa fa-sign-out pull-right"></i> Выйти',
													'/logout', [
														'data' => [
															'method' => 'post',
														],
													]) ?>
											</li>
										</ul>
									</li>

								</ul>
							</div>
						<?php }?>
					</div>
			   </div>
		  </div>
	 </div>
	  <div>
		<div class="main-menu-bottom">
			<div class="container">
				<div class="row">
					<div class="main-menu-bottom-div">
						<a href="/about" class="main-menu-bottom-item">
							О системе
						</a>
					</div>
					<div class="main-menu-bottom-div">
						<a href="/connect" class="main-menu-bottom-item">
							Как подключиться
						</a>
					</div>
					<div class="main-menu-bottom-div">
						<a href="/services" class="main-menu-bottom-item">
							Услуги
						</a>
					</div>
					<div class="main-menu-bottom-div">
						<a href="/agency" class="main-menu-bottom-item">
							Агенствам
						</a>
					</div>
					<div class="main-menu-bottom-div">
						<a href="/blog" class="main-menu-bottom-item">
							Блог
						</a>
					</div>
					<div class="main-menu-bottom-div">
						<a href="/contacts" class="main-menu-bottom-item">
							Контакты
						</a>
					</div>
					<div class="main-menu-bottom-div visible-xs" style="border-bottom:1px solid rgba(255,255,255,0.3);margin-top:0px;">
						<div style="float:left;width:50%;padding-top:10px;padding-bottom:10px;">
							<a href="#" class="white" style="border-bottom:1px solid transparent;padding-bottom:0px;">Войти</a>
						</div>
						<div style="float:left;width:50%;border-left:1px solid rgba(255,255,255,0.3);padding-top:10px;padding-bottom:10px;">
							<a href="#" class="white" style="border-bottom:1px solid transparent;padding-bottom:0px;">Регистрация</a>
						</div>
					</div>
				</div>
			</div>
		</div>
      
	  </div>
    </div>
  </div>
</nav>
</div>

<div class="wrap">
    <?php


/*NavBar::begin([
    'brandLabel' => 'profitpanda.ru',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
/*$menuItems = [
    ['label=> 'Home', 'url' => ['/site/index']],
    ['label' => 'About', 'url' => ['/site/about']],
    ['label' => 'Contact', 'url' => ['/site/contact']],
];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Logout (' . Yii::$app->user->identity->username . ')',
            ['class' => 'btn btn-link']
        )
        . Html::endForm()
        . '</li>';
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
]);
    NavBar::end();*/
	
	$controller = Yii::$app->controller;
	$default_controller = Yii::$app->defaultRoute;
	$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;
	
	$breadcrumbs_class = '';
	if(!$isHome) {
		$breadcrumbs_class = 'breadcrumbs';
	}
    ?>

    <div class="container <?php echo  $breadcrumbs_class;?>">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			'homeLink' => ['label' => 'Главная', 'url' => '/'],
        ]) ?>
        <?= Alert::widget() ?>
	</div>
	<?php
		print $content;
	?>
</div>
<div>

<div class="footer-menu">
	<div class="container">
		<div class="row">			
            <div class="footer-menu-item">
				<a href="/about" class="footer-menu-item">
					О системе
				</a>
			</div>
            <div class="footer-menu-item">
				<a href="/connect" class="footer-menu-item">
					Как подключиться
				</a>
			</div>
            <div class="footer-menu-item">
				<a href="/services" class="footer-menu-item">
					Услуги
				</a>
			</div>
            <div class="footer-menu-item">
				<a href="/agency" class="footer-menu-item">
					Агенствам
				</a>
			</div>
            <div class="footer-menu-item">
				<a href="/blog" class="footer-menu-item">
					Блог
				</a>
			</div>
            <div class="footer-menu-item">
				<a href="/contacts" class="footer-menu-item">
					Контакты
				</a>
			</div>
        </div>
	</div>
</div>

<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 footer-col1">
						<div class="logo-div">
							<a href="/" style="color:#40A5BD;">
								<img class="logo-image" src="/img/logo-blue.png">
								ProfitPanda
							</a>
						</div>
						<div style="font-family:Roboto-Light;font-size:18px;color:#333;padding-top:10px;">Система сквозной бизнес-аналитики и увеличения прибыли</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 footer-col2" style="">
				<div style="font-family:Roboto-Light;font-size:36px;color:#333;">8-800-505-03-81</div>
				<div>
					<a class="order_a_call" data-toggle="modal" style="cursor:pointer;color: #bf5c30;text-decoration: underline;" data-target="#orderACall">
						Заказать обратный звонок
					</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 footer-col3" style="">
				<div class="footer-col3-div" style="">
					<a href="mailto:support@profitpanda.com" target="_top" style="color: #bf5c30;text-decoration: underline;">
						support@profitpanda.com
					</a>
				</div>
				<div class="footer-col3-inner" style="">
					<?php 
						echo SocialsWidgets::init(
							'max-width:220px;',
							'float:left;padding-bottom:20px;padding-left:15px;padding-right:15px;'
						);
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div style="display:table;width:100%;text-align:center;">
					<div style="display:table-cell;vertical-align:bottom;">
						<img src="/img/footerpanda.png" style="max-width:100%;width:347px;">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
echo $this->render('../site/call');
?>

<?php $this->endBody() ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter39756495 = new Ya.Metrika({
                    id:39756495,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/39756495" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84604907-2', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
<?php $this->endPage() ?>
