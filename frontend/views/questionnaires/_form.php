<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Questionnaires */
/* @var $form yii\widgets\ActiveForm */
?>


        <div class="company-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'site_address')->textInput(['maxlength' => true, 'placeholder' => 'Пример: http://yoursite.ru'])->label('Веб-сайт') ?>

            <?= $form->field($model, 'advertising_district')->textInput(['maxlength' => true, 'placeholder' => 'Пример: Новосибирск'])->label('Регион рекламы') ?>

            <?= $form->field($model, 'advertising_systems')->checkboxList([
                'Google' => 'Google',
                'Yandex' => 'Yandex',
                'мобильная реклама' => 'мобильная реклама',
                'Youtube' => 'Youtube'], ['style' =>
                'padding-top:17px;', 'class' => 'questionnaires-checkbox-list'])->label('Типы рекламы') ?>


            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value'=>'create_add', 'name'=>'submit']) ?>
                <?= Html::submitButton('Пропустить', ['class' => 'btn btn-warning', 'value'=>'skip', 'name'=>'submit' ]) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

