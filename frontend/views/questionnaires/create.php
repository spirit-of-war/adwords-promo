<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Questionnaires */

$this->title = 'Анкета';

?>
<div class="company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="x_panel">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
    </div>
</div>
