<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>



    <div class="row">
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'fio')->textInput(['autofocus' => true])->label('Имя(ФИО)') ?>

            <?= $form->field($model, 'phone')->label('Мобильный телефон') ?>

            <?= $form->field($model, 'site_address')->label('URL веб-сайта') ?>

            <?= $form->field($model, 'email')->label('E-mail') ?>

                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'form-button brown-button', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
    </div>
</div>
