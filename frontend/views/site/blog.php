<?php
use yii\widgets\LinkPager;
use yii\helpers\Html;

//$this->title = 'Новости';
$this->params['breadcrumbs'][] = $page->h1;

$setCanonical = false;
$checkDb = true;
Yii::$app->seotools->setMeta([
        'title' => $page->title,
        'keywords' => $page->keywords,
        'description' => $page->description
    ]
    , $setCanonical, $checkDb);

?>
<div class="container">
    <div class="row">
        <div class="hidden-xs hidden-sm col-md-2 col-lg-2">
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <h1><?= Html::encode($page->h1) ?></h1>
            <?php
            if($count > 0) { ?>
            <div class="row" style="padding-top:15px;">
                <div class="blog-item">
                    <?php
                    foreach($models as $model):
                    if($model->icon) {
                        $paddingLeftClass = 'news-block-2';
                        $newsClasses = 'col-xs-12 col-sm-9 col-md-9 col-lg-9';
                        ?>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <img src="img/news/<?= $model->icon ?>" alt="<?= $model->h1?>" title="<?= $model->h1?>" style="max-width:200px;width:100%;">
                        </div>
                    <?php } else {
                        $newsClasses = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
                        $paddingLeftClass = '';
                     } ?>
                    <div class="<?= $newsClasses?>">
                        <div class="<?=$paddingLeftClass?>" style="clear:both;">
                            <div style="clear:both;position:relative;">
                                <h2 class="black news-h2" style="text-align:left;margin-top:0;"><a class="black" href="/<?=$model->url?>" style="color:#000;text-decoration:none;"><?=$model->h1?></a></h2>
                                <span class="news-date"><?=date('d.m.Y',$model->created)?></span>
                            </div>
                            <div class="text-black" style="clear:both;">
                                <?=$model->announcement?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
            <div class='row' style='text-align:center;'>
                <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                    <?= LinkPager::widget(['pagination' => $pages])?>
                </div>
            </div>
            <?php
            } else {
                echo Html::encode('На данный момент нет новостей');
            }
            ?>
        </div>
        <div class="hidden-xs hidden-sm col-md-2 col-lg-2">
        </div>
    </div>
</div>
