<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\OrderForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$setCanonical = false;
$checkDb = true;
Yii::$app->seotools->setMeta([
		'title' => $page->title,
		'keywords' => $page->keywords,
		'description' => $page->description
	]
, $setCanonical, $checkDb);

//$this->title = 'Оставить заявку';
$this->params['breadcrumbs'][] = $page->h1;
?>



			<h1><?= Html::encode($page->h1) ?></h1>
			<div style="max-width:320px;width:100%;margin: 0 auto;">

			<?php $form = ActiveForm::begin(['id' => 'order-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Имя(ФИО)') ?>

                <?= $form->field($model, 'phone')->label('Мобильный телефон') ?>

                <?= $form->field($model, 'site')->label('URL веб-сайта') ?>

                <?= $form->field($model, 'email')->label('E-mail') ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'form-button brown-button', 'name' => 'order-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
			</div>
