<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Установите пароль';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>



    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'form-enter-password']); ?>

            <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>
            <?= $form->field($model, 'password_repeat')->passwordInput()->label('Подтверждение пароля') ?>

            <div class="form-group">
                <?= Html::submitButton('Подтвердить регистрацию', ['class' => 'form-button brown-button', 'name' => 'enter-password-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
    </div>
</div>