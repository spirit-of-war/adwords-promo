<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$message = 'Страница в разработке';
//$this->title = $name;
$this->title = 'Страница в разработке';
?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="site-error">
				<h1><?= Html::encode($this->title) ?></h1>
				<div class="alert alert-danger">
					<?= nl2br(Html::encode($message)) ?>
				</div>
			</div>
		</div>
	</div>
</div>

