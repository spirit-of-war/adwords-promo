<?php

/* @var $this yii\web\View */
//use frontend\models\Socials;

$this->registerJsFile(Yii::getAlias('@web/js/index.js'), ['depends' => [
	'yii\web\JqueryAsset',
]]); 
	
$setCanonical = false;
$checkDb = true;
Yii::$app->seotools->setMeta([
		'title' => $page->title,
		'keywords' => $page->keywords,
		'description' => $page->description
	]
, $setCanonical, $checkDb);
?>
<div class="container-fluid main-slider">
    <?php
    echo yii\bootstrap\Carousel::widget ([
        'items' => [
            [
				'content' => '
							<div class="main-slider-background" style="background:url(\'/img/Sad1.png\');background-size:cover;">
								<div class="container" style="text-align:center;height:100%;padding-left:0px!important;padding-right:0px!important;">
									<div class="row" style="height:100%;width:100%;">
										<div class="hidden-xs col-sm-4 col-md-5 col-lg-6">
										</div>
										<div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 main-slider-div" style="">
											<div class="main-slider-div-div" style="">
												<div class="main-slider-div-div-div" style="">
													<h1 class="h1-main-slider" style="margin-bottom:0px;">'.$page->h1.'</h1>
													<p class="text-main-slider" style="">Достигаем KPI. Сквозная аналитика.<br/>Максимизируем прибыль</p>
													<div>
														<a href="/signup" class="brown-button">
															Попробовать
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							',
                'caption' => '
							
							',
                'options' => []
            ],
           [
                'content' => '<div class="main-slider-background" style="background:url(\'/img/Sad2.png\');background-size:cover;">
								<div class="container" style="text-align:center;height:100%;padding-left:0px!important;padding-right:0px!important;">
									<div class="row" style="height:100%;width:100%;">
										<div class="hidden-xs col-sm-4 col-md-5 col-lg-6">
										</div>
										<div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 main-slider-div" style="">
											<div class="main-slider-div-div" style="">
												<div class="main-slider-div-div-div" style="">
													<h1 class="h1-main-slider" style="margin-bottom:0px;">'.$page->h1.'</h1>
													<p class="text-main-slider" style="">Достигаем KPI. Сквозная аналитика.<br/>Максимизируем прибыль</p>
													<div>
														<a href="/signup" class="brown-button">
															Попробовать
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>',
                'caption' => '',
                'options' => []
            ],
			[
                'content' => '<div class="main-slider-background" style="background:url(\'/img/Sad3.png\');background-size:cover;">
								<div class="container" style="text-align:center;height:100%;padding-left:0px!important;padding-right:0px!important;">
									<div class="row" style="height:100%;width:100%;">
										<div class="hidden-xs col-sm-4 col-md-5 col-lg-6">
										</div>
										<div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 main-slider-div" style="">
											<div class="main-slider-div-div" style="">
												<div class="main-slider-div-div-div" style="">
													<h1 class="h1-main-slider" style="margin-bottom:0px;">'.$page->h1.'</h1>
													<p class="text-main-slider" style="">Достигаем KPI. Сквозная аналитика.<br/>Максимизируем прибыль</p>
													<div>
														<a href="/signup" class="brown-button">
															Попробовать
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>',
                'caption' => '',
                'options' => []
            ],
			[
                'content' => '<div class="main-slider-background" style="background:url(\'/img/Sad4.png\');background-size:cover;">
								<div class="container" style="text-align:center;height:100%;padding-left:0px!important;padding-right:0px!important;">
									<div class="row" style="height:100%;width:100%;">
										<div class="hidden-xs col-sm-4 col-md-5 col-lg-6">
										</div>
										<div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 main-slider-div" style="">
											<div class="main-slider-div-div" style="">
												<div class="main-slider-div-div-div" style="">
													<h1 class="h1-main-slider" style="margin-bottom:0px;">'.$page->h1.'</h1>
													<p class="text-main-slider" style="">Достигаем KPI. Сквозная аналитика.<br/>Максимизируем прибыль</p>
													<div>
														<a href="/signup" class="brown-button">
															Попробовать
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>',
                'caption' => '',
                'options' => []
            ],
        ],
        'options' => [
            'style' => 'width:100%;',
			'class' => 'carousel-fade'
		],
		'id' => 'main-slider'
    ]);
    ?>
</div>
<div class="scopes">
	<div class="container">
		<div class="row scopes-divs">
			<div class="h1-white">Возможности сервиса</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 scopes-div1">
				<div class="scopes-div-header">
					<img src="/img/advantage1.png">
					<h2 class="white">Полная<br/>автоматизация</h2>
				</div>
				<div>
					<ol class="white">
						<li>Автоматическое управление ставками для достижения KPI</li>
						<li>4 базовые стратегии: повышение прибыли при заданном CPA, удержание позиции, оптимизация по трафику или расходам</li>
						<li>Корректировка ставок под сезонные изменения</li>
						<li>Создание своих правил</li>
						<li>Лимиты. Автоматическое напоминание и отключение кампаний, групп, слов во избежание неэффективного расхода бюджета</li>
						<li>Проверка и отключение объявлений с битыми URL</li>
					</ol>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 scopes-div2">
				<div class="scopes-div-header">
					<img src="/img/advantage2.png">
					<h2 class="white">Сквозная<br/>аналитика</h2>
				</div>
				<div>
					<ol class="white">
						<li>Данные из CRM, Google Analytics и Яндекс Директ</li>
						<li>Возможность видеть ассоциированные конверсии из Google Analytics</li>
						<li>Создание регулярных отчетов и отправка их по почте</li>
					</ol>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 scopes-div3">
				<div class="scopes-div-header">
					<img src="/img/advantage3.png">
					<h2 class="white">Полный<br/>контроль</h2>
				</div>
				<div>
					<ol class="white">
						<li>Возможность видеть реальный результат по каждому ключевому слову, объявлению, группе, кампании</li>
						<li>Возможность остановки и запуска элементов в интерфейсе (кампании, группы, объявления, ключи)</li>
						<li>Корректировка ставок в сервисе (не нужно больше переключаться между Adwords и Директ)</li>
						<li>Удобные фильтры и возможность сортировки для анализа выбранной группы запросов</li>
					</ol>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:center;padding-bottom:50px;">
				<a href="/scopes" class="white-button">
					Узнать больше
				</a>
			</div>
		</div>
	</div>
</div>

<div class="container how-it-works" style="">
    <div class="row">
        <div class="h1-black" style="padding-bottom:20px;">Как работает система</div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 how-it-works-col1" style="">
            <div class="h2-black" style="text-align:left;">1. Собираем данные</div>
            <div class="text-black">Точные данные со всех источников трафика, включая итоговый результат из вашей CRM.
                Больше не придется переключаться между Метрикой и Директом, Adwords и Analytics и вашей CRM, чтобы понять, что происходит в
                аккаунте.</div>
            <div class="h2-black" style="text-align:left;">2. Анализируем</div>
            <div class="text-black">Анализ рекламы по прибыли в разрезе слов, объявлений, групп, кампаний. Получать на почту регулярные, кастомизированные отчеты.
                Фильтровать прибыльные ключевые слова с низкой позиции, самые затратные кампании без конверсий. Ставку можно подправить на мест.</div>
            <div class="h2-black" style="text-align:left;">3. Управляем рекламой</div>
            <div class="text-black">Автоматические правила для управления ставками на всех уровнях (ключевые слова, группы, гео, аудитории, уйстройства). Возможность
                самим настроить правила, а также готовые стратегии.</div>
        </div>
        <div class="hidden-xs hidden-sm col-md-8 col-lg-8">
            <img src="/img/how-it-works.png" alt="Как это работает?" title="Как это работает?" style="width:100%;max-width:709px;">
        </div>
    </div>
</div>

<div class="advantages">
	<div class="container">
		<div class="row">
			<div class="h1-white">Преимущества</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-bottom:50px;">
				<div class="row">
					
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
							<div class="advantages-table">
								<img src="/img/advantages-1.png">
								<h3 class="white advantages-table-cell">Прибыль в разрезе ключевиков</h3>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
							<div class="advantages-table">
								<img src="/img/advantages-6.png">
								<h3 class="white advantages-table-cell">Экономия до 80% времени работы с контекстом</h3>
							</div>
						</div>
					
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
						<div class="advantages-table">
							<img src="/img/advantages-2.png">
							<h3 class="white advantages-table-cell">Исключение слива бюджета</h3>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
						<div class="advantages-table">
							<img src="/img/advantages-7.png">
							<h3 class="white advantages-table-cell">Удобная аналитика в одном окне</h3>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
						<div class="advantages-table">
							<img src="/img/advantages-3.png">
							<h3 class="white advantages-table-cell">Достижение KPI</h3>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
						<div class="advantages-table">
							<img src="/img/advantages-8.png">
							<h3 class="white advantages-table-cell">Настраиваемые отчеты любой сложности</h3>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
						<div class="advantages-table advantages-item-1" style="">
							<img src="/img/advantages-4.png">
							<h3 class="white advantages-table-cell">Точная сквозная аналитика<br/>Учет ассоциированных конверсий</h3>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
						<div class="advantages-table">
							<img src="/img/advantages-9.png">
							<h3 class="white advantages-table-cell">Автоматизация ставок на всех уровнях - сезонные, гео, устройства, аудитории</h3>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
						<div class="advantages-table advantages-item-2" style="">
							<img src="/img/advantages-5.png">
							<h3 class="white advantages-table-cell">Проверка битых ссылок</h3>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 advantages-item">
						<div class="advantages-table">
							<img src="/img/advantages-10.png">
							<h3 class="white advantages-table-cell">Настраиваемые автоматические правила и уже готовые стратегии</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="cases">
	<div class="container">
		<?php
		$cases = yii\bootstrap\Carousel::widget ([
			'items' => [
				[
					'content' => '<div style="text-align:center;">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div style="clear:both;">
											<div style="float:left;">
												<img src="/img/panda.jpg" class="cases-avatar">
											</div>
											<div class="cases-header-text" style="">
												<div class="cases-header-text-1">Королев Андрей</div>
												<div class="cases-header-text-2">Директор, ЦФТ</div>
											</div>
										</div>
										<div class="text-white cases-text">
											Когда от милой там долины поднимается пар и стоит чаща, то я иду гулять под зведным небом, потому что только так можно купить мороженое и посмотреть на небо на звезды. Когда от милой там долины поднимается пар и стоит чаща, то я иду гулять под зведным небом, потому что
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div>
											<img style="width:100%;" src="img/chart.png">
										</div>
									</div>
								</div>
							</div>',
					'caption' => '',
					'options' => []
				],
				[
					'content' => '<div style="text-align:center;">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div style="clear:both;">
											<div style="float:left;">
												<img src="/img/panda.jpg" class="cases-avatar">
											</div>
											<div class="cases-header-text" style="">
												<div class="cases-header-text-1">Королев Андрей</div>
												<div class="cases-header-text-2">Директор, ЦФТ</div>
											</div>
										</div>
										<div class="text-white cases-text">
											Пати мекка улчиный пати мейкер дискотека века Пати мекка улчиный пати мейкер дискотека века Пати мекка улчиный пати мейкер дискотека века
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div>
											<img style="width:100%;" src="img/chart.png">
										</div>
									</div>
								</div>
							</div>',
					'caption' => '',
					'options' => []
				],
				[
					'content' => '<div style="text-align:center;">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div style="clear:both;">
											<div style="float:left;">
												<img src="/img/panda.jpg" class="cases-avatar">
											</div>
											<div class="cases-header-text" style="">
												<div class="cases-header-text-1">Королев Андрей</div>
												<div class="cases-header-text-2">Директор, ЦФТ</div>
											</div>
										</div>
										<div class="text-white cases-text">								
											Когда от милой там долины поднимается пар и стоит чаща, то я иду гулять под зведным небом, потому что только так можно купить мороженое и посмотреть на небо на звезды. Когда от милой там долины поднимается пар и стоит чаща, то я иду гулять под зведным небом, потому что
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div>
											<img style="width:100%;" src="img/chart.png">
										</div>
									</div>
								</div>
							</div>',
					'caption' => '',
					'options' => []
				],
			],
			'options' => [
				'style' => 'width:100%;' // Задаем ширину контейнера
			],
			'id' => 'cases-slider'
		]);
		?>
		
		<div class="row">
			<div class="h1-white">Кейсы</div>
		</div>
		<div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php echo $cases; ?>
			</div>
		</div>
	</div>
</div>

<div class="container tariffs">
    <div class="row">
        <div class="h1-black">Тарифы</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div style="text-align:center;">
				<div class="row" style="max-width: 960px;width:100%;display: inline-block;padding-top:40px;">		
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tariffs-col1" style="">
						<div class="h2-black" style="">Попробуй бесплатно</div>
						<div class="text-black" style="text-align:left;">Мы предоставляем весь функционал бесплатно до 1 февраля 2017 года. Затем будет линейка тарифов.</div>
						<div class="tariffs-col1-button" style=""><a href="/signup" class="brown-button">Попробовать</a></div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tariffs-col2" style="">
						<div class="h2-black" style="">Понравился сервис?</div>
						<div class="text-black" style="text-align:left;">Поделись с коллегами и друзьями, чтобы они тоже смогли сделать свою контекстную рекламу эффективнее.</div>
						<?php
							echo $socialWidget;
						?>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<div class="additional_services">
	<div class="container">
		<div class="h1-white">Услуги по контекстной рекламе</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="additional-services-item">
				<div class="additional-services-item-header"><h2 class="white">Настройка<br/>от 10 000 руб.</h2></div>
				<div class="additional-services-item-body"><p class="text-white">Профессиональная настройка рекламных кампаний: большие, информативные объявления под ваш запрос, подбор типов соответствия ключевых слов и эффективная структура аккаунта, настройка сквозной аналитики.</p></div>
				<div class="additional-services-item-button"><a href="/services" class="brown-button">Подробнее</a></div>
			</div>
		</div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="additional-services-item">
				<div class="additional-services-item-header"><h2 class="white">Ведение<br/>от 15 000 руб.</h2></div>
				<div class="additional-services-item-body"><p class="text-white">Управление вашими рекламными кампаний для достижения KPI: корректировка ключевых слов, работа с минус словами и реальными запросами, запуск новых кампаний и функционала.</p></div>
				<div class="additional-services-item-button"><a href="/services" class="brown-button">Подробнее</a></div>
			</div>
		</div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="additional-services-item">
				<div class="additional-services-item-header"><h2 class="white">Аудит<br/>от 5 000 руб.</h2></div>
				<div class="additional-services-item-body"><p class="text-white">Предоставляем качественный аудит вашей рекламы.</p></div>
				<div class="additional-services-item-button"><a href="/services" class="brown-button">Подробнее</a></div>
			</div>
		</div>
	</div>
</div>
<?php
if(count($news) > 0)
{
?>
<div class="container news">
    <div class="hidden-xs col-sm-2 col-md-2 col-lg-2">
    </div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<div class="row">
			<div class="h1-black">Новости</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php
				$newsForm = '';
				foreach($news as $new) {

					$newsForm .='<div style="clear:both;">
									<div style="clear:both;position:relative;">
										
											<h2 class="black news-h2" style="text-align:left;"><a class="black" href="/'.$new['url'].'" style="color:#000;text-decoration:none;">'.$new['h1'].'</a></h2>
										
											<span class="news-date">'.date('d.m.Y',$new['created']).'</span>
										
									</div>
									<div class="text-black" style="clear:both;">
										'.$new['announcement'].'
									</div>
								</div>';
				}
				echo $newsForm;
				?>
			</div>
		</div>
		<div class="row" style="text-align:center;">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<a href="/news" class="brown-button">Все новости</a>
			</div>
		</div>
    </div>
    <div class="hidden-xs col-sm-2 col-md-2 col-lg-2">
    </div>
</div>
<?php
}
?>