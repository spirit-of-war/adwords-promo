<?php
use yii\helpers\Html;
//$this->title = $page->h1;

$counter = 1;
foreach($breadcrumbs as $item) {
	if($counter == count($breadcrumbs)) {
		$this->params['breadcrumbs'][] = ['label' => $item['label']];			
	} else {
		$this->params['breadcrumbs'][] = ['label' => $item['label'], 'url' => [$item['url']]];		
	}
	$counter++;
}

if($page->partial_name !== null) {
	$paramsForPartial = [];
	$paramsForPartia['model'] = (isset($model)? $model : '');
}

$this->registerJs("
$('.template-container .template').find('h2').each(function(){ $(this).attr('id',$(this).text()); $('.left-menu ul').append('<li><a href=\"#'+$(this).text()+'\">'+$(this).text()+'</a></li>')})
");
$setCanonical = false;
$checkDb = true;
Yii::$app->seotools->setMeta([
		'title' => $page->title,
		'keywords' => $page->keywords,
		'description' => $page->description
	]
, $setCanonical, $checkDb);

?>
<div class="container template-container" style="padding-bottom:60px;">
	<div class="row">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
			<div class="left-menu">
				<ul>
				</ul>
			</div>
<!--			<div class="banner">-->
<!---->
<!--			</div>-->
<!--			<div class="banner">-->
<!---->
<!--			</div>-->
		</div>
		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
			<div class="template">
				<h1><?= Html::encode($page->h1) ?></h1>
				<?= $page->body; ?>

				<?php if($page->partial_name !== null){
					echo $this->render('_'.$page->partial_name,[ 'params' => $paramsForPartial]);
				} ?>

			</div>
		</div>
		<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
<!--			<div class="banner">-->
<!---->
<!--			</div>-->
		</div>
	</div>
</div>