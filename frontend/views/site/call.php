<?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use common\models\admin\Order;

	$this->registerJsFile(Yii::getAlias('@web/js/call.js'), ['depends' => [
		'yii\web\JqueryAsset',
	]]); 
	

	$orderForm = new Order();

    yii\bootstrap\Modal::begin([
        'header' => '<div style="font-family:Exo2-Light;font-size:24px;color:#000;text-align:center;">Заказ обратного звонка</div>',
        'id' => 'orderACall',
        'size' => 'modal-size',
    ]);
	
	
?>
 <div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="font-family:Roboto-Light;font-size:18px;color:#000;text-align:justify;">
		Оставьте свой сотовый и мы свяжемся с вами в течении 15 минут.
	</div>
	<?php
		$form = ActiveForm::begin([
			'id' => 'orderACallForm',
			//'action' => '/call'
			//'enableAjaxValidation' => true,
			//'enableClientScript' => true,
			//'enableClientValidation' => true
			//'validationUrl' => '/call',
			//'action' => ''
		]);
	?>
	

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top:30px;padding-bottom:15px;text-align:center;">	
		<div style="max-width:300px;width:100%;display:inline-block;">
			<?=$form->field($orderForm, 'phone')->label('Мобильный телефон')->textInput(['placeholder' => '+7-913-948-25-09']);?>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 call-info">
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align:center;padding-bottom:20px;">
		<div style="max-width:220px;width:100%;display:inline-block;">
				<?=Html::submitButton('Ок', ['class' => 'form-button brown-button', 'name' => 'orderACallFormButton']);?>
		</div>
		
	</div>
	<?php
		ActiveForm::end();
	?>
</div>
<?php yii\bootstrap\Modal::end(); ?>
