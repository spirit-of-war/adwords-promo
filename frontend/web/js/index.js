jQuery.noConflict();
$ = jQuery;
(function($){
	preCacheImages();
	
	auto = true;
	$('.video .menu-item').on('click touch', function(){
		auto = false;
		console.log('head');
		index = parseInt($(this).attr('aim'))
		$('#video-slider').carousel(index);
		
		setVideoSliderLabel(index);
	});
	
	setVideoSliderLabel = function(index){
		$.each($('.video .menu-item'), function(i,e){
			
			if(index == i) {
				$($(this)).removeClass('h2-brown').addClass('h2-brown-active');
			} else {
				$($(this)).removeClass('h2-brown-active').addClass('h2-brown');
			}
		});
	};
	
	$('#video-slider').on('slide.bs.carousel', function (e) {
		if(auto == true) {
			active = $('#video-slider div.active').index();
			total = $('#video-slider div.item').length-1;
			direction = e.direction;
			if(direction == 'left') {
				if(active == total) {
				index = 0;
				} else {
					index = active + 1;
				}
			} else {
				if(active == 0) {
					index = total;
				} else {
					index = active - 1;
				}
			}
			
			setVideoSliderLabel(index);
		}
		auto = true;
	});
	
})(jQuery);

function preCacheImages(){
	$.each($('.main-slider-background'), function(){
        var img = new Image();
		var bg = $(this).css('background-image');
        bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
		img.src =bg;
	});
};
