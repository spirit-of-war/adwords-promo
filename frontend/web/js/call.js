jQuery.noConflict();
$ = jQuery;
(function($){
	$('.order_a_call').on('click', function(){
		$('.call-info').html('');
	});
	
	$('#orderACall').on('beforeSubmit', function(e){
		data = new Object();
		data.phone = $('#order-phone').val();  
		if(data.phone!='') {
			$('.call-info').html('');
			$.ajax({
				url: '/call',
				method: "post",
				data: data,
				dataType: 'json',
				success: function(result){
					$('.call-info').html('<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>'+result.message+'</div>');
					$('#order-phone').val('');
				}
			});
		}
		return false;
	});
})(jQuery);