<?php
use yii\helpers\Html;
use common\models\admin\Order;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$site = Yii::$app->urlManager->createAbsoluteUrl(['/']);
$title = 'Звонок';
$body = '<p>Номер телефона: ' . $model->phone . '</p>';

if(isset($model->type) && $model->type == Order::TYPE_CONTACT){
    $title = 'Обратная связь';
    $body = 'Имя: ' . $model->name .'</br>'.
        'e-mail:' .$model->email.'</br>'.
        'Тема:' .$model->subject.'</br>'.
        'Текст:' .$model->body.'</br>';
}
?>
<div class="password-reset">
    <p>Добрый день!</p>

    <p>На сайте <?= $site ?> была оставлена заявка. Раздел: <?= $title?></p>
    <p><?= $body?></p>
</div>
