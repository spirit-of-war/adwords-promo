<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$site = Yii::$app->urlManager->createAbsoluteUrl(['/']);
?>
<div class="password-reset">
    <p>Добрый день!</p>

    <p>На сайте <?= $site ?> был зарегистрирован новый пользователь</p>
    <p>
        Имя пользователя: <?= $model['username'];?><br>
        ФИО: <?= $model['fio'];?><br>
        Телефон: <?= $model['phone'];?><br>
        Сайт: <?= $model['site_address'];?><br>
        e-mail: <?= $model['email'];?>
    </p>
</div>
