<?php
namespace frontend\controllers;

use common\models\User;
use frontend\models\EnterPasswordForm;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\swiftmailer\Mailer;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\widgets\SocialsWidgets;
//use frontend\models\OrderForm;
use common\models\admin\Order;
use common\models\admin\Page;
use yii\data\Pagination;

use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $socials      = new SocialsWidgets();
        $socialWidget = $socials->init();

        $news = (new \yii\db\Query())
            ->select(['id', 'h1', 'announcement', 'created', 'url'])
            ->from('page')
            ->where(['type' => Page::TYPE_NEWS, 'active' => Page::PUBLISH])
            ->limit(2)
            ->all();

        $page = Page::findOne(['id' => 1]);

        return $this->render('index', [
            'page'         => $page,
            'socialWidget' => $socialWidget,
            'news'         => $news
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionCall()
    {
        $phone = $_REQUEST['phone'];
        if ($phone) {
            $model             = new Order();
            $model->name       = ' ';
            $model->phone      = $phone;
            $model->type       = Order::TYPE_CALL;
            $model->created_at = time();
            $result            = [];
            if($model->save() && $model->sendEmail($model)){

                $result['message'] = 'Спасибо! В ближайшее время мы с вами свяжемся!';
                $result['status']  = 1;

            } else {
                $result['message'] = 'Ошибка отправки';
                $result['status']  = 0;
            }
            echo json_encode($result);
        }
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
//    public function actionOrder()
//    {
//        $session = Yii::$app->session;
//        $session->open();
//
//        if ($session->get('_csrf') != $_REQUEST['_csrf'] && $_REQUEST['Order']) {
//            $session->set('_csrf', $_REQUEST['_csrf']);
//
//            $model     = new Order();
//            $orderForm = $_REQUEST['Order'];
//
//            $model->name       = $orderForm['name'];
//            $model->phone      = $orderForm['phone'];
//            $model->site       = $orderForm['site'];
//            $model->email      = $orderForm['email'];
//            $model->created_at = time();
//            $model->type       = Order::TYPE_ORDER;
//
//            $result = $model->save();
//            $model->sendEmail($model);
//            Yii::$app->session->setFlash('success', "Ваша заявка успешно отправлена!");
//        }
//
//        $session->close();
//
//        return $this->render('order', [
//            'model' => new Order(),
//            'page'  => Page::findOne(['id' => 8])
//        ]);
//    }

    public function actionContact()
    {
        $model = new ContactForm();

        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
//        $user = User::findOne(18);
//        var_dump($user->questionnaires->site_address);
//        $this->sendAdminNotifr($user);
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                $transport = Yii::$app->mailer->transport;
                $mailer    = new Mailer();
                $mailer->setTransport($transport);

                $mailer
                    ->compose('registrationConfirmLink', [
                        'user' => $user,
                    ])
                    ->setFrom($transport->getUsername())
                    ->setTo($user->email)
                    ->setSubject('Подтверждение регистрации на PROFITPANDA.RU')
                    ->send();

                return $this->render('confirmSent');
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionActivateUser()
    {
        if ($key = Yii::$app->request->get('key')) {
            if ($user = User::findByAuthKey($key)) {
                if($user->status != User::STATUS_ACTIVE){
                    $model = new EnterPasswordForm($user);

                    if($model->load(Yii::$app->request->post()) && $model->activateUser()){
                        $this->sendAdminNotifr($user);
                        Yii::$app->user->login($user);
                        return $this->render('activateUser');
                    }
                    return $this->render('enterPassword', ['model' => $model]);
                }

            }
        }
        return $this->render('error');
    }

    private function sendAdminNotifr(User $model)
    {
        $field = [];
        $field['username'] = $model->username;
        $field['email'] = $model->email;
        $field['fio'] = $model->questionnaires->fio;
        $field['phone'] = $model->questionnaires->phone;
        $field['site_address'] = $model->questionnaires->site_address;

        $transport = Yii::$app->mailer->transport;
        $mailer    = new Mailer();
        $mailer->setTransport($transport);

        $mailer
            ->compose('registerNotification', [
                'model' => $field,
            ])
            ->setFrom($transport->getUsername())
            ->setTo(Yii::$app->params['notificationEmail'])
            ->setSubject('Зарегистрирован новый пользователь')
            ->send();
    }
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionNews()
    {
        $news       = Page::find()->where(['type' => Page::TYPE_NEWS, 'active' => 1])->orderBy('id DESC');
        $countQuery = clone $news;

        $page = Page::findOne(['id' => 10]);

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

        $pages->pageSizeParam = false;
        $models               = $news->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('news', [
            'page'   => $page,
            'models' => $models,
            'pages'  => $pages,
            'count'  => $countQuery->count()
        ]);
    }

    public function actionBlog()
    {
        $posts      = Page::find()->where(['type' => Page::TYPE_BLOG, 'active' => 1])->orderBy('id DESC');
        $countQuery = clone $posts;

        $page = Page::findOne(['id' => 6]);

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

        $pages->pageSizeParam = false;
        $models               = $posts->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('blog', [
            'page'   => $page,
            'models' => $models,
            'pages'  => $pages,
            'count'  => $countQuery->count()
        ]);
    }

//    public function actionContacts()
//    {
//        $url  = substr(Url::to('@web'), 1);
//        $page = Page::findOne(['url' => $url]);
//        $breadcrumbs[1]['label'] = $page['h1'];
//        $breadcrumbs[1]['url']   = $page['url'];
//
//        return $this->render('contacts',['breadcrumbs' => $breadcrumbs]);
//    }

    public function actionTemplate()
    {
        $url  = substr(Url::to('@web'), 1);
        $page = Page::findOne(['url' => $url]);


        if ($page->active == Page::PUBLISH) {
            if ($page->type == Page::TYPE_NEWS) {
                $breadcrumbs[0]['label'] = 'Новости';
                $breadcrumbs[0]['url']   = 'news';
            }
            if ($page->type == Page::TYPE_BLOG) {
                $breadcrumbs[0]['label'] = 'Блог';
                $breadcrumbs[0]['url']   = 'blog';
            }
            $breadcrumbs[1]['label'] = $page['h1'];
            $breadcrumbs[1]['url']   = $page['url'];

            if(Yii::$app->request->post('contact-button') == 'create_order'){
                $model = new ContactForm();
                $model->load(Yii::$app->request->post());
                if($model->saveOrder()){
                    Yii::$app->session->setFlash('success', 'Успешно отправлено');
                } else {
                    Yii::$app->session->setFlash('error', 'Ошибка отправки');
                }
            }
            return $this->render('template', [
                'page'        => $page,
                'breadcrumbs' => $breadcrumbs
            ]);
        } else {
            return $this->render('process', [
                'page' => $page
            ]);
        }
    }

    public function actionProcess()
    {
        return $this->render('process');
    }
}