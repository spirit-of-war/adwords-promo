<?php

namespace frontend\controllers;

use common\models\Questionnaires;
use yii\web\Controller;
use Yii;

class QuestionnairesController extends Controller
{
    public function actionIndex()
    {
//        Yii::$app->user->logout();
        return $this->render('index');
    }

    public function actionTest(){
        return $this->render('index');
    }
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $userId = Yii::$app->user->id;
        if (!$model = Questionnaires::findOne(['user_id' => $userId])) {
            $model = new Questionnaires();
            $model->user_id = Yii::$app->user->id;
        }

        if(Yii::$app->request->post('submit') == 'create_add'){
            $model->load(Yii::$app->request->post());
        } elseif (Yii::$app->request->post('submit') == 'skip') {

        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
        if ($model->save()) {
            return $this->redirect(Yii::$app->params['domainAdmin'], 301);
        } else {
            return NULL;
        }
    }

}
